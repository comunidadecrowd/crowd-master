﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Data;

namespace Freelancers.Crowd.WCF.Entity
{
    [DataContract]
    public class CategoryDR
    {
        [DataMember]
        public DataTable CategoryTable { get; set; }
    }
}