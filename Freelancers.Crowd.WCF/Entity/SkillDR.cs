﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Freelancers.Crowd.WCF.Entity
{
    [DataContract]
    public class SkillDR
    {
        [DataMember]
        public DataTable SkillTable { get; set; }
    }
}