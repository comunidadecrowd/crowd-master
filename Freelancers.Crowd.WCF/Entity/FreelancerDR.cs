﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Freelancers.Crowd.WCF.Entity
{
    [DataContract]
    public class FreelancerDR
    {
        [DataMember]
        public DataTable FreelanceTable { get; set; }
    }
}