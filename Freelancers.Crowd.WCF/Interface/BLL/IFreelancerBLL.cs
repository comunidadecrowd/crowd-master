﻿using Freelancers.Crowd.WCF.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Freelancers.Crowd.WCF.Interface.DAL
{
    public interface IFreelancerBLL
    {
        FreelancerDR Get(int id);

        FreelancerDR GetAll();

        FreelancerDR GetAll(int page, int amount);

        FreelancerDR Search(String query, String city, int minValueHour, int maxValueHour, int categoriaId, int abilityID, int availabilityId, int segmentId, int page, int amount);

        int Count();

        int CountSearch(String query, String city, int minValueHour, int maxValueHour, int categoriaId, int abilityID, int availabilityId, int segmentId);
    }
}
