﻿using System;
using System.ServiceModel;
using Freelancers.Crowd.WCF.Entity;

namespace Freelancers.Crowd.WCF.Interface.Service
{
    [ServiceContract]
    public interface IFreelancerService
    {
        [OperationContract]
        FreelancerDR Get(int id);

        [OperationContract]
        FreelancerDR GetAll(int? page, int? amount);

        [OperationContract]
        FreelancerDR Search(String query, String city, int minValueHour, int maxValueHour, int categoriaId, int abilityID, int availabilityId, int segmentId, int page, int amount);

        [OperationContract]
        int Count();

        [OperationContract]
        int CountSearch(String query, String city, int minValueHour, int maxValueHour, int categoriaId, int abilityID, int availabilityId, int segmentId);
    }
}
