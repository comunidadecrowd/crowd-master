﻿using Freelancers.Crowd.WCF.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Freelancers.Crowd.WCF.Interface.Service
{
    [ServiceContract]
    public interface ICategoryService
    {
        [OperationContract]
        CategoryDR GetCategorys();
    }
}
