﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Freelancers.Crowd.WCF.Interface.DAL
{
    public interface ISegmentDAL
    {
        DataTable GetAll();
    }
}
