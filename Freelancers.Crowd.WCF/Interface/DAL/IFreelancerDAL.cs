﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Freelancers.Crowd.WCF.Interface.DAL
{
    public interface IFreelancerDAL
    {
        DataTable Get(int id);

        DataTable GetAll();

        DataTable GetAll(int page, int amount);

        DataTable Search(String query, String city, int minValueHour, int maxValueHour, int categoriaId, int abilityID, int availabilityId, int segmentId, int page, int amount, bool limit = true);

        int Count();

        int CountSearch(String query, String city, int minValueHour, int maxValueHour, int categoriaId, int abilityID, int availabilityId, int segmentId);
    }
}
