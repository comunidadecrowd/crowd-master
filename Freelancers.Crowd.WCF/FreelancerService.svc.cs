﻿using Freelancers.Crowd.WCF.Interface.Service;
using Freelancers.Crowd.WCF.BLL;
using Freelancers.Crowd.WCF.Entity;
using System;

namespace Freelancers.Crowd.WCF
{
    public class FreelancerService : IFreelancerService, ICategoryService, ISegmentService, ISkillService
    {

        FreelancerBLL freela; 

        public FreelancerService()
        {
            freela = new FreelancerBLL();
        }

        public FreelancerDR Get(int id)
        {
            return freela.Get(id);
        }
        
        public FreelancerDR GetAll(int? page = null, int? amount = null)
        {
            if (page != null && amount != null)
                return freela.GetAll(page.Value, amount.Value);
            else
                return freela.GetAll();
        }

        public FreelancerDR Search(string query, string city, int minValueHour, int maxValueHour, int categoriaId, int abilityID, int availabilityId, int segmentId, int page, int amount)
        {
            return freela.Search(query, city, minValueHour, maxValueHour, categoriaId, abilityID, availabilityId, segmentId, page, amount);
        }
        
        public int Count()
        {
            return freela.Count();
        }

        public int CountSearch(string query, string city, int minValueHour, int maxValueHour, int categoriaId, int abilityID, int availabilityId, int segmentId)
        {
            return freela.CountSearch(query, city, minValueHour, maxValueHour, categoriaId, abilityID, availabilityId, segmentId);
        }

        public CategoryDR GetCategorys()
        {
            var category = new CategoryBLL();
            return category.GetAll();
        }

        public SegmentDR GetSegments()
        {
            var segment = new SegmentBLL();
            return segment.GetAll();
        }

        public SkillDR GetSkills()
        {
            var skill = new SkillBLL();
            return skill.GetAll();
        }
    }
}
