﻿using Freelancers.Crowd.WCF.DAL.Repositories;
using Freelancers.Crowd.WCF.Entity;
using Freelancers.Crowd.WCF.Interface.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Freelancers.Crowd.WCF.BLL
{
    public class CategoryBLL : ICategoryBLL
    {
        private readonly CategoryDAL CategoryDALAccess;

        public CategoryBLL()
        {
            CategoryDALAccess = new CategoryDAL();
        }

        public CategoryDR GetAll()
        {
            var dt = CategoryDALAccess.GetAll();
            var categoryDataResponse = new CategoryDR
            {
                CategoryTable = dt
            };
            return categoryDataResponse;
        }
    }
}