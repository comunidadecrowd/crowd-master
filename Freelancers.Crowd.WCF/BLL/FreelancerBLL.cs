﻿using Freelancers.Crowd.WCF.DAL.Repositories;
using Freelancers.Crowd.WCF.Entity;
using Freelancers.Crowd.WCF.Interface.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Freelancers.Crowd.WCF.BLL
{
    public class FreelancerBLL : IFreelancerBLL
    {
        private readonly FreelancerDAL FreelancerDALAccess; 

        public FreelancerBLL()
        {
            FreelancerDALAccess = new FreelancerDAL();
        }
        public FreelancerDR Get(int id)
        {
            var dt = FreelancerDALAccess.Get(id);
            var freelaDataResponse = new FreelancerDR {
                FreelanceTable = dt
            };
            return freelaDataResponse;
        }

        public FreelancerDR GetAll()
        {
            var dt = FreelancerDALAccess.GetAll();
            var freelaDataResponse = new FreelancerDR
            {
                FreelanceTable = dt
            };
            return freelaDataResponse;
        }

        public FreelancerDR GetAll(int page, int amount)
        {
            var dt = FreelancerDALAccess.GetAll(page, amount);
            var freelaDataResponse = new FreelancerDR
            {
                FreelanceTable = dt
            };
            return freelaDataResponse;
        }

        public FreelancerDR Search(String query, String city, int minValueHour, int maxValueHour, int categoriaId, int abilityID, int availabilityId, int segmentId, int page, int amount)
        {
            var dt = FreelancerDALAccess.Search(query, city, minValueHour, maxValueHour, categoriaId, abilityID, availabilityId, segmentId, page, amount);
            var freelaDataResponse = new FreelancerDR
            {
                FreelanceTable = dt
            };
            return freelaDataResponse;
        }

        public int Count()
        {
            return FreelancerDALAccess.Count();
        }

        public int CountSearch(String query, String city, int minValueHour, int maxValueHour, int categoriaId, int abilityID, int availabilityId, int segmentId)
        {
            return FreelancerDALAccess.CountSearch(query, city, minValueHour, maxValueHour, categoriaId, abilityID, availabilityId, segmentId);
        }
    }
}