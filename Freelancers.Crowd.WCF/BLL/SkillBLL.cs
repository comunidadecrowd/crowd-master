﻿using Freelancers.Crowd.WCF.DAL.Repositories;
using Freelancers.Crowd.WCF.Entity;
using Freelancers.Crowd.WCF.Interface.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Freelancers.Crowd.WCF.BLL
{
    public class SkillBLL : ISkillBLL
    {
        private readonly SkillDAL SkillDALAccess;

        public SkillBLL()
        {
            SkillDALAccess = new SkillDAL();
        }

        public SkillDR GetAll()
        {
            var dt = SkillDALAccess.GetAll();
            var skillDataResponse = new SkillDR
            {
                SkillTable = dt
            };
            return skillDataResponse;
        }
    }
}