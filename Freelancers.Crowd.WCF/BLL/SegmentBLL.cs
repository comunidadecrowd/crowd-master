﻿using Freelancers.Crowd.WCF.DAL.Repositories;
using Freelancers.Crowd.WCF.Entity;
using Freelancers.Crowd.WCF.Interface.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Freelancers.Crowd.WCF.BLL
{
    public class SegmentBLL : ISegmentBLL
    {
        private readonly SegmentDAL SegmentDALAccess;

        public SegmentBLL()
        {
            SegmentDALAccess = new SegmentDAL();
        }

        public SegmentDR GetAll()
        {
            var dt = SegmentDALAccess.GetAll();
            var segmentDataResponse = new SegmentDR
            {
                SegmentTable = dt
            };
            return segmentDataResponse;
        }
    }
}