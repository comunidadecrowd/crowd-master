﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Freelancers.Crowd.WCF.DAL
{
    internal class CreateReadUpdateDelete
    {

        private readonly Connection conn;

        private readonly String nameTable; 

        public CreateReadUpdateDelete(String nameTable)
        {
            conn = new Connection();
            this.nameTable = nameTable;
        }

        private MySqlCommand FillQuery(string query, string[] paramNames, string[] parameters)
        {
            MySqlCommand command = new MySqlCommand(query, conn.ConnectionDatabase);
            if (paramNames.Length > 0)
                for (int i = 0; i < paramNames.Length; i++)
                    command.Parameters.AddWithValue(paramNames[i], parameters[i]);
            return command; 
        }

        public int? Insert(string query, string[] paramNames, string[] parameters)
        {
            if (paramNames.Length == parameters.Length)
                using (conn)
                {
                    try
                    {
                        conn.Open();
                        var command = FillQuery(query, paramNames, parameters);
                         var id = (int?)command.ExecuteScalar();
                        conn.Close();
                        return id.HasValue ? id : null;
                    }
                    catch
                    {
                        return null;
                    }
                }
            return null;     
        }

        public bool UpdateOrDelete(string query, string[] paramNames, string[] parameters)
        {
            if (paramNames.Length == parameters.Length)
                using (conn)
                {
                    try
                    {
                        conn.Open();
                        var command = FillQuery(query, paramNames, parameters);
                        command.ExecuteNonQuery();
                        conn.Close();
                        return true;
                    }
                    catch
                    {
                        return false;
                    }
                }
            return false;
        }


        public DataTable Select(string query, string[] paramNames, string[] parameters)
        {
            if (paramNames.Length == parameters.Length)
                using (conn)
                {
                    try
                    {
                        conn.Open();
                        var command = FillQuery(query, paramNames, parameters);
                        DataTable dt = new DataTable(nameTable);
                        MySqlDataAdapter dataAdapter = new MySqlDataAdapter(command);
                        dataAdapter.Fill(dt);
                        conn.Close();
                        return dt;
                    }
                    catch(Exception e)
                    {
                        throw new Exception(e.Message);
                        return null;
                    }
                }
            return null;

        }
    }
}