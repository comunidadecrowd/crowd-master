﻿using Freelancers.Crowd.WCF.Interface.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Freelancers.Crowd.WCF.DAL.Repositories
{
    public class SegmentDAL : ISegmentDAL
    {
        private CreateReadUpdateDelete crud;

        public SegmentDAL()
        {
            crud = new CreateReadUpdateDelete("Segment");
        }

        public DataTable GetAll()
        {
            var query = "SELECT * FROM segment";
            var dt = crud.Select(query, new String[] { }, new String[] { });
            return dt;
        }
    }
}