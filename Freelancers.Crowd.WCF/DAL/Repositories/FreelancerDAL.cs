﻿using Freelancers.Crowd.WCF.Interface.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Freelancers.Crowd.WCF.DAL.Repositories
{
    public class FreelancerDAL : IFreelancerDAL
    {
        private CreateReadUpdateDelete crud;

        public FreelancerDAL()
        {
            crud = new CreateReadUpdateDelete("Freelancers");
        }

        public DataTable Get(int id)
        {
            var query = "SELECT freelancers.*, category.name as name_category FROM freelancers JOIN category ON freelancers.category_id = category.id WHERE freelancers.id = @ID";
            var dt = crud.Select(query, new String[] { "@ID" }, new String[] { id.ToString() });
            return dt; 
        }

        public DataTable GetAll()
        {
            var query = "SELECT freelancers.*, category.name as name_category FROM freelancers JOIN category ON freelancers.category_id = category.id";
            var dt = crud.Select(query, new String[] {  }, new String[] {  });
            return dt;
        }

        public DataTable GetAll(int page, int amount)
        {
            var query = "SELECT freelancers.*, category.name as name_category FROM freelancers JOIN category ON freelancers.category_id = category.id LIMIT " + ((page * amount) - amount) + "," + amount ;
            var dt = crud.Select(query, new String[] { }, new String[] { });
            return dt;
        }

        public DataTable Search(string query, string city, int minValueHour, int maxValueHour, int categoriaId, int skillId, int availabilityId, int segmentId, int page, int amount, bool limit = true)
        {

            List<String> paramNames = new List<string>()
            {
                "MIN_VALUE_HOUR",
                "MAX_VALUE_HOUR"

            };
            List<String> param = new List<string>()
            {
                minValueHour.ToString(),
                maxValueHour.ToString(),
            };

            var command = "SELECT freelancers.*, category.name as name_category FROM freelancers"
                + " LEFT JOIN freelancer_skill ON freelancers.id = freelancer_skill.freelancer_id"
                + " LEFT JOIN skill ON skill.id = freelancer_skill.skill_id"
                + " LEFT JOIN freelancer_segment ON freelancers.id = freelancer_segment.freelancer_id"
                + " LEFT JOIN segment ON segment.id = freelancer_segment.segment_id"
                + " LEFT JOIN category ON freelancers.category_id = category.id"
                + " WHERE"
                + " freelancers.price >= @MIN_VALUE_HOUR AND freelancers.price <= @MAX_VALUE_HOUR";
            if (!String.IsNullOrEmpty(query))
            {
                query = "%" + query + "%";
                paramNames.Add("@QUERY");
                param.Add(query);
                command += " AND (freelancers.name LIKE @QUERY OR freelancers.email LIKE @QUERY OR freelancers.title LIKE @QUERY)";
            }
            if (!String.IsNullOrEmpty(city))
            {
                paramNames.Add("@CITY");
                param.Add(city);
                command += " AND freelancers.city = @CITY";
            }
            if (categoriaId > 0)
            {
                paramNames.Add("@CATEGORY_ID");
                param.Add(categoriaId.ToString());
                command += " AND freelancers.category_id = @CATEGORY_ID";
            }
            if (skillId > 0)
            {
                paramNames.Add("@SKILL_ID");
                param.Add(skillId.ToString());
                command += " AND skill.id = @SKILL_ID";
            }
            if (availabilityId > 0)
            {
                paramNames.Add("@AVAILABILITY_ID");
                param.Add(availabilityId.ToString());
                command += " AND freelancers.availability = @AVAILABILITY_ID";
            }
            if (segmentId > 0)
            {
                paramNames.Add("@SEGMENT_ID");
                param.Add(segmentId.ToString());
                command += " AND segment.id = @SEGMENT_ID";
            }
            if (limit)
            {
                command += " LIMIT " + ((page * amount) - amount) + "," + amount;
            }

            var dt = crud.Select(command, paramNames.ToArray(), param.ToArray());
            return dt;

        }
        public int Count()
        {
            var query = "SELECT count(*) FROM freelancers";
            var dt = crud.Select(query, new String[] { }, new String[] { });
            return dt != null ? int.Parse(dt.Rows[0][0].ToString()) : 0;
        }

        public int CountSearch(string query, string city, int minValueHour, int maxValueHour, int categoriaId, int abilityID, int availabilityId, int segmentId)
        {
            var dt = Search(query, city, minValueHour, maxValueHour, categoriaId, abilityID, availabilityId, segmentId, 0, 0, false);
            return dt != null ? int.Parse(dt.Rows.Count.ToString()) : 0;
        }
    }
}