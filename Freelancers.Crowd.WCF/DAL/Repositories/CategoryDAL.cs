﻿using Freelancers.Crowd.WCF.Interface.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Freelancers.Crowd.WCF.DAL.Repositories
{
    public class CategoryDAL : ICategoryDAL
    {
        private CreateReadUpdateDelete crud;

        public CategoryDAL()
        {
            crud = new CreateReadUpdateDelete("Category");
        }

        public DataTable GetAll()
        {
            var query = "SELECT * FROM category";
            var dt = crud.Select(query, new String[] { }, new String[] { });
            return dt;
        }
    }
}