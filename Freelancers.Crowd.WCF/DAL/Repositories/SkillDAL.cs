﻿using Freelancers.Crowd.WCF.Interface.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Freelancers.Crowd.WCF.DAL.Repositories
{
    public class SkillDAL : ISkillDAL
    {
        private CreateReadUpdateDelete crud;

        public SkillDAL()
        {
            crud = new CreateReadUpdateDelete("Skill");
        }

        public DataTable GetAll()
        {
            var query = "SELECT * FROM skill";
            var dt = crud.Select(query, new String[] { }, new String[] { });
            return dt;
        }
    }
}