﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Freelancers.Crowd.WCF.DAL
{
    internal class Connection : IDisposable
    {
        private readonly String ConnectionString; 
        public MySqlConnection ConnectionDatabase;

        public Connection()
        {
            ConnectionString = ConfigurationManager.ConnectionStrings["connection"].ToString(); 
        }

        public void Open() {
            if (ConnectionDatabase == null)
                ConnectionDatabase = new MySqlConnection(ConnectionString);
            if(ConnectionDatabase.State != System.Data.ConnectionState.Open)
                ConnectionDatabase.Open();
        }

        public void Close() {
            if(ConnectionDatabase != null && ConnectionDatabase.State != System.Data.ConnectionState.Closed)
                ConnectionDatabase.Close();
        }

        public void Dispose()
        {
            Close();
        }
    }
}