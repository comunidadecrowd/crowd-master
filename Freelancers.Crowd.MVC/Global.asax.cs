﻿using Freelancers.Crowd.MVC.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Freelancers.Crowd.MVC
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            /* MvcHandler.DisableMvcResponseHeader = true;
             ViewEngines.Engines.Clear();
             ViewEngines.Engines.Add(new RazorViewEngine());*/

            AutofacConfiguration.Init();


            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);


        }
    }
}
