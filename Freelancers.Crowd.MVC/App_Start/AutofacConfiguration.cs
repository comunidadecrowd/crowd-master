﻿using Autofac;
using Autofac.Integration.Mvc;
using Freelancers.Crowd.Domain.Entities;
using Freelancers.Crowd.Domain.Interfaces.Repositories;
using Freelancers.Crowd.Domain.Services;
using Freelancers.Crowd.Infrasctructure.Database.Context;
using Freelancers.Crowd.Infrasctructure.Database.Repositories;
using Freelancers.Crowd.MVC.Models.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace Freelancers.Crowd.MVC.App_Start
{
    public static class AutofacConfiguration
    {
        public static void Init()
        {
            var builder = new ContainerBuilder();

            //builder.RegisterAssemblyModules(typeof(MvcApplication).Assembly);
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterType<CrowdContext>().InstancePerRequest();
            builder.RegisterGeneric(typeof(RepositoryBase<>)).As(typeof(IRepositoryBase<>)).InstancePerRequest();
            builder.RegisterGeneric(typeof(ServiceBase<>)).InstancePerRequest();
            builder.RegisterType(typeof(ServiceFreelancer)).InstancePerRequest();
            builder.RegisterType(typeof(ServiceUser)).InstancePerRequest();

            builder.RegisterType(typeof(ServiceCampaign)).InstancePerRequest();



            //builder.RegisterType<RepositoryBase<Freelancer>>().As<IRepositoryBase<Freelancer>>().PropertiesAutowired().InstancePerRequest();
            //builder.RegisterType<RepositoryBase<User>>().As<IRepositoryBase<User>>().PropertiesAutowired().InstancePerRequest();
            //builder.RegisterType<RepositoryBase<Segment>>().As<IRepositoryBase<Segment>>().PropertiesAutowired().InstancePerRequest();
            //builder.RegisterType<RepositoryBase<Category>>().As<IRepositoryBase<Category>>().PropertiesAutowired().InstancePerRequest();
            //builder.RegisterType<RepositoryBase<Skill>>().As<IRepositoryBase<Skill>>().PropertiesAutowired().InstancePerRequest();
            //builder.RegisterType<RepositoryBase<Rating>>().As<IRepositoryBase<Rating>>().PropertiesAutowired().InstancePerRequest();
            //builder.RegisterType<RepositoryBase<Customer>>().As<IRepositoryBase<Customer>>().PropertiesAutowired().InstancePerRequest();
            //builder.RegisterType<RepositoryBase<Briefing>>().As<IRepositoryBase<Briefing>>().PropertiesAutowired().InstancePerRequest();
            //builder.RegisterType<RepositoryBase<Message>>().As<IRepositoryBase<Message>>().PropertiesAutowired().InstancePerRequest();
            //builder.RegisterType<RepositoryBase<ReadBriefing>>().As<IRepositoryBase<ReadBriefing>>().PropertiesAutowired().InstancePerRequest();


            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}