﻿function openModal(title, message, icon, button) {
    $('#modal-default').find('.modal-title').text(title);
    $('#modal-default').find('.modal-body p').html(message);
    $("#modal-default").find("i").addClass(icon);
    $('#modal-default').find('.modal-footer .btn').text(button);
    $('#modal-default').modal('show');
}