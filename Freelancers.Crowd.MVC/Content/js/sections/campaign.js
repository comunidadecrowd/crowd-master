﻿$(document).ready(function () {
    jQuery.validator.setDefaults({
        errorPlacement: function (error, element) {
            if ($(element).is(":checkbox")) {
                //$(element).parent('div').parent('div').addClass('has-error');
                $(element).parent('div').parent('div').css('border', '1px solid red');
                $(element).parent('div').parent('div').css('border-radius', '3px');
            } else {
                $(element).parent('div').addClass('has-error');
            }
        },
        errorClass: "has-error"
    });

    $('#form-campaign-participation').validate();


    $("#form-campaign-send").on("click", function () {
		if(tinyMCE.get("message-campaign").getContent() == "")
		{
			alertify.alert("Digite sua mensagem antes de enviar.");
			return;
		}
        $.ajax({
            url: $('#form-campaign-participation').attr("action"),
            type: "POST",
            data: { message: tinyMCE.get("message-campaign").getContent(), idCampaign: $('#idCampaign').val(), idFreelancer: $('#idFreelancer').val() },
            success: function (data) {
                var html =
                    '<div class="row"><div class="col-md-12"><div class="" style="margin:10px 0;"><a class="avatar" href="javascript:void(0)">' +
                    '<img src="#photo"></a><h5 style="display:inline-block;margin: 10px 10px 0px 10px;vertical-align: top;">#name</h5>' +
                    '<span style="float: right;"><small>#data</small></span></div></div><div class="col-md-12"style="font-weight:400;padding: 10px 25px;">#text' +
                    '</div></div>';

                console.log(data);

                html = html.replace("#data", data.CreatedAt);
                html = html.replace("#photo", data.UserPhoto);
                html = html.replace("#name", data.UserName);
                html = html.replace("#text", tinyMCE.get("message-campaign").getContent());

                $('#msg').append(html);

                if (data.SatusCode == 200)
                    alertify.alert("Participação enviada com sucesso!");

                if (data.SatusCode == 401)
                    alertify.alert("Você excedeu o limite de participações para essa campanha! Aguarde o resultado.");

                tinyMCE.get("message-campaign").setContent("");

            }
        });
    });

});