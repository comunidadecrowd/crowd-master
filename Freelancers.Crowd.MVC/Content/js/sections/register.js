﻿$(document).ready(function () {
    jQuery.validator.setDefaults({
        errorPlacement: function (error, element) {
            if ($(element).is(":checkbox")) {
                //$(element).parent('div').parent('div').addClass('has-error');
                $(element).parent('div').parent('div').css('border', '1px solid red');
                $(element).parent('div').parent('div').css('border-radius', '3px');
            } else {
                $(element).parent('div').addClass('has-error');
            }
        },
        errorClass: "has-error"
    });
    $('#form-login').validate();
    $('#form-cadastro').validate();
    $('#form-forget').validate();
    $('#form-recovery').validate();
    $('#form-new-user').validate();

    $('select[name="skill"]').select2({
        width: '100%',
        placeholder: "Habilidades * max 5 ",
        maximumSelectionLength: 5
    });


    $('select[name="segments"]').select2({
        width: '100%',
        placeholder: "Segmentos * max 3 ",
        maximumSelectionLength: 3
    });

    $('.select').select2({
        width: '100%',
        placeholder: $(this).data("placeholder")
    });


    $('input[name="Phone"]').mask("(99) 9999-9999?9");
    $('input[name="Price"]').maskMoney();

    $('.url-mask').on('focus', function (e) {
        if ($.trim($(e.target).val()) === '') $(e.target).val('http://www.');
    }).on('blur', function(e){
        if ($.trim($(e.target).val()) === 'http://www.') $(e.target).val('');
    });

    $(function () {
        new dgCidadesEstados({
            estado:
                $('input[name="State"]').get(0),
            cidade:
               $('input[name="City"]').get(0),
            change: true
        });
    });

    $('#cadastrar').click(function () {
        $('#painel-login').fadeOut('slow', function () { $('#painel-cadastrar').fadeIn('slow'); });
    });

    $('.forget_password').click(function (e) {
        e.preventDefault();
        $(this).parents(".panel").fadeOut('slow', function () { $('#painel-forget').fadeIn('slow'); });
    });

    $('#new_user').click(function () {
        $('#painel-login').fadeOut('slow', function () { $('#painel-new-user').fadeIn('slow'); });
    });

    $('.back-login').on("click", function (e) {
        e.preventDefault();
        $(this).parents(".panel").fadeOut('slow', function () { $('#painel-login').fadeIn('slow'); });
        //$('#painel-forget, #painel-cadastrar').fadeOut('slow', function () { $('#painel-login').fadeIn('slow'); });
    });


    $('form').submit(function () {
        //if ($('form').valid())
        //$('input[name="Price"]').val($('input[name="Price"]').val().replace(",", "."));
    });


    $(".page-register-depoimentos-menu ul li a").on("click", function (e) {
        e.preventDefault();
        var video_url = $(this).attr("data-video");
        var url_tpl = "https://www.youtube.com/embed/VIDEOID";

        $(".video-depoimento").attr("src", url_tpl.replace("VIDEOID", video_url));
    });



    $(".page-register-menu-item a").on("click", function (e) {
        e.preventDefault();
        $(".page-register-menu-item").removeClass("active");
        $(this).parent().addClass("active");

        var data_content= $(this).attr("data-content");
        $('.page-register-panel:visible').fadeOut('slow', function () {
            $("." + data_content)
                .fadeIn('slow')
                .css("display", "table");
        });
        
    });
});

function showImagem(input, src) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(src)
                .attr('src', e.target.result)
                .width(80)
                .height(80);
        };

        reader.readAsDataURL(input.files[0]);
    }
}