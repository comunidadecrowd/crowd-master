﻿$(function () {
    $(".phone-mask").inputmask('(99) 9999[9]-9999');
    $(".cnpj-mask").inputmask('99.999.999/9999-99');
    
    

    $(".searchForm-style-view a").on("click", function (e) {
        e.preventDefault();
        var currentEl = $(".searchForm-style-view a.active").data("content");
        var that = $(this);
        $(".searchForm-style-view a").removeClass("active");
        $(this).addClass("active");
        
        $("#visual").val($(that).data("content").split("-")[1]);
        
        $("." + currentEl).hide("fast", function () {            
            $("." + $(that).data("content")).fadeIn();
        });
        
    });



    if (window.location.search.lastIndexOf("visual") != -1) {
        var pos = window.location.search.lastIndexOf("visual");
        
        var item = window.location.search.substring(pos + 7);
        
        $(".searchForm-style-view a[data-content='freelancer-" + item + "']").click();

    }

    $(".new-briefing-freelancer-remove").on("click", function (e) {
        e.preventDefault();
        var tplAddnew = '<input type="submit" class="btn btn-primary col-md-12" value="Selecionar colaboradores" />';
        $(this).parent().remove();


        if ($(".new-briefing-freelancer").length == 0) {
            $(".new-briefing-freelancer-content").parent().hide();

            $(".new-briefing-content-block:last-child").html(tplAddnew);
        }

       
    });

    $('#form-send-briefing').validate();


    $('#form-send-briefing .btn').on("click", function (e) {
        e.preventDefault();
        
        if (tinyMCE.get("text") != null) {
            if (tinyMCE.get("text").getContent() == "") {
                $(".mce-edit-area").addClass("error");
                return;
            }
        }

        /*if ($("input[name='para']").length == 0) {
            openModal("Briefing", "Selecione os profissionais que<br /> você gostaria de enviar um briefing.", "fa-check-square", "Ok");
            return;
        }*/

        $('#form-send-briefing').submit();
    });


    $(".btn-send-briefing").on("click", function (e) {
        console.log(e);
        e.preventDefault();

        addBriefing($("#briefingId").val(), $("#toNewPeople").val());
    });

    if ($('.tiny').length) {
        tinymce.PluginManager.add('image_upload', function (editor, url) {
            // Add a button that opens a window
            editor.addButton('image_upload', {
                icon: "image",
                onclick: function () {
                    editor.windowManager.open({
                        title: "Upload de Imagem",
                        url: '/Project/BriefingImageUpload/',
                        width: 500,
                        height: 300
                    });
                }
            });


        });
        tinymce.init({
            selector: ".tiny",
            theme: "modern",
            skin: 'light',
            language: 'pt_BR',
            menubar: false,
            statusbar: true,
            visual: false,
            height: 250,
            menubar: false,
            statusbar: false,
            paste_data_images: true,
            plugins: ["advlist autolink link image lists code textcolor image_upload"],
            toolbar: "forecolor | bold italic | alignleft aligncenter alignright image_upload"

        });

       
    }


    $('.dropify').dropify();


    
});


function addBriefing(briefingId, toNewPeople) {

    var ids = $('input[type="checkbox"]:not(#selectAll):checked');

    if (ids.length > 0) {
        var queryString = "?";

        $.each(ids, function (i, e) {
            queryString = queryString + (i == 0 ? "" : "&") + "para=" + $(e).data("id");
        });


        window.location = "/Project/BriefingForm/" + queryString + (briefingId != null ? "&briefingId=" + briefingId : "") + (toNewPeople != false ? "&toNewPeople=" + toNewPeople : "");


    }
    else {

        openModal("Briefing", "Selecione os profissionais que<br /> você gostaria de enviar um briefing.", "fa-check-square", "Ok");
        //alertify.alert("");
    }
}