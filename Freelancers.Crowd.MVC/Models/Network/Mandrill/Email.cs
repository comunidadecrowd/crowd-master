﻿using Mandrill;
using Mandrill.Models;
using Mandrill.Requests.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace Freelancers.Crowd.MVC.Models.Network.Mandrill
{
    public class Email
    {

        public async void Send(string to, string name, string subject, string template, Dictionary<string, string> variables = null)
        {
            var thread = new Thread(() =>
            {
                var mandrill = new MandrillApi("rwDDi6a4pO47Za8tvJBDqA");
                var mandrillRecipients = new List<EmailAddress>();

                mandrillRecipients.Add(new EmailAddress() { Email = to, Name = name });

                var email = new EmailMessage
                {
                    To = mandrillRecipients,
                    FromEmail = "noreply@comunidadecrowd.com.br",
                    FromName = "Comunidade Crowd",
                    Subject = subject,
                };

                email.AddRecipientVariable(to, "NOME", name);

                if (variables != null)
                    foreach (var variable in variables.Keys)
                        if (!variable.Equals("NOME"))
                            email.AddRecipientVariable(to, variable, variables[variable]);
                mandrill.SendMessageTemplate(new SendMessageTemplateRequest(email, template, null));

            });
            thread.IsBackground = true;
            thread.Start();
        }
    }
}