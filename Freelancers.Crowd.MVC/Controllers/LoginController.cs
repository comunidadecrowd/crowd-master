﻿using Freelancers.Crowd.Domain.Entities;
using Freelancers.Crowd.Domain.Entities.Enum;
using Freelancers.Crowd.Domain.Interfaces.Repositories;
using Freelancers.Crowd.Domain.Services;
using Freelancers.Crowd.MVC.Models.Network.Mandrill;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Freelancers.Crowd.MVC.Controllers
{
    public class LoginController : Controller
    {

        private readonly ServiceUser _serviceUser;
        private readonly ServiceFreelancer _serviceFreelancer;

        public LoginController(ServiceUser serviceUser, ServiceFreelancer serviceFreelancer)
        {
            _serviceUser = serviceUser;
            _serviceFreelancer = serviceFreelancer;
        }

        

        
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                var email = HttpContext.User.Identity.Name;
                var user = _serviceUser.GetById(x => x.Email.Equals(email));

                var role = new HttpCookie("role", Enum.GetName(typeof(RoleType), user.Role));
                role.Expires = DateTime.Now.AddMinutes(525600);
                role.Expires = DateTime.Now.AddDays(1);
                Response.Cookies.Add(role);

                if (user.Role == RoleType.MASTER)
                {                    
                    return RedirectToAction("Index", "Profissional");
                }
                else if (user.Role == RoleType.FREELANCER)
                {
                    return RedirectToAction("Briefing", "Project");
                }
            }
            if (TempData["Status"] != null)
                ViewBag.Status = TempData["Status"];

           /* if (Request.Url.Host.Contains(ConfigurationManager.AppSettings["url:freelancer"]))
            {*/
                TempData["Status"] = TempData["Status"];
                return RedirectToAction("Index", "SingIn");
            //}
            /*else
            {
                if (TempData["Error"] != null)
                    ViewBag.Error = true;
            }*/


            //return View();
        }


        //This method is just used after register 
        [HttpGet]
        public ActionResult Login()
        {
            String email = TempData["email"].ToString(); 
            String password = TempData["password"].ToString();
            return Index(email, password);
        }

  

        [HttpPost]
        public ActionResult Index(String email, String password)
        {
            
            var user = _serviceUser.GetById(x => x.Email == email);

            if (user != null)
            {
                if (!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(password))
                {
                    user = _serviceUser.Authentication(email, password);
                    if (user != null && user.Id > 0)
                    {
                        var imgUser = new HttpCookie("img_user", user.Photo);
                        imgUser.Expires = DateTime.Now.AddMinutes(525600);
                        Response.Cookies.Add(imgUser);

                        FormsAuthentication.SetAuthCookie(user.Email, true);
                        var role = new HttpCookie("role", Enum.GetName(typeof(RoleType), user.Role));
                        role.Expires = DateTime.Now.AddMinutes(525600);
                        role.Expires = DateTime.Now.AddDays(1);
                        Response.Cookies.Add(role);

                        if (user.Role == RoleType.MASTER || user.Role == RoleType.EMPLOYEE || user.Role == RoleType.CLIENT)
                        {
                            return RedirectToAction("Index", "Profissional");
                        }
                        else if (user.Role == RoleType.FREELANCER)
                        {
                            return RedirectToAction("Briefing", "Project");
                        }
                    }
                    else
                    {
                        ViewBag.Error = true;
                        TempData["Error"] = ViewBag.Error;
                        TempData["StatusUser"] = "pass";


                        //if (Request.Url.Host.Contains(ConfigurationManager.AppSettings["url:freelancer"]))
                        return RedirectToAction("Index", "SingIn", new { email = email });
                        //else
                            //return RedirectToAction("Index", "Login");
                                                
                    }
                }
                else
                {
                    TempData["StatusUser"] = "pass";
                    return RedirectToAction("Index", "SingIn", new { email = email });
                }
                
            }
            else
            {
                var freelancer = _serviceFreelancer.GetById(x => x.Email == email);

                if (freelancer != null)
                {
                    TempData["StatusUser"] = "newUser";
                    return RedirectToAction("Index", "SingIn", new { email = email });
                }
                else
                {
                    TempData["StatusUser"] = "register";
                    return RedirectToAction("Index", "SingIn", new { email = email });
                }
            }
            

            /*
            ViewBag.Error = true;
            if (Request.Url.Host.Contains(ConfigurationManager.AppSettings["url:freelancer"]))
            {
                TempData["Error"] = ViewBag.Error;
                return RedirectToAction("Index", "SingIn");
            }*/
            return View("Index");
        }

        public ActionResult ForgetPassword()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult ForgetPassword(String email)
        {
            var guid = _serviceUser.RecoverPassword(email);
            if (guid != null)
            {
                var freelancer = _serviceUser.GetById(p => p.Email.Equals(email), "Freelancers");
                new Thread(() => {
                    var variables = new Dictionary<string, string> { { "EMAIL", email }, { "ID", guid.ToString() } };
                    Email sendEmail = new Email();
                    sendEmail.Send(freelancer.Email, freelancer.Name, "CROWD - Recuperação de Senha", "change-password", variables);
                }).Start();
                ViewBag.Status = "sucesso";
            }
            else
            {
                ViewBag.Status = "error";
            }
            //if (Request.Url.Host.Contains(ConfigurationManager.AppSettings["url:freelancer"]))
            //{
                TempData["StatusForget"] = ViewBag.Status;
                return RedirectToAction("Index", "SingIn");
            //}
            //return View();
        }


        public ActionResult RecoverPassword(string email, string hash)
        {
            if (Request.Url.Host.Contains(ConfigurationManager.AppSettings["url:freelancer"]))
                return RedirectToAction("RecoverPassword", "SingIn", new { email = email, hash = hash });
            ViewBag.Email = email;
            ViewBag.Hash = hash;
            return View();
        }

        [HttpPost]
        public ActionResult RecoverPassword(string email, string hash, string password)
        {
            var change = _serviceUser.ChangePassword(email, password, hash);
            if (change)
                //Alterado com sucesso
                return Index(email, password);
            else
            {
                TempData["StatusRecovery"] = "error";
                //if (Request.Url.Host.Contains(ConfigurationManager.AppSettings["url:freelancer"]))
                return RedirectToAction("Index", "SingIn");
            }
            //Não foi possivel alterar sua senha
            //return View();
        }
        
        [HttpGet]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult NewUser()
        {
            return View();
        }

        [HttpPost]
        public ActionResult NewUser(string email, string password)
        {
            var freelancer = _serviceFreelancer.GetById(x => x.Email.Equals(email));
            if (freelancer != null && freelancer.Id > 0)
            {
                var user = _serviceUser.GetById(x => x.IdFreelancer == freelancer.Id);

                if (freelancer != null && freelancer.Id > 0 && user != null && user.Id > 0)
                {
                    _serviceUser.Add(new User
                    {
                        Name = freelancer.Name,
                        Email = email,
                        Password = password,
                        IdFreelancer = freelancer.Id,
                        Role = Domain.Entities.Enum.RoleType.FREELANCER
                    });

                    TempData["Status"] = "sucesso-new";
                    return Index(email, password);
                }
                else
                {
                    TempData["Status"] = "error-freela";
                }
            }
            else
            {
                TempData["Status"] = "error-freela";
            }
            //if (Request.Url.Host.Contains(ConfigurationManager.AppSettings["url:freelancer"]))
            //{
                TempData["StatusNewUser"] = TempData["Status"];
                return RedirectToAction("Index", "SingIn");
            //}
            //return RedirectToAction("Index");
        }
    }

}