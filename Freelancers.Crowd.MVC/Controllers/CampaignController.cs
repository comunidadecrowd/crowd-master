﻿using Freelancers.Crowd.Domain.Entities;
using Freelancers.Crowd.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Freelancers.Crowd.MVC.Controllers
{
    public class CampaignController : Controller
    {
        private readonly ServiceCampaign _serviceCampaign;
        private readonly ServiceBase<CampaignParticipation> _serviceCampaignParticipation;
        private readonly ServiceUser _serviceUser;
        private readonly ServiceFreelancer _serviceFreelancer;

        public CampaignController(
           ServiceUser serviceUser,
           ServiceCampaign serviceCampaign,
           ServiceBase<CampaignParticipation> serviceCampaignParticipation,
           ServiceFreelancer serviceFreelancer
           )
        {
            _serviceCampaign = serviceCampaign;
            _serviceCampaignParticipation = serviceCampaignParticipation;
            _serviceUser = serviceUser;
            _serviceFreelancer = serviceFreelancer;
        }


        [HttpGet]
        [Authorize(Roles = "MASTER, GERENTE, ADMIN, FREELANCER")]
        public ActionResult ComunicadoCampanha()
        {
            var email = HttpContext.User.Identity.Name;
            var user = _serviceUser.GetById(x => x.Email.Equals(email));
            var campaign = _serviceCampaign.GetById(1);
            ViewBag.User = user;
            ViewBag.ExceedLimitParticipation = false;
            ViewBag.Campaign = campaign;

            ViewBag.Freelancer = _serviceFreelancer.GetById(user.Freelancers.Id);

            var participations = _serviceCampaignParticipation.GetAll(x => x.FreelancerId == user.IdFreelancer && x.CampaignId == campaign.Id).ToList();

            ViewBag.Participations = participations;

            if (participations.Count() >= campaign.LimitParticipation)
                ViewBag.ExceedLimitParticipation = true;

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        [Authorize(Roles = "MASTER, GERENTE, ADMIN, FREELANCER")]
        public ActionResult ParticipationSend(int idCampaign, int idFreelancer, string message)
        {
            var email = HttpContext.User.Identity.Name;
            var user = _serviceUser.GetById(x => x.Email.Equals(email));
            ViewBag.Name = user.Name;


            var campaign = _serviceCampaign.GetById(1);

            var participations = _serviceCampaignParticipation.GetAll(x => x.FreelancerId == user.IdFreelancer).Select(x => x.CampaignId == idCampaign).ToList();

            if (participations.Count() >= campaign.LimitParticipation)
                return Json(new { SatusCode = 401 });

            var participation = new CampaignParticipation
            {
                CampaignId = idCampaign,
                FreelancerId = idFreelancer,
                Text = message
            };

            _serviceCampaignParticipation.Add(participation);

            return Json(new { SatusCode = 200, CreatedAt = participation.CreatedAt.ToString("dd/MM/yyyy hh:MM"), Message = message, UserName = user.Name, UserPhoto = (!String.IsNullOrEmpty(user.Photo) ? Url.Content("~" + user.Photo) : Url.Content("~/Content/images/default-user-image.png")) });
            //return Json(new { SatusCode = 200,  });
        }


        [HttpGet]
        [Authorize(Roles = "MASTER, GERENTE, ADMIN, FREELANCER")]
        public ActionResult List(int id)
        {
            IEnumerable<CampaignParticipation> participations = null;

            var email = HttpContext.User.Identity.Name;
            var user = _serviceUser.GetById(x => x.Email.Equals(email));
            var campaign = _serviceCampaign.GetById(id);
            ViewBag.User = user;

            ViewBag.Campaign = campaign;


            participations = _serviceCampaignParticipation.GetAll(x => x.CampaignId == campaign.Id).ToList();


            return View(participations);
        }
    }
}