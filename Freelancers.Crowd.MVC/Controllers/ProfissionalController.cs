﻿using Freelancers.Crowd.Domain.Entities;
using Freelancers.Crowd.Domain.Interfaces.Repositories;
using Freelancers.Crowd.Domain.Services;
using Freelancers.Crowd.Infrasctructure.Database.Context;
using Freelancers.Crowd.Infrasctructure.Database.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Freelancers.Crowd.MVC.Controllers
{
    
    [SessionState(System.Web.SessionState.SessionStateBehavior.ReadOnly)]
    public class ProfissionalController : Controller
    {
        private readonly ServiceFreelancer _serviceFreelancer;
        private readonly ServiceBase<Freelancer.Category> _serviceCategory;
        private readonly ServiceBase<Freelancer.Skill> _serviceSkill;
        private readonly ServiceBase<Freelancer.Segment> _serviceSegment;
        private readonly ServiceBase<User> _serviceUser;
        private readonly ServiceBase<Freelancer.RatingValue> _serviceRating;

        public ProfissionalController(ServiceFreelancer serviceFreelancer,
            ServiceBase<Freelancer.Category> serviceCategory, ServiceBase<Freelancer.Segment> serviceSegment,
            ServiceBase<Freelancer.Skill> serviceSkill, ServiceBase<User> serviceUser,
            ServiceBase<Freelancer.RatingValue> serviceRating)
        {
            _serviceFreelancer = serviceFreelancer;
            _serviceCategory = serviceCategory;
            _serviceSegment = serviceSegment;
            _serviceSkill = serviceSkill;
            _serviceUser = serviceUser;
            _serviceRating = serviceRating;
        }

        [Authorize(Roles = "MASTER, GERENTE, ADMIN")]
        [HttpGet]
        public ActionResult Index(string query, string city, int[] skillId, int? categoryId, int? availabilityId, 
            int? segmentId, int? briefingId, bool? toNewPeople = false, string minValueHour = "0", string maxValueHour = "999", int page = 1, int totalItem = 100)
        {
            List<Freelancer> listFreelancer = null;
            IEnumerable<Freelancer.Category> categorys = null;
            IEnumerable<Freelancer.Skill> skills = null;
            IEnumerable<Freelancer.Segment> segments = null;
            int count = 0;

                categorys  = _serviceCategory.GetAll();
                skills = _serviceSkill.GetAll().OrderBy(x => x.Name).ToList();
                segments = _serviceSegment.GetAll();
            

            if (!String.IsNullOrEmpty(query) || !String.IsNullOrEmpty(minValueHour)
                || !String.IsNullOrEmpty(maxValueHour) || skillId != null 
                || categoryId.HasValue || availabilityId.HasValue || segmentId.HasValue
                || !String.IsNullOrEmpty(city))
            {

                listFreelancer = _serviceFreelancer.Search(query, city, int.Parse(minValueHour), int.Parse(maxValueHour),
                     (categoryId.HasValue ? categoryId.Value : 0), skillId,
                     (availabilityId.HasValue ? availabilityId.Value : 0), (segmentId.HasValue ? segmentId.Value : 0),
                     page, totalItem, ref count, "Skills", "Segments", "Ratings", "Categorys").ToList();
            }
            else
            {
                listFreelancer = _serviceFreelancer.GetAll(page, totalItem, ref count).ToList();
            }



            ViewBag.Citys = _serviceFreelancer.GetAll().OrderBy(item => item.City).Select(x => x.City.Name.ToUpper()).Distinct().ToList();

            ViewBag.Count = count;
            ViewBag.TotalPage = count % totalItem > 0 ? (count / totalItem) + 1 : (count / totalItem);
            ViewBag.PageNow = page;
            ViewBag.TotalItem = totalItem;


            ViewBag.Query = query;
            ViewBag.Min = minValueHour;
            ViewBag.Max = maxValueHour;
            ViewBag.City = city;
            ViewBag.CategoryId = categoryId;
            ViewBag.SkillId = skillId;
            ViewBag.SegmentId = segmentId;
            ViewBag.AvailabilityId = availabilityId;

            ViewBag.Category = categorys;
            ViewBag.Skill = skills;
            ViewBag.Segment = segments;

            if (briefingId != null)
                ViewBag.briefingId = briefingId;

            
            ViewBag.toNewPeople = toNewPeople;


            if (TempData["BriefingStatus"] != null)
                ViewBag.BriefingStatus = TempData["BriefingStatus"];

            if(TempData["ComplementAdded"] != null)
                ViewBag.ComplementAdded = TempData["ComplementAdded"];


            return View(listFreelancer);
        }

        [HttpGet]
        public ActionResult Detail(int id)
        {
            var freelancer = _serviceFreelancer.GetById(id);

            var email = HttpContext.User.Identity.Name;
            var user = _serviceUser.GetById(x => x.Email.Equals(email));

            ViewBag.loggedUser = user;

            return PartialView(freelancer);
        }

        [Authorize(Roles = "MASTER, GERENTE, ADMIN")]
        [HttpPost]
        public JsonResult Rating(int freelancerId, string comment, int responsability = 0, int agility = 0, int quality = 0)
        {
            var email = HttpContext.User.Identity.Name;
            var user = _serviceUser.GetById(x => x.Email.Equals(email));

            var rating = new Freelancer.RatingValue()
            {
                FreelancerId = freelancerId,
                UserId = user.Id,
                Responsability = responsability,
                Agility = agility,
                Quality = quality,
                Comment = comment
            };

            _serviceRating.Add(rating);

            return Json(new { StatusCode = 200 });
        }

        
        [HttpPost]
        public JsonResult InactiveRating(int ratingId)
        {
            var email = HttpContext.User.Identity.Name;
            var user = _serviceUser.GetById(x => x.Email.Equals(email));

            var rating = _serviceRating.GetById(ratingId);

            rating.Active = false;

            _serviceRating.Update(rating);

            return Json(new { StatusCode = 200 });
        }
    }
}