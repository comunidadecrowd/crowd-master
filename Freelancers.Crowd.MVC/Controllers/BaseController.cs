﻿using Freelancers.Crowd.Domain.Entities;
using Freelancers.Crowd.Domain.Interfaces.Repositories;
using Freelancers.Crowd.Domain.Services;
using Freelancers.Crowd.Domain.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Freelancers.Crowd.MVC.Controllers
{
    public class BaseController : Controller
    {

        private readonly ServiceBase<Briefing> _serviceBriefing;
        private readonly ServiceUser _serviceUser;

        public BaseController(ServiceBase<Briefing> serviceBriefing, ServiceUser serviceUser)
        {
            _serviceBriefing = serviceBriefing;
            _serviceUser = serviceUser;
        }

        [HttpGet]
        public ActionResult Menu()
        {
            if (HttpContext.Request.Cookies["role"] == null)
            {
                RedirectToAction("Logout", "Login");
            }
            
            IEnumerable<Briefing> briefings = null;

            var email = HttpContext.User.Identity.Name;
            var user = _serviceUser.GetById(x => x.Email.Equals(email));


            var role = new HttpCookie("role", Enum.GetName(typeof(RoleType), user.Role));
            role.Expires = DateTime.Now.AddMinutes(525600);
            role.Expires = DateTime.Now.AddDays(1);
            Response.Cookies.Add(role);

            if (user.Role == RoleType.MASTER || user.Role == RoleType.EMPLOYEE || user.Role == RoleType.CLIENT)
                briefings = _serviceBriefing.GetAll().OrderBy(x => x.CreatedAt).ToList();
            else if (user.Role == RoleType.FREELANCER)
                briefings = _serviceBriefing.GetAll(x => x.Freelancers.Any(y => y.Id == user.Freelancers.Id)).OrderBy(x => x.CreatedAt).ToList();

            var countBadge = briefings.Sum(x => x.Messages.Where(y => y.User != null && y.Read == false && y.FreelancerId == user.IdFreelancer).GroupBy(y => y.FreelancerId).Select(y => y.First()).ToList().Count);
            ViewBag.CountBriefing = countBadge;
            return PartialView("_MenuBar");
        }
    }
}