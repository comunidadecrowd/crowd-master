﻿using Freelancers.Crowd.Domain.Entities;
using Freelancers.Crowd.Domain.Interfaces.Repositories;
using Freelancers.Crowd.Domain.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using ImageResizer;
using System.Web.Security;
using Freelancers.Crowd.MVC.Models.Network.Mandrill;

namespace Freelancers.Crowd.MVC.Controllers
{
    [SessionState(System.Web.SessionState.SessionStateBehavior.ReadOnly)]
    public class UserController : Controller
    {
        private readonly ServiceUser _serviceUser;
        private readonly ServiceFreelancer _serviceFreelancer;

        private readonly ServiceBase<Freelancer.Category> _serviceCategory;
        private readonly ServiceBase<Freelancer.Skill> _serviceSkill;
        private readonly ServiceBase<Freelancer.Segment> _serviceSegment;


        private readonly ServiceBase<State> _serviceState;
        private readonly ServiceBase<City> _serviceCity;

        public UserController(ServiceUser serviceUser, ServiceFreelancer serviceFreelancer,
            ServiceBase<Freelancer.Category> serviceCategory, ServiceBase<Freelancer.Skill> serviceSkill,
            ServiceBase<Freelancer.Segment> serviceSegment, ServiceBase<State> serviceState, ServiceBase<City> serviceCity)
        {
            _serviceUser = serviceUser;
            _serviceFreelancer = serviceFreelancer;

            _serviceCategory = serviceCategory;
            _serviceSkill = serviceSkill;
            _serviceSegment = serviceSegment;

            _serviceState = serviceState;
            _serviceCity = serviceCity;
        }

        [HttpGet]
        public ActionResult Index(int? idCustomer)
        {
            if (!idCustomer.HasValue)
            {
                var email = HttpContext.User.Identity.Name;
                var user = _serviceUser.GetById(x => x.Email.Equals(email), "Customer");
                idCustomer = user.IdCustomer;
            }

            var allCustomer = _serviceUser.GetAll(x => x.IdCustomer == idCustomer);
            ViewBag.IdCustomer = idCustomer;
            return View(allCustomer);
        }

        [HttpGet]
        public ActionResult Inactive(int idUser)
        {
            var user = _serviceUser.GetById(idUser);

            user.Active = false;
            _serviceUser.Update(user);

            return RedirectToAction("Index", new { idCustomer = user.IdCustomer });
        }

        [HttpGet]
        public ActionResult Add(int idCustomer, bool existing = false)
        {
            ViewBag.ExistingUser = existing;

            ViewBag.IdCustomer = idCustomer;
            return View();
        }

        [HttpPost]
        public ActionResult Add(User user)
        {
            var password = user.Password;

            var existing_user = _serviceUser.GetById(x => x.Email == user.Email);
            if (existing_user != null)
            {
                TempData["existing_user"] = "Email já cadastrado!";

                return RedirectToAction("Add", new { idCustomer = user.IdCustomer, existing = true });
            }

            _serviceUser.Add(user);

            Email sendEmail = new Email();
            sendEmail.Send(user.Email, user.Name, "Dados do usuário", "new-customer-user", new Dictionary<string, string> { { "NAME", user.Name }, { "USER", user.Email }, { "PASS", password } });

            return RedirectToAction("Index", new { idCustomer = user.IdCustomer});
        }

        [HttpGet]
        public ActionResult Edit()
        {
            var email = HttpContext.User.Identity.Name;
            var user = _serviceUser.GetById(x => x.Email.Equals(email), "Customer");

            var categorys = _serviceCategory.GetAll();
            var skills = _serviceSkill.GetAll().OrderBy(x => x.Name);
            var segments = _serviceSegment.GetAll();

            ViewBag.Category = categorys.ToList();
            ViewBag.Skill = skills.ToList();
            ViewBag.Segment = segments.ToList();


            ViewBag.States = _serviceState.GetAll();
            ViewBag.Cities = _serviceCity.GetAll();

            return PartialView(user);
        }

        [HttpPost]
        public ActionResult Edit(User usuario, string price1, int[] segments, int[] skill, HttpPostedFileBase picture)
        {
            var user = _serviceUser.GetById(usuario.Id);
            user.Name = usuario.Name;
            user.Email = usuario.Email;
            if(!String.IsNullOrEmpty(usuario.Password))
                user.Password = usuario.Password;

            string path = Server.MapPath("~/Content/images/user/");
            if (picture != null && picture.ContentLength > 0)
            {
                string ext = Path.GetExtension(picture.FileName);
                path += user.Name.Replace(" ", "") + "_" + user.Id + ext;
                picture.SaveAs(path);
                user.Photo = "/Content/images/user/" + user.Name.Replace(" ", "") + "_" + user.Id + ext;
                Response.Cookies.Add(new HttpCookie("img_user", user.Photo));

                //Crops to a square (in place)
                ImageBuilder.Current.Build(path, path,
                                           new ResizeSettings("width=100&height=100&crop=auto"));

            }

            _serviceUser.Update(user);

            if (user.Role == Domain.Entities.Enum.RoleType.FREELANCER)
            {
                var freelancer = _serviceFreelancer.GetById(user.IdFreelancer.Value);
                freelancer.Segments.Clear();
                freelancer.Skills.Clear();
                Array.ForEach(segments, x => { freelancer.Segments.Add(_serviceSegment.GetById(x)); });
                Array.ForEach(skill, x => { freelancer.Skills.Add(_serviceSkill.GetById(x)); });

                freelancer.Name = usuario.Name;


                freelancer.Email = usuario.Email;
                freelancer.City = usuario.Freelancers.City;
                freelancer.State = usuario.Freelancers.State;
                freelancer.Country = usuario.Freelancers.Country;
                freelancer.Title = usuario.Freelancers.Title;
                freelancer.Price = decimal.Parse(price1.Replace(".", ","));
                freelancer.Phone = usuario.Freelancers.Phone;
                freelancer.Name = usuario.Name;
                freelancer.Email = usuario.Email;
                freelancer.PortfolioURL = usuario.Freelancers.PortfolioURL;
                freelancer.Description = usuario.Freelancers.Description;
                freelancer.Availability = usuario.Freelancers.Availability;
                freelancer.Skype = usuario.Freelancers.Skype;

                _serviceFreelancer.Update(freelancer);

                if(HttpContext.User.Identity.Name != freelancer.Email)
                    FormsAuthentication.SignOut();
                

            }

            return RedirectToAction("Index", "Profissional");
        }
    }
}