﻿using Freelancers.Crowd.Domain.Entities;
using Freelancers.Crowd.Domain.Entities.Enum;
using Freelancers.Crowd.Domain.Interfaces.Repositories;
using Freelancers.Crowd.Domain.Services;
using Freelancers.Crowd.MVC.Models.Network.Mandrill;
using ImageResizer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Freelancers.Crowd.MVC.Controllers
{
    public class ProjectController : Controller
    {
        private readonly ServiceBase<Briefing> _serviceBriefing;
        private readonly ServiceFreelancer _serviceFreelancer;
        private readonly ServiceUser _serviceUser;
        private readonly ServiceBase<Message> _serviceMessage;
        private readonly ServiceBase<ReadBriefing> _serviceReadBriefing;


        

        public ProjectController(ServiceBase<Briefing> serviceBriefing,
            ServiceFreelancer serviceFreelancer, ServiceUser serviceUser,
            ServiceBase<Message> serviceMessage, ServiceBase<ReadBriefing> serviceReadBriefing)
        {
            _serviceBriefing = serviceBriefing;
            _serviceFreelancer = serviceFreelancer;
            _serviceUser = serviceUser;
            
            _serviceMessage = serviceMessage;
            _serviceReadBriefing = serviceReadBriefing;
        }

        [Authorize(Roles = "MASTER, GERENTE, ADMIN, FREELANCER")]
        [HttpGet]
        public ActionResult Briefing()
        {
            IEnumerable<Briefing> briefings = null;

            var email = HttpContext.User.Identity.Name;
            var user = _serviceUser.GetById(x => x.Email.Equals(email));

            ViewBag.User = user;
            ViewBag.Role = user.Role;

            var role = new HttpCookie("role", Enum.GetName(typeof(RoleType), user.Role));
            role.Expires = DateTime.Now.AddMinutes(525600);
            role.Expires = DateTime.Now.AddDays(1);
            Response.Cookies.Add(role);

            if (user.Role == RoleType.MASTER)
                briefings = _serviceBriefing.GetAll().OrderByDescending(x => x.CreatedAt).ToList();
            else if (user.Role == RoleType.EMPLOYEE || user.Role == RoleType.CLIENT)
            {
                briefings = _serviceBriefing.GetAll(x => x.User.Customer.Id == user.IdCustomer).OrderByDescending(x => x.CreatedAt).ToList();
            }
            else if (user.Role == RoleType.FREELANCER)
            {

                ViewBag.IdFreelancers = user.IdFreelancer;
                briefings = _serviceBriefing.GetAll(x => x.Freelancers.Any(y => y.Id == user.Freelancers.Id)).OrderByDescending(x => x.CreatedAt).ToList();
            }

            var countBadge = briefings.Sum(x => x.Messages.Where(y => y.User == null && y.Read == false).GroupBy(y => y.FreelancerId).Select(y => y.First()).ToList().Count);

            return View(briefings);
        }

        [Authorize(Roles = "MASTER, GERENTE, ADMIN, FREELANCER")]
        [HttpGet]
        public ActionResult BriefingForm(int[] para, int? briefingId, bool toNewPeople = false)
        {
            var briefing = new Briefing();

            var email = HttpContext.User.Identity.Name;
            var user = _serviceUser.GetById(x => x.Email.Equals(email));

            if (briefingId != null)
            {
                briefing = _serviceBriefing.GetById(long.Parse(briefingId.ToString()));
                
                if (user.Role != RoleType.MASTER)
                {
                    if (user.Role == RoleType.FREELANCER && briefing.Freelancers.Where(x => x.Id == user.IdFreelancer).Count() == 0)
                    {
                        
                        return RedirectToAction("Briefing", "Project");                            
                    }
                    else if (user.Role != RoleType.FREELANCER && user.IdCustomer != briefing.User.IdCustomer)
                    {
                        return RedirectToAction("Index", "Profissional", new { briefingId = briefing.Id });
                    }
                }
            }

            var selectedUsers = new List<Freelancer>();

           

            if (para != null)
            {
                for (int i = 0; i < para.Length; i++)
                {
                    selectedUsers.Add(_serviceFreelancer.GetById(para[i]));
                }
            }

            if (briefing.Freelancers.Count != 0)
            {
                if(toNewPeople)
                {
                    selectedUsers.AddRange(briefing.Freelancers.ToList());
                } else
                {
                    selectedUsers = briefing.Freelancers.ToList();
                }

                
            }
            
            
            ViewBag.User = user;
            ViewBag.SelectedUsers = selectedUsers;

            return View(briefing);
        }


        [Authorize(Roles = "MASTER, GERENTE, ADMIN")]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult BriefingForm(Briefing briefing, string complement, int[] para = null, bool toNewPeople = false, bool sending = false)
        {

            var email = HttpContext.User.Identity.Name;
            var user = _serviceUser.GetById(x => x.Email.Equals(email));

            briefing.IdUser = user.Id;
            briefing.User = user;

            if (briefing.Id != 0)
            {
                var briefingAux = briefing;
                briefing = _serviceBriefing.GetById(briefingAux.Id);

                briefing.Title = briefingAux.Title;
                briefing.Text = briefingAux.Text;
                briefing.Excerpt = briefingAux.Excerpt;
            }


            if (para != null)
            {
                foreach (int p in para)
                {
                    var freelancer = _serviceFreelancer.GetById(p);

                    var db_briefing = _serviceBriefing.GetById(briefing.Id);
                    if (briefing.Id != 0)
                    {
                        if (!db_briefing.Freelancers.Contains(freelancer))
                        {
                            briefing.Freelancers.Add(freelancer);

                            Email sendEmail = new Email();
                            sendEmail.Send(freelancer.Email, freelancer.Name, "Novo Briefing: " + briefing.Title, "new-briefing", new Dictionary<string, string> { { "DE", user.Name }, { "EMPRESA", user.Customer.Name }, { "EXCERPT", briefing.Excerpt } });

                            TempData["BriefingStatus"] = 200;
                            briefing.Sended = true;
                        }
                    }
                    else
                    {
                        briefing.Freelancers.Add(freelancer);

                        Email sendEmail = new Email();
                        sendEmail.Send(freelancer.Email, freelancer.Name, "Novo Briefing: " + briefing.Title, "new-briefing", new Dictionary<string, string> { { "DE", user.Name }, { "EMPRESA", user.Customer.Name }, { "EXCERPT", briefing.Excerpt } });

                        TempData["BriefingStatus"] = 200;
                        briefing.Sended = true;
                    }
                }
            }


            if (briefing.Id != 0)
            {
                if (!string.IsNullOrEmpty(complement))
                {
                    briefing.Complements.Add(new Briefing.Complement()
                    {
                        Active = true,
                        BriefingId = briefing.Id,
                        Text = complement
                    });

                    foreach(var freelancer in briefing.Freelancers)
                    {
                        Email sendEmail = new Email();
                        sendEmail.Send(freelancer.Email, freelancer.Name, "Briefing Modificado: " + briefing.Title, "new-briefing-complement", new Dictionary<string, string> { { "DE", user.Name }, { "EMPRESA", user.Customer.Name }, { "COMPLEMENT", complement } });
                    }
                    

                    TempData["ComplementAdded"] = true;

                }

                _serviceBriefing.Update(briefing);               

            }
            else {
                _serviceBriefing.Add(briefing);
            }



            

            if (toNewPeople && !sending)
                return RedirectToAction("Index", "Profissional", new { briefingId = briefing.Id, toNewPeople = toNewPeople });
            else
                return RedirectToAction("Index", "Profissional");
        }

        [Authorize(Roles = "MASTER, GERENTE, ADMIN, FREELANCER")]
        [HttpGet]
        public ActionResult InfoBriefing(int idBriefing, int idFreelancer)
        {
            var email = HttpContext.User.Identity.Name;
            var user = _serviceUser.GetById(x => x.Email.Equals(email));
            var briefing = _serviceBriefing.GetById(idBriefing);

            //Mark as read messages in briefing by freelancer and by user not freelancer
            var messageNotRead = briefing.Messages.Where(x => x.Read == false).ToList();
            if (messageNotRead.Count > 0)
            {
                //Mark as read for user that is freelancer
                if (user.IdFreelancer.HasValue)
                {
                    var messageNotReadByFreelancer = messageNotRead
                        .Where(x => x.Freelancers != null && x.Freelancers.Id == user.IdFreelancer.Value && x.User != null).ToList();
                    if (messageNotReadByFreelancer.Count > 0)
                        foreach (var m in messageNotReadByFreelancer)
                        {
                            m.Read = true;
                            _serviceMessage.Update(m);
                        }
                }
                //Mark as read for user that is not freelancer
                else
                {
                    var messageNotReadByUser = messageNotRead.Where(x => x.User == null && x.Briefings.IdUser == user.Id).ToList();
                    if (messageNotReadByUser.Count > 0)
                        foreach (var m in messageNotReadByUser)
                        {
                            m.Read = true;
                            _serviceMessage.Update(m);
                        }
                }
            }

            //Mark as read when freelancer open first time
            if (briefing.Read.Where(x => x.Freelancer.Id == user.IdFreelancer).ToList().Count == 0)
            {
                if (user.IdFreelancer.HasValue && briefing.Freelancers.Any(x => x.Id == user.IdFreelancer))
                {
                    var read = new ReadBriefing()
                    {
                        BriefingId = idBriefing,
                        FreelancerId = user.IdFreelancer.Value,
                        Read = true
                    };

                    _serviceReadBriefing.Add(read);
                }
            }

            ViewBag.Freelancer = _serviceFreelancer.GetById(idFreelancer);

            return PartialView(briefing);
        }

        [Authorize(Roles = "MASTER, GERENTE, ADMIN, FREELANCER")]
        [HttpGet]
        public ActionResult BriefingUser(int idBriefing)
        {
            var briefing = _serviceBriefing.GetById(idBriefing);
            return PartialView(briefing);
        }

        [Authorize(Roles = "MASTER, GERENTE, ADMIN, FREELANCER")]
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult Message(String message, int idBriefing, int idFreelancer)
        {
            var email = HttpContext.User.Identity.Name;
            var user = _serviceUser.GetById(x => x.Email.Equals(email));
            Message msgSend = null;

            if (user.Role == RoleType.MASTER || user.Role == RoleType.EMPLOYEE || user.Role == RoleType.CLIENT)
            {
                msgSend = new Message { Text = message, BriefingId = idBriefing, UserId = user.Id, FreelancerId = idFreelancer };
                // new Thread(() =>
                //{
                    var freelancer = _serviceFreelancer.GetById(idFreelancer);
                    Email sendEmail = new Email();
                    sendEmail.Send(freelancer.Email, freelancer.Name, "Novo Comentário", "new-commentary", new Dictionary<string, string> { { "DE", user.Name } });
                //}).Start();
            }
            else if (user.Role == RoleType.FREELANCER)
                msgSend = new Message { Text = message, BriefingId = idBriefing, FreelancerId = idFreelancer };

            _serviceMessage.Add(msgSend);

            return Json(new { SatusCode = 200, CreatedAt = msgSend.CreatedAt.ToString("dd/MM/yyyy hh:MM"), Message = message });
        }

        [Authorize(Roles = "MASTER, GERENTE, ADMIN")]
        [HttpPost]
        public ActionResult DeleteBriefing(int idBriefing, int[] idFreelancers)
        {
            var briefing = _serviceBriefing.GetById(idBriefing);
            foreach (var id in idFreelancers)
            {
                var listRead = briefing.Read.Where(x => x.FreelancerId == id).ToList();
                var listMessage = briefing.Messages.Where(x => x.FreelancerId == id).ToList();
                foreach (var read in listRead)
                {
                    _serviceReadBriefing.Remove(_serviceReadBriefing.GetById(read.Id));
                }
                foreach (var msg in listMessage)
                {
                    _serviceMessage.Remove(_serviceMessage.GetById(msg.Id));
                }
                briefing.Freelancers.Remove(_serviceFreelancer.GetById(id));
            }

            _serviceBriefing.Update(briefing);

            return RedirectToAction("Briefing");
        }

        [Authorize(Roles = "MASTER, GERENTE, ADMIN, FREELANCER")]
        [HttpGet]
        public ActionResult Search(String search)
        {
            var briefing = _serviceBriefing.GetAll(x => x.Title.Contains(search) || x.Text.Contains(search) || x.Messages.Any(y => y.Text.Contains(search)));
            return PartialView(briefing);
        }

        [HttpGet]
        public ActionResult ComunicadoEspecial()
        {
            var email = HttpContext.User.Identity.Name;
            var user = _serviceUser.GetById(x => x.Email.Equals(email));
            ViewBag.Name = user.Name;
            return View();
        }

        [HttpPost]
        public JsonResult MarkAs(int idBriefing, int[] idsFreelancer, bool read)
        {
            var email = HttpContext.User.Identity.Name;
            var user = _serviceUser.GetById(x => x.Email.Equals(email));
            var briefing = _serviceBriefing.GetById(idBriefing);
            foreach (var id in idsFreelancer)
            {
                var messageNotRead = briefing.Messages.Where(x => x.Read == !read && x.FreelancerId == id).ToList();
                if (messageNotRead.Count > 0)
                {
                    //Mark as read for user that is not freelancer
                    var messageNotReadByUser = messageNotRead.Where(x => x.User == null && x.Briefings.IdUser == user.Id).ToList();
                    if (messageNotReadByUser.Count > 0)
                        foreach (var m in messageNotReadByUser)
                        {
                            m.Read = read;
                            _serviceMessage.Update(m);
                        }
                }
            }
            return Json(new { StatusCode = 200 });
        }


        [HttpPost]
        [ValidateInput(false)]
        public string BriefingImageUpload(HttpPostedFileBase image)
        {
            var email = HttpContext.User.Identity.Name;
            var user = _serviceUser.GetById(x => x.Email.Equals(email));

            var url = "";

            if (image.ContentType.ToLower() != "image/jpg" &&
                    image.ContentType.ToLower() != "image/jpeg" &&
                    image.ContentType.ToLower() != "image/pjpeg" &&
                    image.ContentType.ToLower() != "image/gif" &&
                    image.ContentType.ToLower() != "image/x-png" &&
                    image.ContentType.ToLower() != "image/png")
            {
                throw new Exception("File is not valid");
            }



            string path = Server.MapPath("~/Content/uploads/briefing/");
            if (image != null && image.ContentLength > 0)
            {
                string image_name = Guid.NewGuid().ToString();
                string ext = Path.GetExtension(image.FileName);
                path += image_name + ext;
                image.SaveAs(path);
                url = "/Content/uploads/briefing/" + image_name + ext;
                
                //ImageBuilder.Current.Build(path, path,
                //                           new ResizeSettings("width=600"));

            }

            return url;
        }


        [Authorize(Roles = "MASTER, GERENTE, ADMIN, FREELANCER")]
        [HttpGet]
        public ActionResult BriefingImageUpload()
        {

            return PartialView();
        }



        [Authorize(Roles = "MASTER, GERENTE, ADMIN")]
        [HttpGet]
        public ActionResult BriefingArchive(int idBriefing)
        {
            var briefing = _serviceBriefing.GetById(idBriefing);
            briefing.Active = false;
            _serviceBriefing.Update(briefing);

            return RedirectToAction("Briefing");
            //return Json(new { StatusCode = 200 }, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "MASTER, GERENTE, ADMIN")]
        [HttpGet]
        public ActionResult BriefingUnArchive(int idBriefing)
        {
            var briefing = _serviceBriefing.GetById(idBriefing);
            briefing.Active = true;
            _serviceBriefing.Update(briefing);

            return RedirectToAction("Briefing");
            //return Json(new { StatusCode = 200 }, JsonRequestBehavior.AllowGet);
        }

    }
}