﻿using Freelancers.Crowd.Domain.Entities;
using Freelancers.Crowd.Domain.Interfaces.Repositories;
using Freelancers.Crowd.Domain.Services;
using ImageResizer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Freelancers.Crowd.MVC.Controllers
{
    [Authorize(Roles = "MASTER")]
    [SessionState(System.Web.SessionState.SessionStateBehavior.ReadOnly)]
    public class CustomerController : Controller
    {
        private readonly ServiceBase<Customer> _serviceCustomer;
        private readonly ServiceBase<State> _serviceState;
        private readonly ServiceBase<City> _serviceCity;

        public CustomerController(ServiceBase<Customer> serviceCustomer, ServiceBase<State> serviceState, ServiceBase<City> serviceCity)
        {
            _serviceCustomer = serviceCustomer;
            _serviceState = serviceState;
            _serviceCity = serviceCity;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var customers = _serviceCustomer.GetAll().Where(x => x.Active == true);
            return View(customers);
        }


        [HttpGet]
        public ActionResult Edit(int idCustomer)
        {
            var customer = _serviceCustomer.GetById(idCustomer);
            ViewBag.States = _serviceState.GetAll();
            ViewBag.Cities = _serviceCity.GetAll();

            return View(customer);
        }

        [HttpPost]
        public ActionResult Edit(Customer customer, HttpPostedFileBase Logo)
        {
            string path = Server.MapPath("~/Content/images/customer/");
            if (Logo != null && Logo.ContentLength > 0)
            {
                string ext = Path.GetExtension(Logo.FileName);
                path += customer.Name.Replace(" ", "") + "_" + customer.Id + ext;
                Logo.SaveAs(path);
                customer.Logo = "/Content/images/customer/" + customer.Name.Replace(" ", "") + "_" + customer.Id + ext;

                //Crops to a square (in place)
                ImageBuilder.Current.Build(path, path,
                                           new ResizeSettings("width=80"));

            }

            _serviceCustomer.Update(customer);


            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Inactive(int idCustomer)
        {
            var customer = _serviceCustomer.GetById(idCustomer);

            customer.Active = false;
            _serviceCustomer.Update(customer);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Add()
        {
            ViewBag.States = _serviceState.GetAll();
            ViewBag.Cities = _serviceCity.GetAll();

            return View();
        }

        [HttpPost]
        public ActionResult Add(Customer customer, HttpPostedFileBase Logo)
        {
            string path = Server.MapPath("~/Content/images/customer/");
            if (Logo != null && Logo.ContentLength > 0)
            {
                string ext = Path.GetExtension(Logo.FileName);
                path += customer.Name.Replace(" ", "") + "_" + customer.Id + ext;
                Logo.SaveAs(path);
                customer.Logo = "/Content/images/customer/" + customer.Name.Replace(" ", "") + "_" + customer.Id + ext;
                
                //Crops to a square (in place)
                ImageBuilder.Current.Build(path, path,
                                           new ResizeSettings("width=80"));

            }


            _serviceCustomer.Add(customer);
            return RedirectToAction("Index");
        }
    }
}