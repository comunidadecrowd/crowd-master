﻿using Freelancers.Crowd.Domain.Entities;
using Freelancers.Crowd.Domain.Services;
using Freelancers.Crowd.MVC.Models.Network.Mandrill;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Freelancers.Crowd.MVC.Controllers
{
    public class SingInController : Controller
    {
        private readonly ServiceFreelancer _serviceFreelancer;
        private readonly ServiceBase<Freelancer.Category> _serviceCategory;
        private readonly ServiceBase<Freelancer.Skill> _serviceSkill;
        private readonly ServiceBase<Freelancer.Segment> _serviceSegment;
        private readonly ServiceUser _serviceUser;

        public SingInController(ServiceFreelancer serviceFreelancer, ServiceBase<Freelancer.Category> serviceCategory, 
            ServiceBase<Freelancer.Skill> serviceSkill, ServiceBase<Freelancer.Segment> serviceSegment, ServiceUser serviceUser)
        {
            _serviceFreelancer = serviceFreelancer;
            _serviceCategory = serviceCategory;
            _serviceSkill = serviceSkill;
            _serviceSegment = serviceSegment;
            _serviceUser = serviceUser;
        }

        [HttpGet]
        public ActionResult Index(string email, string hash)
        {
            string form = "login"; 

            if (TempData["Status"] != null)
                ViewBag.Status = TempData["Status"];
            if (TempData["Error"] != null)
                ViewBag.Error = TempData["Error"];

            if (!String.IsNullOrEmpty(email))
            {
                form = "newUser";
                ViewBag.Email = email;
            }
            
            if (!String.IsNullOrEmpty(email) && !String.IsNullOrEmpty(hash))
            {
                form = "recovery";
                ViewBag.Email = email;
                ViewBag.Hash = hash;
            }


            if (TempData["StatusUser"] != null)
            {
                ViewBag.Email = email;
                form = TempData["StatusUser"].ToString();
            }

            /*if (TempData["Status"] != null)
            {
                ViewBag.Status = TempData["StatusUser"];
                form = "register";
            }
                

            if (TempData["StatusNewUser"] != null)
                ViewBag.Status = TempData["StatusNewUser"];

            if (TempData["StatusForget"] != null)
            {
                form = "forget";
                ViewBag.Status = TempData["StatusForget"];
            }
            if (TempData["StatusRecovery"] != null)
            {
                form = "recovery";
                ViewBag.Status = TempData["StatusRecovery"];
            }*/

            if (TempData["StatusForget"] != null)
            {
                form = "forget";
                ViewBag.Status = TempData["StatusForget"];
            }

            ViewBag.Form = form;
            
            return View();
        }

        [HttpGet]
        public ActionResult Formulary()
        {
            var categorys = _serviceCategory.GetAll();
            var skills = _serviceSkill.GetAll().OrderBy(x => x.Name);
            var segments = _serviceSegment.GetAll();

            ViewBag.Category = categorys.ToList();
            ViewBag.Skill = skills.ToList();
            ViewBag.Segment = segments.ToList();

            return PartialView("_Form");
        }

        [HttpPost]
        public ActionResult Register(Freelancer freelancer, string Price, int[] segments, int[] skill, string password, HttpPostedFileBase picture)
        {
            var userExist = _serviceFreelancer.GetById(x => x.Email.Equals(freelancer.Email));
            if (userExist == null || userExist.Id == 0)
            {
                Array.ForEach(segments, x => { freelancer.Segments.Add(_serviceSegment.GetById(x)); });
                Array.ForEach(skill, x => { freelancer.Skills.Add(_serviceSkill.GetById(x)); });
                freelancer.Price = decimal.Parse(Price.Replace(".", ","));
                _serviceFreelancer.Add(freelancer);

                var user = new User
                {
                    IdFreelancer = freelancer.Id,
                    Role = Domain.Entities.Enum.RoleType.FREELANCER,
                    Name = freelancer.Name,
                    Email = freelancer.Email,
                    Password = password
                };

                string path = Server.MapPath("~/Content/images/user/");
                if (picture != null && picture.ContentLength > 0)
                {
                    string ext = Path.GetExtension(picture.FileName);
                    path += user.Name.Replace(" ", "") + "_" + user.Id + ext;
                    picture.SaveAs(path);
                    user.Photo = "/Content/images/user/" + user.Name.Replace(" ", "") + "_" + user.Id + ext;
                }


                _serviceUser.Add(user);

                new Thread(() =>
                {
                    Email sendEmail = new Email();
                    sendEmail.Send(freelancer.Email, freelancer.Name, "Bem Vindo a Crowd", "new-freelancer");
                }).Start();

                TempData["email"] = user.Email;
                TempData["password"] = password;

                return RedirectToAction("Login", "Login");
            }
            TempData["StatusRegister"] = "error";
            return RedirectToAction("Index", "SingIn");
        }

        public ActionResult RecoverPassword(string email, string hash)
        {
            return RedirectToAction("Index", new { email = email, hash = hash });
        }

    }
}