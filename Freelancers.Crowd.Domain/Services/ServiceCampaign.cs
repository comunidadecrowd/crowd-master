﻿using Freelancers.Crowd.Domain.Entities;
using Freelancers.Crowd.Domain.Interfaces.Repositories;
using Freelancers.Crowd.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Freelancers.Crowd.Domain.Services
{

    public class ServiceCampaign : ServiceBase<Campaign>, IServiceCampaign
    {
        private readonly new IRepositoryBase<Campaign> _repository;

        public ServiceCampaign(IRepositoryBase<Campaign> repository)
            : base(repository)
        {
            _repository = repository;
        }
    }
}