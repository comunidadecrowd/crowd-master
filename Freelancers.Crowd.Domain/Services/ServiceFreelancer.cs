﻿using Freelancers.Crowd.Domain.Entities;
using Freelancers.Crowd.Domain.Entities.Enum;
using Freelancers.Crowd.Domain.Interfaces.Repositories;
using Freelancers.Crowd.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Freelancers.Crowd.Domain.Util;

namespace Freelancers.Crowd.Domain.Services
{
    public class ServiceFreelancer : ServiceBase<Freelancer>, IServiceFreelancer
    {
        private readonly new IRepositoryBase<Freelancer> _repository;

        public ServiceFreelancer(IRepositoryBase<Freelancer> repository)
            :base(repository)
        {
            _repository = repository;
        }

        public IEnumerable<Freelancer> GetAll(int page, int amount, ref int totalRecords) {

            return GetAll(ref totalRecords, null, null, false, ((page * amount) - amount), amount);
        }

        public IEnumerable<Freelancer> Search(string query, string city, int minValueHour, int maxValueHour, int categoriaId, int[] skillId, int availabilityId, int segmentId, int page, int amount, ref int totalRecords, params string[] includes)
        {
            Expression<Func<Freelancer, bool>> queryFunc = x => x.Price >= minValueHour && x.Price <= maxValueHour;

            if (!String.IsNullOrEmpty(query))
            {
                Expression<Func<Freelancer, bool>> queryFuncTemp = x => (x.Name.Contains(query) || x.Email.Contains(query) || x.Title.Contains(query) || x.PortfolioURL.Contains(query));
                queryFunc = queryFunc.And(queryFuncTemp);
            }

            if (!string.IsNullOrEmpty(city))
            {
                Expression<Func<Freelancer, bool>> cityFuncTemp = x => x.City.Equals(city);
                queryFunc = queryFunc.And(cityFuncTemp);
            }

            if (categoriaId > 0) {
                Expression<Func<Freelancer, bool>> categoryFuncTemp = x => x.CategoryId == categoriaId;
                queryFunc = queryFunc.And(categoryFuncTemp);
            }

            if (availabilityId > 0) { 
                Expression<Func<Freelancer, bool>> availabilityFuncTemp = x => x.Availability == (AvailabilityType)availabilityId;
                queryFunc = queryFunc.And(availabilityFuncTemp);
            }

            if (skillId != null && skillId.Length > 0)
            {
                Expression<Func<Freelancer, bool>> skillFuncTemp = x => x.Skills.Any(y => skillId.Contains(y.Id));
                queryFunc = queryFunc.And(skillFuncTemp);
            }

            if (segmentId > 0)
            {
                Expression<Func<Freelancer, bool>> segmentFuncTemp = x => x.Segments.Any(y => y.Id == segmentId);
                queryFunc = queryFunc.And(segmentFuncTemp);
            }

            return GetAll(ref totalRecords, queryFunc, null, false, ((page * amount) - amount), amount,  true, includes);
        }

        public Freelancer GetByCode(string code)
        {
            return _repository.GetByExpression(f => f.Code.ToString() == code);
        }
    }
}
