﻿using Freelancers.Crowd.Domain.Entities;
using Freelancers.Crowd.Domain.Interfaces.Repositories;
using Freelancers.Crowd.Domain.Interfaces.Services;
using Freelancers.Crowd.Domain.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Freelancers.Crowd.Domain.Util.Security;

namespace Freelancers.Crowd.Domain.Services
{
    public class ServiceUser : ServiceBase<User>, IServiceUser
    {
        private readonly new IRepositoryBase<User> _repository;

        public ServiceUser(IRepositoryBase<User> repository)
            :base(repository)
        {
            _repository = repository;
        }

        public User Authentication(string name, string password)
        {
            password = Criptografy.GetMD5Hash(password);
            return GetByExpression(x => x.Email.Equals(name) && x.Password.Equals(password));
        }

        public override void Add(User obj)
        {
            obj.Password = Criptografy.GetMD5Hash(obj.Password);
            base.Add(obj);
        }

        public override void Update(User obj)
        {
            if (!String.IsNullOrEmpty(obj.Password))
                base.Update(obj);
        }

        public Guid? RecoverPassword(String email)
        {
            var user = GetByExpression(x => x.Email.Equals(email));
            if (user != null && user.Id > 0)
            {
                if (user.RecoverPassword.HasValue)
                    return user.RecoverPassword.Value;
                user.RecoverPassword = Guid.NewGuid();
                Update(user);
                return user.RecoverPassword.Value;
            }
            return null;
        }

        public bool ChangePassword(String email, String newPassword, String guid)
        {
            var user = GetByExpression(x => x.Email.Equals(email) && x.RecoverPassword.HasValue 
                && x.RecoverPassword.Value.ToString().Equals(guid));
            if (user != null && user.Id > 0)
            {
                user.RecoverPassword = null;
                user.Password = Criptografy.GetMD5Hash(newPassword);
                Update(user);
                return true;
            }
            return false;
        }

        public bool ChangePassword(int id, String newPassword)
        {
            var user = GetByExpression(x => x.Id == id);
            if (user != null && user.Id > 0)
            {
                user.RecoverPassword = null;
                user.Password = Criptografy.GetMD5Hash(newPassword);
                Update(user);
                return true;
            }
            return false;
        }


    }
}
