﻿using Mandrill;
using Mandrill.Models;
using Mandrill.Requests.Messages;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;



namespace Freelancers.Crowd.Domain.Util.Network
{
    public class Email
    {

        public void Send(string to, string name, string subject, string template,
            Dictionary<string, string> variables = null)
        {
            //Thread th = new Thread(() =>
            //{
                var mandrill = new MandrillApi(ConfigurationManager.AppSettings["MandrillKey"]);
                var mandrillRecipients = new List<EmailAddress>();

                mandrillRecipients.Add(new EmailAddress() { Email = to, Name = name });

                var email = new EmailMessage
                {
                    To = mandrillRecipients,
                    FromEmail = ConfigurationManager.AppSettings["MandrillFrom"],
                    FromName = ConfigurationManager.AppSettings["MandrillFromName"],
                    Subject = subject,
                };

                email.AddRecipientVariable(to, "NOME", name);

                if (variables != null)
                    foreach (var variable in variables.Keys)
                        if (!variable.Equals("NOME"))
                            email.AddRecipientVariable(to, variable, variables[variable]);
                mandrill.SendMessageTemplate(new SendMessageTemplateRequest(email, template, null));
            //});

            //th.IsBackground = true;
            //th.Start();
        }
    }
}
