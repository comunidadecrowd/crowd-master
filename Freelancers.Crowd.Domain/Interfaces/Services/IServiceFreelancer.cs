﻿using Freelancers.Crowd.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Freelancers.Crowd.Domain.Interfaces.Services
{
    public interface IServiceFreelancer : IServiceBase<Freelancer>
    {
        IEnumerable<Freelancer> GetAll(int page, int amount, ref int totalRecords);

        IEnumerable<Freelancer> Search(String query, String city, int minValueHour, int maxValueHour, int categoriaId, int[] skillId, int availabilityId, int segmentId, int page, int amount, ref int totalRecords, params string[] includes);

        Freelancer GetByCode(string code);
    }
}
