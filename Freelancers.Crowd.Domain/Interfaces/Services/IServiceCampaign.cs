﻿using Freelancers.Crowd.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Freelancers.Crowd.Domain.Interfaces.Services
{
    public interface IServiceCampaign : IServiceBase<Campaign>
    {
        
    }
}
