﻿using Freelancers.Crowd.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Freelancers.Crowd.Domain.Interfaces.Services
{
    public interface IServiceUser : IServiceBase<User>
    {
        User Authentication(String name, String password);

        Guid? RecoverPassword(String email);

        bool ChangePassword(String email, String newPassword, String guid);

        bool ChangePassword(int id, String newPassword);
    }
}
