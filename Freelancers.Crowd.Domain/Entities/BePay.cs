﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Freelancers.Crowd.Domain.Entities.Enum;

namespace Freelancers.Crowd.Domain.Entities
{
    public class BePay : BaseEntity
    {
        public int IdCustomer { get; set; }
        public int IdUser { get; set; }
        public BePayType BePayType { get; set; }

        public string Transaction { get; set; }
        public string Token { get; set; }
        public string ReadableLine { get; set; }
        public string FinancialStatus { get; set; }
        public string AuthorizationNumber { get; set; }


        public PlanType PlanType { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual User User { get; set; }
    }
}
