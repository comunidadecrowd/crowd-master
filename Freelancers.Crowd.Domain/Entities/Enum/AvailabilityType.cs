﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Freelancers.Crowd.Domain.Entities.Enum
{
    public enum AvailabilityType
    {
        INTEGRAL = 1,
        PARCIAL = 2
    }
}
