﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Freelancers.Crowd.Domain.Entities.Enum
{
    public enum PlanType
    {
        START = 1,
        ONE = 2,
        PRO = 3,
        TOP = 4
    }
}
