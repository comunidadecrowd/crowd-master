﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Freelancers.Crowd.Domain.Entities.Enum
{
    public enum TaskStatus
    {
        TO_BE_DONE = 1,
        DOING = 2,
        WAITING = 3,
        DONE = 4
    }
}
