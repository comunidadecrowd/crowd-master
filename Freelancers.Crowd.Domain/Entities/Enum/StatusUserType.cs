﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Freelancers.Crowd.Domain.Entities.Enum
{
    public enum StatusUserType
    {
        Disabled = 0, 
        Active = 1
    }
}
