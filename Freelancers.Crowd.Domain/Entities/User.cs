﻿using Freelancers.Crowd.Domain.Entities.Enum;
using Freelancers.Crowd.Domain.Util;
using Freelancers.Crowd.Domain.Util.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Freelancers.Crowd.Domain.Entities
{
    public class User : BaseEntity
    {
        public int? IdCustomer { get; set; }
        public int? IdFreelancer { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string NewPassword { get { return Password; } set {
                if (!string.IsNullOrEmpty(value)) Password = Criptografy.GetMD5Hash(value);
            }
        }
        public Guid? RecoverPassword { get; set; }
        public RoleType Role { get; set; }
        public string Photo { get; set; }
        public string Phone { get; set; }

        public string RoleCompany { get; set; }

        public virtual Customer Customer { get; set; }
        //[ScriptIgnore]
        public virtual Freelancer Freelancers { get; set; }

        public bool AcceptTerms { get; set; }
        public DateTime AcceptTermsAt { get; set; }

        public DateTime? ConfirmEmailAt { get; set; }

        public Guid? ConfirmEmail { get; set; }

        public class ActionLog : BaseEntity
        {
            public int UserId { get; set; }
            public ActionLogType Type { get; set; }

            public virtual User User { get; set; }
        }
    }
}
