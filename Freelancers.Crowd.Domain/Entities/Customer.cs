﻿using Freelancers.Crowd.Domain.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Freelancers.Crowd.Domain.Entities
{
    public class Customer : BaseEntity
    {
        public Customer()
        {
            Prospect = false;
        }

        public String Name { get; set; }
        public String Trade { get; set; }

        public string Logo { get; set; }
        public string CNPJ { get; set; }
        public int QtyPersons { get; set; }

        public int StateId { get; set; }
        public int CityId { get; set; }

        public string NameResponsible { get; set; }
        public string EmailResponsible { get; set; }

        public virtual State State { get; set; }
        public virtual City City { get; set; }

        public string InscricaoEstadual { get; set; }
        public string InscricaoMunicipal { get; set; }

        public string Address { get; set; }
        public string Number { get; set; }
        public string Complement { get; set; }
        public string CEP { get; set; }

        public bool Prospect { get; set; }

        public string Phone { get; set; }

        public DateTime? LastChangeActivation { get; set; }

        public PlanType PlanType { get; set; }

        public class Tracking : BaseEntity
        {
            public int UserId { get; set; }
            public int FreelancerId { get; set; }
            public string FieldRequested { get; set; }
            public string SearchTerm { get; set; }

            public PlanType PlanType { get; set; }

            public virtual User User { get; set; }
            public virtual Freelancer Freelancer { get; set; }
        }

       
    }
}
