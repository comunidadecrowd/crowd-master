var gulp = require("gulp");
// var jshint = require("gulp-jshint");
var clean = require("gulp-clean");
var uglify = require("gulp-uglify");
var concat = require("gulp-concat");
var brotli = require('gulp-brotli');
var htmlmin = require("gulp-html-minifier");
var watch = require("gulp-watch");
var iife = require("gulp-iife");
var cssmin = require("gulp-cssmin");
var webserver = require("gulp-webserver");
var sourcemaps = require("gulp-sourcemaps");
var compiler = require("google-closure-compiler-js").gulp();
var runSequence = require('run-sequence');
var eslint = require('gulp-eslint');

//var wrap    = require("gulp-wrap");

var srcBasePath = "./src/module";
var srcDist = "./dist/";
var jsDest = "./dist/js";
var indexDestPortal = "./dist/portal/";
var viewDest = "./dist/templates";
var indexDestFreelancer = "./dist/freelancer/";
var viewDestFreelancer = "./dist/templates/professional";
var cssDest = "./dist/css";
var vendor = [
  //"./vendor/jquery/dist/jquery.min.js",
  "./vendor/angular/angular.min.js",
  "./vendor/angular-route/angular-route.min.js",
  "./vendor/angular-ui-router/release/angular-ui-router.min.js",
  "./vendor/ng-tags-input/ng-tags-input.min.js",
  "./vendor/angular-sanitize/angular-sanitize.min.js",
  "./vendor/jquery-asRange/dist/jquery-asRange.min.js",
  "./vendor/bootstrap-select/dist/js/bootstrap-select.min.js",
  "./vendor/blockUI/jquery.blockUI.js",
  "./vendor/ng-dialog/js/ngDialog.min.js",
  "./vendor/angular-ui-switch/angular-ui-switch.min.js",
  "./vendor/webui-popover/dist/jquery.webui-popover.min.js",
  "./vendor/ngMask/dist/ngMask.min.js",
  "./vendor/ngstorage/ngStorage.min.js",
  "./vendor/angular-ui-select/dist/select.min.js",
  "./vendor/angularjs-slider/dist/rzslider.min.js",
  "./vendor/ng-table/dist/ng-table.min.js",
  "./vendor/angular-animate/angular-animate.min.js",
  "./vendor/AngularJS-Toaster/toaster.min.js",
  "./vendor/angular-input-masks/angular-input-masks-standalone.min.js",
  "./vendor/moment/min/moment.min.js",
  "./vendor/moment/min/moment-with-locales.min.js",
  "./vendor/angular-moment/angular-moment.min.js",
  "./vendor/sweetalert2/dist/sweetalert2.min.js",
  "./vendor/angulartics/dist/angulartics.min.js",
  "./vendor/angulartics-google-analytics/dist/angulartics-ga.min.js",
  "./vendor/angular-moment-picker/dist/angular-moment-picker.min.js",
  "./vendor/lodash/dist/lodash.min.js",
  "./vendor/ng-rollbar/ng-rollbar.min.js",
  "./vendor/editor.md/editormd.min.js",
	"./vendor/plyr/dist/plyr.js",
	"./vendor/plyr/demo/dist/demo.js",
	"./vendor/angulartics-google-analytics/dist/angulartics-ga.min.js",
  "./vendor/angulartics/dist/angulartics.min.js",
  "./vendor/angucomplete-alt/dist/angucomplete-alt.min.js",

  //	"./vendor/typeahead.js/dist/bloodhound.min.js",
  //	"./vendor/angular-bootstrap/ui-bootstrap.min.js",
  //	"./vendor/angular-bootstrap/ui-bootstrap-tpls.min.js",
  //	"./vendor/typeahead.js/dist/typeahead.bundle.min.js",
  //	"./vendor/angular-typeahead/dist/angular-typeahead.min.js",

  "./vendor/typeahead.js/dist/bloodhound.min.js",
  "./vendor/angular-bootstrap/ui-bootstrap-tpls.min.js",
  "./vendor/typeahead.js/dist/typeahead.bundle.min.js",
  "./vendor/angular-typeahead/dist/angular-typeahead.min.js",
  "./vendor/highcharts/highcharts.js",

  "./lib/quill/quill.min.js",
  "./lib/angular-locale/angular-locale_pt-br.js",
  "./lib/cropper/cropper.min.js",
  "./lib/directives/input-mask-date/input-mask-date.js",
	"./lib/bxslider-4/src/js/jquery.bxslider.js"
];

var vendorPublic = [
  "./vendor/angular/angular.min.js",
  "./vendor/ngstorage/ngStorage.min.js",
  "./vendor/ng-tags-input/ng-tags-input.min.js",
  "./vendor/angular-input-masks/angular-input-masks-standalone.min.js",
  "./lib/cropper/cropper.min.js",
  "./lib/directives/input-mask-date/input-mask-date.js",
  "./src/module/checkLocalStorage.js",
  "./vendor/ng-rollbar/ng-rollbar.min.js",
];

// Source Files...
var modules = {
  comum: {
    jsFiles: srcBasePath + "/comum/**/*.js",
    cssFiles: srcBasePath + "/comum/css/**/*.css",
    cssFilesC: srcBasePath + "/comum/components/**/*.css",
    htmlFiles: srcBasePath + "/comum/components/**/*.html",
  },

  sharedScreen: {
    jsFiles: srcBasePath + "/sharedScreen/**/*.js",
    cssFiles: srcBasePath + "/sharedScreen/components/**/*.css",
    htmlFiles: srcBasePath + "/sharedScreen/components/**/*.html",
  },

  master: {
    jsFiles: srcBasePath + "/master/**/*.js",
    htmlFiles: srcBasePath + "/master/components/**/*.html",
    cssFiles: srcBasePath + "/master/components/**/*.css",
  },

  professional: {
    jsFiles: srcBasePath + "/professional/**/*.js",
    htmlFiles: srcBasePath + "/professional/components/**/*.html",
    cssFiles: srcBasePath + "/professional/components/**/*.css",
  },

  public: {
    jsFiles: srcBasePath + "/public/**//*.js",
    htmlFiles: srcBasePath + "/public/**//*.html",
  },
  base: {
    assets: srcBasePath + "/base/assets/**/*",
    global: srcBasePath + "/base/global/**/*",
    portal: srcBasePath + "/base/portal/*.html",
    freelancer: srcBasePath + "/base/freelancer/*.html",
  },
};

website = {
  src: "./src/website/**/*",
  dist: "./dist",
};

gulp.task("clean", function() {
  return gulp.src(srcDist, {read: false}).pipe(clean());
});

gulp.task('lint', function () {
  // ESLint ignores files with "node_modules" paths.
  // So, it's best to have gulp ignore the directory as well.
  // Also, Be sure to return the stream from the task;
  // Otherwise, the task may end before the stream has finished.
  return gulp.src([
    modules.public.jsFiles,
    modules.master.jsFiles,
    modules.comum.jsFiles,
    modules.sharedScreen.jsFiles,
    modules.professional.jsFiles,
  ])

    // eslint() attaches the lint output to the "eslint" property
    // of the file object so it can be used by other modules.
    .pipe(eslint({
      fix: true,
      rules: {
        // enforce line breaks after opening and before closing array brackets
        // https://eslint.org/docs/rules/array-bracket-newline
        // TODO: enable? semver-major
        'array-bracket-newline': ['off', 'consistent'], // object option alternative: { multiline: true, minItems: 3 }

        // enforce line breaks between array elements
        // https://eslint.org/docs/rules/array-element-newline
        // TODO: enable? semver-major
        'array-element-newline': ['off', {
          multiline: true,
          minItems: 3
        }],

        // enforce spacing inside array brackets
        'array-bracket-spacing': ['warn', 'never'],

        // enforce spacing inside single-line blocks
        // https://eslint.org/docs/rules/block-spacing
        'block-spacing': ['warn', 'always'],

        // enforce one true brace style
        'brace-style': ['warn', '1tbs', {
          allowSingleLine: true
        }],

        // require camel case names
        camelcase: ['warn', {
          properties: 'never'
        }],

        // enforce or disallow capitalization of the first letter of a comment
        // https://eslint.org/docs/rules/capitalized-comments
        'capitalized-comments': ['off', 'never', {
          line: {
            ignorePattern: '.*',
            ignoreInlineComments: true,
            ignoreConsecutiveComments: true,
          },
          block: {
            ignorePattern: '.*',
            ignoreInlineComments: true,
            ignoreConsecutiveComments: true,
          },
        }],

        // require trailing commas in multiline object literals
        'comma-dangle': ['warn', {
          arrays: 'always-multiline',
          objects: 'always-multiline',
          imports: 'always-multiline',
          exports: 'always-multiline',
          functions: 'ignore',
        }],

        // enforce spacing before and after comma
        'comma-spacing': ['warn', {
          before: false,
          after: true
        }],

        // enforce one true comma style
        'comma-style': ['warn', 'last'],

        // disallow padding inside computed properties
        'computed-property-spacing': ['warn', 'never'],

        // enforces consistent naming when capturing the current execution context
        'consistent-this': 'warn',

        // enforce newline at the end of file, with no multiple empty lines
        'eol-last': ['warn', 'always'],

        // enforce spacing between functions and their invocations
        // https://eslint.org/docs/rules/func-call-spacing
        'func-call-spacing': ['warn', 'never'],

        // requires function names to match the name of the variable or property to which they are
        // assigned
        // https://eslint.org/docs/rules/func-name-matching
        'func-name-matching': ['error', 'always', {
          includeCommonJSModuleExports: true
        }],

        // require function expressions to have a name
        // https://eslint.org/docs/rules/func-names
        'func-names': 'off',

        // enforces use of function declarations or expressions
        // https://eslint.org/docs/rules/func-style
        // TODO: enable
        'func-style': ['warn', 'declaration'],

        // enforce consistent line breaks inside function parentheses
        // https://eslint.org/docs/rules/function-paren-newline
        'function-paren-newline': ['warn', 'multiline'],

        // Blacklist certain identifiers to prevent them being used
        // https://eslint.org/docs/rules/id-blacklist
        'id-blacklist': 'off',

        // this option enforces minimum and maximum identifier lengths
        // (variable names, property names etc.)
        'id-length': 'off',

        // require identifiers to match the provided regular expression
        'id-match': 'error',

        // this option sets a specific tab width for your code
        // https://eslint.org/docs/rules/indent
        indent: ['warn', 2, {
          SwitchCase: 1,
          VariableDeclarator: 1,
          outerIIFEBody: 1,
          // MemberExpression: null,
          FunctionDeclaration: {
            parameters: 1,
            body: 1
          },
          FunctionExpression: {
            parameters: 1,
            body: 1
          },
          CallExpression: {
            arguments: 1
          },
          ArrayExpression: 1,
          ObjectExpression: 1,
          ImportDeclaration: 1,
          flatTernaryExpressions: false,
          ignoredNodes: ['JSXElement', 'JSXElement *']
        }],

        // specify whether double or single quotes should be used in JSX attributes
        // https://eslint.org/docs/rules/jsx-quotes
        'jsx-quotes': ['off', 'prefer-double'],

        // enforces spacing between keys and values in object literal properties
        'key-spacing': ['warn', {
          beforeColon: false,
          afterColon: true
        }],

        // require a space before & after certain keywords
        'keyword-spacing': ['warn', {
          before: true,
          after: true,
          overrides: {
            return: {
              after: true
            },
            throw: {
              after: true
            },
            case: {
              after: true
            }
          }
        }],

        // enforce position of line comments
        // https://eslint.org/docs/rules/line-comment-position
        // TODO: enable?
        'line-comment-position': ['off', {
          position: 'above',
          ignorePattern: '',
          applyDefaultPatterns: true,
        }],

        // disallow mixed 'LF' and 'CRLF' as linebreaks
        // https://eslint.org/docs/rules/linebreak-style
        'linebreak-style': ['warn', 'unix'],

        // require or disallow an empty line between class members
        // https://eslint.org/docs/rules/lines-between-class-members
        'lines-between-class-members': ['warn', 'always', {
          exceptAfterSingleLine: false
        }],

        // enforces empty lines around comments
        'lines-around-comment': 'off',

        // require or disallow newlines around directives
        // https://eslint.org/docs/rules/lines-around-directive
        'lines-around-directive': ['warn', {
          before: 'always',
          after: 'always',
        }],

        // specify the maximum depth that blocks can be nested
        'max-depth': ['warn', 4],

        // specify the maximum length of a line in your program
        // https://eslint.org/docs/rules/max-len
        'max-len': ['off', 100, 2, {
          ignoreUrls: true,
          ignoreComments: false,
          ignoreRegExpLiterals: true,
          ignoreStrings: true,
          ignoreTemplateLiterals: true,
        }],

        // specify the max number of lines in a file
        // https://eslint.org/docs/rules/max-lines
        'max-lines': ['off', {
          max: 300,
          skipBlankLines: true,
          skipComments: true
        }],

        // specify the maximum depth callbacks can be nested
        'max-nested-callbacks': 'error',

        // limits the number of parameters that can be used in the function declaration.
        'max-params': ['warn', 3],

        // specify the maximum number of statement allowed in a function
        'max-statements': ['warn', 10],

        // restrict the number of statements per line
        // https://eslint.org/docs/rules/max-statements-per-line
        'max-statements-per-line': ['off', {
          max: 1
        }],

        // enforce a particular style for multiline comments
        // https://eslint.org/docs/rules/multiline-comment-style
        'multiline-comment-style': ['off', 'starred-block'],

        // require multiline ternary
        // https://eslint.org/docs/rules/multiline-ternary
        // TODO: enable?
        'multiline-ternary': ['off', 'never'],

        // require a capital letter for constructors
        'new-cap': ['warn', {
          newIsCap: true,
          newIsCapExceptions: [],
          capIsNew: false,
          capIsNewExceptions: ['Immutable.Map', 'Immutable.Set', 'Immutable.List'],
        }],

        // disallow the omission of parentheses when invoking a constructor with no arguments
        // https://eslint.org/docs/rules/new-parens
        'new-parens': 'warn',

        // allow/disallow an empty newline after var statement
        'newline-after-var': 'off',

        // https://eslint.org/docs/rules/newline-before-return
        'newline-before-return': 'off',

        // enforces new line after each method call in the chain to make it
        // more readable and easy to maintain
        // https://eslint.org/docs/rules/newline-per-chained-call
        'newline-per-chained-call': ['warn', {
          ignoreChainWithDepth: 4
        }],

        // disallow use of the Array constructor
        'no-array-constructor': 'warn',

        // disallow use of bitwise operators
        // https://eslint.org/docs/rules/no-bitwise
        'no-bitwise': 'warn',

        // disallow use of the continue statement
        // https://eslint.org/docs/rules/no-continue
        'no-continue': 'warn',

        // disallow comments inline after code
        'no-inline-comments': 'off',

        // disallow if as the only statement in an else block
        // https://eslint.org/docs/rules/no-lonely-if
        'no-lonely-if': 'warn',

        // disallow un-paren'd mixes of different operators
        // https://eslint.org/docs/rules/no-mixed-operators
        'no-mixed-operators': ['warn', {
          // the list of arthmetic groups disallows mixing `%` and `**`
          // with other arithmetic operators.
          groups: [
            ['%', '**'],
            ['%', '+'],
            ['%', '-'],
            ['%', '*'],
            ['%', '/'],
            ['**', '+'],
            ['**', '-'],
            ['**', '*'],
            ['**', '/'],
            ['&', '|', '^', '~', '<<', '>>', '>>>'],
            ['==', '!=', '===', '!==', '>', '>=', '<', '<='],
            ['&&', '||'],
            ['in', 'instanceof']
          ],
          allowSamePrecedence: false
        }],

        // disallow mixed spaces and tabs for indentation
        'no-mixed-spaces-and-tabs': 'warn',

        // disallow use of chained assignment expressions
        // https://eslint.org/docs/rules/no-multi-assign
        'no-multi-assign': ['warn'],

        // disallow multiple empty lines and only one newline at the end
        'no-multiple-empty-lines': ['warn', {
          max: 2,
          maxEOF: 1
        }],

        // disallow negated conditions
        // https://eslint.org/docs/rules/no-negated-condition
        'no-negated-condition': 'off',

        // disallow nested ternary expressions
        'no-nested-ternary': 'warn',

        // disallow use of the Object constructor
        'no-new-object': 'warn',

        // disallow use of unary operators, ++ and --
        // https://eslint.org/docs/rules/no-plusplus
        'no-plusplus': 'warn',

        // disallow certain syntax forms
        // https://eslint.org/docs/rules/no-restricted-syntax
        'no-restricted-syntax': [
          'warn',
          {
            selector: 'ForInStatement',
            message: 'for..in loops iterate over the entire prototype chain, which is virtually never what you want. Use Object.{keys,values,entries}, and iterate over the resulting array.',
          },
          {
            selector: 'ForOfStatement',
            message: 'iterators/generators require regenerator-runtime, which is too heavyweight for this guide to allow them. Separately, loops should be avoided in favor of array iterations.',
          },
          {
            selector: 'LabeledStatement',
            message: 'Labels are a form of GOTO; using them makes code confusing and hard to maintain and understand.',
          },
          {
            selector: 'WithStatement',
            message: '`with` is disallowed in strict mode because it makes code impossible to predict and optimize.',
          },
        ],

        // disallow space between function identifier and application
        'no-spaced-func': 'warn',

        // disallow tab characters entirely
        'no-tabs': 'warn',

        // disallow the use of ternary operators
        'no-ternary': 'off',

        // disallow trailing whitespace at the end of lines
        'no-trailing-spaces': ['warn', {
          skipBlankLines: false,
          ignoreComments: false,
        }],

        // disallow dangling underscores in identifiers
        'no-underscore-dangle': ['warn', {
          allow: [],
          allowAfterThis: false,
          allowAfterSuper: false,
          enforceInMethodNames: false,
        }],

        // disallow the use of Boolean literals in conditional expressions
        // also, prefer `a || b` over `a ? a : b`
        // https://eslint.org/docs/rules/no-unneeded-ternary
        'no-unneeded-ternary': ['warn', {
          defaultAssignment: false
        }],

        // disallow whitespace before properties
        // https://eslint.org/docs/rules/no-whitespace-before-property
        'no-whitespace-before-property': 'warn',

        // enforce the location of single-line statements
        // https://eslint.org/docs/rules/nonblock-statement-body-position
        'nonblock-statement-body-position': ['warn', 'beside', {
          overrides: {}
        }],

        // require padding inside curly braces
        'object-curly-spacing': ['warn', 'always'],

        // enforce line breaks between braces
        // https://eslint.org/docs/rules/object-curly-newline
        'object-curly-newline': ['warn', {
          ObjectExpression: {
            minProperties: 4,
            multiline: true,
            consistent: true
          },
          ObjectPattern: {
            minProperties: 4,
            multiline: true,
            consistent: true
          }
        }],

        // enforce "same line" or "multiple line" on object properties.
        // https://eslint.org/docs/rules/object-property-newline
        'object-property-newline': ['warn', {
          allowMultiplePropertiesPerLine: true,
        }],

        // allow just one var statement per function
        'one-var': ['error', 'never'],

        // require a newline around variable declaration
        // https://eslint.org/docs/rules/one-var-declaration-per-line
        'one-var-declaration-per-line': ['warn', 'always'],

        // require assignment operator shorthand where possible or prohibit it entirely
        // https://eslint.org/docs/rules/operator-assignment
        'operator-assignment': ['warn', 'always'],

        // Requires operator at the beginning of the line in multiline statements
        // https://eslint.org/docs/rules/operator-linebreak
        'operator-linebreak': ['warn', 'before'],

        // disallow padding within blocks
        'padded-blocks': ['warn', {
          blocks: 'never',
          classes: 'never',
          switches: 'never'
        }],

        // Require or disallow padding lines between statements
        // https://eslint.org/docs/rules/padding-line-between-statements
        'padding-line-between-statements': 'off',

        // require quotes around object literal property names
        // https://eslint.org/docs/rules/quote-props.html
        'quote-props': ['error', 'as-needed', {
          keywords: false,
          unnecessary: true,
          numbers: false
        }],

        // specify whether double or single quotes should be used
        quotes: ['warn', 'double', {
          avoidEscape: true
        }],

        // do not require jsdoc
        // https://eslint.org/docs/rules/require-jsdoc
        'require-jsdoc': 'off',

        // require or disallow use of semicolons instead of ASI
        semi: ['warn', 'always'],

        // enforce spacing before and after semicolons
        'semi-spacing': ['warn', {
          before: false,
          after: true
        }],

        // Enforce location of semicolons
        // https://eslint.org/docs/rules/semi-style
        'semi-style': ['error', 'last'],

        // requires object keys to be sorted
        'sort-keys': ['error', 'asc', {
          caseSensitive: false,
          natural: true
        }],

        // sort variables within the same declaration block
        'sort-vars': 'error',

        // require or disallow space before blocks
        'space-before-blocks': 'warn',

        // require or disallow space before function opening parenthesis
        // https://eslint.org/docs/rules/space-before-function-paren
        'space-before-function-paren': ['warn', {
          anonymous: 'always',
          named: 'never',
          asyncArrow: 'always'
        }],

        // require or disallow spaces inside parentheses
        'space-in-parens': ['warn', 'never'],

        // require spaces around operators
        'space-infix-ops': 'warn',

        // Require or disallow spaces before/after unary operators
        // https://eslint.org/docs/rules/space-unary-ops
        'space-unary-ops': ['warn', {
          words: true,
          nonwords: false,
          overrides: {},
        }],

        // require or disallow a space immediately following the // or /* in a comment
        // https://eslint.org/docs/rules/spaced-comment
        'spaced-comment': ['warn', 'always', {
          line: {
            exceptions: ['-', '+'],
            markers: ['=', '!'], // space here to support sprockets directives
          },
          block: {
            exceptions: ['-', '+'],
            markers: ['=', '!'], // space here to support sprockets directives
            balanced: true,
          }
        }],

        // Enforce spacing around colons of switch statements
        // https://eslint.org/docs/rules/switch-colon-spacing
        'switch-colon-spacing': ['warn', {
          after: true,
          before: false
        }],

        // Require or disallow spacing between template tags and their literals
        // https://eslint.org/docs/rules/template-tag-spacing
        'template-tag-spacing': ['warn', 'never'],

        // require or disallow the Unicode Byte Order Mark
        // https://eslint.org/docs/rules/unicode-bom
        'unicode-bom': ['warn', 'never'],

        // require regex literals to be wrapped in parentheses
        'wrap-regex': 'off'
      }
    }))

    // eslint.format() outputs the lint results to the console.
    // Alternatively use eslint.formatEach() (see Docs).
    .pipe(eslint.formatEach())
    // To have the process exit with an error code (1) on
    // lint error, return the stream and pipe to failAfterError last.
    .pipe(eslint.failAfterError());
});


gulp.task("jshint", function() {
  var files = [
    modules.public.jsFiles,
    modules.master.jsFiles,
    modules.comum.jsFiles,
    modules.sharedScreen.jsFiles,
    modules.professional.jsFiles,
  ];

  return gulp.src(files).pipe(jshint()).pipe(jshint.reporter("fail"));
});

gulp.task("uglify", function() {
  var files = [
    modules.public.jsFiles,
    modules.master.jsFiles,
    modules.comum.jsFiles,
    modules.sharedScreen.jsFiles,
    modules.professional.jsFiles,
  ];

  return gulp
    .src(files)
    .pipe(sourcemaps.init())
    .pipe(concat("main.min.js"))
    .pipe(uglify({
      compress: {
   //     drop_console: true
    }}))
    .pipe(sourcemaps.write("maps"))
    .pipe(gulp.dest(jsDest));
});

gulp.task("concat", function() {
  var files = [
    modules.public.jsFiles,
    modules.master.jsFiles,
    modules.comum.jsFiles,
    modules.sharedScreen.jsFiles,
    modules.professional.jsFiles,
  ];

  return gulp
    .src(files)
    .pipe(sourcemaps.init())
    .pipe(concat("main.min.js"))
    .pipe(sourcemaps.write("maps"))
    .pipe(gulp.dest(jsDest));
});

gulp.task("brotli", function() {
  var files = [
    modules.public.jsFiles,
    modules.master.jsFiles,
    modules.comum.jsFiles,
    modules.sharedScreen.jsFiles,
    modules.professional.jsFiles,
  ];

  return gulp
    .src(files)
    .pipe(brotli.compress())
    .pipe(gulp.dest(jsDest));
});


gulp.task("uglifyPublic", function() {
  var files = [modules.public.jsFiles];

  return gulp
    .src(files)
    .pipe(sourcemaps.init())
    .pipe(concat("public.js"))
    .pipe(uglify({
      compress: {
        drop_console: true
      }
    }))
    .pipe(sourcemaps.write("maps"))
    .pipe(gulp.dest(jsDest));
});

gulp.task("concatPublic", function() {
  var files = [modules.public.jsFiles];

  return gulp
    .src(files)
    .pipe(sourcemaps.init())
    .pipe(concat("public.js"))
    .pipe(sourcemaps.write("maps"))
    .pipe(gulp.dest(jsDest));
});

gulp.task("uglifyFreelancer", function() {
  var files = [
    modules.public.jsFiles,
    modules.comum.jsFiles,
    modules.sharedScreen.jsFiles,
    modules.professional.jsFiles,
  ];

  return gulp
    .src(files)
    .pipe(sourcemaps.init())
    .pipe(concat("freelancer.min.js"))
    .pipe(uglify({
      compress: {
        drop_console: true
      }
    }))
    .pipe(sourcemaps.write("maps"))
    .pipe(gulp.dest(jsDest));
});

gulp.task("concatFreelancer", function() {
  var files = [
    modules.public.jsFiles,
    modules.comum.jsFiles,
    modules.sharedScreen.jsFiles,
    modules.professional.jsFiles,
  ];

  return gulp
    .src(files)
    .pipe(sourcemaps.init())
    .pipe(concat("freelancer.min.js"))
    .pipe(sourcemaps.write("maps"))
    .pipe(gulp.dest(jsDest));
});

/****************************************
HTML and CSS
*****************************************/

gulp.task("htmlmin", function() {
  var files = [
    modules.comum.htmlFiles,
    modules.master.htmlFiles,
    modules.sharedScreen.htmlFiles,
    modules.professional.htmlFiles,
    modules.public.htmlFiles,
  ];

  return gulp
    .src(files)
    .pipe(
      htmlmin({
        collapseWhitespace: true,
      })
    )
    .pipe(gulp.dest(viewDest));
});

gulp.task("htmlminFreelancer", function() {
  var files = [modules.professional.htmlFiles];

  return gulp
    .src(files)
    .pipe(
      htmlmin({
        collapseWhitespace: true,
      })
    )
    .pipe(gulp.dest(viewDestFreelancer));
});

gulp.task("cssmin", function() {
  var files = [
    modules.master.cssFiles,
    modules.sharedScreen.cssFiles,
    modules.comum.cssFiles,
    modules.comum.cssFilesC,
    modules.professional.cssFiles,
  ];

  return gulp
    .src(files)
    .pipe(cssmin())
    .pipe(concat("main.min.css"))
    .pipe(gulp.dest(cssDest));
});

/*******************
Theme files CSS ****
********************/

gulp.task("fontThemeFile", function() {
  return gulp
    .src("./themeFiles/global/fonts/web-icons/web-icons.min.css")
    .pipe(gulp.dest(cssDest));
});

gulp.task("cssThemeFile", ["fontThemeFile", "cssVendorPublic"], function() {
  return gulp
    .src([
      "./vendor/ng-dialog/css/ngDialog.min.css",
      "./vendor/ng-dialog/css/ngDialog-theme-default.min.css",
      "./vendor/angucomplete-alt/angucomplete-alt.css",
      "./vendor/angular-ui-switch/angular-ui-switch.min.css",
      "./vendor/webui-popover/dist/jquery.webui-popover.min.css",
      "./vendor/angular-ui-select/dist/select.min.css",
      "./vendor/angularjs-slider/dist/rzslider.min.css",
      "./vendor/ng-table/dist/ng-table.min.css",
      "./vendor/AngularJS-Toaster/toaster.min.css",
      "./vendor/sweetalert2/dist/sweetalert2.css",
      "./vendor/angular-moment-picker/dist/angular-moment-picker.min.css",
      "./vendor/editor.md/css/editormd.min.css",
		  "./vendor/plyr/dist/plyr.css",		

		"./lib/bxslider-4/src/css/jquery.bxslider.css",
      "./lib/quill/quill.snow.css",
      "./lib/cropper/cropper.min.css",
    ])
    .pipe(concat("theme.min.css"))
    .pipe(gulp.dest(cssDest));
});

gulp.task("cssVendorPublic", function() {
  return gulp
    .src([
      "./vendor/ng-dialog/css/ngDialog.min.css",
      "./vendor/ng-dialog/css/ngDialog-theme-default.min.css",
      "./lib/cropper/cropper.min.css",
    ])
    .pipe(sourcemaps.init())
    .pipe(concat("public.css"))
    .pipe(gulp.dest(cssDest))
    .pipe(sourcemaps.write("maps"))
    .pipe(gulp.dest(cssDest));
});

/*******************
Theme files JS ****
********************/

gulp.task("vendor", function() {
  var vendor2 = ["./vendor/dropzone/dist/min/dropzone.min.js"];

  gulp
    .src(vendor2)
    .pipe(sourcemaps.init())
    .pipe(gulp.dest(jsDest))
    .pipe(sourcemaps.write("maps"))
    .pipe(gulp.dest(jsDest));

  return gulp
    .src(vendor)
    .pipe(sourcemaps.init())
    .pipe(concat("bootstrap.min.js"))
    .pipe(gulp.dest(jsDest))
    .pipe(sourcemaps.write("maps"))
    .pipe(gulp.dest(jsDest));
});

gulp.task("vendorPublic", function() {
  return (gulp
      .src(vendorPublic)
      .pipe(sourcemaps.init())
      .pipe(concat("bootstrap.public.min.js"))
      // .pipe(uglify())
      .pipe(sourcemaps.write("maps"))
      .pipe(gulp.dest(jsDest)) );
});

gulp.task("jsThemeFile", ["vendor"], function() {
  return gulp
    .src([
      "./themeFiles/global/vendor/babel-external-helpers/babel-external-helpers.js",
      "./themeFiles/global/vendor/tether/tether.min.js",
      "./themeFiles/global/vendor/bootstrap/bootstrap.js",
    ])
    .pipe(concat("theme.min.js"))
    .pipe(gulp.dest(jsDest));
});

gulp.task("watch", function() {
  var files = [
    modules.comum.jsFiles,
    modules.comum.htmlFiles,
    modules.comum.cssFiles,
    modules.comum.cssFilesC,

    modules.sharedScreen.jsFiles,
    modules.sharedScreen.htmlFiles,
    modules.sharedScreen.cssFiles,

    modules.master.htmlFiles,
    modules.master.jsFiles,
    modules.master.cssFiles,

    modules.professional.htmlFiles,
    modules.professional.jsFiles,
    modules.professional.cssFiles,

    modules.public.jsFiles,
    modules.public.htmlFiles,

    modules.base.assets,
    modules.base.portal,
    modules.base.freelancer,
    modules.base.global,
  ];

  var tasks = [
    "compiler",
    "htmlmin",
    "htmlminFreelancer",
    "concat",
    "concatPublic",
    "concatFreelancer",
    "cssmin",
    "dist",
  ];

  gulp.watch(files, tasks);
});

gulp.task("dist", function(end) {
  //Copy template to folder freelancer
  gulp.src(modules.base.freelancer).pipe(gulp.dest(indexDestFreelancer));
  //Copy template to folder portal
  gulp.src(modules.base.portal).pipe(gulp.dest(indexDestPortal));
  //Copy files of theme to dist
  gulp.src(modules.base.global).pipe(gulp.dest(srcDist + "/global"));
  //Copy assets to dist
  gulp.src(modules.base.assets).pipe(gulp.dest(srcDist + "/assets"));

  return gulp.src(website.src).pipe(gulp.dest(website.dist));
});

gulp.task("default", [
  "compiler",
  "concat",
  "concatPublic",
  "concatFreelancer",
  "htmlmin",
  "htmlminFreelancer",
  "cssmin",
  "cssThemeFile",
  "jsThemeFile",
  "vendorPublic",
  "dist",
  "watch",
]);

// A build irá rodar nesta ordem:
// * clean
// * o array completo em paralelo
// * dist
// * E então sim finaliza
gulp.task('build', function (callback) {
  runSequence('clean', "compiler",
    [ "uglify",
      "uglifyPublic",
      "uglifyFreelancer",
      "brotli",
      "htmlmin",
      "htmlminFreelancer",
      "cssmin",
      "cssThemeFile",
      "jsThemeFile",
      "vendorPublic"],
    'dist',
    callback);
});

/**
	* @name compiler
	* @desc Google Closure Compiler, responsável por validar e otimizar o código. Por favor seguir o styleguide imposto.
  * @example  http://closuretools.blogspot.com.br/2012/09/which-compilation-level-is-right-for-me.html
  * @example  https://developers.google.com/closure/compiler/docs/limitations
  * @example  https://github.com/google/closure-compiler/wiki/Annotating-Types
  * @example  https://developers.google.com/closure/compiler/docs/api-tutorial3#dangers
  * @example  https://github.com/google/closure-compiler/wiki/FAQ
	*/
gulp.task("compiler", function() {
  var files = [
    modules.comum.jsFiles,
    modules.sharedScreen.jsFiles,
    modules.master.jsFiles,
    modules.professional.jsFiles,
    modules.public.jsFiles
  ];

  return (gulp
      .src(files)
      // your other steps here
      .pipe(
        compiler({
          angularPass: "true",
          checksOnly: "true",
          compilationLevel: "SIMPLE",
          warningLevel: "DEFAULT", //UTILIZAR VERBOSE ENQUANTO ESTIVER REFATORANDO O CÓDIGO
          jsOutputFile: "output.min.js", // ELE NÃO FAZ OUTPUT POR SER SÓ UM LINTER
        })
      )
      .pipe(gulp.dest("./dist")) );
});

/****************************************
WEB SERVER
*****************************************/
gulp.task("s", function() {
  gulp.src("dist").pipe(
    webserver({
      port: "8080",
      livereload: true,
      //directoryListing: true,
      open: true,
    })
  );
});
