/**
 * InputMaskDate Module
 *
 * Description
 */
angular.module('InputMaskDate', [])
 .
directive("inputMaskDate", function ($filter, $timeout) {
 return {
  require: "ngModel",
  link: function (scope, element, attrs, ctrl) {


   scope.$emit('InputMaskDate');

   var _formatDate = function (date) {

    date = date.replace(/[^0-9]+/g, "");

    if (date.length > 2) {
     date = date.substring(0, 2) + "/" + date.substring(2, 6);
    }
    /*if(date.length > 5) {
    	date = date.substring(0,5) + "/" + date.substring(5,9);
    }*/
    return date;
   };

   $timeout(function () {

    console.log("DIRETIVA");

    console.log(ctrl.$viewValue.toString().length);

    if(ctrl.$viewValue.toString().length == 7) return;
    
    var viewValue = ctrl.$viewValue.toString().substring(3);
    console.log(viewValue);
    
    ctrl.$setViewValue(_formatDate(viewValue));
    ctrl.$render();

   });

   element.on("keyup", function () {
    ctrl.$setViewValue(_formatDate(ctrl.$viewValue));
    ctrl.$render();
   });

   element.on("change", function () {
    ctrl.$setViewValue(_formatDate(ctrl.$viewValue));
    ctrl.$render();
   });

   element.on("focus", function () {
    ctrl.$setViewValue(_formatDate(ctrl.$viewValue));
    ctrl.$render();
   });

   element.on("blur", function () {
    ctrl.$setViewValue(_formatDate(ctrl.$viewValue));
    ctrl.$render();
   });

   ctrl.$parsers.push(function (value) {
    if (value.length === 7) {
     var dateArray = value.split("/");
     //return new Date(dateArray[2], dateArray[1]-1, dateArray[0]).getTime();
     //return new Date(dateArray[2], dateArray[1]-1, dateArray[0]);
     return dateArray[1] + '-' + dateArray[0] + '-01';
    }
   });

   ctrl.$formatters.push(function (value) {
    return $filter("date")(value, "MM/yyyy");
   });

  }


 };
});
