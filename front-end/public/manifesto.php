<?php
	/* CONFIGURAÇÃO */
	$logo = '<img src="./images/crowd.png" />';
	$titulo = 'Manifesto - Comunidade Crowd';
	$descricao = '';
	$class_page = 'page-manifesto';
	$color_link = 'link-blue';
?>
<?php include 'header.php';?>
<section class="fullscreen m_bg1" data-speed="6">
	<div class="txt-empresa-1 txt-mobile830">
		<h1>Crowd</h1>
		<p>Transformando o jeito de trabalhar<br>o marketing e a comunicação</p>
	</div>
</section>
<section class="fullscreen m_bg2" data-speed="6">
	<div class="texto-manifesto">
		<p> Somos muitos. Somos em rede. Somos mais. Somos a ruptura. Somos o presente. Somos a Crowd.
		</p><p>
		A Crowd é mais que uma agência, <br>
		uma empresa, <br>
		um bureau, <br>
		uma produtora<br>
		um banco de profissionais<br>
		uma house ou <br>
		uma plataforma. <br>
		A Crowd é a nova forma de trabalhar a comunicação e o marketing<br>
	</p>
	<span class="line-green-small"></span>
	<p>Somos uma <span>comunidade de conhecimento</span> <br>
	e habilidades disponível para os que acreditam em <span>soluções inteligentes e criativas,</span> <br>construídas por profissionais altamente capacitados e disponíveis em qualquer lugar do Brasil<br>
e do mundo.  <br></p><p>
Somos uma comunidade única de pessoas e empresas que acreditam no <span>trabalho conectado</span><br> em rede e na possibilidade de expandir os limites profissionais para muito além de restrições<br> trabalhistas, geográficas ou temáticas.
</p><p>
Somos uma só Crowd, uma rede que vai além de cargos, um mix de conhecimentos, centrada <br>
na entrega, na <span>realização</span>, com <span>agilidade</span> e <span>valor agregado</span>.<br>
</p><p>
Nossa existência vai além dos profissionais e empresas conectadas nos escritórios, co-workings. e coffee shops. </p>
</p>
<p><br>
<em>Trabalhamos.</em><br>
Por um mercado de comunicação e marketing com mais valor e realizações
</p>
<span class="line-green"></span>
<blockquote>
Dos custos ao valor<br>
Dos escritórios ao mundo<br>
Da folha de pagamento ao arsenal ilimitado de conhecimento<br>
Do cartão de ponto às entregas<br>
Da tarefa às realizações<br>
</blockquote>
<span class="line-green"></span>
<p>
Por uma nova dinâmica de trabalho<br>
Mais ágil, mais conectada, mais inteligente, mais qualificada, mais profissional e com mais satisfação.<br>
</p>
<p class="padding40">
Por uma comunidade do mesmo lado da mesa com<br>
mais especialização, menos crachá<br>
mais tema, menos limitação <br>
menos geografia, mais tecnologia<br>
menos cargos, mais projetos de qualidade<br>
</p>
<p>
Por mais satisfação e felicidade de<br>
<span>Criar, planejar, escrever, produzir, desenhar, codificar, atender, desenvolver</span>|<br>
em qualquer lugar e qualquer momento.
</p>
<span class="line-green-small"></span>
<p class="padding40">
O jeito de trabalhar Crowd é pautado pelo profissionalismo de ponta a ponta. <br>
Uma entrega qualificada se faz por um pedido tal qual.
</p>
<p>
<strong class="txt-empatizamos"><span>EMPATIZAMOS</span></strong><br>
Porque acreditamos que podemos construir melhor quando nos colocamos no lugar do outro.<br>
</p>
<p class="padding20">
<span class="txt-flexibilidade">Flexibilidade</span><br>
é nossa formula da felicidade  a partir da combinação das melhores equipes com o projeto<br>
adequado, executado em qualquer lugar e momento.
<br><br>
<span>Confiança</span> é mais do que um valor, é nossa base de existência.
<br><br>Curtimos a equação <span>gerar mais valor</span> e menos custos.
</p>
<p>
Transformar o jeito de trabalhar comunicação e marketing é Crowd.
</p>
</div>
</section>
<?php include 'footer.php';?>