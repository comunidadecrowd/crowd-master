<?php
	/* CONFIGURAÇÃO */
	$logo = '<img src="./images/crowd.png" />';
	$titulo = 'Comunidade Crowd';
    $descricao = '';
	$class_page = 'page-manifesto';
?>
<?php include 'header.php';?>

			<video autoplay loop poster="./images/c_bg1.png" class="bg_video">
                <source src="images/bg.webm" type="video/webm">
                <source src="images/bg.mp4" type="video/mp4">
            </video>
            <section class="fullscreen c_bg1" data-speed="6">
                <div class="txt-crowd-1">
                    <h1 class="title_somos"><span>Somos muitos. <br>Somos em rede. Somos mais.</span></h1>
                    <p>A Crowd é mais que uma agência, uma empresa, um bureau, uma produtora ou<br> uma plataforma. Somos mais que um banco de profissionais ou uma house. Somos<br>uma comunidade de conhecimento e habilidades disponível para os que acreditam<br>em soluções inteligentes e criativas, construídas por profissionais altamente<br>capacitados e disponíveis em qualquer lugar do Brasil e do mundo.</p>
                    <a href="quero-expandir-minha-empresa.php" class="btncadastrar btn-roxo btn-mobile-full">Quero expandir minha empresa</a>
                    <a href="quero-ser-profissional.php" class="btncadastrar btncad btn-mobile-full ">Quero ser Profissional</a>
                </div>
            </section>
            <section class="fullscreen c_bg2">
                <h2 class="title-transformando">
                Transformando o jeito de trabalhar <br>
                Comunicação e Marketing.
                </h2>
                <div class="text2 text2_lp">
                    <p>Somos uma comunidade única de pessoas e empresas que acreditam no
                        <br> trabalho conectado em rede e na possibilidade de expandir os limites <br>
                        profissionais para muito além de restrições trabalhistas, geográficas ou temáticas.<p>
                            <p>
                                Somos uma só Crowd, uma rede que vai além do cargos, um mix de <br>
                                conhecimentos, centrada na entrega, na realização, com agilidade e valor<br>
                                Agregado.<br>
                            </p><p>
                            Nossa existência vai além dos profissionais e empresas conectadas nos <br>
                            escritórios, co-workings e coffee shops. Trabalhamos por um mercado de <br>
                            comunicação e marketing com mais valor e realizações:
                        </p>
                        <p>
                            Dos custos ao valor<br>
                            Dos escritórios ao mundo<br>
                            Da folha de pagamento ao arsenal ilimitado de conhecimento<br>
                            Do cartão de ponto às entregas<br>
                            Da tarefa às realizações<br>
                        </p>
                    </div>
                </section>
                <section class="fullscreen c_bg3">
                    <div class="text3 text3_lp">
                        <h2 class="titulo-recursos">
                        Mais que recursos:<br>soluções criativas
                        </h2>
                        <p>Designers e redatores que querem criar enquanto viajam pelo mundo. <br>
                            Programadores que trabalham nos horários em que produzem melhor. <br>
                            Jornalistas e especialistas de marketing que sabem que qualidade de vida é essencial para seu desempenho profissional.<br>
                        </p>
                        <p>
                            Líderes criativos, oferecendo mais que os recursos técnicos de suas
                        <br>especialidades, mas soluções projetadas que ajudam marcas a crescerem. </p>
                    </div>
                    <img src="./images/c_bg3.png" class="visible-mobile" width="100%" />
                </section>
                <section class="fullscreen c_bg4">


                    <div class="text4_lp">
                        <h2>Organização e praticidade.</h2>
                        <p>
                            Enviar briefings completos e receber propostas de forma mais prática.<br>
                            Detalhar seu projeto e escolher a melhor proposta para a sua demanda.<br>
                            Oferecemos mais comodidade e agilidade em contratações, simplificando<br>
                            processos e permitindo que profissionais e clientes se <br>
                            comuniquem com mais clareza e organização.<br><br>
                        </div>
                        <img src="./images/c_bg4_mobile.png" class="visible-mobile" width="100%" />

                    </section>
                    <section class="c_bg5">
                        <div class="linha-bg-tempo"></div>
                        <div class="txt_tempo colorwhite">
                            <h2>Tempo para o que realmente importa</h2>
                            <p>A Crowd faz a ponte entre clientes e profissionais para que as atenções<br>
                                sejam voltadas completamente para o projeto, desde o orçamento até a<br>
                                entrega.
                            </p>
                            <p>
                                Um fluxo de tarefas organizado em etapas de execução, integrando as<br>
                                responsabilidades de empresas e profissionais, garantindo qualidade de<br>
                                comunicação e agilizando entregas.
                            </p>


                        </div>
                    </section>
                    <section class="c_bg6">
                        <div class="txt_projeto_entregue">
                            <h2>Projeto Entregue? Nós cuidamos do resto.</h2>
                            <p>Nosso objetivo é oferecer a agências e criativos um sistema de <br>
                                contratação mais livre, menos burocrático e confiável.
                            </p>
                            <p>
                                Se você é cliente, isso significa realizar pagamentos referentes apenas<br>
                                às tarefas finalizadas.<br>
                                Se é profissional, quer dizer que recebe a cada quinzena os valores por<br>
                                trabalhos realizados.
                            </p>
                            <p>A Crowd organiza relatórios de gastos e faturamento para que você <br>
                                acompanhe o fluxo financeiro de cada projeto. Do resto, cuidamos nós.
                            </p>
                        </div>
                    </section>
                    <section class="fullscreen videos">
                        <div class="container">
                            <div class="col-md-12">
                                <h3>Vamos ser Parceiros?</h3>
                                <div class="single-video">
                                    <iframe width="100%" height="466" src="https://www.youtube.com/embed/h15kbmgFWVw" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div id="thumbnails-videos">
                                        <div class="bxslider">
                                            <div class="item">
                                                <a class="abrir-video" value="h15kbmgFWVw" rel="prettyPhoto">
                                                    <img src="https://i.ytimg.com/vi/h15kbmgFWVw/default.jpg" width="163" class="img-responsive" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-xs-12 padding-none-mobile">
                                        <div class="row txt-videos01">
                                            <div class="col-sm-12 col-xs-12">
                                                <div class="mais3000">
                                                    <em>3000+</em><br>
                                                    <p>Profissionais</p>
                                                </div>
                                                <div class="skills-profissionais">
                                                    Atendimento • Designers • Desenvolvedores • Fotógrafos •  Jornalistas • Marketing
                                                    <br>
                                                    Mídia • Planejamento • Produtores de Vídeo • Redatores • Tradutores • Eventos
                                                </div>
                                            </div>
                                        </div>
                                        <div class="txt-videos02">
                                            A Comunidade Crowd foi criada pela <a href="http://agenciacrowd.com.br" target="_blank">Agência Crowd</a>, que realiza 100% dos seus jobs por meio do<br />
                                            crowdsourcing. Durante 3 anos desenvolvemos processos e metodologias aplicados em uma plataforma até <br>
                                            então exclusiva, que agora disponibilizamos a empresas de comunicação e marketing dispostas a expandir
                                            <br>suas capacidades de criação e desenvolvimento.<br>
                                            Hoje somos mais de 3000 profissionais cadastrados. Faça parte da Comunidade Crowd!<br>
                                            <a href="quero-expandir-minha-empresa.php" class="btncadastrar btn-roxo btn-mobile-full ">Quero expandir minha empresa</a>
                                            <a href="quero-ser-profissional.php" class="btncadastrar btncad btn-mobile-full ">Quero ser Profissional</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

<?php include 'footer.php';?>
