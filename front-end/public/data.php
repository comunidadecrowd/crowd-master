<?php

  $data = array(
    array( 'nome' => 'Leandro', 'idade' => '30', 'cor' => 'Azul' ),
    array( 'nome' => 'Caio',    'idade' => '15', 'cor' => 'Preta' ),
    array( 'nome' => 'Alice',   'idade' => '90', 'cor' => 'Vermelha' ),
    array( 'nome' => 'Lucila',  'idade' => '50', 'cor' => 'Roxa' ),
    array( 'nome' => 'Nicolly', 'idade' => '4',  'cor' => 'Rosa' ),
  );

  echo json_encode($data);

  die;

?>
