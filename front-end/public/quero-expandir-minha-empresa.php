<?php
	/* CONFIGURAÇÃO */
	$logo = '<img src="./images/crowd.png" />';
	$titulo = 'Quero expandir minha empresa - Comunidade Crowd';
    $descricao = '';
	$class_page = 'page-manifesto';
?>
<?php include 'header.php';?>
			<section class="fullscreen e_bg1" data-speed="6">
                <div class="txt-empresa-1 txt-mobile830">
                    <h1>Como a Crowd pode ser<br>a extensão da minha empresa?</h1>
                    <p>Imagine ampliar as habilidades e possibilidades criativas da sua empresa sem precisar aumentar o time.<br>
                        Pelo contrário: podendo até reduzí-lo.<br>
                        Atualmente, o mundo conta com cada vez mais especialistas e segmentos. Acreditamos que é possível <br>
                        simplificar processos e conectar o maior número possível de profissionais qualificados às necessidades<br>
                        específicas de cada empresa, no momento em que você precisa.
                    </p>
                    <!--<p>
                        A Comunidade Crowd funciona como uma grande agência de comunicação, com mais de 3000 profissionais<br>
                        cadastrados, porém customizável de acordo com suas demandas e preferências: aqui, você seleciona o<br>
                        profissional ideal para o seu projeto, monta seu próprio time e acompanha o desenvolvimento de cada job,<br>
                        pagando apenas o valor referente ao seu uso na plataforma e às tarefas finalizadas no mês.
                    </p>-->
                    <button type="button" id="cad_empresa" class="btncad btn btn-white btn-inline btn-round">Cadastrar</button>
                </div>
            </section>
            <section class="fullscreen e_bg2">
                <div class="txt-empresa-2 txt-mobile830">
                    <h2>Transforme custos fixos em variáveis</h2>
                    <p>Nosso propósito é oferecer a agências, veículos e anunciantes um meio de<br>
                        expandir times e reduzir custos fixos enquanto seus projetos são desenvolvidos<br>
                        por profissionais motivados e especializados em sua área de atuação.
                    </p>
                    <p>
                        Com um simples “Buscar”, conectamos a sua empresa a mais de 3.000 profissonais das mais<br>
                        variadas áreas da economia criativa, que trabalham criando sua própria rotina<br>
                    produtiva, facilitando e agilizando a contratação.</p>
                </div>
            </section>
            <section class="fullscreen e_bg3">
                <div class="text3 text3_lp align-left txt-mobile830">
                    <h3 class="titulo-recursos">Encontre especialistas<br>de forma mais rápida</h3>
                    <p>
                        Monte seu briefing e selecione os profissionais com os quais você<br>
                        gostaria de orçar o projeto.<br><br>
                        A Crowd dispõe de uma plataforma com mais de 3000 profissionais<br>
                        cadastrados. São Designers, Redatores, Fotógrafos, Tradutores, uma<br>
                        rede de pessoas que desenvolvem projetos para marcas de todo país.<br>
                    </p>

                </div>
                <img src="./images/e_bg3_mobile.png" class="visible-mobile" width="100%" />
            </section>
            <section class="fullscreen e_bg4">
                <div class="txt-empresa-4 txt-mobile830">
                    <h2>Receba propostas em poucas horas</h2>
                    <p>
                        Assim que uma proposta é enviada, você é notificado por email e nós<br>
                        organizamos todos os orçamentos para cada projeto.<br>
                        Selecione o profissional mais adequado e a melhor proposta, sem vínculos<br>
                        trabalhistas e com muito mais agilidade.<br>
                        <img src="./images/e_bg4_mobile.png" class="visible-mobile" width="100%" />
                    </div>
                </section>
                <section class="fullscreen e_bg5">
                    <div class="faixa-bg-empresa-mobile"></div>
                    <div class="txt-empresa-5 txt-mobile830">
                        <h2>Profissional escolhido? Mãos à obra!</h2>
                        <p>Em cada projeto as tarefas são organizadas em etapas, garantindo comunicação <br>
                            e foco. Avalie orçamentos específicos do projeto e troque informações importantes. <br>
                            Quando o profissional marca a tarefa como finalizada, é sua vez de avaliar e aprovar.<br>
                        </p><p>
                        Uma vez aprovado o trabalho, faça uma avaliação. Suas experiências ajudam <br>
                        a Crowd e outras empresas a trabalhar sempre com os melhores profissionais.<br>
                    </p>
                    <img src="./images/e_bg5.png" class="visible-mobile" width="100%" />

                </div>
            </section>
                <div class="faixa-bg-empresa"></div>
            <section class="fullscreen e_bg7">
                <div class="txt_boleto">
                    <h2>Todos os jobs em um único boleto</h2>
                    <p>Mais qualidade, menos burocracia. Na Crowd você paga mensalmente <br>
                        o valor equivalente ao seu plano e à quantidade de tarefas aprovadas.<br>
                        Isso significa que os trabalhos só são finalizados e pagos quando tudo<br>
                        estiver do jeito que você precisa.
                    </p>
                    <p>
                        Nós organizamos relatórios para que você acompanhe o fluxo financeiro<br> de cada projeto e cuidamos do pagamento dos profissionais que<br> trabalharam com a sua empresa.
                    </p>
                </div>
            </section>
            <div class="container container-cadastro container-cadastro-empresa" ng-controller="publicCustomerController as Public">
                <form ng-submit="Public.register()" name="frmCustomer">
                    <div class="texto-form-empresa">
                        <h2>Faça parte da Crowd!</h2>
                        <p>Para saber mais, cadastre-se.</p>
                    </div>
                    <div class="col-sm-12 col-xs-12 formulario-empresa" id="frmCustomer">
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" placeholder="Razão social" class="form-control" ng-model="Public.customer.Name" ng-required="true">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="col-sm-4 col-xs-4 padding-none">
                                    <select class="form-control" ng-options="state as state.Name for state in Public.states track by state.Id" ng-model="Public.customer.State" ng-change="Public.loadCities()" ng-required="true">
                                        <option value="">UF</option>
                                    </select>
                                </div>
                                <div class="col-sm-8 col-xs-8 padding-none" style="margin-bottom: 10px;">
                                    <select class="form-control" ng-options="city as city.Name for city in Public.cities track by city.Id" ng-model="Public.customer.City" ng-required="true">
                                        <option value="">Cidade</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="col-sm-9 col-xs-9 padding-none">
                                    <input type="text" placeholder="Rua" class="form-control" ng-model="Public.customer.Address" ng-required="true">
                                </div>
                                <div class="col-sm-3 col-xs-3 padding-none">
                                    <input type="text" placeholder="Nº" class="form-control just-number" ng-model="Public.customer.Number" ng-required="true">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5">
                                <input type="text" placeholder="Complemento" class="form-control" ng-model="Public.customer.Complement">
                            </div>
                            <div class="col-sm-7">
                                <input type="text" placeholder="Telefone" class="form-control" ng-model="Public.customer.Phone" ui-br-phone-number ng-required="true">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="text" placeholder="Nome responsável" class="form-control" ng-model="Public.customer.NameResponsible" ng-required="true">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6" style="margin-bottom: 10px;">
                                <input type="email" placeholder="Email" class="form-control" ng-model="Public.customer.EmailResponsible" ng-required="true">
                            </div>
                            <div class="col-sm-6">
                                <input type="text" placeholder="Quantidade de pessoas" class="form-control just-number" ng-model="Public.customer.QtyPersons" ng-required="true">
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-12"><br>
                                <button type="submit" class="btn btn-rosa btn-block btn-round">cadastrar</button>
                                <br><br>
                            </div>
                        </div>
                        <div class="alert alert-danger" ng-show="Public.error">
                            <strong>Ooops...</strong> {{Public.errorMessage}}
                        </div>
                    </div>
                </form>
                <div class="col-sm-12">
                    <div class="alert alert-success" ng-show="Public.success">
                        Cadastro Realizado com sucesso. Em breve entraremos em contato.
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                $('.just-number').keyup(function(e)                                {
                  if (/\D/g.test(this.value))
                    this.value = this.value.replace(/\D/g, '');
                });
            </script>
<?php include 'footer.php';?>
