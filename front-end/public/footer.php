<footer>
    <div class="container-fluid">
        <div class="col-sm-2 col-xs-12">
            <img src="./images/crowd-rodape-logo.png" class="logo-footer" />
        </div>
        <div class="col-sm-8 col-xs-12 text-center">
            <ul class="links">
                <a href="quero-expandir-minha-empresa.php" >quero expandir minha empresa</a>
                <a href="quero-ser-profissional.php" >quero ser profissional</a>
                <a href="blog.php">Blog</a>
                <a href="faq.php">Faq</a>
                <a href="mailto:contato@comunidadecrowd.com.br">Contato</a>
            </ul>
        </div>
        <div class="col-sm-2 col-xs-12 text-right text-xs-center">
            <a href="https://www.facebook.com/comunidadecrowd/" target="_blank"><i class="icon fa-facebook-f" aria-hidden="true"></i></a>
            <a target="_blank" href="https://www.linkedin.com/company/9194576?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A9194576%2Cidx%3A1-1-1%2CtarId%3A1477495842076%2Ctas%3Acomunidade%20crowd" target="_blank"><i class="icon fa-linkedin-square" aria-hidden="true"></i></a>
        </div>
    </div>
</footer>                        
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script src="https://unpkg.com/masonry-layout@4.1/dist/masonry.pkgd.min.js"></script>
<script src="./js/jquery.lazyload.min.js"></script>
<script src="https://unpkg.com/imagesloaded@4.1/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" charset="utf-8">
      var $jQuery = $.noConflict();
    </script>
    <script type="text/javascript" charset="utf-8">
          $jQuery(document).ready(function ()
          {
              page('CBIQAA');
          });


          function page(page)
          {


              $jQuery.getJSON("https://www.googleapis.com/youtube/v3/search?order=date&part=snippet&maxResults=40&channelId=UCdIy3KWGHdZbade-pwrnKGQ&key=AIzaSyBW251w2F5DWTN7sxw8rMSDuuF7lityfmU&pageToken=", function (data)
              {

                  //PAGINACAO PROXIMO
                  if (data.nextPageToken)
                  {
                      $jQuery("#next").html("<a href='#' class='bx-next' onclick=\"page('" + data.nextPageToken + "')\"></a>");
                      $jQuery('#thumbnails').html("");
                  }
                  else
                  {
                      $jQuery('#thumbnails').html("");
                      $jQuery("#next").html("");
                  }
                  //PAGINACAO VOLTAR
                  if (data.prevPageToken)
                  {
                      $jQuery("#prev").html("<a href='#' class='bx-prev' onclick=\"page('" + data.prevPageToken + "')\"></a>");
                      $jQuery('#thumbnails').html("");
                  }
                  else
                  {
                      $jQuery('#thumbnails').html("");
                      $jQuery("#prev").html("");
                  }
                  $jQuery.each(data.items, function (i, item)
                  {
                      if (item.id.channelId)
                      {
                          //profile
                      }
                      else
                      {
                          //canal
                          if (item.id.videoId != 'h15kbmgFWVw')
                          {
                              $jQuery('.bxslider').append('<div class="item"><a class="abrir-video" value="' + item.id.videoId + '" rel="prettyPhoto"><img src=\'' + item.snippet.thumbnails.high.url + '\' width="163" class="img-responsive" /></a></div>');
                              
                              
                          }
                      }
                      $jQuery(".abrir-video").click(function ()
                      {
                          console.log('click');
                          var idVideo = $jQuery(this).attr('value');
                          var iFrame = '<iframe width="100%" height="466" src="https://www.youtube.com/embed/' + idVideo + '?autoplay=1" frameborder="0" allowfullscreen></iframe>';
                          $jQuery('.single-video').html(iFrame);
                      });


                  });
              });
          }
          $jQuery('.menu-mobile').click(function ()
          {
              $jQuery('.menu-xs-mobile').toggle("slow");
              $jQuery('.login-mobile').hide("slow");
          });
          $jQuery('.btn-logar').click(function ()
          {
              $jQuery('.login-mobile').toggle("slow");
              $jQuery('.menu-xs-mobile').hide("slow");
          });
          $jQuery('.btn-esqueceu-senha').click(function ()
          {
              $jQuery('.fazer-login').hide("slow");
              $jQuery('.btn-esqueceu-senha').hide("slow");
              $jQuery('.form-esqueceu-senha').show("slow");
              $jQuery('.voltar-senha').show("slow");
          });
          $jQuery('.voltar-senha').click(function ()
          {
              $jQuery('.fazer-login').show("slow");
              $jQuery('.btn-esqueceu-senha').show("slow");
              $jQuery('.form-esqueceu-senha').hide("slow");
              $jQuery('.voltar-senha').hide("slow");
          });
          $jQuery('#cad_empresa').click(function(){
                $jQuery('html, body').animate({scrollTop:$jQuery('.texto-form-empresa').position().top}, 'slow');
          })
          $jQuery('#cad_profissional').click(function(){
                $jQuery('html, body').animate({scrollTop:$jQuery('.facaparte').position().top}, 'slow');
          })
                       
    </script>
                   
    <script>
    $jQuery(function() {
      $jQuery("img.lazy").lazyload();
    });

    var pagina_atual = 1;  
    var isLoading = false;  
    $jQuery(window).load(function() {
        $jQuery(window).scroll(function() {
          if( !isLoading ){

            if($jQuery(window).scrollTop() == $jQuery(document).height() - $jQuery(window).height()) {
           
                        isLoading = true;
                        pagina_atual++;             
                        $jQuery.ajax({
                            type: "POST",
                            url: './load-more-post.php?pageid='+pagina_atual,
                            data: {
                                  action: 'load_posts',
                            },
                            cache: false,
                            beforeSend: function(){
                              $jQuery('.load-posts').show();
                            },
                            success: function (html) {
                              $jQuery('.load-posts').hide();
                                if (html) {
                                    var el = $jQuery(html);
                                    $jQuery(el).imagesLoaded( function() {
                                    $jQuery(".masonry").append(el).masonry( 'appended', el, true );
                                    });
                                isLoading = false;
                                }else{
                                  isLoading = true;
                                  $jQuery('.no-posts').show();
                                }
                            }
                        });    
                    }
                  }
        });
    });
    $jQuery(window).load(function() {
      $jQuery('.masonry').masonry({      
        columnWidth: '.item',
        itemSelector: '.item',
      });
    });

    $jQuery("img.lazy").load(function() {
      $jQuery('.masonry').masonry({      
        columnWidth: '.item',
        itemSelector: '.item',
      });
    });
    $jQuery('.segments-list').click(function(e) {
        e.stopPropagation();
    });
    $jQuery(window).load(function() {
       $jQuery('header').css('opacity','1');                           
    });
    $jQuery('.placeholder-habilidades').click(function(){ 
          var placeholder = $jQuery('input.input').hasClass('ng-not-empty');
          if (placeholder){
            $jQuery('.tags').addClass('noafter');
          }
          else{
            $jQuery('.tags').removeClass('noafter');
          }
    });
    $jQuery('.placeholder-habilidades').keypress(function(){ 
          var placeholder = $jQuery('input.input').hasClass('ng-not-empty');
          if (placeholder){
            $jQuery('.tags').addClass('noafter');
          }
          else{
            $jQuery('.tags').removeClass('noafter');
          }
    });
     $jQuery(window).load(function (){
        $jQuery('.bxslider').bxSlider({
            minSlides: 2,
            maxSlides: 5,
            slideWidth: 154,
            slideMargin: 0,
            pager: false,
            infiniteLoop: false,
            hideControlOnEnd: true

        });
      });
    </script>
  </body>
</html>
