(function(angular){
  angular.module('site').controller('ButtonCtrl', ['$scope', function($scope){
    $scope.laddaStyle='expand-left';
    $scope.title='Expand Left';
    $scope.items = [];
    $scope.change = function(){
      $scope.laddaStyle = "expand-right";
      $scope.title = "Expand Right";
    }
  }])

})(angular)
