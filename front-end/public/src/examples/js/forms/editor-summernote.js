(function(angular, document, window, $) {
  angular.module('site').controller('SummernoteCtrl', function() {

    window.edit = function() {
      $('.click2edit').summernote();
    };
    window.save = function() {
      $('.click2edit').destroy();
    };
  });
})(angular, document, window, jQuery);
