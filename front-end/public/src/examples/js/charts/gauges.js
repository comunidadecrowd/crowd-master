(function(angular, document, window, $) {
  angular.module('site').controller('GaugesCtrl', function() {

    // Example Gauge Dynamic
    // ---------------------
    setInterval(function() {
      var dynamicGauge = $("#exampleDynamicGauge").data('gauge');
      if (!dynamicGauge) return;

      var random = Math.round(Math.random() * 1000);

      var options = {
        strokeColor: Config.colors("primary", 500)
      };
      if (random > 700) {
        options.strokeColor = Config.colors("pink", 500);
      } else if (random < 300) {
        options.strokeColor = Config.colors("green", 500);
      }

      dynamicGauge.setOptions(options)
        .set(random);
    }, 1500);

    // Example Donut Dynamic
    // ---------------------
    setInterval(function() {
      var dynamicDonut = $("#exampleDynamicDonut").data('donut');
      if (!dynamicDonut) return;

      var random = Math.round(Math.random() * 1000);

      var options = {
        strokeColor: Config.colors("primary", 500)
      };
      if (random > 700) {
        options.strokeColor = Config.colors("pink", 500);
      } else if (random < 300) {
        options.strokeColor = Config.colors("green", 500);
      }

      dynamicDonut.setOptions(options)
        .set(random);
    }, 1500);
  });
})(angular, document, window, jQuery);
