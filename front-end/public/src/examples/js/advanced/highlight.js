(function(angular, document, window, $) {
  angular.module('site').controller('HighlightController', function() {
    hljs.initHighlighting();
  });
})(angular, document, window, jQuery);
