import $ from 'jQuery';
import Base from 'Base';
import {getDefaults, factory as pluginFactory} from 'Plugin';

const $BODY = $('body');
const $HTML = $('html');

export default class extends Base{
  processed(){
    if (typeof $.slidePanel === 'undefined') return;
    let sidebar = this;
    $(document).on('click', '[data-toggle="site-sidebar"]', function() {
      let $this = $(this);

      let direction = 'right';
      if ($('body').hasClass('site-menubar-flipped')) {
        direction = 'left';
      }

      let options = $.extend({}, getDefaults("slidePanel"), {
        direction: direction,
        skin: 'site-sidebar',
        dragTolerance: 80,
        template: function(options) {
          return '<div class="' + options.classes.base + ' ' + options.classes.base + '-' + options.direction + '">' +
            '<div class="' + options.classes.content + ' site-sidebar-content"></div>' +
              '<div class="slidePanel-handler"></div>' +
                '</div>';
        },
        afterLoad: function() {
          let self = this;
          this.$panel.find('.tab-pane').asScrollable({
            namespace: 'scrollable',
            contentSelector: "> div",
            containerSelector: "> div"
          });

          sidebar.renderPlugins(self.$panel);

          this.$panel.on('shown.bs.tab', function() {
            self.$panel.find(".tab-pane.active").asScrollable('update');
          });
        },
        beforeShow: function() {
          if (!$this.hasClass('active')) {
            $this.addClass('active');
          }
        },
        afterHide: function() {
          if ($this.hasClass('active')) {
            $this.removeClass('active');
          }
        }
      });

      if ($this.hasClass('active')) {
        $.slidePanel.hide();
      } else {
        let url = $this.data('url');
        if (!url) {
          url = $this.attr('href');
          url = url && url.replace(/.*(?=#[^\s]*$)/, '');
        }

        $.slidePanel.show({
          url: url
        }, options);
      }
    });

    $(document).on('click', '[data-toggle="show-chat"]', function() {
      $('#conversation').addClass('active');
    });


    $(document).on('click', '[data-toggle="close-chat"]', function() {
      $('#conversation').removeClass('active');
    });
  }
}
