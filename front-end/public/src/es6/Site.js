import $ from 'jQuery';
import Component from 'Component';
import Plugin from 'Plugin';
import Menubar from 'Menubar';
import GridMenu from 'GridMenu';
import Sidebar from 'Sidebar';
import PageAside from 'PageAside';


const DOC = document;
const $DOC = $(document);
const $BODY = $('body');

class Site extends Component{
  // willProcess() {
  //   this.registerPluginAPI();
  //   this.renderPlugins();
  // }
  processed(){
    this.polyfillIEWidth();
    this.initBootstrap();

    this.setupMenubar();
    this.setupGridMenu();
    this.setupFullScreen();
    this.setupMegaNavbar();
    
    // Dropdown menu setup
    // ===================
    this.$el.on('click', '.dropdown-menu-media', function(e) {
      e.stopPropagation();
    });
  }

  _getDefaultMeunbarType(){
    let breakpoint = this.getCurrentBreakpoint(),
        type = false;

    if ($BODY.data('autoMenubar') === false || $BODY.is('.site-menubar-keep')) {
      if ($BODY.hasClass('site-menubar-fold')) {
        type = 'fold';
      } else if ($BODY.hasClass('site-menubar-unfold')) {
        type = 'unfold';
      }
    }

    switch (breakpoint) {
     case 'lg':
      type = type || 'unfold';
      break;
     case 'md':
     case 'sm':
      type = type || 'fold';
      break;
     case 'xs':
      type = 'hide';
      break;
    }
    return type;
  }

  getDefaultState(){
    let menubarType = this._getDefaultMeunbarType();
    return{
      menubarType : menubarType,
      gridmenu : false
    }
  }

  getDefaultActions(){
    return {
      menubarType(type){
        console.log(type)
        let self = this,
          toggle = function($el) {
            $el.toggleClass('hided', !(type === 'open') );
            $el.toggleClass('unfolded', !(type === 'fold'));
          };

        $('[data-toggle="menubar"]').each(function() {
          let $this = $(this);
          let $hamburger = $(this).find('.hamburger');
          
          if ($hamburger.length > 0) {
            toggle($hamburger);
          } else {
            toggle($this);
          }
        });
      }
    }
  }

  getDefaultChildren(){
    let menubar = new Menubar({$el : $('.site-menubar')});
    let gridmenu = new GridMenu({$el : $('.site-gridmenu')});
    let sidebar = new Sidebar();
    let children = [menubar, gridmenu, sidebar];
    let $aside = $('.page-aside');
    if($aside.length > 0) {
      children.push( new PageAside({$el:$aside}) );
    }
    return children;
  }

  getCurrentBreakpoint(){
    let bp = Breakpoints.current();
    return bp ? bp.name : 'lg';
  }

  initBootstrap(){
    // Tooltip setup
    // =============
    $DOC.tooltip({
      selector: '[data-tooltip=true]',
      container: 'body'
    });

    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
  }

  polyfillIEWidth() {
    if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
      let msViewportStyle = DOC.createElement('style');
      msViewportStyle.appendChild(
        DOC.createTextNode(
          '@-ms-viewport{width:auto!important}'
        )
      );
      DOC.querySelector('head').appendChild(msViewportStyle);
    }
  }

  setupFullScreen(){
    // Fullscreen
    // ==========
    if (typeof screenfull !== 'undefined') {
      $DOC.on('click', '[data-toggle="fullscreen"]', () => {
        if (screenfull.enabled) {
          screenfull.toggle();
        }

        return false;
      });

      if (screenfull.enabled) {
        DOC.addEventListener(screenfull.raw.fullscreenchange, () => {
          $('[data-toggle="fullscreen"]').toggleClass('active', screenfull.isFullscreen);
        });
      }
    }
  }

  setupGridMenu(){
    let self = this;
    $DOC.on('click', '[data-toggle="gridmenu"]', function() {
      let $this = $(this),
          isOpened = self.getState('gridmenu');

      if (isOpened) {
        $this.addClass('active').attr('aria-expanded', true);
      } else {
        $this.removeClass('active').attr('aria-expanded', false);
      }

      self.setState('gridmenu', !isOpened);
    });
  }

  setupMegaNavbar(){
    // Mega navbar setup
    // =================
    $DOC.on('click', '.navbar-mega .dropdown-menu', (e) => {
      e.stopPropagation();
    }).on('show.bs.dropdown', (e) => {
      let $target = $(e.target);
      let $trigger = e.relatedTarget ? $(e.relatedTarget) : $target.children('[data-toggle="dropdown"]');
      let animation = $trigger.data('animation');

      if (animation) {
        let $menu = $target.children('.dropdown-menu');
        $menu
          .addClass('animation-' + animation)
          .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', () => {
            $menu.removeClass('animation-' + animation);
          });
      }
    }).on('shown.bs.dropdown', (e) => {
      let $menu = $(e.target).find('.dropdown-menu-media > .list-group');

      if ($menu.length > 0) {
        let api = $menu.data('asScrollable');
        if (api) {
          api.update();
        } else {
          $menu.asScrollable({
            namespace: "scrollable",
            contentSelector: "> [data-role='content']",
            containerSelector: "> [data-role='container']"
          });
        }
      }
    });
  }

  setupMenubar(){
    $(document).on('click', '[data-toggle="menubar"]', () => {
      let type = this.getState('menubarType');
      switch(type){
        case 'fold':
          type = 'unfold';
          break;
        case 'unfold':
          type = 'fold';
          break;
        case 'open':
          type = 'hide';
          break;
        case 'hide':
          type = 'open';
          break;
      } 

      this.setState('menubarType', type);
      return false;
    });

    Breakpoints.on('change', () => {
      this.setState('menubarType', this._getDefaultMeunbarType());
    });
  }
}


let instance = null;

function getInstance(){
  if(! instance){
    instance = new Site(); 
  }
  return instance;
}

function run(){
  let site = getInstance();
  site.run();
}

export default Site;
export {Site, run, getInstance};
