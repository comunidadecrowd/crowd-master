(function (angular) {
 var site = angular.module('site', ['ui.router', 'oc.lazyLoad', 'ngProgress']);

 site.config(['$stateProvider', '$ocLazyLoadProvider', function ($stateProvider, $ocLazyLoadProvider) {
   var modules = [];
   for (var key in requireConfig) {
    if (requireConfig[key].length > 0) {
     modules.push({
      name: key,
      files: requireConfig[key]
     })
    }
   }
   $ocLazyLoadProvider.config({
    modules: modules
   })
   $stateProvider.state('index', {
     url: "/",
     templateUrl: 'dashboard/v1.html',
     resolve: {
      deps: ["$ocLazyLoad", function ($ocLazyLoad) {
       return $ocLazyLoad.load({
        serie: true,
        files: requireConfig['dashboard/v1']
       });
        }]
     }
    })
    .state('pages', {
     url: "/{path:.+}",
     templateUrl: function ($stateParams) {
      return $stateParams.path + '.html'
     },
     resolve: {
      deps: ["$stateParams", "$ocLazyLoad", 'ngProgressFactory', function ($stateParams, $ocLazyLoad, ngProgressFactory) {
       var ngProgress = ngProgressFactory.createInstance();
       ngProgress.setColor(Config.colors('primary', '600'));
       var type = $stateParams.path;
       if (requireConfig[type].length > 0) {
        ngProgress.start();
        return $ocLazyLoad.load(requireConfig[type], {
          serie: true,
          cache: false
         })
         .then(function () {
          ngProgress.complete();
         });
       } else {
        return true;
       }
        }]
     }
    });
  }])
  .directive('plugin', function () {
   return {
    restrict: 'A',
    link: function (scope, element, attrs) {
     var name = attrs.plugin,
      plugin = Plugin.factory(attrs.plugin, $(element), $(element)
       .data()),
      api;
     if (plugin) {
      plugin.renderPlugin();
     } else if (api = Plugin.getPluginAPI(name)) {
      api(element);
     }
    }
   };
  })
  .controller('MainCtrl', ['$scope', '$http', "$location", function ($scope, $http, $location) {
   $scope.items = [];

   var handleMenuData = function (data) {
    var res = [],
     l = data.length;
    for (var i = 0; i < l; i++) {
     res.push({
      type: 'category',
      title: data[i].title
     });

     res = res.concat(handleItemData(data[i].items));
    }
    return res;
   };
   var handleItemData = function (items) {
    var res = [],
     l = items.length;

    for (var i = 0; i < l; i++) {
     var item = items[i];
     var itemData = {
      type: 'item',
      title: item.title,
      icon: item.icon
     };


     if (item.badge) {
      itemData.label = {
       type: "badge",
       text: item.badge.text,
       modifier: item.badge.modifier
      }
     } else if (item.label) {
      itemData.label = {
       type: "label",
       text: item.label.text,
       modifier: item.label.modifier
      };
     }

     if (item.children) itemData.children = item.children;

     res.push(itemData);
    }
    return res;
   };

   $http.get('../src/data/site_menu.json')
    .success(function (data) {
     $scope.items = handleMenuData(data);
     Site.run();
     setTimeout(function () {
      var path = $location.path()
       .substr(1);
      var $item = $('[href="#' + path + '"]')
      $item.trigger('click.site.menu');
      console.log('CLICK DO MENU');

      $item.parents('.has-sub')
       .trigger('open.site.menu');
     })


    });

   $scope.menu = ['hello', 'world'];

   $scope.setMenu = function () {
    $scope.menu = ['new'];
   }

  }])
})(angular);
