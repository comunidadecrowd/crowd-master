<?php
	/* CONFIGURAÇÃO */
	$logo = '<img src="./images/crowd-black.png" />';
	$titulo = 'Login - Crowd';
    $descricao = '';
	$class_page = 'page-manifesto';
?>
<?php include 'header.php';?>
<div class="container container-login container-cadastro" ng-app="Public" ng-controller="publicController as Public">
        <h2 style="margin-top:50px">Acesse Agora</h2>
        <h3>Somos muito. Somos em rede</h3>
		<form name="formLogin" class="login fazer-login" style="margin-top:30px;position: relative;" ng-submit="Public.login()">
    		<input ng-if="Public.hidePassword" type="text" placeholder="E-mail" ng-model="Public.user.Email">

            <input ng-if="!Public.hidePassword" type="password" placeholder="Senha"
    	        ng-model="Public.user.Password" ng-required="true" ng-focus="true">

            <button  type="submit" class="logar-sistema"
				        ng-click="Public.login()"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
            <div ng-show="Public.error" class="msg-erro-landing">
                <i class="fa fa-caret-up" aria-hidden="true"></i>
                {{Public.errorMessage}}
            </div>
        </form>

        <div class="row">
            <div class="col-md-12">
                <h3 style="font-weight: bold;margin:50px 0 10px 0;">Ainda não tem cadastro?</h3>
                <br>
                <a href="quero-expandir-minha-empresa.php" class="btncadastrar btn-roxo btn-mobile-full">Quero expandir minha empresa</a>
                <a href="quero-ser-profissional.php" class="btncadastrar btncad btn-mobile-full ">Quero ser Profissional</a>
            </div>
        </div>
	</div>
<?php include 'footer.php';?>
