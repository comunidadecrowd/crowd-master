(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['exports', 'jQuery', 'Plugin', 'Config'], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require('jQuery'), require('Plugin'), require('Config'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.jQuery, global.Plugin, global.Config);
    global.peityLine = mod.exports;
  }
})(this, function (exports, _jQuery, _Plugin2, _Config) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  var _jQuery2 = babelHelpers.interopRequireDefault(_jQuery);

  var _Plugin3 = babelHelpers.interopRequireDefault(_Plugin2);

  var NAME = 'peityLine';

  var PeityLine = function (_Plugin) {
    babelHelpers.inherits(PeityLine, _Plugin);

    function PeityLine() {
      babelHelpers.classCallCheck(this, PeityLine);
      return babelHelpers.possibleConstructorReturn(this, Object.getPrototypeOf(PeityLine).apply(this, arguments));
    }

    babelHelpers.createClass(PeityLine, [{
      key: 'getName',
      value: function getName() {
        return NAME;
      }
    }, {
      key: 'render',
      value: function render() {
        if (!_jQuery2.default.fn.peity) return;

        var $el = this.$el,
            options = this.options,
            defaults = _jQuery2.default.Plugin.getDefaults(NAME);

        if (options.skin) {
          if ((0, _Config.colors)(options.skin)) {
            var skinColors = (0, _Config.colors)(options.skin);
            defaults.fill = [skinColors[200]];
            defaults.stroke = skinColors[600];
          }
        }

        $el.peity('line', options);
      }
    }], [{
      key: 'getDefaults',
      value: function getDefaults() {
        return {
          delimiter: ",",
          fill: [(0, _Config.colors)("primary", 200)],
          height: 22,
          max: null,
          min: 0,
          stroke: (0, _Config.colors)("primary", 600),
          strokeWidth: 1,
          width: 44
        };
      }
    }]);
    return PeityLine;
  }(_Plugin3.default);

  (0, _Plugin2.register)(NAME, PeityLine);

  exports.default = PeityLine;
});
