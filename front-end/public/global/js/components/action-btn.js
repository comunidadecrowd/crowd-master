(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['exports', 'jQuery', 'Plugin'], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require('jQuery'), require('Plugin'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.jQuery, global.Plugin);
    global.actionBtn = mod.exports;
  }
})(this, function (exports, _jQuery, _Plugin2) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  var _jQuery2 = babelHelpers.interopRequireDefault(_jQuery);

  var _Plugin3 = babelHelpers.interopRequireDefault(_Plugin2);

  var NAME = 'actionBtn';

  var ActionBtn = function (_Plugin) {
    babelHelpers.inherits(ActionBtn, _Plugin);

    function ActionBtn() {
      babelHelpers.classCallCheck(this, ActionBtn);
      return babelHelpers.possibleConstructorReturn(this, Object.getPrototypeOf(ActionBtn).apply(this, arguments));
    }

    babelHelpers.createClass(ActionBtn, [{
      key: 'getName',
      value: function getName() {
        return NAME;
      }
    }, {
      key: 'render',
      value: function render() {
        if (!_jQuery2.default.fn.actionBtn) return;

        this.$el.actionBtn(this.options);
      }
    }]);
    return ActionBtn;
  }(_Plugin3.default);

  (0, _Plugin2.register)(NAME, ActionBtn);

  exports.default = ActionBtn;
});
