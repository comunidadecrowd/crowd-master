(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['exports', 'jQuery', 'Plugin'], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require('jQuery'), require('Plugin'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.jQuery, global.Plugin);
    global.toastrJs = mod.exports;
  }
})(this, function (exports, _jQuery, _Plugin2) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  var _jQuery2 = babelHelpers.interopRequireDefault(_jQuery);

  var _Plugin3 = babelHelpers.interopRequireDefault(_Plugin2);

  var NAME = 'toastr';

  var Toastr = function (_Plugin) {
    babelHelpers.inherits(Toastr, _Plugin);

    function Toastr() {
      babelHelpers.classCallCheck(this, Toastr);
      return babelHelpers.possibleConstructorReturn(this, Object.getPrototypeOf(Toastr).apply(this, arguments));
    }

    babelHelpers.createClass(Toastr, [{
      key: 'getName',
      value: function getName() {
        return NAME;
      }
    }, {
      key: 'render',
      value: function render() {
        this.$el.data('toastrWrapApi', this);
      }
    }, {
      key: 'show',
      value: function show(e) {
        if (typeof toastr === "undefined") return;

        e.preventDefault();

        var options = this.options;
        var message = options.message || '';
        var type = options.type || "info";
        var title = options.title || undefined;

        switch (type) {
          case "success":
            toastr.success(message, title, options);
            break;
          case "warning":
            toastr.warning(message, title, options);
            break;
          case "error":
            toastr.error(message, title, options);
            break;
          case "info":
            toastr.info(message, title, options);
            break;
          default:
            toastr.info(message, title, options);
        }
      }
    }], [{
      key: 'api',
      value: function api() {
        return 'click|show';
      }
    }]);
    return Toastr;
  }(_Plugin3.default);

  (0, _Plugin2.register)(NAME, Toastr);

  exports.default = Toastr;
});
