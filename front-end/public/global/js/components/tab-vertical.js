(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['exports', 'jQuery', 'Plugin'], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require('jQuery'), require('Plugin'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.jQuery, global.Plugin);
    global.tabVertical = mod.exports;
  }
})(this, function (exports, _jQuery, _Plugin2) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  var _jQuery2 = babelHelpers.interopRequireDefault(_jQuery);

  var _Plugin3 = babelHelpers.interopRequireDefault(_Plugin2);

  var NAME = 'verticalTab';

  var VerticalTab = function (_Plugin) {
    babelHelpers.inherits(VerticalTab, _Plugin);

    function VerticalTab() {
      babelHelpers.classCallCheck(this, VerticalTab);
      return babelHelpers.possibleConstructorReturn(this, Object.getPrototypeOf(VerticalTab).apply(this, arguments));
    }

    babelHelpers.createClass(VerticalTab, [{
      key: 'getName',
      value: function getName() {
        return NAME;
      }
    }, {
      key: 'render',
      value: function render() {
        if (!_jQuery2.default.fn.matchHeight) return;

        var $el = this.$el;

        $el.children().matchHeight();
      }
    }], [{
      key: 'getDefaults',
      value: function getDefaults() {
        return {};
      }
    }]);
    return VerticalTab;
  }(_Plugin3.default);

  (0, _Plugin2.register)(NAME, VerticalTab);

  exports.default = VerticalTab;
});
