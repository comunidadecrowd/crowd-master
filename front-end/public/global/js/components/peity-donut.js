(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['exports', 'jQuery', 'Plugin', 'Config'], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require('jQuery'), require('Plugin'), require('Config'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.jQuery, global.Plugin, global.Config);
    global.peityDonut = mod.exports;
  }
})(this, function (exports, _jQuery, _Plugin2, _Config) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  var _jQuery2 = babelHelpers.interopRequireDefault(_jQuery);

  var _Plugin3 = babelHelpers.interopRequireDefault(_Plugin2);

  var NAME = 'peityDonut';

  var PeityDonut = function (_Plugin) {
    babelHelpers.inherits(PeityDonut, _Plugin);

    function PeityDonut() {
      babelHelpers.classCallCheck(this, PeityDonut);
      return babelHelpers.possibleConstructorReturn(this, Object.getPrototypeOf(PeityDonut).apply(this, arguments));
    }

    babelHelpers.createClass(PeityDonut, [{
      key: 'getName',
      value: function getName() {
        return NAME;
      }
    }, {
      key: 'render',
      value: function render() {
        if (!_jQuery2.default.fn.peity) return;

        var $el = this.$el,
            options = this.options,
            defaults = _jQuery2.default.Plugin.getDefaults(NAME);

        if (options.skin) {
          if ((0, _Config.colors)(options.skin)) {
            var skinColors = (0, _Config.colors)(options.skin);
            defaults.fill = [skinColors[700], skinColors[400], skinColors[200]];
          }
        }

        $el.peity('donut', options);
      }
    }], [{
      key: 'getDefaults',
      value: function getDefaults() {
        return {
          delimiter: null,
          fill: [(0, _Config.colors)("primary", 700), (0, _Config.colors)("primary", 400), (0, _Config.colors)("primary", 200)],
          height: null,
          innerRadius: null,
          radius: 11,
          width: null
        };
      }
    }]);
    return PeityDonut;
  }(_Plugin3.default);

  (0, _Plugin2.register)(NAME, PeityDonut);

  exports.default = PeityDonut;
});
