(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['exports', 'jQuery', 'Plugin'], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require('jQuery'), require('Plugin'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.jQuery, global.Plugin);
    global.tabHorizontal = mod.exports;
  }
})(this, function (exports, _jQuery, _Plugin2) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  var _jQuery2 = babelHelpers.interopRequireDefault(_jQuery);

  var _Plugin3 = babelHelpers.interopRequireDefault(_Plugin2);

  var NAME = 'horizontalTab';

  var HorizontalTab = function (_Plugin) {
    babelHelpers.inherits(HorizontalTab, _Plugin);

    function HorizontalTab() {
      babelHelpers.classCallCheck(this, HorizontalTab);
      return babelHelpers.possibleConstructorReturn(this, Object.getPrototypeOf(HorizontalTab).apply(this, arguments));
    }

    babelHelpers.createClass(HorizontalTab, [{
      key: 'getName',
      value: function getName() {
        return NAME;
      }
    }, {
      key: 'render',
      value: function render() {
        if (!_jQuery2.default.fn.responsiveHorizontalTabs) return;

        (0, _jQuery2.default)('.nav-tabs-horizontal', context).responsiveHorizontalTabs();
      }
    }], [{
      key: 'getDefaults',
      value: function getDefaults() {
        return {};
      }
    }]);
    return HorizontalTab;
  }(_Plugin3.default);

  (0, _Plugin2.register)(NAME, HorizontalTab);

  exports.default = HorizontalTab;
});
