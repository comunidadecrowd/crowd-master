(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['exports', 'jQuery', 'Plugin'], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require('jQuery'), require('Plugin'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.jQuery, global.Plugin);
    global.moreButton = mod.exports;
  }
})(this, function (exports, _jQuery, _Plugin2) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  var _jQuery2 = babelHelpers.interopRequireDefault(_jQuery);

  var _Plugin3 = babelHelpers.interopRequireDefault(_Plugin2);

  var NAME = 'moreButton';

  var MoreButton = function (_Plugin) {
    babelHelpers.inherits(MoreButton, _Plugin);

    function MoreButton() {
      babelHelpers.classCallCheck(this, MoreButton);
      return babelHelpers.possibleConstructorReturn(this, Object.getPrototypeOf(MoreButton).apply(this, arguments));
    }

    babelHelpers.createClass(MoreButton, [{
      key: 'getName',
      value: function getName() {
        return NAME;
      }
    }, {
      key: 'render',
      value: function render() {
        this.$target = (0, _jQuery2.default)(this.options.more);
        this.$el.data('moreButtonApi', this);
      }
    }, {
      key: 'toggle',
      value: function toggle() {
        this.$target.toggle();
      }
    }], [{
      key: 'getDefaults',
      value: function getDefaults() {
        return {
          more: ''
        };
      }
    }, {
      key: 'api',
      value: function api() {
        return 'click|toggle';
      }
    }]);
    return MoreButton;
  }(_Plugin3.default);

  (0, _Plugin2.register)(NAME, MoreButton);

  exports.default = MoreButton;
});
