(function(global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['exports', 'jQuery', 'Component', 'Plugin', 'Menubar', 'GridMenu', 'Sidebar', 'PageAside'], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require('jQuery'), require('Component'), require('Plugin'), require('Menubar'), require('GridMenu'), require('Sidebar'), require('PageAside'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.jQuery, global.Component, global.Plugin, global.Menubar, global.GridMenu, global.Sidebar, global.PageAside);
    global.Site = mod.exports;
  }
})(this, function(exports, _jQuery, _Component2, _Plugin, _Menubar, _GridMenu, _Sidebar, _PageAside) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.getInstance = exports.run = exports.Site = undefined;

  var _jQuery2 = babelHelpers.interopRequireDefault(_jQuery);

  var _Component3 = babelHelpers.interopRequireDefault(_Component2);

  var _Plugin2 = babelHelpers.interopRequireDefault(_Plugin);

  var _Menubar2 = babelHelpers.interopRequireDefault(_Menubar);

  var _GridMenu2 = babelHelpers.interopRequireDefault(_GridMenu);

  var _Sidebar2 = babelHelpers.interopRequireDefault(_Sidebar);

  var _PageAside2 = babelHelpers.interopRequireDefault(_PageAside);

  var DOC = document;
  var $DOC = (0, _jQuery2.default)(document);
  var $BODY = (0, _jQuery2.default)('body');

  var Site = function(_Component) {
    babelHelpers.inherits(Site, _Component);

    function Site() {
      babelHelpers.classCallCheck(this, Site);
      return babelHelpers.possibleConstructorReturn(this, Object.getPrototypeOf(Site).apply(this, arguments));
    }

    babelHelpers.createClass(Site, [{
      key: 'processed',
      value: function processed() {
        this.polyfillIEWidth();
        this.initBootstrap();

        this.setupMenubar();
        this.setupGridMenu();
        this.setupFullScreen();
        this.setupMegaNavbar();

        // Dropdown menu setup
        // ===================
        this.$el.on('click', '.dropdown-menu-media', function(e) {
          e.stopPropagation();
        });
      }
    }, {
      key: '_getDefaultMeunbarType',
      value: function _getDefaultMeunbarType() {
        var breakpoint = this.getCurrentBreakpoint(),
          type = false;

        if ($BODY.data('autoMenubar') === false || $BODY.is('.site-menubar-keep')) {
          if ($BODY.hasClass('site-menubar-fold')) {
            type = 'fold';
          } else if ($BODY.hasClass('site-menubar-unfold')) {
            type = 'unfold';
          }
        }

        switch (breakpoint) {
          case 'lg':
            type = type || 'unfold';
            break;
          case 'md':
          case 'sm':
            type = type || 'fold';
            break;
          case 'xs':
            type = 'hide';
            break;
        }
        return type;
      }
    }, {
      key: 'getDefaultState',
      value: function getDefaultState() {
        var menubarType = this._getDefaultMeunbarType();
        return {
          menubarType: menubarType,
          gridmenu: false
        };
      }
    }, {
      key: 'getDefaultActions',
      value: function getDefaultActions() {
        return {
          menubarType: function menubarType(type) {
            console.log(type);
            var self = this,
              toggle = function toggle($el) {
                $el.toggleClass('hided', !(type === 'open'));
                $el.toggleClass('unfolded', !(type === 'fold'));
              };

            (0, _jQuery2.default)('[data-toggle="menubar"]').each(function() {
              var $this = (0, _jQuery2.default)(this);
              var $hamburger = (0, _jQuery2.default)(this).find('.hamburger');

              if ($hamburger.length > 0) {
                toggle($hamburger);
              } else {
                toggle($this);
              }
            });
          }
        };
      }
    }, {
      key: 'getDefaultChildren',
      value: function getDefaultChildren() {
        var menubar = new _Menubar2.default({
          $el: (0, _jQuery2.default)('.site-menubar')
        });
        var gridmenu = new _GridMenu2.default({
          $el: (0, _jQuery2.default)('.site-gridmenu')
        });
        var sidebar = new _Sidebar2.default();
        var children = [menubar, gridmenu, sidebar];
        var $aside = (0, _jQuery2.default)('.page-aside');
        if ($aside.length > 0) {
          children.push(new _PageAside2.default({
            $el: $aside
          }));
        }
        return children;
      }
    }, {
      key: 'getCurrentBreakpoint',
      value: function getCurrentBreakpoint() {
        var bp = Breakpoints.current();
        return bp ? bp.name : 'lg';
      }
    }, {
      key: 'initBootstrap',
      value: function initBootstrap() {
        // Tooltip setup
        // =============
        $DOC.tooltip({
          selector: '[data-tooltip=true]',
          container: 'body'
        });

        (0, _jQuery2.default)('[data-toggle="tooltip"]').tooltip();
        (0, _jQuery2.default)('[data-toggle="popover"]').popover();
      }
    }, {
      key: 'polyfillIEWidth',
      value: function polyfillIEWidth() {
        if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
          var msViewportStyle = DOC.createElement('style');
          msViewportStyle.appendChild(DOC.createTextNode('@-ms-viewport{width:auto!important}'));
          DOC.querySelector('head').appendChild(msViewportStyle);
        }
      }
    }, {
      key: 'setupFullScreen',
      value: function setupFullScreen() {
        // Fullscreen
        // ==========
        if (typeof screenfull !== 'undefined') {
          $DOC.on('click', '[data-toggle="fullscreen"]', function() {
            if (screenfull.enabled) {
              screenfull.toggle();
            }

            return false;
          });

          if (screenfull.enabled) {
            DOC.addEventListener(screenfull.raw.fullscreenchange, function() {
              (0, _jQuery2.default)('[data-toggle="fullscreen"]').toggleClass('active', screenfull.isFullscreen);
            });
          }
        }
      }
    }, {
      key: 'setupGridMenu',
      value: function setupGridMenu() {
        var self = this;
        $DOC.on('click', '[data-toggle="gridmenu"]', function() {
          var $this = (0, _jQuery2.default)(this),
            isOpened = self.getState('gridmenu');

          if (isOpened) {
            $this.addClass('active').attr('aria-expanded', true);
          } else {
            $this.removeClass('active').attr('aria-expanded', false);
          }

          self.setState('gridmenu', !isOpened);
        });
      }
    }, {
      key: 'setupMegaNavbar',
      value: function setupMegaNavbar() {
        // Mega navbar setup
        // =================
        $DOC.on('click', '.navbar-mega .dropdown-menu', function(e) {
          e.stopPropagation();
        }).on('show.bs.dropdown', function(e) {
          var $target = (0, _jQuery2.default)(e.target);
          var $trigger = e.relatedTarget ? (0, _jQuery2.default)(e.relatedTarget) : $target.children('[data-toggle="dropdown"]');
          var animation = $trigger.data('animation');

          if (animation) {
            (function() {
              var $menu = $target.children('.dropdown-menu');
              $menu.addClass('animation-' + animation).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                $menu.removeClass('animation-' + animation);
              });
            })();
          }
        }).on('shown.bs.dropdown', function(e) {
          var $menu = (0, _jQuery2.default)(e.target).find('.dropdown-menu-media > .list-group');

          if ($menu.length > 0) {
            var api = $menu.data('asScrollable');
            if (api) {
              api.update();
            } else {
              $menu.asScrollable({
                namespace: "scrollable",
                contentSelector: "> [data-role='content']",
                containerSelector: "> [data-role='container']"
              });
            }
          }
        });
      }
    }, {
      key: 'setupMenubar',
      value: function setupMenubar() {
        var _this2 = this;

        (0, _jQuery2.default)(document).on('click', '[data-toggle="menubar"]', function() {
          var type = _this2.getState('menubarType');
          switch (type) {
            case 'fold':
              type = 'unfold';
              break;
            case 'unfold':
              type = 'fold';
              break;
            case 'open':
              type = 'hide';
              break;
            case 'hide':
              type = 'open';
              break;
          }

          _this2.setState('menubarType', type);
          return false;
        });

        Breakpoints.on('change', function() {
          _this2.setState('menubarType', _this2._getDefaultMeunbarType());
        });
      }
    }]);
    return Site;
  }(_Component3.default);

  var instance = null;

  function getInstance() {
    if (!instance) {
      instance = new Site();
    }
    return instance;
  }

  function run() {
    var site = getInstance();
    site.run();
  }

  exports.default = Site;
  exports.Site = Site;
  exports.run = run;
  exports.getInstance = getInstance;
});
