(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "jQuery"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("jQuery"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.jQuery);
    global.Config = mod.exports;
  }
})(this, function (exports, _jQuery) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.colors = exports.set = exports.get = undefined;

  var _jQuery2 = babelHelpers.interopRequireDefault(_jQuery);

  var configs = {
    fontFamily: "Noto Sans, sans-serif",
    primaryColor: "blue",
    assets: "../assets"
  };

  function get() {
    var data = configs;
    var callback = function callback(data, name) {
      return data[name];
    };

    for (var _len = arguments.length, names = Array(_len), _key = 0; _key < _len; _key++) {
      names[_key] = arguments[_key];
    }

    for (var i = 0; i < names.length; i++) {
      var name = names[i];

      data = callback(data, name);
    }

    return data;
  }

  function set(name, value) {
    if (typeof name === 'string' && typeof value !== 'undefined') {
      configs[name] = value;
    } else if (name) {
      configs = _jQuery2.default.extend(true, {}, configs, name);
    }
  }

  function colors(name, level) {
    if (name === 'primary') {
      name = get('primaryColor');
      if (!name) {
        name = 'red';
      }
    }

    if (typeof configs.colors === 'undefined') {
      return null;
    }

    if (typeof configs.colors[name] !== 'undefined') {
      if (level && typeof configs.colors[name][level] !== 'undefined') {
        return configs.colors[name][level];
      }

      if (typeof level === 'undefined') {
        return configs.colors[name];
      }
    }

    return null;
  }

  exports.get = get;
  exports.set = set;
  exports.colors = colors;
});
