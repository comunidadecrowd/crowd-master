/*!
 * Remark (http://getbootstrapadmin.com/remark)
 * Copyright 2016 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(window, document, $) {
  'use strict';

  var pluginName = 'editlist';

  var Plugin = $[pluginName] = function(element, options) {
    this.element = element;
    this.$element = $(element);
    this.$content = this.$element.find('.list-content');
    this.$text = this.$element.find('.list-text');
    this.$editable = this.$element.find('.list-editable');
    this.$editBtn = this.$element.find('[data-toggle=list-editable]');
    this.$delBtn = this.$element.find('[data-toggle=list-delete]');
    this.$closeBtn = this.$element.find('[data-toggle=list-editable-close]');
    this.$input = this.$element.find('input');
    this.options = $.extend({}, Plugin.defaults, options, this.$element.data());
    this.init();
  };
  Plugin.defaults = {};

  Plugin.prototype = {
    constructor: Plugin,
    init: function() {
      this.bind();
    },
    bind: function() {
      var self = this;
      this.$editBtn.on('click', function() {
        self.enable();
      });

      this.$closeBtn.on('click', function() {
        self.disable();
      });

      this.$delBtn.on('click', function() {
        if (typeof bootbox === 'undefined') return;
        bootbox.dialog({
          message: "Do you want to delete the contact?",
          buttons: {
            success: {
              label: "Delete",
              className: "btn-danger",
              callback: function() {
                // $(e.target).closest('.list-group-item').remove();
              }
            }
          }
        });
      });
      this.$input.on('keydown', function(e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);

        if (keycode == 13 || keycode == 27) {
          if (keycode == 13) {
            self.$text.html(self.$input.val());
          } else {
            self.$input.val(self.$text.text());
          }

          self.disable();
        }
      });
    },

    enable: function() {
      this.$content.hide();
      this.$editable.show();
      this.$input.focus().select();
    },
    disable: function() {
      this.$content.show();
      this.$editable.hide();
    }
  };



  $.fn[pluginName] = function(options) {
    if (typeof options === 'string') {
      var method = options;
      var method_arguments = Array.prototype.slice.call(arguments, 1);

      if (/^\_/.test(method)) {
        return false;
      } else if ((/^(get)$/.test(method))) {
        var api = this.first().data(pluginName);
        if (api && typeof api[method] === 'function') {
          return api[method].apply(api, method_arguments);
        }
      } else {
        return this.each(function() {
          var api = $.data(this, pluginName);
          if (api && typeof api[method] === 'function') {
            api[method].apply(api, method_arguments);
          }
        });
      }
    } else {
      return this.each(function() {
        if (!$.data(this, pluginName)) {
          $.data(this, pluginName, new Plugin(this, options));
        }
      });
    }
  };
})(window, document, jQuery);
