<?php
	/* CONFIGURAÇÃO */
	$logo = '<img src="./images/crowd.png" />';
	$titulo = 'Quero ser profissional - Comunidade Crowd';
    $descricao = '';
	$class_page = 'page-manifesto';
?>
<?php include 'header.php';?>
<section class="fullscreen bg1" data-speed="30">
                <div class="text-profissional1">
                    <h1 class="title1">Por que ser um profissional da Crowd?</h1>
                    <a href="#" class="down-page"><i class="fa fa-sort-desc" aria-hidden="true"></i></a>
                    <p><button type="button" id="cad_profissional" class="btncad btn btn-white btn-round  hidden-md-down">cadastrar</button></p>
                </div>
            </section>
            <section class="fullscreen bg2">
                <div class="text2">
                    <p>
                        Ser freelancer hoje é mais que uma alternativa de trabalho: <br>
                        se tornou um estilo de vida.
                    </p>
                    <p>
                        A Crowd é uma comunidade que reúne profissionais e empresas <br>
                        que acreditam em uma equação de trabalho mais equilibrada e feliz,<br>
                        com mais qualidade, mais demandas, mais agilidade e menos<br>
                        pressão, mais qualidade e prazos justos.
                    </p>
                    <p>
                        Por isso, a Agência Crowd criou a Comunidade Crowd. Há 3 anos<br>
                        realizamos 100% de nossos jobs por meio do crowdsourcing,<br>
                        desenvolvendo projetos com profissionais que trabalham nos<br>
                        lugares e horários que preferem.
                    </p>
                </div>
            </section>
            <section class="fullscreen bg3">
                <div class="text3">
                    <h2>Trabalhe de qualquer lugar,<br>em qualquer horário</h2>
                    <p>Criar enquanto viaja pelo mundo. Trabalhar nos horários em que produz melhor. Menos horas no trânsito.<br>
                    Acreditamos que liberdade e qualidade de vida são essenciais para o bom desempenho de um profissional. Se você pensa como a gente, faça parte da Comunidade Crowd.</p>
                </section>
                <section class="fullscreen bg4">

                    <div class="bg-portfolio">
                        <div class="text4">
                            <h2>Crie seu Perfil</h2>
                            <p>A Crowd oferece uma plataforma completa de publicação de suas experiências,<br>
                                portfolio, habilidades e outras informações importantes sobre você. <br>
                                Quanto mais completo seu perfil, maior sua chance de ser selecionado <br>
                                por grandes clientes.
                            </p>
                            <p>
                                Em breve:<br><br>
                                • Um vídeo no seu perfil: fale mais sobre você e seu trabalho.<br>
                                • Monte um portfolio com imagens dos projetos que você desenvolveu ou participou.<br>
                                • Trabalha em equipe? Você também pode indicar e convidar colegas. Monte seu time.
                            </p>
                        </div>
                        <img class="visible-mobile" src="./images/bg4mobile.png" width="100%" />
                    </div>
                </section>
                <section class="fullscreen bg5">
                    <div class="bg-briefing">
                        <div class="text5">
                            <h2>Briefings claros, projetos relevantes</h2>
                            <p>Na Crowd, as empresas escrevem os briefings e selecionam até 10 profissionais com<br>
                                quem querem orçar o projeto. Quando você é escolhido para enviar uma proposta, nós <br>
                                avisamos por email e organizamos todos os briefings em um só lugar na plataforma.<br><br>
                                Quer saber quais empresas estão procurando profissionais? <br>
                            Em breve disponibilizaremos a seção Jobs Públicos.</p>

                        </div>
                        <img class="visible-mobile" src="./images/bg5.png" width="100%" />
                    </div>
                </section>
                <section class="fullscreen bg8">
                    <div class="bg-briefing">
                        <div class="text5">
                            <h2>Foi selecionado? Mãos à obra!</h2>
                            <p>Quando um projeto é iniciado, suas tarefas são organizadas em etapas, garantindo<br> comunicação e foco. Tire dúvidas, envie orçamentos específicos do projeto e anexe<br>
                                arquivos. Terminou a tarefa? Marque como completa e envie para a aprovação.<br><br>
                                Uma vez aprovado o trabalho, avalie a empresa. Suas experiências ajudam a <br>
                                Crowd e outros profissionais a trabalhar sempre com os melhores clientes.
                            </p>
                        </div>
                    </div>
                </section>
                <section class="fullscreen bg6">
                    <div class="text3 colorwhite">
                        <h2>Pagamento no dia certo.</h2>
                        <p>Entregou o trabalho? Todas as empresas da Crowd seguem a mesma regra de pagamentos quinzenais, sempre referentes a tarefas aprovadas.
                        </p>
                        <p>
                        Aqui, você recebe no dia 15, pelos trabalhos aprovados entre os dias 16 e 31 (do mês anterior), e 30, pelos jobs aceitos entre os dias 1 e 15. Trabalhamos com total seriedade e nunca atrasamos os pagamentos.</p>
                    </div>
                </section>
                <section class="facaparte bg7" data-speed="100">
                    <h3>Gostou? Faça parte da Crowd!</h3>
                    <p><span>Nossa plataforma está aberta para uso sem custo algum para profissionais até o fim desse ano.<br>
                        A partir de 2017, o valor equivalente a 8% dos jobs fechados será recolhido.<br>
                    Aproveite e embarque em uma forma mais inteligente de trabalhar!</span></p>
                    <br><br><br>
                    <p>Os criativos cadastrados na plataforma são profissionais regulamentados, que emitem nota para a Crowd. Ainda não tem CNPJ? Recomendamos a formalização como MEI. <a target="_blank" href="blog.php?post=MEI%20traz%20vantagens%20para%20freelancers">Saiba mais.</a></p>
                </section>
                <!--<section class="clientes-crowd" data-speed="100">
                    <p>Quem já está na Crowd:</p>
                    <img src="./images/cl1.jpg" />
                    <img src="./images/cl2.jpg" />
                    <img src="./images/cl3.jpg" />
                    <img src="./images/cl4.jpg" />
                    <img src="./images/cl5.jpg" />
                </section>
                -->
                <div class="container container-cadastro" ng-controller="publicProfessionalController as Public">
                    <form ng-submit="Public.register()" name="frmProfessional">
                        <div class="col-sm-12 col-xs-12" id="frmProfessional">
                            <div class="row">
                                <div class="col-sm-5 col-xs-12">
                                    <div class="cadastrar-foto-perfil">
                                        <div class="placeholder-foto radius100" ng-click="Public.openDialog()" style="cursor: pointer;"><span>Adicionar Foto</span><img id="imagem-perfil" width="100%" height="100%" alt="Imagem Perfil" ng-show="Public.imagemPerfil" /></div>

                                        <label>
                                           <input ng-model="Public.professional.Name" type="text" class="input-name-profissional form-control" placeholder="Seu nome |" ng-required="true" /><i class="fa fa-pencil" aria-hidden="true"></i>
                                        </label>
                                        <label>
                                            <input ng-model="Public.professional.Title" type="text" class="input-cargo-profissional form-control" placeholder="Cargo, ex: Designer |" ng-required="true" />

                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-xs-12 visible-xs">
                                    <div class="require inline">*</div> Campos Obrigatorios
                                </div>
                                <div class="col-sm-3 col-xs-12 padding-none inputTop">
                                    <input ng-model="Public.professional.Email" type="email" placeholder="Email" class="form-control brr-none" ng-required="true">
                                </div>
                                <div class="col-sm-2 col-xs-6 padding-none inputTop">
                                    <input ng-model="Public.professional.Password" type="password" placeholder="Senha" class="form-control brr-none brl-none brn bln" ng-required="true">
                                </div>
                                <div class="col-sm-2 col-xs-6 padding-none inputTop">
                                    <input ng-model="Public.professional.ConfirmPassword" type="password" placeholder="Confirmar" class="form-control brl-none" ng-required="true">
                                    <div class="require right">*</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <h3>Sobre você</h3>
                                    <textarea class="form-control" rows="4" ng-model="Public.professional.Description"  placeholder="Escreva aqui um breve descritivo sobre você. Essa informação será vista por clientes buscando profissionais e  por toda a nossa rede."></textarea>

                                    <h3>
                                        Atividades e Habilidades:<div class="require right">* (máx 10)</div>
                                    </h3>
                                    <div class="placeholder-habilidades">
                                        <tags-input
                                            ng-model="Public.professional.Skills"
                                            class="input-tags"
                                            on-tag-adding="Public.tagsLimit()"
                                            replace-spaces-with-dashes="false"
                                            placeholder="."
                                            add-on-space="true"
                                            add-from-autocomplete-only="true">
                                                <auto-complete
                                                    source="Public.loadSkills($query)"
                                                    min-length="2"
                                                    debounce-delay="0"
                                                    max-results-to-show="10000"
                                                    select-first-match="false">
                                                </auto-complete>
                                        </tags-input>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12 col-right">
                                    <div class="row">
                                        <div class="col-sm-3 col-xs-8 padding-none">
                                            <div class="require left">*</div>
                                            <select class="form-control" ng-options="country as country.nome_pais for country in Public.countries track by country.id" ng-model="Public.professional.Country" ng-required="true">
                                                <option value="">Pais</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2 col-xs-4 padding-none">
                                            <select class="form-control" ng-options="state as state.Name for state in Public.states track by state.Id" ng-model="Public.professional.State" ng-change="Public.loadCities()" ng-required="true">
                                                <option value="">UF</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-7 col-xs-12 padding-none">
                                            <select class="form-control" ng-options="city as city.Name for city in Public.cities track by city.Id" ng-model="Public.professional.City" ng-required="true">
                                                <option value="">Cidade</option>
                                            </select>
                                            <div class="require right">*</div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-5 col-xs-6 padding-none">
                                            <div class="require left">*</div>
                                            <select class="form-control" ng-options="category as category.Name for category in Public.categories track by category.Id" ng-model="Public.professional.Category" ng-required="true">
                                                <option value="">Categoria</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-7 col-xs-12 padding-none">

                                            <!--COMBO MULTIPLA ESCOLHA
                                            <ul class="combo-segmento"><span>Segmento</span>
                                                <ul>
                                                    <li ng-repeat="segment in Public.segments track by segment.Id">
                                                        <label >
                                                            <input ng-click="Public.addSegment(segment.Id)" type="checkbox" id='{{"checkbox-"+segment.Id}}' class="checkbox-segment" />
                                                            <span> {{segment.Name}}</span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </ul>-->

                                            <ul class="combo-segmento dropdown-toggle" data-toggle="dropdown">
                                                <span>{{Public.segmentLabel}}</span>
                                            </ul>
                                            <ul class="segments-list dropdown-menu">
                                              <li ng-repeat="segment in Public.segments track by segment.Id">
                                                <label>
                                                  <input ng-click="Public.addSegment(segment)" type="checkbox" id='{{"checkbox-"+segment.Id}}' class="checkbox-segment" />
                                                  <i></i>
                                                  <span> {{segment.Name}}</span>
                                                </label>
                                              </li>
                                            </ul>

                                            <div class="require right">* (máx 3)</div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-5 padding-none">
                                            <div class="input-group">
                                                <div class="require left">*</div>
                                                <span class="input-group-addon">Valor/h</span>
                                                <input ng-model="Public.professional.Price" type="text" class="form-control" placeholder="" ui-number-mask="2" ng-required="true">
                                            </div>
                                        </div>
                                        <div class="col-sm-7 padding-none-right">

                                            <select class="form-control" ng-options="availability as availability.Name for availability in Public.availabilitys track by availability.Id" ng-model="Public.professional.Availability" ng-required="true">
                                                <option value="">Disponibilidade</option>
                                            </select>

                                            <div class="require right">*</div>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-7 padding-none">
                                            <div class="input-group">
                                                <div class="require left">*</div>
                                                <span class="input-group-addon">Skype</span>
                                                <input ng-model="Public.professional.Skype" type="text" class="form-control" placeholder="" ng-required="true">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-7 padding-none">
                                            <div class="input-group">
                                                <div class="require left">*</div>
                                                <span class="input-group-addon">Telefone</span>
                                                <input ng-model="Public.professional.Phone" type="text" class="form-control" placeholder="(99) 999-9999" ui-br-phone-number ng-required="true">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-7 padding-none">
                                            <div class="input-group">
                                                <span class="input-group-addon">Portfólio</span>
                                                <input ng-model="Public.professional.Portfolio" type="text" class="form-control" placeholder="" />
                                            </div>
                                        </div>
                                        <div class="col-sm-5">
                                            <button type="submit" class="btn btn-rosa btn-block btn-round">Cadastrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="require inline">*</div> Campos Obrigatórios
                                </div>
                                <div class="col-sm-6">
                                    <label>
                                        <input type="checkbox" ng-model="Public.professional.agree" type="checkbox" ng-required="true" /> Li e aceito os <a target="_blank" class="termos-uso-link" href="http://comunidade.agenciacrowd.com.br/Content/pdf/POLIITICA_DE_PRIVACIDADE_TERMOS_DE_USO.pdf">Termos de uso<div class="require right">*</div></a>
                                    </label>
                                </div>
                            </div>
                            <div class="alert alert-danger" ng-show="Public.error">
                                <strong>Ooops...</strong> {{Public.errorMessage}}
                            </div>
                            <div class="alert alert-info" ng-show="Public.waiting">
                                Aguarde...
                            </div>
                        </div>
                    </form>
                    <div class="col-sm-12">
                        <div class="alert alert-success" ng-show="Public.success">
                            <strong>Obrigado!</strong> Seu cadastro foi realizado com sucesso.
                        </div>
                    </div>
                </div>
<?php include 'footer.php';?>
