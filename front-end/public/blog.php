<?php
  /* CONFIGURAÇÃO */
  $logo = '<img src="./images/crowd.png" />';
  $titulo = 'Blog - Comunidade Crowd';
  $descricao = '';
  $class_page = 'blog';
?>

<?php include 'header.php';?>
      <?php
      if(isset($_GET['post']) and $_GET['post']){
      $xml=("https://agenciacrowd.com.br/feed/?fsk=57d5806cc5dbf");
      function getFeed($feed_url) {
      $feeds = file_get_contents($feed_url);
      $feeds = str_replace("<content:encoded>","<contentEncoded>",$feeds);
      $feeds = str_replace("</content:encoded>","</contentEncoded>",$feeds);
      $rss = simplexml_load_string($feeds);

      $x=$rss;
      $pagina = $_GET['post'];
      foreach($x->channel->item as $entry) {
      if($entry->title == $pagina){
      $img_bg = $entry->enclosure['url'];
      ?>
      <section class="fullscreen" data-speed="6" style="min-height: 500px;background-image: url('<?php echo $img_bg; ?>'); background-position:center; background-size:cover;">
        <div class="black-mask">
          <div class="txt-empresa-1">
            <h1 class="title-blog"><?php echo $entry->title; ?></h1>
          </div>
        </div>
      </section>
      <?php
      echo "<section class='blog-single'><div class='container'><h2>".$entry->title."</h2>";
      echo "$entry->contentEncoded</div></section>";
      }
      else{
      //var_dump($x->channel);
      }
      }

      }
      getFeed($xml);
      }
      else{
      $rss = 'https://agenciacrowd.com.br/blog/feed/?fsk=57d5806cc5dbf';
      $xml = simplexml_load_file( $rss, 'SimpleXMLElement', LIBXML_NOCDATA );
      $i = 0;
      $itempost = '';
      foreach( $xml->channel->item AS $item ){
        //var_dump($item);die;

      if( $i==40 ) break; //limitando a 7 posts
      $itempost .= '<div class="item"><div class="box-blog">';
      $itempost .= '<div class="thumbnail-blog"><a href="blog.php?post='.$item->title.'"><img src="./images/preload.png" class="lazy" data-original="'.$item->enclosure['url'].'" /></a></div>';
        $itempost .= '<div class="text-blog"><h3><a href="blog.php?post='.$item->title.'">'.$item->title.'</a></h3>';
        $itempost .= '<p>'.substr( strip_tags( $item->description ), 0, 100 ).'...</p></div>';
      $itempost .= '</div></div>';
      $i++;
      }

      ?>
      <section class="fullscreen bg-blog" data-speed="6" style="min-height: 334px;">
        <div class="txt-empresa-1 txt-mobile830">
          <h1>Blog</h1>
        </div>
      </section>
      <section class="fullscreen blog-listagem">
        <div class="container-fluid">
          <div class="masonry">
            <?php echo $itempost;?>
          </div>
        </div>
        <div class="load-posts">
            <img src="./images/ajax-loader.gif" />
        </div>
        <div class="no-posts" style="display:none"></div>
        <?php }?>
      </section>
      <?php include 'footer.php';?>

<!--
      -->
      <?php // include 'footer.php';?>
