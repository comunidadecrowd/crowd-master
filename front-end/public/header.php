<!DOCTYPE html>
<html class="no-js css-menubar" lang="pt-br">
    <html>
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
            <meta name="description" content="<?php echo $descricao;?>">
            <title><?php echo $titulo;?></title>
            <link rel="stylesheet" href="./global/css/bootstrap.min.css">
            <link rel="stylesheet" href="./global/css/bootstrap-extend.min.css">
            <link rel="stylesheet" href="./css/cadastro-de-profissionais.css">
            <link rel="stylesheet" href="./css/public.css">
            <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600' rel='stylesheet' type='text/css'>
            <link rel="stylesheet" href="./font-awesome/font-awesome.min.css">
            <link rel="stylesheet" href="./css/bxslider.css">

            <script src="./js/jquery.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="./js/bootstrap.public.min.js"></script>
            <script src="./js/public.js"></script>
            <script src="./js/bxslider.min.js"></script>


        </head>
        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-51170795-3', 'auto');
        ga('send', 'pageview');

        </script>
        <body ng-app="Public" ng-controller="publicController as Public" class="<?php echo $class_page;?>">
            <header>
                <div class="container-fluid">
                    <div class="col-sm-1 col-xs-12 div-logo">
                        <i class="fa fa-bars hidden-lg-up menu-mobile" aria-hidden="true"></i>
                        <a href="index.php" class="hidden-xs logo-mobile"><?php echo $logo;?></a>
                        <a href="index.php" class="visible-xs logo-mobile"><?php echo $logo;?></a>
                    </div>
                    <div class="col-sm-8 col-xs-12 hidden-md-down"><?php $color_link = null; ?>
                        <ul class="links">
                            <a href="quero-expandir-minha-empresa.php" class="<?php echo ( $color_link == 'link-blue' ? 'link-blue' : 'link-green'); ?>">quero expandir minha empresa</a>
                            <a href="quero-ser-profissional.php" class="<?php echo ( $color_link == 'link-blue' ? 'link-blue' : 'link-green'); ?>">quero ser profissional</a>
                            <a href="manifesto.php">Manifesto</a>
                            <a href="blog.php">Blog</a>
                            <a href="faq.php">Faq</a>
                            <a href="mailto:contato@comunidadecrowd.com.br">Contato</a>
                        </ul>
                    </div>
                    <!-- <div class="col-sm-3 col-xs-12 div-login hidden-md-down">
                        <form class="login fazer-login" id="fazer-login" name="formLogin" ng-submit="Public.login()">
                            <input ng-if="Public.hidePassword" type="email" placeholder="Possui login? Então insira seu email" ng-model="Public.user.Email" ng-required="true">
                            <input ng-if="!Public.hidePassword" id="user-password" type="password" autofocus placeholder="Senha" name="password" ng-model="Public.user.Password" ng-required="true" ng-focus="true">
                            <button type="submit" class="logar-sistema">
                                <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                            </button>
                            <div ng-show="Public.error" class="msg-erro-landing"><i class="fa fa-caret-up" aria-hidden="true"></i>
                                {{Public.errorMessage}}
                            </div>
                        </form>
                        <form class="login form-esqueceu-senha" id="recuperar-senha" ng-submit="Public.forgotPassword()">
                            <input type="email" placeholder="Digite o email de cadastro" ng-model="Public.email" ng-required="true">
                            <button type="submit" class="logar-sistema">
                                <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                            </button>
                            <div ng-show="Public.forgotMessage" class="msg-erro-landing" style="border-color: #3875B2;">
                                <span id="text-forgot"></span>
                            </div>
                        </form><br>
                        <span class="voltar-senha"><a href="#" ><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Voltar</a></span>
                        <span class="btn-esqueceu-senha"><a href="#">Esqueceu sua senha?</a></span>
                    </div> -->
                </div>
                <i class="fa fa-user hidden-lg-up btn-logar" aria-hidden="true"></i>
                <ul class="links menu-xs-mobile">
                    <a href="quero-expandir-minha-empresa.php" class="link-green">quero expandir minha empresa</a>
                    <a href="quero-ser-profissional.php" class="link-green">quero ser profissional</a>
                    <a href="manifesto.php">Manifesto</a>
                    <a href="blog.php">Blog</a>
                    <a href="faq.php">Faq</a>
                    <a href="mailto:contato@comunidadecrowd.com.br">Contato</a>
                </ul>
                <div class="beta" ng-click="Public.click()"></div>
                <!-- <div class=" login-mobile">
                <form class="login fazer-login" id="fazer-login" name="formLogin" ng-submit="Public.login()">
                            <input ng-if="Public.hidePassword" type="email" placeholder="Possui login? Então insira seu email" ng-model="Public.user.Email" ng-required="true">
                            <input ng-if="!Public.hidePassword" type="password" placeholder="Senha" name="password" ng-model="Public.user.Password" ng-required="true">
                            <button type="submit" class="logar-sistema">
                                <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                            </button>
                            <div ng-show="Public.error" class="msg-erro-landing"><i class="fa fa-caret-up" aria-hidden="true"></i>
                                {{Public.errorMessage}}
                            </div>
                        </form>
                        <form class="login form-esqueceu-senha" id="recuperar-senha">
                            <input type="email" placeholder="Digite o email de cadastro" ng-required="true">
                            <button type="submit" class="logar-sistema">
                                <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                            </button>
                        </form><br>
                        <span class="voltar-senha"><a href="#" ><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Voltar</a></span>
                        <span class="btn-esqueceu-senha"><a href="#">Esqueceu sua senha?</a></span>
                    </div> -->
            </header>
