<?php
	/* CONFIGURAÇÃO */
	$logo = '<img src="./images/crowd-black.png" />';
	$titulo = 'Faq - Comunidade Crowd';
	$descricao = '';
	$class_page = 'page-faq';
?>
<?php include 'header.php';?>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<section class="faq">
	<div class="container">
		<div class="container-faq">
			<h1>FAQ</h1>
			<p>Confira as respostas para as dúvidas mais<br>
				frequentes sobre a Comunidade Crowd.
			</p>
			<p>Tem mais dúvidas? <a href="mailto:contato@comunidadecrowd.com.br">Entre em contato!</a></p>
		</div>
		<h2>Sobre a Crowd</h2>
		<div class="panel-group" id="accordion1">
			<div class="panel panel-default border-top-none">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion1" href="#cat_a1">O que é a Crowd?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_a1" class="panel-collapse collapse">
					<div class="panel-body">A Crowd é uma comunidade de profissionaise e empresas, criada com a missão de transformar profundamente o jeito de trabalhar nos mercados de comunicação e marketing.
						A Crowd acredita em um mundo totalmente conectado e que isso traz benefícios para as empresas e para os profissionais desse segmento.
						- Para os profissionais: A opção de ter demandas profissionais e trabalhar apenas em projetos que são do seu interesse, do local que quiser, no momento que quiser. Seja para mudar seu estilo de vida sendo um freelancer em tempo integral ou apenas para complementar a renda.
						- Para as empresas: A vantagem de ter um time fixo reduzido, porém plugado em uma rede de especialistas para produzir projetos com qualidade. A possibilidade de transformar custos fixos em variáveis ao mesmo tempo que se amplia exponecialmente a disponibilidade de profissionais para qualquer demanda.
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion1" href="#cat_a2">Em quais mercados a Crowd atua?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_a2" class="panel-collapse collapse">
					<div class="panel-body">A Crowd é focada exclusivamente nos mercados de Marketing e Comunicação, abrangendo Agências, Anunciantes e Veículos. </div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion1" href="#cat_a3">Que tipo de empresas podem fazer parte da Comunidade Crowd?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_a3" class="panel-collapse collapse">
					<div class="panel-body">Qualquer empresa que contrate ou preste serviços de Marketing e Comunicação pode fazer parte da Crowd. Nossa única exigência é que realmente seja uma entusiasta do modelo de trabalho em rede, colaborativo. Que acredite que este modelo é imprescindível para a realização de mais e melhores trabalhos, com menores custos e prazos reduzidos.
						Buscamos evitar empresas ""aventureiras"" ou que busquem a Crowd apenas para avaliar preços e pressionar seus fornecedores atuais. Ou aquelas que não entendem a essência e os princípios que guiam este novo jeito de trabalhar.
					Em relação ao tamanho da empresa não temos restrições - tanto faz se são grandes, médias ou pequenas, afinal temos planos de assinatura para todos os portes - o que importa é o tamanho do seu sonho, afinal a Crowd existe para possibilitar grandes realizações.</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion1" href="#cat_a4">A Crowd permite que pessoas físicas ou empresas não cadastradas solicitem trabalhos?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_a4" class="panel-collapse collapse">
					<div class="panel-body">Não. Para solicitar qualquer trabalho é necessário primeiramente realizar o cadastro na plataforma e arcar com uma assinatura mensal. Dessa forma, conseguimos manter um nível mais qualificado e profissional dos trabalhos requisitados.</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion1" href="#cat_a5">Quais profissionais podem fazer parte da Comunidade Crowd?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_a5" class="panel-collapse collapse">
					<div class="panel-body">A Comunidade Crowd foi pensada para atender todo o universo de Comunicação e Marketing. Atualmente nossas categorias são: Atendimento • Designers • Desenvolvedores • Fotógrafos • Jornalistas • Marketing • Mídia • Planejamento • Produtores de Vídeo • Redatores • Tradutores • Eventos.
						Em breve novas categorias profissionais relativas a Marketing e Comunicação serão adicionadas.
					</div>
				</div>
			</div>
			<div class="panel panel-default border-bottom-none">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion1" href="#cat_a6">Como a Crowd sustenta seu negócio?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_a6" class="panel-collapse collapse">
					<div class="panel-body">A Crowd tem duas fontes de receita:
						1 - Assinatura mensal das empresas, de acordo com o seu tamanho, para terem acesso à plataforma.
					2 - Taxa de 8% cobrada sobre o valor de cada trabalho realizado, cobrada dos profissionais</div>
				</div>
			</div>
		</div>
		<h2>CADASTRAMENTO DE PROFISSIONAIS</h2>
		<div class="panel-group" id="accordion2">
			<div class="panel panel-default border-top-none">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion2" href="#cat_b1">Qualquer profissional pode se cadastrar?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_b1" class="panel-collapse collapse">
					<div class="panel-body">Qualquer profissional que se encaixe em uma das nossa categorias de Comunicação, Marketing e Tecnologia pode se cadastrar. Não há exigência de formação acadêmica ou tempo de experiência mínimo
					Qualquer um que acredite deter conhecimentos e competências nessas áreas pode se inscrever e, a partir daí, o recebimento de propostas ocorrerá de acordo com seu perfil e a qualidade do seu portifólio/referências.</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion2" href="#cat_b2">Quais dados dos profissionais as empresas e agência tem acesso?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_b2" class="panel-collapse collapse">
					<div class="panel-body">As empresas e agências cadastradas na plataforma podem visualizar todas as informações inseridas pelos profissionais em seu perfil. No perfil há a opção ""Visualizar meu perfil"" na qual é possível ver seu cadastro como os contratantes veem. Além disso, as empresas podem ver informações de review e comentários feitos por outras empresas sobre o seu perfil. Lista de informações a serem preenchidas:
						- Nome<br />
						- Título Profissional<br />
						- Foto<br />
						- E-mail<br />
						- Telefone<br />
						- Skype<br />
						- Valor/hora<br />
						- Portifólio<br />
						- Habilidades<br />
						- Categoria<br />
						- Segmento<br />
						- Disponibilidade<br />
						- Descrição<br />
						- Experiências<br />
						- Prêmios<br />
						- Pessoas com quem tem afinidade de trabalhar.<br />
						- Vídeo<br />
						- Avaliações<br />
					As únicas informações dos profissionais que não ficam disponíveis para qualquer empresa consultar são seus dados cadastrais necessários para a contratação e pagamento dos serviços. Tais informações só são fornecidas às empresas que contratarem tais profissionais.</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion2" href="#cat_b3">Os profissionais tem acesso aos dados das empresas e agências?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_b3" class="panel-collapse collapse">
					<div class="panel-body">Os profissionais tem acesso, essencialmente, à<br />
						1. nível de atividade da empresa na plataforma - quantidade de trabalhos contratados, % de briefings que efetivamente se tornaram jobs, entre outras;<br />
						2. reputação da empresa na plataforma - constituído pelos comentários e avaliações de outros profissionais que já realizaram algum job com a empresa;<br />
						3. informações a respeito de cada briefing/pedido de proposta - para quantos profissionais o briefing está sendo enviado, se é público ou privado, entre outras.<br />
					Dessa forma, os profissionais tem a opção de priorizar trabalhar com empresas e agências que atuam bastante na Crowd, contam com boa reputação e em briefings que há mais chances de ser o escolhido.</div>
				</div>
			</div>
			<div class="panel panel-default border-bottom-none">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion2" href="#cat_b4">A Crowd permite que pessoas físicas ou empresas não cadastradas solicitem trabalhos?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_b4" class="panel-collapse collapse">
					<div class="panel-body">Não. Para solicitar qualquer trabalho é necessário primeiramente realizar o cadastro na plataforma e arcar com uma assinatura mensal. Dessa forma, conseguimos manter um nível mais qualificado e profissional dos trabalhos requisitados.</div>
				</div>
			</div>

		</div>

		<h2>SELEÇÃO DE PROFISSIONAIS</h2>
		<div class="panel-group" id="accordion3">
			<div class="panel panel-default border-top-none">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion3" href="#cat_c1">Como as empresas selecionam os profissionais para pedir propostas?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_c1" class="panel-collapse collapse">
					<div class="panel-body">Em breve...</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion3" href="#cat_c2">Quais os possíveis critérios para a busca de profissionais?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_c2" class="panel-collapse collapse">
					<div class="panel-body">Em breve</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion3" href="#cat_c3">Quais os filtros disponíveis para refinar os resultados de busca?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_c3" class="panel-collapse collapse">
					<div class="panel-body">Em breve</div>
				</div>
			</div>
			<div class="panel panel-default border-bottom-none">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion3" href="#cat_c4">Até quantos profissionais podem ser selecionados para cada briefing?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_c4" class="panel-collapse collapse">
					<div class="panel-body">A empresa ou agência que está criando o briefing tem a opção de convidar no máximo até 10 profissionais. Esse limite é para garantir que já tenha sido feita uma pré seleção dos profissionais com o perfil do job e para dar chances reais ao profissional convidado que irá orçar conseguir o job.</div>
				</div>
			</div>
		</div>
		<h2>BRIEFINGS</h2>
		<div class="panel-group" id="accordion4">
			<div class="panel panel-default border-top-none">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion4" href="#cat_d1">Quais recursos podem ser utilizados para a transmissão de um briefing dentro da plataforma?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_d1" class="panel-collapse collapse">
					<div class="panel-body">O briefing pode ser registrado e transmitido através de um formulário específico, desenvolvido e testado em projetos reais durante mais de 2 anos. Este formulário possibilita a formatação dos textos, inserção de imagens e inclusive a anexação de arquivos de qualquer tipo. É possível também trocar mensagens diretamente junto ao briefing, o que mantém tudo organizado.</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion4" href="#cat_d2">Para que serve o briefing e qual a sua importância no processo?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_d2" class="panel-collapse collapse">
					<div class="panel-body">Em marketing e comunicação, todos sabem que um briefing bem elaborado é a base para que o trabalho seja bem feito e dentro do prazo. É imprescindível também para que o andamento do trabalho ocorra sem percalços ou mal-entendidos.
						No caso específico da Crowd, devido ao fato de se brifar vários profissionais ao mesmo tempo, o briefing é ainda mais importante pois significa uma enorme economia de tempo e a uniformidade de entendimento do projeto pelos diferentes fornecedores que enviarão suas propostas.
						Um briefing bem feito também aumenta a motivação dos profissionais e melhora a imagem da empresa contratante dentro da plataforma.
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion4" href="#cat_d3">Como funcionam os briefings públicos e privados?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_d3" class="panel-collapse collapse">
					<div class="panel-body">Em ambos os briefings as empresas e agências deverão selecionar um máximo de até 10 profissionais ao postar um briefing. Esses profissionais selecionados são notificados por email para que eles possam orçar rapidamente. No caso do briefings privados apenas esses convidados tem acesso ao briefing. Já no caso dos briefings públicos, estes são colocados em um "mural" de briefings abertos e qualquer profissional que acessa a plataforma pode localizar jobs abertos e enviar uma proposta.</div>
				</div>
			</div>
			<div class="panel panel-default border-bottom-none">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion4" href="#cat_d4">Posso responder briefings por email?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_d4" class="panel-collapse collapse">
					<div class="panel-body">Não. Os emails que você recebe da plataforma são todos automatizados e ninguém tem acesso a esses emails. Logo para responder briefings, comentários, tarefas você deverá acessar com seu login e senha para responder.</div>
				</div>
			</div>
			<div class="panel panel-default border-bottom-none">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion4" href="#cat_d5">Eu irei receber um feedback sempre que eu participar de algum briefing?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_d5" class="panel-collapse collapse">
					<div class="panel-body">Toda vez que você enviar uma proposta na plataforma você receberá um feedback. Há 3 tipos de feedback possíveis:
						- Comentário - algum comentário seja esse da sua proposta ou dos seus questionamentos.<br />
						- Aprovado - você é a pessoa que o cliente quer realizar o trabalho.<br />
						- Job fechado - o cliente preferiu seguir esse trabalho com outro profissional.<br />
					</div>
				</div>
			</div>
		</div>

		<h2>PROJETOS E TAREFAS</h2>
		<div class="panel-group" id="accordion5">
			<div class="panel panel-default border-top-none">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion5" href="#cat_e1">Como funciona quando eu recebo uma tarefa para orçar?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_e1" class="panel-collapse collapse">
					<div class="panel-body">O cliente ou agência quer trabalhar com você nessa tarefa e somente você e o cliente tem acesso a ela. Dessa forma apenas você estará orçando e sendo aprovada você será notifica para que possa começar a realiza-lá.</div>
				</div>
			</div>
			<div class="panel panel-default border-bottom-none">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion5" href="#cat_e2">Posso responder tarefas por email?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_e2" class="panel-collapse collapse">
					<div class="panel-body">Não. Os emails que você recebe da plataforma são todos automatizados e ninguém tem acesso a esses emails. Logo para responder briefings, comentários, tarefas você deverá acessar com seu login e senha para responder.</div>
				</div>
			</div>
		</div>
		<h2>PAGAMENTOS E RECEBIMENTOS</h2>
		<div class="panel-group" id="accordion6">
			<div class="panel panel-default border-top-none">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion6" href="#cat_f1">Eu preciso ter um CNPJ para receber meu pagamento?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_f1" class="panel-collapse collapse">
					<div class="panel-body">Sim. Após uma tarefa realizada ser aprovada pela empresa ou agência que o contratou você precisará emitir uma nota fiscal. O seu pagamento só é feito mediante a emissão da nota fiscal. Atualmente é muito fácil abrir uma empresa MEI. Aqui você pode obter mais informações de como abrir uma MEI acessando esse artigo: https://comunidadecrowd.com.br/mei-vantagens-para-freelancers/</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion6" href="#cat_f2">Posso emitir uma Nota de terceiro de uma empresa em que eu não sou proprietário nem sócio?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_f2" class="panel-collapse collapse">
					<div class="panel-body">Não. O contrato da tarefa é fechado diretamente com você e para que você receba o trabalho de forma legal é necessário que você emita uma nota fiscal de uma empresa em que você seja proprietário ou sócio. Atualmente é muito facil e benéfico abrir uma MEI. Aqui você pode obter mais informações de como abrir uma MEI acessando esse artigo: https://comunidadecrowd.com.br/mei-vantagens-para-freelancers/
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion6" href="#cat_f3">Quais são os dias que eu irei receber por um trabalho aprovado?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_f3" class="panel-collapse collapse">
					<div class="panel-body">A política de recebimento dos trabalhos entregues e aprovados na plataforma é a mesma para todos o profissionais. São feitos pagamentos quinzenais, todo o dia 15 e 30. Aqui, você recebe no dia 15, pelos trabalhos aprovados entre os dias 16 e 31 (do mês anterior), e 30, pelos jobs aceitos entre os dias 1 e 15.
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion6" href="#cat_f4">Quais são os dias que eu deverei pagar por um trabalho aprovado
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_f4" class="panel-collapse collapse">
					<div class="panel-body">A política de pagamento dos trabalhos entregues e aprovados na plataforma é a mesma para todas as empresas e agências. Deverão ser feitos pagamentos quinzenais, todo o dia 10 e 25 de cada mês. Aqui, você paga no dia 10, pelos trabalhos aprovados entre os dias 16 e 31 (do mês anterior), e 25, pelos jobs aceitos entre os dias 1 e 15.
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion6" href="#cat_f5">Como fornecedor, quando eu sei que irei receber?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_f5" class="panel-collapse collapse">
					<div class="panel-body">Quando o seu trabalho executado for aprovado pela empresa ou agência que o contratou, ou seja, trabalhos que deixaram a coluna de "Aprovação" e estão na coluna de trabalhos "Entregues". Você é notificado por email a cada status ou comentário do seu job.
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion6" href="#cat_f6">O que acontece se a empresa ou agência aprovou a tarefa e não pagou?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_f6" class="panel-collapse collapse">
					<div class="panel-body">Você poderá avaliar a empresa ou agência de forma negativa ajudando a todos os membros da comunidade. Além disso contará como o suporte da Crowd para ajudar na cobrança do pagamento. Para isso você deverá entrar em contato pelo email contato@comunidadecrowd.com.br
					</div>
				</div>
			</div>
			<div class="panel panel-default border-bottom-none">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion6" href="#cat_f7">Contratei um profissional e o trabalho ficou abaixo das minhas expectativas. O que devo fazer?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_f7" class="panel-collapse collapse">
					<div class="panel-body">Quando um trabalho fica abaixo das expectativas você pode pedir refação ao profissional até que chegue em um resultado satisfatório. Caso já tenha tentado isso algumas vezes e ainda encontra-se insatisfeito, você poderá "desistir da tarefa" e realizá-lá com outro profissional.
					</div>
				</div>
			</div>
		</div>

		<h2>AVALIAÇÃO DE PROFISSIONAIS E EMPRESAS</h2>
		<div class="panel-group" id="accordion7">
			<div class="panel panel-default border-top-none">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion7" href="#cat_g1">Em que momento é possível avaliar o trabalho de um fornecedor ou de um cliente?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_g1" class="panel-collapse collapse">
					<div class="panel-body">Quando um trabalho é realizado e finalizado, o cliente avalia o fornecedor e o fornecedor avalia o cliente. Somente neste momento. Afinal, só é possível avaliar alguém a partir da execução de algum trabalho.</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion7" href="#cat_g2">Por que os fornecedores (freelancers) também avaliam os contratantes (clientes)?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_g2" class="panel-collapse collapse">
					<div class="panel-body">Na Crowd nós acreditamos que esta avaliação "de duas mãos" é essencial para a consolidação e o fortalecimento da comunidade. Desta forma, não somente os freelancers se esforçam para fazer um exclente trabalho e melhorar sua reputação na comunidade, como também as empresas contratantes se esforçarão em ter uma boa imagem e elevar seu score na plataforma o que as possibilitará sempre ter os melhores profissionais dispostos a trabalhar para elas e cobrando preços mais competitivos.
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion7" href="#cat_g3">Quais os critérios de avaliação dos profissionais pelas empresas e quem pode avaliá-los?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_g3" class="panel-collapse collapse">
					<div class="panel-body">Em breve...
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion7" href="#cat_g4">Quais os critérios de avaliação das empresas pelos profissionais e quem pode avaliá-las?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_g4" class="panel-collapse collapse">
					<div class="panel-body">Em breve...
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion7" href="#cat_g5">Como são atribuídas as notas médias em cada critério e a média geral em "estrelas"?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_g5" class="panel-collapse collapse">
					<div class="panel-body">A nota média em cada um dos 3 critérios de cada participante é calculada pela média das notas atribuídas, só que ponderada pelo valor de cada trabalho. Isto é, a nota de um trabalho que custou R$1.000 vale a metade daquela cujo trabalho custou R$2.000. Já a nota geral em estrelas é a média simples das notas dos 3 critérios.
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion7" href="#cat_g6">Por que é importante atribuir notas e avaliar os participantes da comunidade?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_g6" class="panel-collapse collapse">
					<div class="panel-body">Na Crowd, entendemos que é crucial haver o máximo de informações disponíveis sobre cada participante para poder avaliá-los na hora de escolher com quem queremos trabalhar. Nesse contexto, a avaliação de quem já realizou trabalhos em conjunto é um dos componentes mais importantes. Dar notas e avaliar é contribuir para o crescimento da comunidade e melhoria da plataforma para todos os seus membros.
					</div>
				</div>
			</div>
			<div class="panel panel-default border-bottom-none">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion7" href="#cat_g7">Por que os comentários sobre profissionais e empresas são anônimos, isto é, a identidade de quem fez o comentário é preservada?
						<i class="glyphicon glyphicon-menu-down pull-right"></i>
					</a>
					</h4>
				</div>
				<div id="cat_g7" class="panel-collapse collapse">
					<div class="panel-body">O intuito é não causar nenhum mal estar ou "saia justa" com um cliente que coloca algum comentário negativo sobre um fornecedor e vice-versa. Sendo anônimos, os comentários tendem a ser muito mais verdadeiros quando negativos e sem exageros quando sinceros.
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</section>
<?php include 'footer.php';?>
