publicProfessionalController.$inject = ['publicService', '$http', 'ngDialog', '$scope', 'publicProfessionalService', '$window', '$timeout'];

function publicProfessionalController(publicService, $http, ngDialog, $scope, publicProfessionalService, $window, $timeout) {
    var Public = this;

    // Private
    var skills = [];
    var segments = [];
    var storageAvailabiity = {};
    var storageConfirmPassword = null;

    // Public
    Public.imagemPerfil = false;
    Public.success = false;
    Public.waiting = false;
    Public.error = false;
    Public.errorMessage = null;
    Public.segmentLabel = 'Segmentos com mais afinidade';

 //   Public.countries = [];
 //   Public.states = [];
  //  Public.cities = [];
    Public.categories = [];
  //  Public.segments = [];
  //  Public.availabilitys = [];

    Public.professional = {};

    Public.register = register;
 //   Public.loadCities = loadCities;
 //   Public.loadSkills = loadSkills;
    Public.tagsLimit = tagsLimit;
    Public.openDialog = openDialog;
 //   Public.addSegment = addSegment;

    $scope.closeDialog = closeDialog;

    function init() {
        Public.availabilitys = publicService.getAvailabilities();
        Public.professional['Referrer'] = document.referrer || null;
        Public.professional['PromoCode'] = null;

        dataConfig(publicService.getCategories(), 'categories');

        if (document.URL.indexOf("?") > -1) {

            var landing = document.URL.substring(document.URL.indexOf("?") + 1)
            landing = JSON.parse('{"' + decodeURIComponent(landing).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g, '":"').replace(/([+])/g, ' ') + '"}');
            landing = _.map(landing)
            Public.professional['Name'] = landing[0];
            Public.professional['Phone'] = landing[1];
            Public.professional['Email'] = landing[2];
            Public.professional['Category.name'] = landing[3];
            Public.professional['Referrer'] = document.referrer;

            $timeout(function () { //NOTE: NG-MODEL ainda não renderizou até aqui, precisa aguardar

                jQuery("input")
                    .filter(function () {
                        return this.value.length !== 0;
                    })
                    .css('border-color', '#4acfa4');

            }, 500)

        }


    }

    function register() {

        window.Rollbar.configure({
            payload: {
                person: {
                    Name: window.jQuery("input[name=nome]").val(),
                    Email: window.jQuery("input[name=email]").val(),
                    Telefone: window.jQuery("input[name=telefone]").val()
                },
            },
        });


        Public.error = false;

        if (!professionalValidate())
            return false;

        //storageAvailabiity = Public.professional.Availability;
        //storageConfirmPassword = Public.professional.ConfirmPassword;

        // delete Public.professional.ConfirmPassword;
        // delete Public.professional.AcceptTerms;

        publicProfessionalService.set(Public.professional);
        var promisse = publicProfessionalService.save();

        Public.waiting = true;

        if (!promisse.success) {
            Public.error = true;
            Public.errorMessage = promisse.message;
            Public.waiting = false;
            addFieldsValues();
        } else {
            promisse
                .success(function(data, status) {

                    // **********************************
                    // Removida o alto login do freelancer cadastrado nesse momento
                    // Agora ele não pode se auto-logar pois seu cadastro esta em estado inativo
                    // **********************************
                    //if (!publicService.userLoginManager(data)) {
                    // alert('Erro ao fazer o cadastro. Por favor, tente novamente.');
                    // addFieldsValues();
                    // Public.waiting = false;
                    // return false;
                    //}
                    // **********************************

                    if(status != 200)  window.Rollbar.critical("publicProfessional Controller > register", data);

                    Public.error = false;

                    window.$jQuery('#frmProfessional')
                        .slideToggle();

                    Public.success = true;
                    Public.waiting = false;
                    
                    $window.ga('send', 'event', 'Freelancer', 'Lead Freelancer');
                    window.localStorage.setItem('ngStorage-user', JSON.stringify(data.loggedUser));
                    window.localStorage.setItem('ngStorage-token',  JSON.stringify(data.token));
                    setTimeout(function () {
                        if (!/beta?/.test(location.href)) {
                            window.location = "/freelancer/#/profile";

                        } else {
                            window.location = "/freelancer/#/profile";

                        }    
                    }, 1000);
                    
                    // alert('Seu cadastro foi efetuado com sucesso. Você receberá um email para confirmar seu cadastro. Obrigado por fazer parte da Crowd!');
                })
                .error(function(data, statusCode) {

                    window.Rollbar.critical("publicProfessional Controller > register", data);

                    if (data == 'User already exist') {
                        Public.error = true;
                        Public.errorMessage = 'Email já utilizado. Você esqueceu a sua senha?';
                    } else {
                        Public.error = true;
                        Public.errorMessage = 'Meu servidor disse: "' + data  + '". Está tudo bem?';
                    }

                    Public.waiting = false;
                    addFieldsValues();
                });
        }

    }

    function addFieldsValues() {
        Public.professional.Availability = storageAvailabiity;
        Public.professional.ConfirmPassword = storageConfirmPassword;
        //        Public.professional.AcceptTerms = true;
    }

    function dataConfig(promisse, obj) {
        promisse
            .success(function(data) {
                switch (obj) {
                    case 'categories':
                        Public.categories = data;
                        break;
                 //   case 'segments':
                //        Public.segments = data;
                //        break;
                //      case 'skills':
                //        skills = configSkills(data);
                //        break;
                }
            })
            .error(function(data, statusCode) {
                throw 'Error to load ' + obj + '\n' + statusCode;
            });
    }

    function loadCities() {
        var state = Public.professional.State;
        dataConfig(publicService.getCities(state.Id), 'cities');
    }

    function configSkills(dataSkills) {
        var arraySkills = [];

        dataSkills.forEach(function(skill, index) {
            arraySkills.push({
                id: skill.Id,
                text: skill.Name
            });
        });

        return arraySkills;
    }

    function loadSkills(query) {
        return skills.filter(function(objSkill) {
            var text = objSkill.text.toLowerCase();
            var string = query.toLowerCase();

            return text.indexOf(string) >= 0;
        });
    }

    function tagsLimit() {
        var tags = Public.professional.Skills;
        return publicProfessionalService.tagsLimit(tags);
    }

    function openDialog() {
        ngDialog.open({
            template: '/templates/view/dialog-image-profile.html',
            appendClassName: "ngdialog-custom",
            scope: $scope
        });
    }

    function closeDialog() {
        ngDialog.close();
        Public.imagemPerfil = true;
    }

    function professionalValidate() {
       // if (!Public.imagemPerfil)
       //     return enableValidation(false, 'Você precisa adicionar uma imagem de perfil');


  //      if (!Public.professional.Password)
  //          return enableValidation('Senha');

  //      if (!Public.professional.ConfirmPassword)
 //           return enableValidation(false, 'Confirme a Senha.');

 //       if (Public.professional.Password !== Public.professional.ConfirmPassword)
//            return enableValidation(false, 'Senha informada e sua confirmação não conferem.');

        if (!Public.professional.AcceptTerms)
            return enableValidation(false, 'Você precisa aceitar os Termos de uso');

        return true;
    }

    function enableValidation(fieldName, message) {
        if (message === undefined)
            message = 'Preencha o campo ' + fieldName + '.';

        Public.errorMessage = message;
        Public.error = true;

        return false;
    }

   /* function addSegment(segment) {
        publicProfessionalService.addSegment(segment.Id);

        if (searchElementInSegmentArray(segment.Id))
            segments = removeElementSegmentArray(segment.Id);
        else
            segments.push(segment);

      //  configSegmentLabel();
    }

    function configSegmentLabel() {
        Public.segmentLabel = publicProfessionalService.configSegmentLabel(segments);
    } 

    function searchElementInSegmentArray(value) {
        var result = false;
        segments.forEach(function(itemObj) {
            if (itemObj.Id === value)
                result = true;
        });

        return result;
    } */

    function removeElementSegmentArray(id) {
        return segments.filter(function(itemObj) {
            return itemObj.Id !== id;
        });
    }


    // Calls
    init();
}
