/**
 * ARQUIVO ORIGINAL publicLogin NA RAIZ DO DOMINIO https://app.crowd.br.com
 * @param {object} message contem todas as informações relevantes para 
 */
window.addEventListener("message", receiveMessage, false);

function receiveMessage(message) {
  if (message.origin !== "https://crowd.br.com") // SÓ QUERO MENSAGENS DO SITE
    return false;

  console.log("postMessage(data)", message);
  var data = JSON.parse(message.data)

  localStorage.setItem("ngStorage-user", JSON.stringify(data.loggedUser));
  localStorage.setItem("ngStorage-token", JSON.stringify(data.token));

  setTimeout(function () {
    window.parent.postMessage("redirect", "https://crowd.br.com");
  }, 1000);

}

/**
 * ISTO DEVE IR NA PÁGINA DO WORDPRESS QUE FARÁ O LOGIN
 * @param {string} event 
 * @returns falso se a resposta não for da plataforma
 */
window.addEventListener("message", receiveMessage, false);

function receiveMessage(event) {
  if (event.origin !== "https://app.crowd.br.com")
    return false;
  window.ga("send", "event", "Login", "Login atavés do site");
  window.location = "https://app.crowd.br.com" + data.loggedUser.defaultURL;

}


// continuação do que deve ir para o WORDPRESS
jQuery('.login-remember').css({ opacity: 0 });

jQuery("#loginform").submit(function (e) {
  e.preventDefault();

  jQuery('.responseText').remove();

  var url = "https://api.comunidadecrowd.com.br/User/Login";
  var inputs = Object();
  jQuery.each(jQuery('#loginform').serializeArray(), function (_, kv) {
    inputs[kv.name] = kv.value;
  });

  jQuery.post(
    url, {
      Email: inputs.log || "",
      Password: inputs.pwd || ""
    },
    function (data, status, xhr) {
      if (xhr.status == 200) {

        console.log("jQuery.post(data)", data);
        window.data = data;
        data = JSON.stringify(data);
        document.getElementById('iframe').contentWindow.postMessage(data, "https://app.crowd.br.com");
        setTimeout(function () {
          window.location = "https://app.crowd.br.com" + data.loggedUser.defaultURL;
        }, 1000);

      } else {
        window.Rollbar.critical(
          "publicLogin",
          data
        );
        jQuery(".wpcf7-response-output")
          .html(
          'Meu servidor respondeu: ' + data.responseText + '. Está tudo bem? <br> <a id="forgotPassword" href="#">Esqueceu sua senha?</a>'
          );
        jQuery(".wpcf7-response-output")
          .addClass('ws-error');

        return false;
      }
    })
    .fail(function (data, status, xhr) {
      window.Rollbar.critical(
        "publicLogin",
        data
      );

      jQuery('#user_pass').removeClass().addClass('shake animated error').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
        jQuery(this).removeClass();
      });
      jQuery('.login-password').append('<p class="responseText">' + data.responseText + '</p> <a id="forgotPassword" href="#">Esqueceu sua senha?</a>');

      jQuery(".wpcf7-response-output")
        .html(
        'Meu servidor respondeu: ' + data.responseText + '. Está tudo bem?'
        );
      jQuery(".wpcf7-response-output")
        .addClass('ws-error');

      return false;
    });


});

/** 
 * O LINK DE ESQUECI A SENHA É CRIADO DINÂMICAMENTE
 * Então é preciso ouvir pelo evento vindo do "document"
 */
jQuery(document).on('click', '#forgotPassword', function () {

  var inputs = Object();
  jQuery.each(jQuery('#loginform').serializeArray(), function (_, kv) {
    inputs[kv.name] = kv.value;
  });

  jQuery.post("https://api.comunidadecrowd.com.br/User/ForgotPassword?email=" + inputs.log, null, function (data, status, xhr) {
    jQuery('.login-password')
      .html(
      'Um email foi enviado para <strong class="color--success">' + inputs.log + '</strong> com as instruções para recuperação da senha.'
      );
  });

});