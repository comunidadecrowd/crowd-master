/**
 * Script para cuidar do pré cadastro de Customer
 * @requires {Contact7} as wpcf7mailfailed
 * @requires {jQuery} as jQuery
 * @requires {Intercom} as window.Intercom
 * @requires {GoogleAnalytics} as window.ga
 * @requires {Rollbar} as window.Rollbar
 */

(function () {
  "use strict";

  /** Este cara ouve esperando arquivo public.wordpress.login.js pela confirmação de ter salvo o localStorage  */
  window.addEventListener("message", receiveMessage, false);

  /**
   * ISTO DEVE IR NA PÁGINA DO WORDPRESS QUE FARÁ O LOGIN
   * @param {object} event 
   * @returns falso se a resposta não for da plataforma
   */
  function receiveMessage(event) {
    if (event.origin !== "https://app.crowd.br.com") // SÓ ACEITO SE FOR DA PLATAFORMA
      return false;

    window.ga("send", "event", "Login", "Login atavés do site");
    window.location = "https://app.crowd.br.com" + window.data.loggedUser.defaultURL;

  }

  /**
   * Começo a ouvir pelo evento que é disparado quando o email não é enviado
   * @description Depois de validar o formulário, o plugin ContactForm7 do Wordpress tenta enviar o email, e como não DEVE estar configurado, ele retorna este evento
   */
  document.addEventListener(
    "wpcf7mailfailed",
    function (event) {
      /** @private {array} Conjunto dos inputs e seus valores */
      var inputs = event.detail.inputs;

      /** @private {string} Referrer de onde o usuário veio para o formulário de cadastro  */
      var docRef = document.referrer;

      /** @private {object} Objeto que será construído conforme o valor selecionado */
      var cat = Object();

      /** @private {string} Endereço do endpoint */
      var url = "https://api.comunidadecrowd.com.br/User/PreCadastro";

      switch (inputs[2].value) {
      case "Assessoria de imprensa":
        cat = {
          Id: 12,
          Name: inputs[2].value
        };
        break;
      case "Atendimento e GP":
        cat = {
          Id: 10,
          Name: inputs[2].value
        };
        break;
      case "Áudio e Vídeo":
        cat = {
          Id: 6,
          Name: inputs[2].value
        };
        break;
      case "Conteúdo":
        cat = {
          Id: 1,
          Name: inputs[2].value
        };
        break;
      case "Desenvolvimento":
        cat = {
          Id: 8,
          Name: inputs[2].value
        };
        break;
      case "Design":
        cat = {
          Id: 3,
          Name: inputs[2].value
        };
        break;
      case "Eventos":
        cat = {
          Id: 13,
          Name: inputs[2].value
        };
        break;
      case "Fotografia":
        cat = {
          Id: 2,
          Name: inputs[2].value
        };
        break;
      case "Marketing de Performance":
        cat = {
          Id: 14,
          Name: inputs[2].value
        };
        break;
      case "Planejamento":
        cat = {
          Id: 15,
          Name: inputs[2].value
        };
        break;
      case "Redação Publicitária":
        cat = {
          Id: 4,
          Name: inputs[2].value
        };
        break;
      case "Tradução":
        cat = {
          Id: 7,
          Name: inputs[2].value
        };
        break;
      default:
        cat = {
          Name: inputs[2].value
        };
        break;
      }

      jQuery.post(
        url, {
          Name: inputs[0].value || "",
          Email: inputs[1].value || "",
          Category: cat, // PREENCHIDO ANTES PELO SWITCH
          Phone: inputs[3].value || "",
          // PlanType: Number(Public.customer.PlanType),
          Referrer: docRef || "",
          // PromoCode: Public.customer.PromoCode || "",
          AcceptTerms: inputs[4].value ? true : false, // FIXME: INCONSISTÊNCIA NO ENDPOINT true false PARA FREELANCER, 1 0 PARA CUSTOMER
        },
        function (data, status, xhr) {
          if (xhr.status == 200) {

            window.data = data;
            document.getElementById('iframe').contentWindow.postMessage(JSON.stringify(data), "https://app.crowd.br.com");

          } else {
            window.Rollbar.critical(
              "publicProfessional.controller > register",
              data
            );
            jQuery(".wpcf7-response-output")
              .html(
              'Meu servidor respondeu: ' + data.responseText + '. Está tudo bem?'
              );
            jQuery(".wpcf7-response-output")
              .addClass('ws-error');
           
            return false;
          }
        })
        .fail(function (data, status, xhr) {
          console.log(".fail(data)", data);
          
          window.Rollbar.critical(
            "publicProfessional.controller > register",
            data
          );
          jQuery(".wpcf7-response-output")
            .html(
            'Meu servidor respondeu: ' + data.responseText + '. Está tudo bem?'
            );
          jQuery(".wpcf7-response-output")
            .addClass('ws-error');
          
          return false;
        });

      console.log(event);
    },
    false
  );
})();
