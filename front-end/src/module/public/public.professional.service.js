publicProfessionalService.$inject = [
  "$http",
  "publicService"
];

function publicProfessionalService($http, publicService) {
  var service = this;

  // Public
  service.save = save;
  service.set = set;
  service.get = get;
  service.addSegment = addSegment;
  service.tagsLimit = tagsLimit;
  service.setSegmentsSelecteds = setSegmentsSelecteds;
  service.configSegmentLabel = configSegmentLabel;

  // Private
  var professional;
  var segmentsSelecteds = [];
  var segmentsLimit = 3;
  var limitTags = 10;
  console.log(limitTags);

  // Functions
  function save() {
    if (!validation()
      .success) return validation();

    if (professional.Id === undefined)
      return $http.post(
        publicService.baseUrlAPI + "/User/PreCadastro",
        professional
      );

    return $http.post(publicService.baseUrlAPI + "/User/Edit", professional);
  }

  function validation() {
    if (!professional.Name) return validationError("Nome");

    // if (!professional.Title)
    //    return validationError('Cargo');

    if (!professional.Email) return validationError("Email");

    //  if (professional.Skills === undefined || professional.Skills.length === 0)
    //       return validationError('Skill', 'Informe pelo menos uma Habilidade.');

    // if (!professional.Country)
    //     return validationError('Pais');

    // if (!professional.State)
    //     return validationError('UF');

    // if (!professional.City)
    //    return validationError('Cidade');

    if (!professional.Category)
      return validationError("Categoria", "Preencha a Categoria.");

    // if (professional.Segments.length === 0)
    //    return validationError('Segmento', 'Selecione pelo menos um Segmento.');

    // if (!professional.Price)
    //    return validationError('Valor/h');

    // if (!professional.Availability)
    //    return validationError('Disponibilidade', 'Preencha a Disponibilidade.');

    //  if (!professional.Skype)
    //     return validationError('Skype');

    if (!professional.Phone) return validationError("Telefone");

    // if (professional.CNPJ === undefined)
    //     professional.CNPJ = null;

    return {
      success: true
    };
  }

  function validationError(fieldName, message) {
    var errorObj = {};

    if (message === undefined) message = "Preencha o campo " + fieldName + ".";

    errorObj.success = false;
    errorObj.fieldName = fieldName;
    errorObj.message = message;

    return errorObj;
  }

  function set(professionalObj) {
    professional = professionalObj;

    // setImageProfile();
    setSegments();
    setSkills();
    setSegments();
    setAvailability();
    setCountry();
    setCategory();
  }

  function get() {
    return professional;
  }

  function addSegment(segmentId) {
    if (segmentId !== undefined) {
      var index = segmentsSelecteds.indexOf(segmentId);
      if (index > -1) segmentsSelecteds.splice(index, 1);
      else segmentsSelecteds.push(segmentId);

      if (segmentsSelecteds.length >= segmentsLimit) {
        window.$jQuery(".checkbox-segment")
          .attr("disabled", true);
        segmentsSelecteds.forEach(function (id, index) {
          window.$jQuery("#checkbox-" + id)
            .removeAttr("disabled");
        });
      } else {
        window.$jQuery(".checkbox-segment")
          .removeAttr("disabled");
      }
    }
  }

  function tagsLimit(tags) {
    if (tags !== undefined && tags.length >= limitTags) return false;

    return true;
  }

  function setSegments() {
    if (professional.Segments) {
      var segments = [];

      professional.Segments.forEach(function (segmentsObj) {
        segments.push(segmentsObj.Id);
      });

      professional.SegmentsIds = segments;
    }
  }

  function setSkills() {
    if (professional.Skills) {
      var skills = [];

      professional.Skills.forEach(function (skillObj) {
        skills.push(skillObj.id);
      });

      professional.SkillsIds = skills;
    }
  }

  function setAvailability() {
    if (professional.Availability)
      professional.Availability = professional.Availability.Id;
  }

  function setCountry() {
    professional.Country = "Brasil";
  }

  /** @deprecated */
  function setCategory() {
    //   if (professional.Category)
    // professional.CategoryId = professional.Category.Id;
  }

  function setImageProfile() {
    //NÃO MEXER NESSA FUNÇÃO SEM O ACOMPANHAMENTO DE ALGUÉM MAIS ANTIGO NA EQUIPE
    var imgSrc = document.getElementById("imagem-perfil")
      .value;

    if (!imgSrc) {
      professional.Photo = "";
      professional.PhotoBase64 = "";
      professional.PhotoExtension = "";
    } else {
      professional.Photo = imgSrc;
      console.log(imgSrc);
      professional.photoExtension = imgSrc.match(/(jpg|jpeg|png|gif|svg)/)[0];
      console.log(professional.photoExtension);
      professional.PhotoBase64 = imgSrc.replace(
        new RegExp("data:image/[^;]+;base64,*"),
        ""
      );
      console.log(professional.PhotoBase64);
    }
  }

  function setSegmentsSelecteds(segments) {
    segmentsSelecteds = segments;
  }

  function configSegmentLabel(segmentArray) {
    var qtdSegment = segmentArray.length - 1;
    var lastSegment = segmentArray[segmentArray.length - 1];

    if (lastSegment) {
      lastSegment = lastSegment.Name;
      if (qtdSegment) {
        if (lastSegment.length > 20)
          lastSegment = lastSegment.substr(0, 20) + "...";

        return lastSegment + " +" + qtdSegment;
      } else {
        return lastSegment;
      }
    } else {
      return "Segmento";
    }
  }
}
