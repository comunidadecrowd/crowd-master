(function () {
  "use strict";

  /**
   * Public Module
   *
   * Description
   */
  window.angular
    .module("Public", [
      "ngStorage",
      "ngTagsInput",
      "ngDialog",
      "ui.utils.masks",
      "InputMaskDate",
    ])
    // Controller
    .controller("publicController", publicController)
    .controller("publicProfessionalController", publicProfessionalController)
    .controller("publicCustomerController", publicCustomerController)
    // Services
    .service("publicService", publicService)
    .service("publicProfessionalService", publicProfessionalService)
    // Directive
    .directive("cutText", publicCutTextDirective)
    // Run
    .run(publicRun);
})();
