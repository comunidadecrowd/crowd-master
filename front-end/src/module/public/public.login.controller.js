publicController.$inject = ['$scope', 'publicService', '$localStorage', '$http', '$timeout'];

function publicController($scope, publicService, $localStorage, $http, $timeout) {
  var Public = this;

  Public.hidePassword = true;
  Public.error = false;
  Public.success = false;
  Public.forgotError = false;
  Public.newPassword = false;
  Public.forgotMessage = false;
  Public.errorMessage = null;
  Public.email = null;

  Public.user = {};
  Public.professional = {};

  Public.login = login;
  Public.forgotPassword = forgotPassword;
  Public.validadeHash = validadeHash;
  Public.recoverPassword = recoverPassword;

  function login() {

    window.Rollbar.configure({
      payload: {
        person: {
          email: jQuery('input[type="email"]')
            .val()
        }
      }
    });

    if (!loginValidate())
      return false;

    if (Public.user.Email !== undefined && Public.user.Password === undefined) {
      publicService.login(Public.user)
        .error(function (data, statusCode) {
          switch (statusCode) {
          case 422:
            window.Rollbar.critical("Unprocessable Entity Login", data);
            Public.hidePassword = false;
            Public.error = false;
            break;
          case 401:
            if (data == "User is inactive")
              enableError("Confirme seu email primeiro.");
            if (data == "User not validate email")
              enableError("Cadastro não concluído. Mandamos um link de acesso para seu email!");
            else
              enableError('Email não encontrado.');
            break;
          case 302:
            Public.hidePassword = false;
            Public.error = false;
            Public.Photo = data.Photo;
            Public.Name = data.Name;
            setPasswordFocus();
            break;
          case 400:
            enableError("Email digitado é inválido.");
            break;
          default:
            window.Rollbar.critical("HTTP login error", data);
            enableError('Erro de comunicação.');
          }
        });
    } else if (Public.user.Email !== undefined && Public.user.Password !== undefined) {
      publicService.login(Public.user)
        .success(function (data) {
          if (!publicService.userLoginManager(data))
            enableError('Email não encontrado.');
        })
        .error(function (data, statusCode) {
          switch (statusCode) {
          case 401:
            enableError('Senha incorreta.');
            break;
          case 400:
            enableError('Email não informado.');
            break;
          }
        });
    }
  }

  function enableError(message) {
    Public.error = true;
    Public.errorMessage = message;
  }

  function loginValidate() {
    if (!Public.user.Email) {
      enableError('Informe um email válido.');
      return false;
    }

    return true;
  }

  function forgotPassword() {
    publicService.forgotPassword(Public.user.Email)
      .success(function (data) {
        Public.forgotMessage = 'Um email foi enviado para <strong>' + Public.user.Email + '</strong> com as instruções para recuperação da senha.';
        window.$jQuery('#text-forgot')
          .html(Public.forgotMessage);
      });
  }

  function validadeHash() {
    var data = getRecoverPasswordData();

    if (!data.email) {
      Public.forgotError = true;
      Public.forgotMessage = 'Informe seu email.';
      return false;
    }

    publicService.validateHash(data)
      .success(function (data) {
        Public.forgotError = false;
        Public.newPassword = true;
      })
      .error(function (data, statusCode) {
        Public.forgotError = true;
        if (statusCode === 401)
          Public.forgotMessage = 'Email não encontrado.';
        else
          Public.forgotMessage = 'Algo de errado acorreu.';
      });
  }

  function recoverPassword() {
    var data = getRecoverPasswordData();
    data.password = Public.Password;

    if (!data.password) {
      Public.forgotError = true;
      Public.forgotMessage = 'Informe sua nova senha.';
      return false;
    }

    publicService.recoverPassword(data)
      .success(function (data) {
        Public.forgotError = false;
        Public.success = true;
      });
  }

  function getRecoverPasswordData() {
    var url = window.location.href;
    var hash = url.substr(url.indexOf('hash') + 5);
    var email = Public.email;
    var data = {
      email: email,
      hash: hash
    };

    return data;
  }

  function setPasswordFocus() {
    $timeout(function () {
      window.$jQuery('#user-password')
        .focus();
      window.$jQuery("input[type='Password']")[0].focus();
    }, 0, false);
  }

}
