publicCutTextDirective.$inject = [
  "comumConfig",
];

function publicCutTextDirective(comumConfig) {
  return {
    restric: "A",
    link: function ($scope, element, elemAttr) {
      var text = element.context.textContent;
      text = comumConfig.cortarTexto(text, 5);
    },
  };
}
