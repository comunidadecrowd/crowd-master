publicService.$inject = ["$http", "$localStorage", "$rootScope"];

function publicService($http, $localStorage, $rootScope) {
  var service = this;

  service.login = login;
  service.logout = logout;
  service.getCategories = getCategories;
  service.getCountries = getCountries;
  service.getStates = getStates;
  service.getCities = getCities;
  service.getSegments = getSegments;
  service.getAvailabilities = getAvailabilities;
  service.getSkills = getSkills;
  service.userLoginManager = userLoginManager;
  service.forgotPassword = forgotPassword;
  service.validateHash = validateHash;
  service.recoverPassword = recoverPassword;
  service.saveCustomer = saveCustomer;
  service.freelancerInfoOnClick = freelancerInfoOnClick;

  service.baseUrlAPI = baseUrl();

  $rootScope.$on("logout", logout);

  var baseUrlAPI = service.baseUrlAPI;

  function baseUrl() {
    var absUrl = window.location.href;

    if (absUrl.indexOf("homolog") > -1 || absUrl.indexOf("localhost") > -1)
      return "http://homolog.api.comunidadecrowd.com.br";

    if (absUrl.indexOf("beta") > -1 || absUrl.indexOf("anne") > -1)
      return "http://beta.api.comunidadecrowd.com.br";

    return "//api.comunidadecrowd.com.br";
  }

  function login(userData) {
    return $http.post(baseUrlAPI + "/User/Login", userData);
  }

  function logout() {
    delete $localStorage.user;
    delete $localStorage.token;
    delete $localStorage.menu;
    window.Intercom('shutdown');
    $http.defaults.headers.common.Authorization = "";
    window.location = "https://crowd.br.com/";
  }

  function freelancerInfoOnClick(data) {
    return $http.post(baseUrlAPI + "/Customer/TrackField", data);
  }

  function getCategories() {
    return $http.get(baseUrlAPI + "/Common/Categories");
  }

  function getCountries() {
    return $http.get("/json/paises.json");
  }

  function getStates() {
    return $http.get(baseUrlAPI + "/Common/States");
  }

  function getCities(stateId) {
    return $http.get(baseUrlAPI + "/Common/Cities?state_id=" + stateId);
  }

  function getSegments() {
    return $http.get(baseUrlAPI + "/Common/Segments");
  }

  function getAvailabilities() {
    return [
      {
        Id: 1,
        Name: "Integral",
      },
      {
        Id: 2,
        Name: "Parcial",
      },
    ];
  }

  function getSkills() {
    return $http.get(baseUrlAPI + "/Common/Skills");
  }

  function userLoginManager(data) {
    if (data !== undefined || data) {
      if (data.loggedUser && data.token) {
        $localStorage.user = data.loggedUser;

        //NOTE: RETROCOMPATIBILIDADE COM LOGIN 1.0
        if (data.redirect) $localStorage.user.defaultURL = data.redirect;
        if (data.menu) $localStorage.menu = data.menu;
        
        //
        $localStorage.token = data.token;
        $localStorage.menu = data.menu;

        $http.defaults.headers.common.Authorization = "Bearer " + data.token;

        // LOGIN 2.0
        if (data.loggedUser.defaultURL) window.location = data.loggedUser.defaultURL;

        //NOTE: ESTE REDIRECT VINHA DO WEBSERVICE
        if (data.loggedUser.Role != 2) {
          //Caso não seja freelancer
          window.location = "portal/#/profissionais";
        } else {
          //Se a role for 2, ele é freelancer
          window.location = "freelancer/#/briefings";
        }

        return true;
      }

      return false;
    }

    return false;
  }

  function forgotPassword(email) {
    return $http.post(baseUrlAPI + "/User/ForgotPassword?email=" + email);
  }

  function validateHash(data) {
    return $http.post(
      baseUrlAPI +
        "/User/ValidateHash?email=" +
        data.email +
        "&hash=" +
        data.hash
    );
  }

  function recoverPassword(data) {
    return $http.post(
      baseUrlAPI +
        "/User/ChangePassword?email=" +
        data.email +
        "&password=" +
        data.password +
        "&hash=" +
        data.hash
    );
  }

  function saveCustomer(data) {
    return $http.post(baseUrlAPI + '/Customer/PreCadastro', data);
  }
}
