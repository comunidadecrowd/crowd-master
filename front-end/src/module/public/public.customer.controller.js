publicCustomerController.$inject = [
  "publicService",
  "$window",
  "$timeout"
];

function publicCustomerController(publicService, $window, $timeout) {
  var Public = this;
  var storageState = {};
  var storageCity = {};

  // Private

  // Public
  Public.success = false;
  Public.error = false;
  Public.errorMessage = null;

  Public.states = [];
  Public.cities = [];

  Public.customer = {};

  Public.register = register;
  Public.loadCities = loadCities;
  Public.submitted = false;
  self.isBoleto = false;
  self.isCC = false;
  self.isConfirmed = false;
  self.acceptTerms = false;
  self.boletoForm = {};
  self.ccForm = {};

  self.setPaymentMethod = function setPaymentMethod (method) {
    console.log(method);
    var tpod = new Date()
      .getUTCDate();
    var month = new Date()
      .getMonth() + 1;
    var year = new Date()
      .getFullYear();
    var mili = new Date()
      .getMilliseconds();
    if (method == "CC") {
      self.isCC = true;
      self.isBoleto = false;
    } else {
      self.isBoleto = true;
      self.isCC = false;
    }
  };

  self.validationOptions = {
    maxLength: 34
  };

  function init() {
    dataConfig(publicService.getStates(), "states");

    if (document.URL.indexOf("?") > -1) {
      var landing = document.URL.substring(document.URL.indexOf("?") + 1);
      landing = JSON.parse(
        '{"' +
        decodeURIComponent(landing)
          .replace(/"/g, '\\"')
          .replace(/&/g, '","')
          .replace(/=/g, '":"')
          .replace(/([+])/g, " ") +
        '"}'
      );
      landing = _.map(landing);

      if (document.URL.indexOf('seu_nome') > -1) {
        Public.customer["TRADE"] = landing[0];
        Public.customer["NameResponsible"] = landing[1];
        Public.customer["Phone"] = landing[2];
        Public.customer["EmailResponsible"] = landing[3];
        Public.customer["PlanType"] = 3;
        Public.customer["Referrer"] = document.referrer;

      }

      $timeout(function () {
        //NOTE: NG-MODEL ainda nÃ£o renderizou atÃ© aqui, precisa aguardar

        fbq("track", "CompleteRegistration", {
          value: document.referrer
        });

        switch (document.referrer) {
          case "https://somos.crowd.br.com/100-startups":
            jQuery("body")
              .append(
              '<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/862100472/?label=LxlACJORuXQQ-LeKmwM&amp;guid=ON&amp;script=0"/>'
              );
            break;
          case "https://somos.crowd.br.com/agencias":
            jQuery("body")
              .append(
              '<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/862100472/?label=p_ZBCJOBoXQQ-LeKmwM&amp;guid=ON&amp;script=0"/>'
              );
            break;
          default:
        }

        document.querySelector("#cadastro")
          .scrollIntoView({
            behavior: "smooth"
          });

        jQuery("input")
          .filter(function () {
            return this.value.length !== 0;
          })
          .css("border-color", "#4acfa4");
      }, 500);
    }
  }

  Public.setPlan = function setPlan (plan) {
    if (plan == 1) {
      Public.plan = {
        name: "START",
        id: 1,
        period: "Anual",
        price: {
          ui: "R$ 0",
          data: 0
        },
        details: {
          first: "1 usuário;",
          second: "Comunicação somente pela plataforma Crowd;",
          third: "Acesso restrito somente a profissionais não avaliados;",
          fourth: "Suporte por email e chat."
        }
      };
    }
    if (plan == 2) {
      Public.plan = {
        name: "ONE",
        id: 2,
        period: "Anual",
        price: {
          ui: "R$ 150/mês",
          data: 150
        },
        details: {
          first: "1 usuário;",
          second: "Converse com os profissionais por e-mail, skype e telefone;",
          third: "Contrate os profissionais 5 estrelas da Crowd;",
          fourth: "Suporte por email, chat, skype e telefone."
        }
      };
    }
    if (plan == 3) {
      Public.plan = {
        name: "PRO",
        id: 3,
        period: "Anual",
        price: {
          ui: "R$ 500/mÃªs",
          data: 500
        },
        details: {
          first: "Até 5 usuários;",
          second: "Converse com os profissionais por e-mail, skype e telefone;",
          third: "Contrate os profissionais 5 estrelas da Crowd;",
          fourth: "Suporte completo + presencial em São Paulo."
        }
      };
    }
    if (plan == 4) {
      Public.plan = {
        name: "ENTERPRISE",
        id: 4,
        period: "Anual",
        price: {
          ui: "R$ 1500/mês",
          data: 1500
        },
        details: {
          first: "Usuários ilimitados;",
          second: "Converse com os profissionais por e-mail, skype e telefone;",
          third: "Contrate os profissionais 5 estrelas da Crowd;",
          fourth: "Suporte completo + presencial em SÃ£o Paulo;",
          fifth: "Gerente de contas compartilhado."
        }
      };
    }
  };

  function register() {

    if (!customerValidate()) return false;

    Public.submitted = true;

    window.Rollbar.configure({
      payload: {
        person: {
          Empresa: Public.customer.TRADE,
          Name: Public.customer.NameResponsible,
          Email: Public.customer.EmailResponsible,
          Telefone: Public.customer.Phone,
          PlanType: Number(Public.customer.PlanType)
        }
      }
    });

    // delete Public.customer.AcceptTerms;
    publicService
      .saveCustomer({
        Name: Public.customer.NameResponsible,
        Trade: Public.customer.TRADE,
        Phone: Public.customer.Phone,
        Email: Public.customer.EmailResponsible,
        PlanType: Number(Public.customer.PlanType),
        Referrer: Public.customer.Referrer || "",
        PromoCode: Public.customer.PromoCode || "",
        AcceptTerms: Public.customer.AcceptTerms ? 1 : 0
      })
      .success(function (data, status) {
        if (status != 200) {
          Public.submitted = false;
          window.Rollbar.critical(
            "publicCustomer.controller > saveCustomer", data
          );
          Public.errorMessage = 'Meu servidor disse: "' + data + '". Está¡ tudo bem?';
          Public.error = true;
          Public.success = false;
          Public.submitted = false;

        }

        if (data) {
          window.$jQuery("#frmCustomer")
            .slideToggle();
          console.debug(data);
          Public.success = true;
          Public.error = false;

          $window.ga("send", "event", "Empresa", "Lead Cadastrado");
          window.localStorage.setItem('ngStorage-user', JSON.stringify(data.loggedUser));
          window.localStorage.setItem('ngStorage-token', JSON.stringify(data.token));

          setTimeout(function () {
            if (!/beta?/.test(location.href)) {
              window.location = "/portal/#/company-profile";
            } else {
              window.location = "/portal/#/company-profile";
            }
          }, 1000);

        } else {
          console.log("error message: ", data);
          Public.errorMessage = 'Meu servidor disse: "' + data + '". Está¡ tudo bem?';
          window.Intercom('showNewMessage', 'Olá¡, tive o problema "' + data + '" tentando cadastrar minha empresa. Pode me ajudar?');
          Public.error = true;
          Public.success = false;
          Public.submitted = false;
          addFieldsValues();
        }
      }).error(function (data, statusCode) {
        // window.Intercom('showNewMessage', 'OlÃ¡, tive o problema "' + data + '" tentando cadastrar minha empresa. Pode me ajudar?');
        window.Rollbar.critical("publicCustomer Controller > register", data);
        if (data == 'User already exist') {
          Public.error = true;
          Public.errorMessage = 'Email já utilizado. Você esqueceu a sua senha?';
        } else {
          Public.error = true;
          Public.errorMessage = 'Meu servidor disse: "' + data + '". Está¡ tudo bem?';
        }
        addFieldsValues();
      });
  }

  function addFieldsValues() {
    Public.customer.State = storageState;
    Public.customer.City = storageCity;
  }

  function dataConfig(promisse, obj) {
    promisse
      .success(function (data) {
        switch (obj) {
          case "states":
            Public.states = data;
            break;
          case "cities":
            Public.cities = data;
            break;
        }
      })
      .error(function (data, statusCode) {
        throw "Error to load " + obj + "\n" + statusCode;
      });
  }

  function loadCities() {
    var state = Public.customer.State;
    dataConfig(publicService.getCities(state.Id), "cities");
  }

  function customerValidate() {
    //  if (!Public.customer.Name)
    //    return enableValidation('RazÃ£o Social.');

    /*     if (!Public.customer.State) {
      return enableValidation(false, 'Selecione o Estado.');
    } else {
      storageState = Public.customer.State;
      Public.customer.State = Public.customer.State.Id;
    } */

    /*     if (!Public.customer.City) {
      return enableValidation(false, 'Selecione a Cidade.');
    } else {
      storageCity = Public.customer.City;
      Public.customer.City = Public.customer.City.Id;
    } */

    /*     if (!Public.customer.Address)
      return enableValidation('EndereÃ§o');

    if (!Public.customer.Number)
      return enableValidation('NÂº'); */

    if (!Public.customer.Phone)
      return enableValidation("Telefone", "Preencha seu telefone");

    if (!Public.customer.NameResponsible)
      return enableValidation("Nome responsÃ¡vel.", "Preencha o nome do responsÃ¡vel");

    if (!Public.customer.EmailResponsible)
      return enableValidation("Email.", "Preencha o campo de");

    if (!Public.customer.PlanType)
      return enableValidation("Tipo de plano", "Por favor selecione um plano");

    //  if (!Public.customer.QtyPersons)
    //    return enableValidation('Quantidade de pessoas.');

    if (!Public.customer.AcceptTerms)
      return enableValidation(false, "VocÃª precisa aceitar os Termos de uso");

    return true;
  }

  function enableValidation(fieldName, message) {
    if (message === undefined) message = "Preencha o campo " + fieldName + ".";

    Public.errorMessage = "Seu cadastro possui campos invÃ¡lidos";
    Public.error = true;
    console.debug(fieldName);

    return false;
  }

  // Calls
  init();
}
