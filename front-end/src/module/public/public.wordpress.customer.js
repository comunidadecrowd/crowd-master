/**
 * Script para cuidar do pré cadastro de Customer
 * @requires {Contact7} as wpcf7mailfailed
 * @requires {jQuery} as jQuery
 * @requires {Intercom} as window.Intercom
 * @requires {GoogleAnalytics} as window.ga
 * @requires {Rollbar} as window.Rollbar
 */

(function init() {
  "use strict";


  /** Este cara ouve esperando arquivo public.wordpress.login.js pela confirmação de ter salvo o localStorage  */
  window.addEventListener("message", receiveMessage, false);

  /**
   * ISTO DEVE IR NA PÁGINA DO WORDPRESS QUE FARÁ O LOGIN
   * @param {object} event 
   * @returns falso se a resposta não for da plataforma
   */
  function receiveMessage(event) {
    if (event.origin !== "https://app.crowd.br.com") // SÓ ACEITO SE FOR DA PLATAFORMA
      return false;
    
    window.ga("send", "event", "Login", "Login atavés do site");
    window.location = "https://app.crowd.br.com" + window.data.loggedUser.defaultURL;

  }

  /**
   * Começo a ouvir pelo evento que é disparado quando o email não é enviado
   * @description Depois de validar o formulário, o plugin ContactForm7 do Wordpress tenta enviar o email, e como não DEVE estar configurado, ele retorna este evento
   */
  document.addEventListener(
    "wpcf7mailfailed",
    function (event) {
      /** @private {array} Conjunto dos inputs e seus valores */
      var inputs = event.detail.inputs;

      /** @private {string} Referrer de onde o usuário veio para o formulário de cadastro  */
      var docRef = document.referrer;

      /** @private {string} Endereço do endpoint */
      var url = "https://api.comunidadecrowd.com.br/Customer/PreCadastro";

      /** POST do formulário para o endpoint */
      jQuery.post(
        url, {
          Trade: inputs[0].value || "",
          Phone: inputs[1].value || "",
          Name: inputs[2].value || "",
          Email: inputs[3].value || "",
          PlanType: 1, // NOTE: TODO MUNDO AGORA COMEÇA NO PLANO FREE
          Referrer: docRef || "",
          // PromoCode: prmCod || "", NOTE: NÃO HÁ AINDA CÓDIGOS PROMOCIONAIS
          AcceptTerms: inputs[4].value ? 1 : 0, // FIXME: INCONSISTÊNCIA NO ENDPOINT true false PARA FREELANCER, 1 0 PARA CUSTOMER
        },
        function success(data, status, hxr) {
          console.log("success(data)", data);
          console.log("success(status)", hxr.status);

          if (hxr.status != 200) {
            window.Rollbar.critical(
              "publicCustomer.controller > saveCustomer",
              data
            );
            jQuery(".wpcf7-response-output")
              .html(
              'Meu servidor respondeu: "' + data.responseText + '". Está tudo bem?'
              );
            jQuery(".wpcf7-response-output")
              .style("display", "block");
            window.Intercom(
              "showNewMessage",
              'Olá, tive o problema "' + data.responseText + '" tentando cadastrar minha empresa. Pode me ajudar?'
            );
            return false;
          }

          if (hxr.status == 200) {

            window.data = data;
            document.getElementById('iframe').contentWindow.postMessage(JSON.stringify(data), "https://app.crowd.br.com");
            
          } else {
            window.Rollbar.critical(
              "publicCustomer.controller > saveCustomer",
              data
            );
            jQuery(".wpcf7-response-output")
              .html(
              'Meu servidor respondeu: "' + data.responseText + '". Está tudo bem?'
              );
            jQuery(".wpcf7-response-output")
              .style("display", "block");
            window.Intercom(
              "showNewMessage",
              'Olá, tive o problema "' + data.responseText + '" tentando cadastrar minha empresa. Pode me ajudar?'
            );
            return false;
          }
        })
        .fail(function (data, status, xhr) {
          window.Rollbar.critical(
            "publicCustomer.controller > saveCustomer",
            data
          );
          jQuery(".wpcf7-response-output")
            .html(
            'Meu servidor respondeu: "' + data.responseText + '". Está tudo bem?'
            );
          jQuery(".wpcf7-response-output")
            .style("display", "block");
          window.Intercom(
            "showNewMessage",
            'Olá, tive o problema "' + data.responseText + '" tentando cadastrar minha empresa. Pode me ajudar?'
          );
          return false;
        });

      console.log(event);
    },
    false
  );
})();
