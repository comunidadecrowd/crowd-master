professionalRoute.$inject = ['$stateProvider', '$urlRouterProvider'];

function professionalRoute($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/index');

    $stateProvider

    // Page Prfessional Profile
        .state('profile', {
        url: "/profile",
        views: {
            'pageTitle': {
                template: 'Perfil'
            },
            'pageContent': {
                template: '<professional-profile-component get-skills="$resolve.getSkills" get-segments="$resolve.getSegments" get-categories="$resolve.getCategories" get-profile="$resolve.getProfile" get-countries="$resolve.getCountries" get-states="$resolve.getStates"></professional-profile-component>'
            },
            'visualizar-perfil-profissional@profile': {
                template: '<comum-visualizar-perfil-profissional-component></comum-visualizar-perfil-profissional-component>'
            }
        },
        resolve: {
            getProfile: professionalGetProfile,
            getCountries: professionalGetCountries,
            getStates: professionalGetStates,
            getCategories: professionalGetCategories,
            getSegments: professionalGetSegments,
            getSkills: professionalGetSkills,
            updateNotifications: updateNotifications,
            // intercom: window.Intercom("update")
        }
    })

    // Page Briefings
    .state('briefing', {
        url: "/briefings",
        views: {
            'pageTitle': {
                template: 'Briefings'
            },
            'pageContent': {
                template: '<professional-briefing-component get-briefings="$resolve.getBriefings"></professional-briefing-component>'
            },
            'briefing-details@briefing': {
                template: '<comum-briefing-details></comum-briefing-details>'
            },
            'tutorial@briefing': {
                template: '<professional-tutorial-component></professional-tutorial-component>'
            },
            "visualizar-perfil-profissional@briefing": { 
               template: "<comum-visualizar-perfil-profissional-component></comum-visualizar-perfil-profissional-component>" 
            }
        },
        resolve: {
            getBriefings: professionalGetBriefings,
            updateNotifications: updateNotifications,
            // intercom: window.Intercom("update")
        }
    })
    // Página Projetos
    .state("project", {
        url: "/project/:id",
        views: {
            "pageTitle": {
                template: "Projeto"
            },
            "pageContent": {
                template: "<comum-project-component></comum-project-component>"
            },
            "briefing-details@project": {
                template: "<comum-briefing-details ></comum-briefing-details>"
            },
            "visualizar-perfil-profissional@project": {
                template: "<comum-visualizar-perfil-profissional-component></comum-visualizar-perfil-profissional-component>"
            }
        },
        resolve: {
            getBriefings: professionalGetBriefings,
            getProfissionais: professionalGetProfile,
            getCategorias: professionalGetCategories,
            getSkills: professionalGetSkills,
            getUsuarios: professionalGetStates,
            getEstados: professionalGetStates,
            updateNotifications: updateNotifications,
            // intercom: window.Intercom("update")
        }

    })
    // Página Projetos
    .state("projects", {
        url: "/projects",
        views: {
            "pageTitle": {
                template: "Projetos"
            },
            "pageContent": {
                template: "<comum-project-component></comum-project-component>"
            },
            "briefing-details@projects": {
                template: "<comum-briefing-details></comum-briefing-details>"
            },
            "visualizar-perfil-profissional@projects": {
                template: "<comum-visualizar-perfil-profissional-component></comum-visualizar-perfil-profissional-component>"
            }
        },
        resolve: {
            getBriefings: professionalGetBriefings,
            getProfissionais: professionalGetProfile,
            getCategorias: professionalGetCategories,
            getSkills: professionalGetSkills,
            getUsuarios: professionalGetStates,
            getEstados: professionalGetStates,
            updateNotifications: updateNotifications,
            // intercom: window.Intercom("update")
        }

    })
    .state("freelancerReports", {
        url: "/freelancer-reports",
        views: {
            "pageTitle": {
                template: "Relatórios"
            },
            "pageContent": {
                template: "<freelancer-reports-component></freelancer-reports-component>"
            }
        }
    })


    // Logout
    .state('logout', {
        url: "/logout",
        views: {
            'pageLogout': {
                controller: comumController
            }
        },
        resolve: {
            // intercom: window.Intercom('shutdown')
        }
    });
}

/* ********** Route 'profile' ********** */
professionalGetProfile.$inject = ['professionalService'];

function professionalGetProfile(professionalService) {
    return professionalService.getProfile();
}

professionalGetCountries.$inject = ['publicService'];

function professionalGetCountries(publicService) {
    return publicService.getCountries();
}

professionalGetStates.$inject = ['publicService'];

function professionalGetStates(publicService) {
    return publicService.getStates();
}

professionalGetCategories.$inject = ['publicService'];

function professionalGetCategories(publicService) {
    return publicService.getCategories();
}

professionalGetSegments.$inject = ['publicService'];

function professionalGetSegments(publicService) {
    return publicService.getSegments();
}

professionalGetSkills.$inject = ['comumService'];

function professionalGetSkills(comumService) {
    return comumService.getSkills();
}

/* ********** Todas as Routes ********** */
updateNotifications.$inject = ["comumService"];

function updateNotifications(comumService) {
    "use strict";
    
    comumService.getNotificationProfessional();

    // comumService.getProjetos();
    // comumService.getProjetosArquivados();

}


/* ********** Route 'briefing' ********** */
professionalGetBriefings.$inject = ['comumService'];

function professionalGetBriefings(comumService) {
    return comumService.getBriefings();
}
