professionalService.$inject = [ '$http', 'comumConfig', 'comumService' ];
function professionalService( $http, comumConfig, comumService )
{
	// Public
	this.getProfile    = getProfile;
	this.savePortfolio = savePortfolio;	

	
	// Private
	var baseUrlAPI = comumConfig.baseUrlAPI();
	var user       = comumService.getUser();
	var code       = user.loggedUser.Code;

	
	// Functions	
	function getProfile()
	{   		
		return $http.get( baseUrlAPI + '/User/Get?code=' + encodeURIComponent(code));
	}

	function savePortfolio( data )
	{
		return $http.post( baseUrlAPI + '/Professional/AddPortfolio', data);
	}

}
