professionalBriefingController.$inject = [
  "comumConfig",
  "comumService",
  "$scope",
  "$timeout",
  "$rootScope"
];

function professionalBriefingController(
  comumConfig,
  comumService,
  $scope,
  $timeout,
  $rootScope
) {
  var self = this;

  // Private
  var user = {};
  var briefingIdAutoMessage = 1;

  // Public
  self.isOpen = false;
  self.briefings = {};
  self.slideOpen = slideOpen;
  self.openTutorial = openTutorial;
  self.isOpenTutorial = false;
  self.pageslideOnOpen = pageslideOnOpen;
  self.slideOpenProfile = slideOpenProfile;

  self.pageslideOnOpen = pageslideOnOpen;
  self.pageslideOnClose = pageslideOnClose;

  self.user = $rootScope.getUser();

  console.log(self.user);

  // Functions
  function init() {
    self.briefings = self.getBriefings.data.ativos;
    user = comumService.getUser();
    arrangeBriefingMessages();
  }

  function arrangeBriefingMessages() {
    console.log("self.briefings : ", self.briefings);

    self.briefings.forEach(function (briefingObj, index) {
      var messages = briefingObj.Messages;
      console.log("briefingObj : ", briefingObj);
      console.log("messages : ", messages);

      if (!messages || messages.length === 0 || messages[0] == null) {
        briefingObj.ShortText = comumConfig.cortarTexto(
          briefingObj.Excerpt,
          200
        );
        briefingObj.CreatedAt = briefingObj.CreatedAt;
      } else {
        var message = briefingObj.Messages[0];
        briefingObj.ShortText = comumConfig.cortarTexto(message.Text, 200);
        briefingObj.CreatedAt = message.CreatedAt;
      }
    });
  }

  function pageslideOnOpen() {
    comumConfig.fadeIn(".mask-content-pageslide");
  }

  function pageslideOnClose() {
    comumConfig.fadeOut(".mask-content-pageslide");
  }

  function slideOpen(briefingData) {
    briefingData.Read = true;

    if (!briefingData.Bulletin) {
      comumService
        .getBriefing(briefingData.Id)
        .success(function (data, status) {
          if (status != 200) {
            toaster.pop(
              "error",
              "Bad Server",
              "Não encontrei seus briefings ):"
            );
            window.Rollbar.critical(
              "sharedScreen.myProfile.controller > saveProfile",
              data
            );
            Intercom(
              "showNewMessage",
              "Olá, estou com problemas tentando abrir meus briefings"
            );
            return false;
          }

          self.isOpen = !self.isOpen;

          var briefing = data;
          var briefingMessages = briefing.Messages;
          briefing.offers = {
            Messages: briefingMessages,
            Freelancer: getDataProfessional()
          };
          briefing.professionalId = self.user.IdFreelancer;
          briefing.isProfessional = true;

          $scope.$broadcast("event:briefing-details", briefing);
          self.isOpen = true;
          comumConfig.scrollPageslideBriefing($timeout);

          var customerMessages = getCustomerMessages(briefingMessages);
          if (customerMessages.length !== 0) {
            var messagesId = customerMessages.filter(function (messagesObj) {
              return messagesObj.Read === false;
            });
            messagesId = comumConfig.getPropertyValueFromObjectArray(
              messagesId,
              "Id"
            );

            if (messagesId.length !== 0)
              messagesId.forEach(function (messageId) {
                comumService.readMessage(messageId)
                  .success(function (data) {
                    comumService.getProjetos(self.user.userId);
                    if (self.user.Role !== 2) {
                      comumService.getNotification(self.user.userId);
                    } else {
                      comumService.getNotificationProfessional(self.user.userId);
                    }
                    comumConfig.removeElement("#message-alert-" + briefing.Id);
                  });
              });
          }
        })
        .error(function (data) {
          toaster.pop("error", "Bad Server", "Não encontrei briefings ):");
          window.Rollbar.critical(
            "sharedScreen.myProfile.controller > saveProfile",
            data
          );
          Intercom(
            "showNewMessage",
            "Olá, tive um problema tentando abrir o briefing " + briefingData.Id
          );
        });
    } else {
      var briefing = briefingData;
      briefing.offers = {
        Messages: [],
        Freelancer: {}
      };

      $scope.$broadcast("event:briefing-details", briefing);
      comumService.readSystemMessage(briefingData.Id);

      self.isOpen = true;
    }
  }

  function slideOpenProfile() {
    //  comumConfig.fadeOut('.mask-content-pageslide');
    self.isOpenProfile = false;
    self.isOpen = true;
  }

  function getDataProfessional() {
    return {
      Photo: user.Photo,
      Name: user.nome,
      Id: user.IdFreelancer
    };
  }

  function getCustomerMessages(messagesArray) {
    if (messagesArray.length !== 0) {
      return messagesArray.filter(function (messagesObg) {
        return messagesObg.UserId && messagesObg.User;
      });
    }

    return [];
  }

  function openTutorial(id) {
    self.isOpenTutorial = true;
    $scope.$broadcast("event:tutorial-init", id);
  }

  // INIT
  init();
}

tutorialController.$inject = [
  "comumConfig",
  "comumService",
  "$scope",
  "$timeout"
];

function tutorialController(comumConfig, comumService, $scope, $timeout) {
  var self = this;
  var comoTeEncontram = {
    title: "Como Funciona",
    next: {
      title: "Conheça o método de pagamento da Crowd",
      href: "1"
    },
    content: '<h4>Busca</h4> <p> Após a criação do briefing, as empresas buscam as categorias de profissionais e habilidades ideais para a execução do projeto. É possível ainda filtrar os resultados de acordo com a disponibilidade, valores e localização desejados. <img src="../assets/img/exemplo-barra-busca.png"> Os primeiros perfis a surgirem são dos profissionais mais bem avaliados da plataforma, de acordo com o segmento procurado. <div> <img src="../assets/img/exemplo-tela-freelancer.png"> </div>Seu perfil é a maneira como as empresas conhecem seu trabalho e suas experiências. Por isso, não se esqueça de mantê-lo sempre atualizado. </p><br/> <br/> <h4>Recebeu um pedido de orçamento?</h4> <p> Para cada job, as empresas podem selecionar apenas até 10 profissionais para orçar um projeto. Não deixe de enviar uma proposta. Em caso de qualquer dúvida sobre o briefing, você pode trocar mensagens com a empresa até ter todas as informações que precisa. Se a sua proposta for aceita, você é notificado e suas tarefas são organizadas em etapas. </p>'
  };
  var pagamento = {
    title: "Como Funciona",
    next: {
      title: "Saiba como as empresas te encontram",
      href: "0"
    },
    content: '<h4>Pagamentos</h4> <p> A Crowd realiza pagamentos quinzenais, sempre referentes a tarefas aprovadas. Trabalhos aprovados entre os dias 16 e 31 são pagos no dia 15 do mês seguinte, e jobs aceitos entre os dias 1 e 15, no dia 30 do mesmo mês.</p><br/><br/><p>Todos os profissionais contratados emitem nota para a Crowd. Ainda não tem CNPJ? Recomendamos a formalização como MEI. <a href="http://api.comunidadecrowd.com.br/agencia/mei-vantagens-para-freelancers/" target="_blank">Saiba mais.</a></p><br/><br/><p>A partir de 2017, o equivalente a 8% do valor dos jobs fechados será recolhido a cada pagamento.</p>'
  };
  var tutoriais = [
    comoTeEncontram,
    pagamento
  ];
  self.tutorial = tutoriais[0];

  $scope.$on("event:tutorial-init", function (event, data) {
    self.tutorial = tutoriais[data];
    $timeout(
      function () {
        var height = window.$jQuery(".janela-profissional-briefing")
          .height();
        //window.$jQuery('.tutorial-900').animate({ scrollTop: height }, 1000);
      },
      500,
      false
    );
  });
}
