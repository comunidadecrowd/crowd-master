function professionalBriefingComponent()
{
	return {
		templateUrl: '../templates/professional/briefing/view/briefing.html',
		controller: professionalBriefingController,
		bindings: {
			getBriefings: '<'
		}
	};
}

function professionalTutorialComponent()
{
	return {
		templateUrl: '../templates/professional/briefing/view/tutorial.html',
		controller: tutorialController
	};
}
