professionalProfileController.$inject = [
  "$rootScope",
  "comumConfig",
  "ngDialog",
  "$scope",
  "$localStorage",
  "comumService",
  "publicService",
  "publicProfessionalService",
  "$timeout",
  "toaster",
  "moment",
  "$state",
];

function professionalProfileController(
  $rootScope,
  comumConfig,
  ngDialog,
  $scope,
  $localStorage,
  comumService,
  publicService,
  publicProfessionalService,
  $timeout,
  toaster,
  moment,
  $state
) {
  var self = this;
  // Private
  var skills = [];
  var http = "";
  var languagesLimit = 5;
  var regexLinks = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/
  var userInfo = JSON.parse(localStorage.getItem("ngStorage-user"));
  // Public
  self.baseUrlAPI = comumConfig.baseUrlAPI();

  self.isOpenProfile = false;
  self.profileImage = false;
  self.error = false;
  self.setCity = setCity;
  self.errorMessage = null;
  self.segmentLabel = null;
  self.qtdSegSelected = 0;

  // self.countries = [];
  self.states = [];
  self.cities = [];
  self.categories = [];
  self.queryCidade = "";
  self.getCities = getCities;
  self.segments = [];
  self.availabilities = [];
  self.idiomas = [];
  self.workExperiences = [];
  self.profileImage = [];
  self.editing = false;

  self.user = {};
  // self.getSkills = getSkills(getSkills);

  self.openDialog = openDialog;
  self.addWorkExperience = addWorkExperience; //FIXME: PROBLEMA COM DUPLA DECLARAÇÃO (1)
  self.slideOpenProfile = slideOpenProfile;
  //self.loadCities = loadCities;
  self.save = save;
  self.addSegment = addSegment;
  self.loadSkills = loadSkills;
  self.hidePopovers = hidePopovers;
  self.tagsLimit = tagsLimit;
  self.removeWorkExperience = removeWorkExperience;
  self.idiomasLimit = idiomasLimit;
  self.isLastExperience = isLastExperience;
  self.unableLastEndDate = unableLastEndDate;
  self.galeriaData = "Ola mundo!";
  $rootScope.showGaleria = false;
  self.profissional_sem_alteracao = undefined;

  $scope.closeDialog = closeDialog;
  $scope.showSalvarProfissional = showSalvarProfissional;
  

  // Functions
  function init() {
    self.professional = self.getProfile.data;
    // self.professional.Country = self.getCountries.data[0];
    self.professional.Skills = getSkills(self.professional.Skills);
    skills = getSkills(self.getSkills.data);
    self.provinces = getSkills(self.getSkills.data);
    // self.countries = self.getCountries.data;
    self.states = self.getStates.data;
    self.queryCidade = self.professional.City || "";
    // self.work = self.work.data;
    self.categories = self.getCategories.data;
    self.segments = self.getSegments.data;
    self.availabilities = publicService.getAvailabilities();
    self.user = comumService.getUser();

    disponibilidadeConfig(self.professional.Availability);
    addPopovers();
    setSelectedsSegments();
    getLanguagesStringToArray();
    setProfileImage();
    setWorkExperiences();
    enableSocialNetworks();

    jQuery(document.body).click(function(event) {
      self.editing = false;
    });

    var id2 = document.getElementById("end-date");

    setTimeout(function() {
      var currentJob = comumConfig.getElement("work-experience2");
      if (!self.workExperiences[0].EndDate) {
        comumConfig.attr(currentJob, "checked", "checked");
        document.formProfissional.elements["work-experience2"].checked = true;
        document.getElementById("end-date").disabled = true;
      }
    }, 500);
    self.profissional_sem_alteracao = Object.assign({}, self.professional);
    $scope.profissional_sem_alteracao = self.profissional_sem_alteracao
  }

  function showSalvarProfissional() {
    sem_alteracao = {}
    com_alteracao = {}
    Object.keys(self.profissional_sem_alteracao).sort().forEach(function(key) {
      sem_alteracao[key] = self.profissional_sem_alteracao[key];
    });
    Object.keys(self.professional).sort().forEach(function(key) {
      com_alteracao[key] = self.professional[key];
    });
    return JSON.stringify(sem_alteracao) != JSON.stringify(com_alteracao)
  }

  function setCity(city) {
    if (city) {
      return $http
        .get(comumConfig.baseUrlAPI() + "/Common/Cities?city=" + city.Name)
        .then(function(data) {
          self.queryCidade = data.data[0].Name;
        });
    } else {
      self.queryCidade = "";
    }
  }

  function getCities(event) {
    // event.preventDefault();
    // event.stopPropagation();

    self.gettingCities = true;
    var typingTimer; //timer identifier
    var doneTypingInterval = 4000; //time in ms, 5 second for example
    var $input = window.jQuery("#city-input");

    //on keyup, start the countdown
    $input.on("keyup", function() {
      clearTimeout(typingTimer);
      typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });

    //on keydown, clear the countdown
    $input.on("keydown", function() {
      clearTimeout(typingTimer);
    });

    //user is "finished typing," do something
    function doneTyping() {
      if (self.isClicking === true) {
        return false;
      }
      self.isClicking = true;
      /* window.jQuery('#city-input').autocomplete({
           showNoSuggestionNotice: true,
           noSuggestionNotice: 'Nenhum resultado encontrado',
           minChars: 4,
           serviceUrl: comumConfig.baseUrlAPI() + '/Common/Cities?city=' + self.queryCidade,
           transformResult: function(result) {
             self.gettingCities = false; 
             return {
               suggestions: window.jQuery.map(JSON.parse(result), function(dataItem) {
                 return { 
                   value: dataItem.Name, 
                   id: dataItem.Id
                 }
               })
             }
           },
           onSelect: function(suggestion) {
             self.professional.City = {
               Name: suggestion.value, 
               Id: suggestion.id
             };
           }
         }) */
      self.isClicking = false;
    }
  }

  function openDialog() {
    ngDialog.open({
      template: "./templates/view/dialog-image-profile.html",
      appendClassName: "ngdialog-custom",
      width: "50%",
      scope: $scope,
    });
  }

  function closeDialog() {
    ngDialog.close();
    self.imagemPerfil = true;
  }

  function slideOpenProfile(idBriefing, idProposta) {
    self.isOpenProfile = !self.isOpenProfile;

    if (self.disponibilidade)
      self.professional.Availability = self.disponibilidade.Id;

    // getImageToPreview();

    if (self.isOpenProfile) {
      comumConfig.fadeIn(".mask-content-pageslide");
      $scope.$broadcast("event:visualizar-perfil", self.professional);
    } else {
      comumConfig.fadeOut(".mask-content-pageslide");
    }
  }

  function getViewData() {
    var segments = comumConfig.getPropertyValueFromObjectArray(
      self.professional.Segments,
      "Name"
    );
    segments =

        segments.length === 0 ? "" :
        segments.join(", ");
    var skills = comumConfig.getPropertyValueFromObjectArray(
      self.professional.Skills,
      "text"
    );

    return {
      profissional: {
        imagem_perfil: comumConfig.attr("#imagem-perfil", "src"),
        nome: self.professional.Name,
        portfolio: self.professional.Portfolio,
        profissao: self.professional.Title,
        segmento: segments,
        skype: self.professional.Skype,
        alocado: self.professional.alocado,
        descricao: self.professional.Description,
        categoria: self.professional.Category.Name,
        email: self.professional.Email,
        //    imagemPerfil: self.professional.Photo,
        valor_hora: self.professional.Price,
        disponibilidade: self.disponibilidade.Name,
        tags: skills,

        endereco: {
          pais: "Brasil",
          estado: self.professional.State.UF,
          cidade: self.professional.City.Name,
        },
        telefone: getPhone(),
        idiomas: self.professional.Languages,
      },
      redes_sociais: {
        twitter: self.professional.TwitterUrl,
        facebook: self.professional.FacebookUrl,
        dribbble: self.professional.DribbbleUrl,
        instagram: self.professional.InstagramUrl,
        linkedin: self.professional.LinkedinUrl,
      },
      experiencia_profissional: self.workExperiences,
    };
  }

  function getImageToPreview() {
    var imgSrc = window.$jQuery("#imagem-perfil").attr("src");
    self.professional.Photo = imgSrc;
  }

  function getPhone() {
    return comumConfig.val("#professional-phone");
  }

  function addWorkExperience() {
    //FIXME: PROBLEMA COM DUPLA DECLARAÇÃO (2)
    self.workExperiences.unshift(getWordExperienceDataEmpty());
  }

  function getWordExperienceDataEmpty() {
    var user = comumService.getUser();

    return {
      Id: 0,
      FreelancerId: user.IdFreelancer,
      Role: null,
      Company: null,
      StartDate: null,
      EndDate: null,
      Description: null,
    };
  }

  function popoverOption(title, contentUrl) {
    return {
      title: title,
      url: contentUrl,
      placement: "bottom",
      animation: "fade",
    };
  }

  function addPopovers() {
    comumConfig.addPopover(
      ".twitter",
      popoverOption("Twitter", ".content-twitter")
    );
    comumConfig.addPopover(
      ".facebook",
      popoverOption("Facebook", ".content-facebook")
    );
    comumConfig.addPopover(
      ".dribbble",
      popoverOption("Dribbble", ".content-dribbble")
    );
    comumConfig.addPopover(
      ".instagram",
      popoverOption("Instagram", ".content-instagram")
    );
    comumConfig.addPopover(
      ".linkedin",
      popoverOption("Linkedin", ".content-linkedin")
    );
    comumConfig.addPopover(
      ".behance",
      popoverOption("Behance", ".content-behance")
    );
  }

  //function loadCities() {
  //  publicService.getCities(self.professional.State.Id)
  //    .success(function (data) {
  //      self.cities = data;
  //    });
  // }

  function getImageToPreview() {
    var imgSrc = $jQuery("#imagem-perfil").attr("src");
    self.professional.Photo = imgSrc;
  }

  function getImageToPreview() {
    var imgSrc = window.$jQuery("#imagem-perfil").attr("src");
    self.professional.Photo = imgSrc;
  }

  function getPhone() {
    return comumConfig.val("#professional-phone");
  }

  function addWorkExperience() {
    //FIXME: PROBLEMA COM DUPLA DECLARAÇÃO (3)
    var id2 = $jQuery("[id=end-date]:eq(1)");
    self.workExperiences.unshift(getWordExperienceDataEmpty());

    $timeout(function() {
      jQuery(".last-exp-date")
        .filter(function() {
          return !this.value;
        })
        .addClass("error");
    }, 500);
  }

  function getWordExperienceDataEmpty() {
    var user = comumService.getUser();

    return {
      Id: 0,
      FreelancerId: user.IdFreelancer,
      Role: null,
      Company: null,
      StartDate: null,
      EndDate: null,
      Description: null,
    };
  }

  function popoverOption(title, contentUrl) {
    return {
      title: title,
      url: contentUrl,
      placement: "bottom",
      animation: "fade",
    };
  }

  function addPopovers() {
    comumConfig.addPopover(
      ".twitter",
      popoverOption("Twitter", ".content-twitter")
    );
    comumConfig.addPopover(
      ".facebook",
      popoverOption("Facebook", ".content-facebook")
    );
    comumConfig.addPopover(
      ".dribbble",
      popoverOption("Dribbble", ".content-dribbble")
    );
    comumConfig.addPopover(
      ".instagram",
      popoverOption("Instagram", ".content-instagram")
    );
    comumConfig.addPopover(
      ".linkedin",
      popoverOption("Linkedin", ".content-linkedin")
    );
    comumConfig.addPopover(
      ".behance",
      popoverOption("Behance", ".content-behance")
    );
  }

  function getImageToPreview() {
    var imgSrc = $jQuery("#imagem-perfil").attr("src");
    self.professional.Photo = imgSrc;
  }

  function disponibilidadeConfig(disponibilidade) {
    if (disponibilidade == 1)
      self.disponibilidade = publicService.getAvailabilities()[0];
    else self.disponibilidade = publicService.getAvailabilities()[1];
  }

  function save($event) {
    checkNetworkHttpValue();
    setLanguagesArrayToString();
    getImageToPreview();
    getImageProfile();
    validateProfessional();

    console.debug("óbject", self.professional);

    if (!validateProfessional()) return false;

    $event.preventDefault();
    // var countryRecord = self.professional.Country;
    var photoRecord = self.professional.Photo;
    self.professional.Availability = self.disponibilidade;
    self.professional.Available = self.professional.Active;
    console.debug("query", self.queryCidade);
    if (self.queryCidade) {
      self.professional.City = {
        Name: self.queryCidade.originalObject.Name,
        Id: self.queryCidade.originalObject.Id,
      };
    }

    publicProfessionalService.set(self.professional);
    var promisse = publicProfessionalService.save();

    if (!promisse.success) {
      self.error = true;
      self.errorMessage = promisse.message;
    } else {
      comumConfig.blockScreen();
      promisse
        .success(function(data, status) {
          //NOTE: AQUI É SUCCESS, NÃO USAR data.data

          if (status == 412) {
            var errMsg = data[0].Message;
            self.invalidField = data[0].Id;
            toaster.pop(
              "success",
              "Falta pouco para ativar seu cadastro:",
              errMsg
            );
            comumConfig.unblockScreen();
            enableSocialNetworks();
            // self.professional.Country = countryRecord;
            return false;
          }

          if (status != 200 && status != 400) {
            toaster.pop(
              "error",
              "Bad Server",
              "Não consegui salvar seu perfil"
            );
            window.Rollbar.critical(
              "professional.profile.controller > save()",
              data
            );
            Intercom(
              "showNewMessage",
              "Olá, tive um problema tentando salvar o meu perfil, poderia me ajudar?"
            );
            return false;
          }

          if (data.loggedUser) {
            window.localStorage.setItem(
              "ngStorage-user",
              JSON.stringify(data.loggedUser)
            );
            window.localStorage.setItem(
              "ngStorage-token",
              JSON.stringify(data.token)
            );
          }

          if (status == 200) {
            //FIXME: FALTA VALIDAR QUE SE ELE AINDA É PROSPECT TRUE
            $rootScope.isProspect = false;
            $state.go("briefing");
          }

          toaster.pop("success", "", "Perfil atualizado!");

          publicProfessionalService.set(data);

          comumConfig.unblockScreen();

          comumService.setUserPhoto(data.loggedUser.Photo);
          comumService.loadImagemPerfil();

          if (!self.professional.Photo) {
            self.professional.Photo = photoRecord;
          }

          enableSocialNetworks();
          // self.professional.Country = countryRecord;
          toaster.pop("success", "Obrigado!", "Perfil atualizado.");
          window.localStorage.setItem("userPic", self.user.Photo);
        })
        .error(function(data, statusCode) {
          comumConfig.unblockScreen();
          toaster.pop("error", "Bad Server", "Não consegui salvar seu perfil");
          window.Rollbar.critical(
            "professional.profile.controller > save()",
            data
          ); //FIXME: TITULO ERRADO PARA O ROLLBAR
          Intercom(
            "showNewMessage",
            "Olá, tive um problema tentando salvar o meu perfil"
          );
          return false;
        });
    }
  }

  function getSkills(dataSkills) {
    var arraySkills = [];

    dataSkills.forEach(function(skill, index) {
      arraySkills.push({
        id: skill.Id,
        text: skill.Name,
      });
    });

    return arraySkills;
  }

  function tagsLimit() {
    var tags = self.professional.Skills;
    return publicProfessionalService.tagsLimit(tags);
  }

  function addSegment(segment) {
    publicProfessionalService.addSegment(segment.Id);
    setSegmentsObjects(segment);
    configSegmentLabel();
  }

  function setSelectedsSegments() {
    var segments = comumConfig.getPropertyValueFromObjectArray(
      self.professional.Segments,
      "Id"
    );

    publicProfessionalService.setSegmentsSelecteds(segments);
    configSegmentLabel();

    if (segments.length > 0) {
      var configCheckboxs = function() { // FIXME: DEVERIAMOS UTILIZAR CALLBACKS ADEQUADOS
        if (segments.length >= 3) {
          var checkboxs = comumConfig.getElement(".checkbox-segment");

          angular.forEach(checkboxs, function(element) {
            comumConfig.attr(element, "disabled", true);
          });

          segments.forEach(function(id) {
            comumConfig.removeAttr("#checkbox-" + id, "disabled");
          });
        }

        segments.forEach(function(id) {
          comumConfig.attr("#checkbox-" + id, "checked", true);
        });
      };

      $timeout(configCheckboxs, 2000, false);
    }
  }

  function configSegmentLabel() {
    self.segmentLabel = publicProfessionalService.configSegmentLabel(
      self.professional.Segments
    );
  }

  function setProfileImage() {
    window.Tipped.create("input");
    window.Tipped.create("textarea");
    window.Tipped.create("select");
    window.Tipped.create("ul");
    if (self.professional.Photo) {
      self.profileImage = true;

      var image = "../images/user.png";
      if (
        self.professional.Photo != null ||
        self.professional.Photo != "" ||
        self.professional.Photo != "null"
      )
        self.professional.Photo =
          comumConfig.baseUrlAPI() + self.professional.Photo;
      comumConfig.attr("#imagem-perfil", "src", self.professional.Photo);
    }
  }

  function loadSkills(query) {
    return comumConfig.searchTextInObjectArray(skills, "text", query);
  }

  function setLanguagesArrayToString() {
    self.professional.Languages = comumConfig
      .getPropertyValueFromObjectArray(self.idiomas, "text")
      .join(", ");
  }

  function getLanguagesStringToArray() {
    if (self.professional.Languages)
      self.idiomas = self.professional.Languages.split(",");
  }

  function getImageProfile() {
    var imgSrc = window.val || self.professional.Photo;
    if (userInfo && !imgSrc) {
      self.professional.Photo = comumConfig.baseUrlAPI() + userInfo.Photo
    }

    if (imgSrc) {
      self.professional.PhotoBase64 = imgSrc.replace(
        /^data:image\/[a-z]+;base64,/,
        ""
      );
      var extension = String();
      var lowerCase = imgSrc.toLowerCase();
      if (lowerCase.indexOf("png") !== -1) extension = "png";
      else if (
        lowerCase.indexOf("jpg") !== -1 ||
        lowerCase.indexOf("jpeg") !== -1
      )
        extension = "jpg";
      else if (lowerCase.indexOf("gif") !== -1) extension = "gif";
      else extension = "tiff";
      self.professional.PhotoExtension = extension;
      if (imgSrc.indexOf("Content") > -1) {
        self.professional.PhotoBase64 = "";
        self.professional.Photo =
          comumConfig.baseUrlAPI() + self.professional.Photo;
      }
    }
  }

  function setSegmentsObjects(segment) {
    // self.professional.segmentsIds.push(segment.Id);
    var result = comumConfig.searchObjectInArray(
      self.professional.Segments,
      "Id",
      segment.Id
    );
    if (!result) self.professional.Segments.push(segment);
    else
      self.professional.Segments = comumConfig.removeObjectFromArray(
        self.professional.Segments,
        "Id",
        segment.Id
      );
  }

  function setWorkExperiences() {
    if (self.professional.Experiences.length === 0)
      self.workExperiences.push(getWordExperienceDataEmpty());
    else self.workExperiences = self.professional.Experiences.reverse();

    window.onload = function() {
      if (self.workExperiences[0].EndDate == null) {
        comumConfig.attr("#work-experience", "checked", true);
      }
    };
  }

  function enableSocialNetworks() {
    addAndRemoveNetworksClass(self.professional.TwitterUrl, ".twitter");
    addAndRemoveNetworksClass(self.professional.FacebookUrl, ".facebook");
    addAndRemoveNetworksClass(self.professional.DribbbleUrl, ".dribbble");
    addAndRemoveNetworksClass(self.professional.InstagramUrl, ".instagram");
    addAndRemoveNetworksClass(self.professional.LinkedinUrl, ".linkedin");
    addAndRemoveNetworksClass(self.professional.BehanceUrl, ".behance");
  }

  function addAndRemoveNetworksClass(networkField, networkElement) {
    var activeClass = "active";

    if (
      networkField &&
      networkField != http &&
      netWorkMinLength(networkField)
    ) {
      comumConfig.addClass(networkElement, activeClass);
    } else {
      comumConfig.removeClass(networkElement, activeClass);
      addNetworkHttp(networkElement);
    }
  }

  function addNetworkHttp(network) {
    switch (network) {
      case ".twitter":
        self.professional.TwitterUrl = http;
        break;
      case ".facebook":
        self.professional.FacebookUrl = http;
        break;
      case ".dribbble":
        self.professional.DribbbleUrl = http;
        break;
      case ".instagram":
        self.professional.InstagramUrl = http;
        break;
      case ".linkedin":
        self.professional.LinkedinUrl = http;
        break;
      case ".behance":
        self.professional.BehanceUrl = http;
        break;
    }
  }

  function checkNetworkHttpValue() {
    if (
      self.professional.TwitterUrl == http ||
      !netWorkMinLength(self.professional.TwitterUrl)
    )
      self.professional.TwitterUrl = null;

    if (
      self.professional.FacebookUrl == http ||
      !netWorkMinLength(self.professional.FacebookUrl)
    )
      self.professional.FacebookUrl = null;

    if (
      self.professional.DribbbleUrl == http ||
      !netWorkMinLength(self.professional.DribbbleUrl)
    )
      self.professional.DribbbleUrl = null;

    if (
      self.professional.InstagramUrl == http ||
      !netWorkMinLength(self.professional.InstagramUrl)
    )
      self.professional.InstagramUrl = null;

    if (
      self.professional.LinkedinUrl == http ||
      !netWorkMinLength(self.professional.LinkedinUrl)
    )
      self.professional.LinkedinUrl = null;

    if (
      self.professional.BehanceUrl == http ||
      !netWorkMinLength(self.professional.BehanceUrl)
    )
      self.professional.BehanceUrl = null;
  }

  function netWorkMinLength(network) {
    if (!network || (network && network.length < 7)) return false;

    return true;
  }

  function hidePopovers() {
    window.WebuiPopovers.hideAll();
    enableSocialNetworks();
  }

  function checkIfAnyFieldAreFilled(workExperiencesObj) {
    if (
      workExperiencesObj.Company ||
      workExperiencesObj.Role ||
      workExperiencesObj.EndDate ||
      workExperiencesObj.StartDate ||
      workExperiencesObj.Description
    )
      return true;

    return false;
  }

  function checkIfAllFieldsAreFilled(workExperiencesObj) {
    if (
      workExperiencesObj.Company &&
      workExperiencesObj.Role &&
      workExperiencesObj.EndDate &&
      workExperiencesObj.StartDate &&
      workExperiencesObj.Description
    )
      return true;

    return false;
  }

  function checkLastWorkExperience(workExperiencesObj) {
    if (
      workExperiencesObj.Company &&
      workExperiencesObj.Role &&
      workExperiencesObj.StartDate &&
      (!workExperiencesObj.EndDate ||
        workExperiencesObj.EndDate === undefined) &&
      workExperiencesObj.Description
    )
      return true;

    return false;
  }

  function validateProfessional() {
    var qtdWorkExp = self.workExperiences.length;
    var currentWork = 0;
    var experiences = [];
    var result = true;

    if (self.professional.Description) {
      var s = self.professional.Description;
      s = s.replace(/(^\s*)|(\s*$)/gi, "");
      s = s.replace(/[ ]{2,}/gi, " ");
      s = s.replace(/\n/, "\n");
      console.debug("QTD DE PALAVRAS:",s.split(" ").length);
      if (s.split(" ").length <= 64) {
        self.error = true;
        self.errorMessage =
          "Fale mais sobre você! Sua descrição pessoal não tem o mínimo de 65 palavras.";
        comumConfig.addClass("#Description", "error");
        result = false;
        return false;
      } else {
        self.error = false;
      }
    } else {
      self.error = true;
      self.errorMessage =
        "Fale mais sobre você! Sua descrição pessoal não tem o mínimo de 65 palavras.";
      comumConfig.addClass("#Description", "error");
      result = false;
      return false;
    }

    for (var i = 0; i < self.workExperiences.length; i++) {
      currentWork = qtdWorkExp - i;

      if (checkIfAnyFieldAreFilled(self.workExperiences[i])) {
        if (!checkIfAllFieldsAreFilled(self.workExperiences[i])) {
          if (i === 0) {
            if (!checkLastWorkExperience(self.workExperiences[i])) {
              self.error = true;
              self.errorMessage =
                "Sua última experiência profissional possui campos inválidos.";
              result = false;
              break;
            } else {
              experiences.push(self.workExperiences[i]);
            }
          } else {
            self.error = true;
            self.errorMessage =
              "Sua " +
              currentWork +
              "º experiência profissional possui campos inválidos.";
            result = false;
            break;
          }
        } else {
          experiences.push(self.workExperiences[i]);
        }
      }
    }

    if (validaLink(self.professional.Portfolio)) {
      self.error = true;
            self.errorMessage =
              "Apenas links são aceitos no campo portifolio";
            result = false;
    }

    if (validaLink(self.professional.TwitterUrl) || validaLink(self.professional.FacebookUrl)
        || validaLink(self.professional.DribbbleUrl) || validaLink(self.professional.InstagramUrl)
        || validaLink(self.professional.LinkedinUrl)) {
      self.error = true;
            self.errorMessage =
              "Apenas links são aceitos nos campos de redes sociais";
            result = false;

    }

    if (self.professional.Skills.length < 5) {
      self.error = true;
      self.errorMessage =
        "Selecione pelo menos 5 abilidades";
      result = false;

    }

    self.professional.Experiences = experiences;

    return result;
  }

  function validaLink(link) {
    return link && link != "" && !regexLinks.test(link)
  }

  function setWorkExperiences() {
    if (self.professional.Experiences.length === 0)
      self.workExperiences.push(getWordExperienceDataEmpty());
    else self.workExperiences = self.professional.Experiences.reverse();

    window.onload = function() {
      if (self.workExperiences[0].EndDate == null) {
        comumConfig.attr("#work-experience", "checked", true);
      }
    };
  }

  function enableSocialNetworks() {
    addAndRemoveNetworksClass(self.professional.TwitterUrl, ".twitter");
    addAndRemoveNetworksClass(self.professional.FacebookUrl, ".facebook");
    addAndRemoveNetworksClass(self.professional.DribbbleUrl, ".dribbble");
    addAndRemoveNetworksClass(self.professional.InstagramUrl, ".instagram");
    addAndRemoveNetworksClass(self.professional.LinkedinUrl, ".linkedin");
    addAndRemoveNetworksClass(self.professional.BehanceUrl, ".behance");
  }

  function addAndRemoveNetworksClass(networkField, networkElement) {
    var activeClass = "active";

    if (
      networkField &&
      networkField != http &&
      netWorkMinLength(networkField)
    ) {
      comumConfig.addClass(networkElement, activeClass);
    } else {
      comumConfig.removeClass(networkElement, activeClass);
      addNetworkHttp(networkElement);
    }
  }

  function addNetworkHttp(network) {
    switch (network) {
      case ".twitter":
        self.professional.TwitterUrl = http;
        break;
      case ".facebook":
        self.professional.FacebookUrl = http;
        break;
      case ".dribbble":
        self.professional.DribbbleUrl = http;
        break;
      case ".instagram":
        self.professional.InstagramUrl = http;
        break;
      case ".linkedin":
        self.professional.LinkedinUrl = http;
        break;
      case ".behance":
        self.professional.BehanceUrl = http;
        break;
    }
  }

  function checkNetworkHttpValue() {
    if (
      self.professional.TwitterUrl == http ||
      !netWorkMinLength(self.professional.TwitterUrl)
    )
      self.professional.TwitterUrl = null;

    if (
      self.professional.FacebookUrl == http ||
      !netWorkMinLength(self.professional.FacebookUrl)
    )
      self.professional.FacebookUrl = null;

    if (
      self.professional.DribbbleUrl == http ||
      !netWorkMinLength(self.professional.DribbbleUrl)
    )
      self.professional.DribbbleUrl = null;

    if (
      self.professional.InstagramUrl == http ||
      !netWorkMinLength(self.professional.InstagramUrl)
    )
      self.professional.InstagramUrl = null;

    if (
      self.professional.LinkedinUrl == http ||
      !netWorkMinLength(self.professional.LinkedinUrl)
    )
      self.professional.LinkedinUrl = null;

    if (
      self.professional.BehanceUrl == http ||
      !netWorkMinLength(self.professional.BehanceUrl)
    )
      self.professional.BehanceUrl = null;
  }

  function netWorkMinLength(network) {
    if (!network || (network && network.length < 7)) return false;

    return true;
  }

  function hidePopovers() {
    window.WebuiPopovers.hideAll();
    enableSocialNetworks();
  }

  function checkIfAnyFieldAreFilled(workExperiencesObj) {
    if (
      workExperiencesObj.Company ||
      workExperiencesObj.Role ||
      workExperiencesObj.EndDate ||
      workExperiencesObj.StartDate ||
      workExperiencesObj.Description
    )
      return true;

    return false;
  }

  function checkIfAllFieldsAreFilled(workExperiencesObj) {
    if (
      workExperiencesObj.Company &&
      workExperiencesObj.Role &&
      workExperiencesObj.EndDate &&
      workExperiencesObj.StartDate &&
      workExperiencesObj.Description &&
      validaData(workExperiencesObj.StartDate) &&
      validaData(workExperiencesObj.EndDate)
    )
      return true;

    return false;
  }

  function validaData(data) {
    var ano = new Date().getFullYear();
    if (data != null) {
      dataQuebrada = data.split("-")
      if (Number(dataQuebrada[1]) > 12) {
        return false;
      }
      if (Number(dataQuebrada[0]) < 1900 || Number(dataQuebrada[0]) > Number(ano)) {
        return false;
      }
      return true;
    } else {
      return false;
    }

  }

  function checkLastWorkExperience(workExperiencesObj) {
    if (
      workExperiencesObj.Company &&
      workExperiencesObj.Role &&
      workExperiencesObj.StartDate &&
      (!workExperiencesObj.EndDate ||
        workExperiencesObj.EndDate === undefined) &&
      workExperiencesObj.Description
    )
      return true;

    return false;
  }

  function validateProfessional2() {
    var qtdWorkExp = self.workExperiences.length;
    var currentWork = 0;
    var experiences = [];
    var result = true;

    for (var i = 0; i < self.workExperiences.length; i++) {
      currentWork = qtdWorkExp - i;

      if (checkIfAnyFieldAreFilled(self.workExperiences[i])) {
        if (!checkIfAllFieldsAreFilled(self.workExperiences[i])) {
          if (i === 0) {
            if (!checkLastWorkExperience(self.workExperiences[i])) {
              self.error = true;
              self.errorMessage =
                "Sua última experiência profissional possui campos inválidos.";
              result = false;
              break;
            } else {
              experiences.push(self.workExperiences[i]);
            }
          } else {
            self.error = true;
            self.errorMessage =
              "Sua " +
              currentWork +
              "º experiência profissional possui campos inválidos.";
            result = false;
            break;
          }
        } else {
          experiences.push(self.workExperiences[i]);
        }
      }
    }

    self.professional.Experiences = experiences;

    return result;
  }

  function removeWorkExperience(index) {
    self.workExperiences = comumConfig.removeItemFromArrayByIndex(
      self.workExperiences,
      index
    );
  }

  function idiomasLimit() {
    if (self.idiomas.length >= languagesLimit) return false;

    return true;
  }

  function isLastExperience(index) {
    if (!index) {
      return true;
    }
    return false;
  }

  function unableLastEndDate() {
    if (self.workExperiences.length) {
      var work = self.workExperiences[0];
      var lastDate = comumConfig.getElement(".last-exp-date");
      var currentJob = comumConfig.getElement("#work-experience");
      lastDate = lastDate[0];

      if (!comumConfig.attr(lastDate, "disabled")) {
        comumConfig.attr(lastDate, "disabled", "true");
        comumConfig.attr(currentJob, "checked", "true");

        work.EndDate = null;
      } else {
        comumConfig.removeAttr(lastDate, "disabled");
        comumConfig.removeClass(".last-exp-date", ".error");
      }
    }
  }

  // INIT
  init();
}
