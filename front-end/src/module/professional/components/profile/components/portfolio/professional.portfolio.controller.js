professionalPortfolioController.$inject = [
  "comumConfig",
  "comumService",
  "toaster",
  "professionalService",
];

function professionalPortfolioController(
  comumConfig,
  comumService,
  toaster,
  professionalService
) {
  var self = this;

  // Private
  // ...

  // Public
  self.urlVideo = "";
  self.imgAttach = false;
  self.imgAttachBg = {};

  self.portfolio = {
    Id: 0,
    Title: "",
    Description: "",
    Type: 1,
    URL: "",
    ClientName: "",
    Media: "",
    FreelancerId: 0,
  };

  self.save = save;
  self.deleteImage = deleteImage;
  self.loadVideo = loadVideo;

  // Functions
  function init() {
    self.portfolio.FreelancerId = comumService.getUser()["IdFreelancer"];

    configDropzone();
  }

  function save() {
    if (!_validate()) return false;

    if (!self.imgAttach) self.portfolio.Type = 2;

    professionalService
      .savePortfolio(self.portfolio)
      .success(function success (data) {
        toaster.pop("success", "", "Portfolio adicionado com sucesso!");
        self.showPortfolio = false;
      })
      .error(function error () {
        toaster.pop(
          "error",
          "",
          "Erro ao salvar o portfolio. Tente novamente."
        );
      });
  }

  function configDropzone() {
    var dropzone = {
      element: "#my-dropzone",
      url: {
        url: comumConfig.baseUrlAPI() + "/Customer/AttachFile",
      },
      options: {
        headers: {
          Authorization: "Bearer " + window.localStorage.getItem("ngStorage-token"),
        },
        maxFilesize: 2,
        name: "myDropzone",
        init: {
          addedfile: function (file) {},
          success: function (file, data) {
            data = data;
            //var imgPath = comumConfig.baseUrlAPI() + data.folder + data.filename;
            self.imgAttach = true;
            self.imgAttachBg = {
              background: "url('" +
                comumConfig.baseUrlAPI() +
                data.folder +
                data.filename +
                "')"
            };
            self.portfolio.Media = data.folder + data.filename;
            self.portfolio.Type = 1;
          }
        },
        createImageThumbnails: false,
        previewTemplate: "preview-template",
        previewsContainer: "dropzone-previews"
      }
    };

    comumConfig.addDropzone(dropzone);
  }

  function deleteImage() {
    self.imgAttach = false;
    self.imgAttachBg = {};
    self.portfolio.Media = "";
  }

  function _validate() {
    if (!self.portfolio.Media) {
      toaster.pop("error", "", "Insira uma imagem ou link de algum vídeo!");
      return false;
    }

    return true;
  }

  function loadVideo() {
    console.log(self.portfolio.Media);
  }

  // init
  init();
}
