function professionalPortfolioComponent() {
  return {
    templateUrl: "./templates/professional/profile/components/portfolio/view/portfolio.html",
    controller: professionalPortfolioController,
    bindings: {
      professionalName: "@",
      showPortfolio: "<",
      portfolioId: "<",
    },
  };
}
