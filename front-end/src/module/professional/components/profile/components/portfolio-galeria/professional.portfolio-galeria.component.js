function professionalPortfolioGaleriaComponent() {
  return {
    templateUrl: "./templates/professional/profile/components/portfolio-galeria/view/portfolio-galeria.html",
    controller: professionalPortfolioGaleriaController,
    bindings: {
      galeriaData: "<",
    },
  };
}
