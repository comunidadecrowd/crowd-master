professionalPortfolioGaleriaController.$inject = [
  "comumConfig",
];

function professionalPortfolioGaleriaController(comumConfig) {
  var self = this;

  // Private
  // ...

  // Public
  self.isYoutube = isYoutube;
  self.isVimeo = isVimeo;
  self.isMp4 = isMp4;

  // Functions
  function init() {
    // ...
  }

  function isYoutube(url) {
    // url : //youtube.com/watch?v=Bo_deCOd1HU
    // share : //youtu.be/Bo_deCOd1HU
    // embed : //youtube.com/embed/Bo_deCOd1HU
    var re = /\/\/(?:www\.)?youtu(?:\.be|be\.com)\/(?:watch\?v=|embed\/)?([a-z0-9_\-]+)/i;
    var matches = re.exec(url);

    if (matches && matches[1]) return _getUrlID(matches[1]);

    return false;
  }

  function isVimeo(url) {
    // http://vimeo.com/86164897
    var re = /\/\/(?:www\.)?vimeo.com\/([0-9a-z\-_]+)/i;
    var matches = re.exec(url);

    return matches && matches[1];

    // if (matches && matches[1]) return _getUrlID(matches[1])
    // return false;
  }

  function isMp4(url) {
    var str = url.split(".");
    var ext = comumConfig.getLastElementFromArray(str);

    // return ext == "mp4" ? true : false;
  }

  function _getUrlID(url) {
    var str = url.indexOf("watch");
    if (str > -1) {
      str = url.split("v=");
      return comumConfig.getLastElementFromArray(str);
    }

    str = url.split("/");

    return comumConfig.getLastElementFromArray(str);
  }

  // init
  init();
}
