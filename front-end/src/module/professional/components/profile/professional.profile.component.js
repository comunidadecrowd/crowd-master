function professionalProfileComponent() {
  return {
    templateUrl: "../templates/profile/view/profile.html",
    controller: professionalProfileController,
    bindings: {
      getProfile: "<",
      getCountries: "<",
      getStates: "<",
      getCategories: "<",
      getSegments: "<",
      getSkills: "<",
    },
  };
}
