/**
* Professional Module
*
* Description
*/
angular.module('Professional', ['Comum', 'SharedScreen',
	'angulartics',
	'angulartics.google.analytics',
	'angulartics.intercom',
	'angucomplete-alt'
])

	// Route
	.config(professionalRoute)
	//.config(analyticsTracking)

	// Components
	.component('professionalProfileComponent', professionalProfileComponent())
	.component('professionalBriefingComponent', professionalBriefingComponent())
	.component('professionalTutorialComponent', professionalTutorialComponent())
	.component('freelancerReportsComponent', freelancerReportsComponent())
	.component('professionalPortfolioComponent', professionalPortfolioComponent())
	.component('professionalPortfolioGaleriaComponent', professionalPortfolioGaleriaComponent())

	// Services
	.service('professionalService', professionalService)
	.service('FreelancerReportsService', FreelancerReportsService);


	function analyticsTracking($analyticsProvider) {
		$analyticsProvider.virtualPageviews(false);
	}