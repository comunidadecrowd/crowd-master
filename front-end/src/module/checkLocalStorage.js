var jsonUser = JSON.parse(localStorage.getItem('ngStorage-user'));
var token    = localStorage.getItem('ngStorage-token');

if (jsonUser && token && window.location.href.indexOf('beta') <= 0 && window.location.href.indexOf('localhost') <= 0){
		
	jQuery.ajax({
		url: "//api.comunidadecrowd.com.br/User/ValidateToken",
		data: '',
		headers: {
			'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('ngStorage-token'))
		},
		statusCode: {
			200: function() {
							window.location = jsonUser.defaultURL
		  	   }
	 	},			
		error: function (data, status) {return false},
		dataType: 'json'
	});
		
}