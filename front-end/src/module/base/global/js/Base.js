(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['exports', 'jQuery', 'Component', 'Plugin'], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require('jQuery'), require('Component'), require('Plugin'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.jQuery, global.Component, global.Plugin);
    global.Base = mod.exports;
  }
})(this, function (exports, _jQuery, _Component2, _Plugin) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  var _jQuery2 = babelHelpers.interopRequireDefault(_jQuery);

  var _Component3 = babelHelpers.interopRequireDefault(_Component2);

  var _Plugin2 = babelHelpers.interopRequireDefault(_Plugin);

  var DOC = document;

  var _class = function (_Component) {
    babelHelpers.inherits(_class, _Component);

    function _class() {
      babelHelpers.classCallCheck(this, _class);
      return babelHelpers.possibleConstructorReturn(this, Object.getPrototypeOf(_class).apply(this, arguments));
    }

    babelHelpers.createClass(_class, [{
      key: 'renderPlugins',
      value: function renderPlugins() {
        var context = arguments.length <= 0 || arguments[0] === undefined ? false : arguments[0];

        (0, _jQuery2.default)('[data-plugin]', context || this.$el).each(function () {
          var $this = (0, _jQuery2.default)(this),
              name = $this.data("plugin"),
              plugin = (0, _Plugin.factory)(name, $this, $this.data());
          if (plugin) {
            plugin.renderPlugin();
          }
        });
      }
    }, {
      key: 'registerPluginAPI',
      value: function registerPluginAPI() {
        var context = arguments.length <= 0 || arguments[0] === undefined ? DOC : arguments[0];

        var apis = (0, _Plugin.getPluginAPI)();
        for (var name in apis) {
          (0, _Plugin.getPluginAPI)(name)('[data-plugin=' + name + ']', context);
        }
      }
    }]);
    return _class;
  }(_Component3.default);

  exports.default = _class;
});
