(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['exports', 'jQuery'], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require('jQuery'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.jQuery);
    global.Plugin = mod.exports;
  }
})(this, function (exports, _jQuery) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.registerPluginAPI = exports.getPluginAPI = exports.factory = exports.getDefaults = exports.get = exports.register = undefined;

  var _jQuery2 = babelHelpers.interopRequireDefault(_jQuery);

  var GLOBAL = window;
  var lists = {};
  var apis = {};

  var Plugin = function () {
    function Plugin($el) {
      var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];
      babelHelpers.classCallCheck(this, Plugin);

      this.name = this.getName();
      this.$el = $el;
      this.options = options;
      this.isRender = false;
    }

    babelHelpers.createClass(Plugin, [{
      key: 'getName',
      value: function getName() {
        return 'plugin';
      }
    }, {
      key: 'render',
      value: function render() {
        if (_jQuery2.default.fn[this.name]) {
          this.$el[this.name](this.options);
        } else {
          return false;
        }
      }
    }, {
      key: 'renderPlugin',
      value: function renderPlugin() {
        if (this.isRender) return false;
        this.render();
        this.isRender = true;
      }
    }], [{
      key: 'getDefaults',
      value: function getDefaults() {
        return {};
      }
    }]);
    return Plugin;
  }();

  exports.default = Plugin;


  function getPluginAPI() {
    var name = arguments.length <= 0 || arguments[0] === undefined ? false : arguments[0];

    if (!name) return apis;else return apis[name];
  }

  function registerPluginAPI(name, obj) {
    var api = obj.api();
    if (typeof api === 'string') {
      (function () {
        var api = obj.api().split('|');
        var event = api[0] + ('.plugin.' + name),
            func = api[1] || 'render';
        var callback = function callback(e) {
          var $el = (0, _jQuery2.default)(this),
              plugin = $el.data('pluginInstance');
          if (!plugin) {
            plugin = new obj($el, _jQuery2.default.extend(true, {}, getDefaults(name), $el.data()));
            plugin.renderPlugin();
            $el.data('pluginInstance', plugin);
          }

          plugin[func](e);
        };
        apis[name] = function (selector, context) {
          if (context) {
            (0, _jQuery2.default)(context).off(event);
            (0, _jQuery2.default)(context).on(event, selector, callback);
          } else {
            (0, _jQuery2.default)(selector).on(event, callback);
          }
        };
      })();
    } else if (typeof api === 'function') {
      apis[name] = api;
    }
  }

  function register(name, obj) {
    if (typeof obj === 'undefined') return;

    lists[name] = obj;
    if (typeof obj.api !== 'undefined') registerPluginAPI(name, obj);
  }

  function get(name) {
    if (typeof lists[name] !== "undefined") {
      return lists[name];
    } else {
      console.warn('Plugin:' + name + ' has no warpped class.');
      return false;
    }
  }

  function getDefaults(name) {
    var PluginClass = get(name);
    if (PluginClass) {
      return PluginClass.getDefaults();
    } else {
      return {};
    }
  }

  function factory(name, $el) {
    var options = arguments.length <= 2 || arguments[2] === undefined ? {} : arguments[2];

    var PluginClass = get(name);
    if (PluginClass && typeof PluginClass.api === 'undefined') {
      return new PluginClass($el, _jQuery2.default.extend(true, {}, getDefaults(name), options));
    } else if (_jQuery2.default.fn[name]) {
      var plugin = new Plugin($el, options);
      plugin.getName = function () {
        return name;
      };
      plugin.name = name;
      return plugin;
    } else if (typeof PluginClass.api !== 'undefined') {
      console.log('Plugin:' + name + ' use api render.');
      return false;
    } else {
      console.warn('Plugin:' + name + ' script is not loaded.');
      return false;
    }
  }

  exports.default = Plugin;
  exports.register = register;
  exports.get = get;
  exports.getDefaults = getDefaults;
  exports.factory = factory;
  exports.getPluginAPI = getPluginAPI;
  exports.registerPluginAPI = registerPluginAPI;
});
