(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['./Component'], factory);
  } else if (typeof exports !== "undefined") {
    factory(require('./Component'));
  } else {
    var mod = {
      exports: {}
    };
    factory(global.Component);
    global.laddaProgress = mod.exports;
  }
})(this, function (_Component2) {
  'use strict';

  var _Component3 = babelHelpers.interopRequireDefault(_Component2);

  var DEFAULT = {
    init: function init(instance) {
      var progress = 0;
      var interval = setInterval(function () {
        progress = Math.min(progress + Math.random() * 0.1, 1);
        instance.setProgress(progress);

        if (progress === 1) {
          instance.stop();
          clearInterval(interval);
        }
      }, 200);
    }
  };

  var LaddaProgress = function (_Component) {
    babelHelpers.inherits(LaddaProgress, _Component);

    function LaddaProgress() {
      babelHelpers.classCallCheck(this, LaddaProgress);

      var _this = babelHelpers.possibleConstructorReturn(this, Object.getPrototypeOf(LaddaProgress).call(this));

      _this.mode = 'default';
      return _this;
    }

    babelHelpers.createClass(LaddaProgress, [{
      key: 'init',
      value: function init() {
        if (typeof Ladda === 'undefined') return;

        var defaults = $.components.getDefaults("laddaProgress");
        // Bind progress buttons and simulate loading progress
        Ladda.bind('[data-plugin="laddaProgress"]', defaults);
      }
    }]);
    return LaddaProgress;
  }(_Component3.default);
});
