(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['exports', 'Plugin'], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require('Plugin'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.Plugin);
    global.laddaProgress = mod.exports;
  }
})(this, function (exports, _Plugin2) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  var _Plugin3 = babelHelpers.interopRequireDefault(_Plugin2);

  var NAME = 'laddaProgress';

  var LaddaProgress = function (_Plugin) {
    babelHelpers.inherits(LaddaProgress, _Plugin);

    function LaddaProgress() {
      babelHelpers.classCallCheck(this, LaddaProgress);
      return babelHelpers.possibleConstructorReturn(this, Object.getPrototypeOf(LaddaProgress).apply(this, arguments));
    }

    babelHelpers.createClass(LaddaProgress, [{
      key: 'getName',
      value: function getName() {
        return NAME;
      }
    }, {
      key: 'render',
      value: function render() {
        if (typeof Ladda === 'undefined') return;

        var defaults = $.Plugin.getDefaults(NAME);;
        // Bind progress buttons and simulate loading progress
        Ladda.bind('[data-plugin="laddaProgress"]', defaults);
      }
    }], [{
      key: 'getDefaults',
      value: function getDefaults() {
        return {
          init: function init(instance) {
            var progress = 0;
            var interval = setInterval(function () {
              progress = Math.min(progress + Math.random() * 0.1, 1);
              instance.setProgress(progress);

              if (progress === 1) {
                instance.stop();
                clearInterval(interval);
              }
            }, 200);
          }
        };
      }
    }]);
    return LaddaProgress;
  }(_Plugin3.default);

  (0, _Plugin2.register)(NAME, LaddaProgress);

  exports.default = LaddaProgress;
});
