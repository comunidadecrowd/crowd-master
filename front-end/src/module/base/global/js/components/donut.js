(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['exports', 'jQuery', 'Plugin', 'Config'], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require('jQuery'), require('Plugin'), require('Config'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.jQuery, global.Plugin, global.Config);
    global.donut = mod.exports;
  }
})(this, function (exports, _jQuery, _Plugin2, _Config) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  var _jQuery2 = babelHelpers.interopRequireDefault(_jQuery);

  var _Plugin3 = babelHelpers.interopRequireDefault(_Plugin2);

  var NAME = 'donut';

  var DonutPlugin = function (_Plugin) {
    babelHelpers.inherits(DonutPlugin, _Plugin);

    function DonutPlugin() {
      babelHelpers.classCallCheck(this, DonutPlugin);
      return babelHelpers.possibleConstructorReturn(this, Object.getPrototypeOf(DonutPlugin).apply(this, arguments));
    }

    babelHelpers.createClass(DonutPlugin, [{
      key: 'getName',
      value: function getName() {
        return NAME;
      }
    }, {
      key: 'render',
      value: function render() {
        if (!Gauge) return;

        var $el = this.$el,
            $text = $el.find('.donut-label'),
            $canvas = $el.find("canvas");

        if ($canvas.length === 0) {
          return;
        }

        var donut = new Donut($canvas[0]).setOptions(this.options);

        $el.data("donut", donut);

        donut.animationSpeed = 50;
        donut.maxValue = $el.data('max-value');

        donut.set($el.data("value"));

        if ($text.length > 0) {
          donut.setTextField($text[0]);
        }
      }
    }], [{
      key: 'getDefaults',
      value: function getDefaults() {
        return {
          lines: 12,
          angle: 0.3,
          lineWidth: 0.08,
          pointer: {
            length: 0.9,
            strokeWidth: 0.035,
            color: (0, _Config.colors)("blue-grey", 400)
          },
          limitMax: false, // If true, the pointer will not go past the end of the gauge
          colorStart: (0, _Config.colors)("blue-grey", 200),
          colorStop: (0, _Config.colors)("blue-grey", 200),
          strokeColor: (0, _Config.colors)("primary", 500),
          generateGradient: true
        };
      }
    }]);
    return DonutPlugin;
  }(_Plugin3.default);

  (0, _Plugin2.register)(NAME, DonutPlugin);

  exports.default = DonutPlugin;
});
