(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['exports', 'jQuery', 'Plugin'], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require('jQuery'), require('Plugin'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.jQuery, global.Plugin);
    global.peityPie = mod.exports;
  }
})(this, function (exports, _jQuery, _Plugin2) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  var _jQuery2 = babelHelpers.interopRequireDefault(_jQuery);

  var _Plugin3 = babelHelpers.interopRequireDefault(_Plugin2);

  var NAME = 'peityPie';

  var PeityPie = function (_Plugin) {
    babelHelpers.inherits(PeityPie, _Plugin);

    function PeityPie() {
      babelHelpers.classCallCheck(this, PeityPie);
      return babelHelpers.possibleConstructorReturn(this, Object.getPrototypeOf(PeityPie).apply(this, arguments));
    }

    babelHelpers.createClass(PeityPie, [{
      key: 'getName',
      value: function getName() {
        return NAME;
      }
    }, {
      key: 'render',
      value: function render() {
        if (!_jQuery2.default.fn.peity) return;

        var $el = this.$el,
            options = this.options,
            defaults = _jQuery2.default.Plugin.getDefaults(NAME);

        if (options.skin) {
          if (_jQuery2.default.colors(options.skin)) {
            var skinColors = _jQuery2.default.colors(options.skin);
            defaults.fill = [skinColors[700], skinColors[400], skinColors[200]];
          }
        }

        $el.peity('pie', options);
      }
    }], [{
      key: 'getDefaults',
      value: function getDefaults() {
        return {
          delimiter: null,
          fill: [_jQuery2.default.colors("primary", 700), _jQuery2.default.colors("primary", 400), _jQuery2.default.colors("primary", 200)],
          height: null,
          radius: 11,
          width: null
        };
      }
    }]);
    return PeityPie;
  }(_Plugin3.default);

  (0, _Plugin2.register)(NAME, PeityPie);

  exports.default = PeityPie;
});
