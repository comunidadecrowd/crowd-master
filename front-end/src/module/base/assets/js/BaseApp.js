/*!
 * Remark (http://getbootstrapadmin.com/remark)
 * Copyright 2016 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "Site"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("Site"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.Site);
    global.BaseApp = mod.exports;
  }
})(this, function(exports, _Site2) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  var _Site3 = babelHelpers.interopRequireDefault(_Site2);

  var BaseApp = function(_Site) {
    babelHelpers.inherits(BaseApp, _Site);

    function BaseApp() {
      babelHelpers.classCallCheck(this, BaseApp);
      return babelHelpers.possibleConstructorReturn(this, Object.getPrototypeOf(BaseApp).apply(this, arguments));
    }

    babelHelpers.createClass(BaseApp, [{
      key: "processed",
      value: function processed() {
        babelHelpers.get(Object.getPrototypeOf(BaseApp.prototype), "processed", this).call(this);
      }
    }]);
    return BaseApp;
  }(_Site3.default);

  exports.default = BaseApp;
});
