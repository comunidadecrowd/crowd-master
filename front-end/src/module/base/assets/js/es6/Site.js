/*!
 * Remark (http://getbootstrapadmin.com/remark)
 * Copyright 2016 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['exports', 'jQuery', 'Base', 'Plugin'], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require('jQuery'), require('Base'), require('Plugin'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.jQuery, global.Base, global.Plugin);
    global.Site = mod.exports;
  }
})(this, function(exports, _jQuery, _Base2, _Plugin) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.getInstance = exports.run = exports.Site = undefined;

  var _jQuery2 = babelHelpers.interopRequireDefault(_jQuery);

  var _Base3 = babelHelpers.interopRequireDefault(_Base2);

  var _Plugin2 = babelHelpers.interopRequireDefault(_Plugin);

  var Site = function(_Base) {
    babelHelpers.inherits(Site, _Base);

    function Site() {
      babelHelpers.classCallCheck(this, Site);
      return babelHelpers.possibleConstructorReturn(this, Object.getPrototypeOf(Site).apply(this, arguments));
    }

    babelHelpers.createClass(Site, [{
      key: 'run',
      value: function run() {
        var _babelHelpers$get;

        for (var _len = arguments.length, state = Array(_len), _key = 0; _key < _len; _key++) {
          state[_key] = arguments[_key];
        }

        (_babelHelpers$get = babelHelpers.get(Object.getPrototypeOf(Site.prototype), 'run', this)).call.apply(_babelHelpers$get, [this].concat(state));

        this.polyfillIEWidth();
        this.initBootstrap();

        this.setupFullScreen();
        this.setupMegaNavbar();

        // Dropdown menu setup
        // ===================
        (0, _jQuery2.default)('body').on('click', '.dropdown-menu-media', function(e) {
          e.stopPropagation();
        });
      }
    }, {
      key: 'getDefaultState',
      value: function getDefaultState() {
        return {};
      }
    }, {
      key: 'initBootstrap',
      value: function initBootstrap() {
        // Tooltip setup
        // =============
        (0, _jQuery2.default)(document).tooltip({
          selector: '[data-tooltip=true]',
          container: 'body'
        });

        (0, _jQuery2.default)('[data-toggle="tooltip"]').tooltip();
        (0, _jQuery2.default)('[data-toggle="popover"]').popover();
      }
    }, {
      key: 'polyfillIEWidth',
      value: function polyfillIEWidth() {
        if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
          var msViewportStyle = document.createElement('style');
          msViewportStyle.appendChild(document.createTextNode('@-ms-viewport{width:auto!important}'));
          document.querySelector('head').appendChild(msViewportStyle);
        }
      }
    }, {
      key: 'setupFullScreen',
      value: function setupFullScreen() {
        // Fullscreen
        // ==========
        if (typeof screenfull !== 'undefined') {
          (0, _jQuery2.default)(document).on('click', '[data-toggle="fullscreen"]', function() {
            if (screenfull.enabled) {
              screenfull.toggle();
            }

            return false;
          });

          if (screenfull.enabled) {
            document.addEventListener(screenfull.raw.fullscreenchange, function() {
              (0, _jQuery2.default)('[data-toggle="fullscreen"]').toggleClass('active', screenfull.isFullscreen);
            });
          }
        }
      }
    }, {
      key: 'setupMegaNavbar',
      value: function setupMegaNavbar() {
        // Mega navbar setup
        // =================
        (0, _jQuery2.default)(document).on('click', '.navbar-mega .dropdown-menu', function(e) {
          e.stopPropagation();
        });

        (0, _jQuery2.default)(document).on('show.bs.dropdown', function(e) {
          var $trigger = e.relatedTarget ? (0, _jQuery2.default)(e.relatedTarget) : (0, _jQuery2.default)(e.target).children('[data-toggle="dropdown"]');
          var animation = $trigger.data('animation');

          if (animation) {
            (function() {
              var $menu = $target.children('.dropdown-menu');
              $menu.addClass('animation-' + animation);

              $menu.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                $menu.removeClass('animation-' + animation);
              });
            })();
          }
        });

        (0, _jQuery2.default)(document).on('shown.bs.dropdown', function(e) {
          var $menu = (0, _jQuery2.default)(e.target).find('.dropdown-menu-media > .list-group');

          if ($menu.length > 0) {
            var api = $menu.data('asScrollable');
            if (api) {
              api.update();
            } else {
              $menu.asScrollable({
                namespace: "scrollable",
                contentSelector: "> [data-role='content']",
                containerSelector: "> [data-role='container']"
              });
            }
          }
        });
      }
    }]);
    return Site;
  }(_Base3.default);

  var isInit = false,
    instance = null;

  function getInstance() {
    if (!instance) {
      instance = new Site();
    }
    return instance;
  }

  function run() {
    var site = getInstance();
    site.run();
  }

  exports.Site = Site;
  exports.run = run;
  exports.getInstance = getInstance;
});
