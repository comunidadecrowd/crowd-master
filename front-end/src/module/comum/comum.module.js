/**
 * Comum Module
 *
 * Description
 */
angular.module("Comum", [
			"Public",
			"ui.select",
			"rzModule",
			"ngTable",
			"toaster",
			"ngAnimate",
			"ngTagsInput",
			"ui.router",
			"ngSanitize",
			"pageslide-directive",
			"uiSwitch",
			"ngDialog",
			"angularMoment",
      "moment-picker",
      "siyfion.sfTypeahead",
      "ui.bootstrap",
      "angulartics",
      "angulartics.google.analytics",
      "angulartics.intercom"
		])

// Controllers
.controller("comumController", comumController)

// Services
.service("comumLoginService", comumLoginService)
 .service("comumService", comumService)
 .service("comumVisualizarPerfilProfissionalService", comumVisualizarPerfilProfissionalService)
 .service("briefingServiceAPI", briefingServiceAPI)
 .service("comumProjectService", comumProjectService)
 .factory("httpInterceptor", ["$rootScope", "comumService", function ($rootScope, comumService) {
  "use strict";
  var service = this;

  if (comumService.getUser().Role == 2) {
   comumService.getNotificationProfessional();
  } else {
   comumService.getNotification(comumService.getUser().userId);
  }

  comumService.getProjetos();
  comumService.getProjetosArquivados();

  service.responseError = function (response) {

  };
  return service;
	}])

// Components
.component("comumLoginComponent", comumLoginComponent())
 .component("comumVisualizarPerfilProfissionalComponent", comumVisualizarPerfilProfissionalComponent())
 .component("comumBriefingDetails", comumBriefingDetailsComponent())
 .component("comumProjectComponent", comumProjectComponent())

// Config Value
.value("comumConfig", comumConfig())

// Directives
 .directive("profileImage", comumProfileImageDirective)
 .directive("baseUrl", comumBaseUrlDirective)
 .directive("justNumber", comumJustNumberDirective)
 .directive("convertToNumber", convertToNumber)
 .filter('phoneFilter', function() {
   return function(input) {
  	 var str = input+ '';
     str = str.replace(/\D/g,'');
     if(str.length === 11 ){
       str=str.replace(/^(\d{2})(\d{5})(\d{4})/,'($1) $2-$3');
     } else {
      str=str.replace(/^(\d{2})(\d{4})(\d{4})/,'($1) $2-$3');
     }
     return str;     
   }
 })

// Run
.run(comumRun);


angular.module("Comum").config(["$httpProvider", function ($httpProvider) {
  $httpProvider.interceptors.push(['$q', '$window', '$rootScope', "toaster", function ($q, $window, $rootScope, toaster) {
  return {
   'responseError': function (response) {
    //Check if has authorization
     if (response.status === 401) {
       toaster.pop({
         type: 'error',
         title: "Sessão expirada!",
         body: "Logue-se novamente",
         onHideCallback: function () {
           if (window.location.href.indexOf('beta') > -1 || window.location.href.indexOf('localhost') > -1) {
             window.location.href = 'http://beta.crowd.br.com';
            } else {
             window.location.href = 'https://crowd.br.com/login/';
            }
         }
       });
      //  publicService.logout();
       window.location.href = 'https://crowd.br.com/login/';
      }

      if (response.status != 200 && response.status != 412) {
        window.Rollbar.critical("Interceptor:" + ' ' + response.status + ' ' + response.statusText, response);
      }

    return response;
   }
  }
  }]);
}]);
