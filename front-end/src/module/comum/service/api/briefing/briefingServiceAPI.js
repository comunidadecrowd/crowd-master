briefingServiceAPI.$inject = [
  "$http",
  "comumConfig",
];

function briefingServiceAPI($http, comumConfig) {
  var service = this;

  // Public
  service.getBriefing = getBriefing;
  service.getBriefings = getBriefings;
  service.getBriefingPropostas = getBriefingPropostas;
  service.arquivarBriefing = arquivarBriefing;
  service.getBriefingDetalhes = getBriefingDetalhes;

  // Private
  var baseUrl = comumConfig.baseUrl();

  function getBriefing(idBriefing) {
    return $http.get(baseUrl + "/json/briefing-edit.json");
  }

  function getBriefings() {
    return $http.get(baseUrl + "/json/briefings.json");
  }

  function getBriefingPropostas(idBriefing) {
    return $http.get(
      baseUrl + "/json/briefing-propostas.json?id=" + idBriefing
    );
  }

  function getBriefingDetalhes(idBriefing) {
    return $http.get(baseUrl + "/json/briefing-detalhes.json?id=" + idBriefing);
  }

  function arquivarBriefing(data) {
    return $http.get(baseUrl + "/json/briefing-arquivar.json?id=" + 1);
  }
}
