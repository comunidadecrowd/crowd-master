function serviceBriefingProfissional() {
  // Private
  var profissionais = [];
  var profissional = false;
  var briefing = false;
  var idBriefing = null;

  var service = this;

  // Public
  service.getProfissionais = getProfissionais;
  service.setProfissionais = setProfissionais;
  service.setBriefing = setBriefing;
  service.isBriefing = isBriefing;
  service.getIdBriefing = getIdBriefing;
  service.setIdBriefing = setIdBriefing;

  // Functions
  function getProfissionais() {
    return profissionais;
  }

  function getIdBriefing() {
    return idBriefing;
  }

  function setProfissionais(arrayProfissionais) {
    profissionais = arrayProfissionais;
  }

  function setBriefing(value) {
    briefing = value;
  }

  function isBriefing() {
    return briefing;
  }

  function setProfissional(value) {
    profissional = value;
  }

  function setIdBriefing(id) {
    idBriefing = id;
  }
}
