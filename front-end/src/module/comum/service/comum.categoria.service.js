comumCategoriaService.$inject = [ '$http', 'comumConfig' ];
function comumCategoriaService($http, comumConfig)
{

	var service = this;
	// Public
	service.getCategorias = getCategorias;	

	// Private
	var baseUrl = comumConfig.baseUrl();

	function getCategorias()
	{	
		return $http.get( baseUrl + '/json/categorias.json' );
	}

}