function comumConfig() {
    /**
     * @deprecated não deve haver mais uso desta variável
     */
    // var projetos = true;

    function baseUrl() {
        var absUrl = window.location.href;
        if (absUrl.indexOf('localhost') > -1) {
            return 'http://localhost:8080';
        } else if (absUrl.indexOf('comunidadecrowd') > -1) {
            return 'http://comunidadecrowd.com.br';
        }
    }

    function baseUrlAPI() {
        var absUrl = window.location.href;

        if (absUrl.indexOf('homolog') > -1 || absUrl.indexOf('localhost') > -1)
            return 'http://homolog.api.comunidadecrowd.com.br';
        
        if (absUrl.indexOf('beta') > -1)
            return 'http://beta.api.comunidadecrowd.com.br';
        
        return 'https://api.crowd.br.com';
    }

    /**
     * Função para o Google Analytics registrar saídas para Portfolios
     * @param {object} event [evento do link clicado]
     * @deprecated Usar convenção do Angulartics
     */
    function outboundPortfolio(event) {
        event.preventDefault();
        window.ga('send', 'event', {
            eventCategory: 'Portfolio',
            eventAction: 'click',
            eventLabel: event.target.href,
            transport: 'beacon'
        });
    }

    function removeObjectFromArray(array, prop, value) {
        return array.filter(function(val) {
            return val[prop] !== value;
        });
    }

    function removeItemFromArray(array, value) {
        return array.filter(function(val) {
            return val !== value;
        });
    }
    function removeItemFromArrayByIndex(array, indice) {
        return array.filter(function(value, index) {
            return index !== indice;
        });
    }
    function searchObjectInArray(array, prop, value) {
        var result = false;
        array.forEach(function(val, index) {
            if (val[prop] === value)
                result = val;
        });
        return result;
    }
    function searchTextInObjectArray(array, prop, string) {
        return array.filter(function(value) {
            var text = value[prop].toLowerCase();
            string = string.toLowerCase();
            return text.indexOf(string) >= 0;
        });
    }
    function getPropertyValueFromObjectArray(array, prop) {
        var arrayValue = [];
        array.forEach(function(obj, index) {
            arrayValue.push(obj[prop]);
        });
        return arrayValue;
    }
    function getLastElementFromArray(array) {
        return array[array.length - 1];
    }
    function getElement(element) {
        return angular.element(element);
    }
    function addCss(element, prop, value) {
        getElement(element).css(prop, value);
    }
    function removeClass(element, classe) {
        getElement(element).removeClass(classe);
    }
    function addClass(element, classe) {
        getElement(element).addClass(classe);
    }
    function getClickedElement(event) {
        return event.target.nodeName;
    }
    function addStarRating(quality, responsability, agility) {
        var sum = quality + responsability + agility;
        var media = sum / 3;
        var htmlStar = '<i class="icon wb-star" aria-hidden="true"></i>';
        var fullStar = 'icon wb-star star-complete';
        var totalStar = 5;
        var stars = [];
        for (var i = 1; i <= totalStar; i++) {
            if (media) {
                if (i <= media)
                    stars.push(htmlStar.replace('icon wb-star', fullStar));
                else
                    stars.push(htmlStar);
            } else {
                stars.push(htmlStar);
            }
        }
        return stars.join('');
    }
    function searchStarRating(media) {
        var htmlStar = '<i class="icon wb-star" aria-hidden="true"></i>';
        var fullStar = 'icon wb-star star-complete';
        var totalStar = 5;
        var stars = [];
        for (var i = 1; i <= totalStar; i++) {
            if (media) {
                if (i <= media)
                    stars.push(htmlStar.replace('icon wb-star', fullStar));
                else
                    stars.push(htmlStar);
            } else {
                stars.push(htmlStar);
            }
        }
        return stars.join('');
    }
    function getFirstAndLastWord(string) {
        var stringSplit = string.split(' ');
        if (stringSplit.length > 1)
            return stringSplit[0] + ' ' + stringSplit[stringSplit.length - 1];
        return stringSplit[0];
    }
    /**
     * Inverte dia e mês do formato 'XX / XX / XXXX XX: XX' para funcionar com o Moment.js
     * @deprecated Não deveria ser necessária caso o Webservice retornasse a data em ISO 8601
     * @param   {string}   date Recebida pela View
     * @returns {string} Valor literal
     */
    function fixDate(date) {
        if (date)
            return date.substr(3, 2) +
                "/" +
                date.substring(0, 2) +
                "/" +
                date.substr(6, 10);
        else
            return false;
    }
    function asScrollable(element) {
        return {
            add: function() {
                window.$jQuery(element).asScrollable({
                    namespace: "scrollable",
                    contentSelector: "> [data-role='content']",
                    containerSelector: "> [data-role='container']"
                });
            },
            update: function() {
                window.$jQuery(element).asScrollable("update");
            }
        };
    }
    function slideToggle(element, type, callback) {
        if (callback === undefined)
            callback = function() {};
        window.$jQuery(element).slideToggle(type, callback);
    }
    function toggleClass(element, classe) {
        getElement(element).toggleClass(classe);
    }
    function cortarTexto(texto, limit) {
        if (!texto) {
            return false
        }
        if (limit !== undefined)
            if (texto.length > limit)
                return texto.substring(0, limit) + '...';
        return texto;
    }
    function blockScreen(textLoading, elementBlock) {
        if (textLoading === undefined)
            textLoading = 'Aguarde...';
        if (elementBlock === undefined)
            elementBlock = '.page-content';
        window.$jQuery(elementBlock).block({
            message: '<img src="/images/ajax-loader.gif"><br><br><p style="font-size: 13px; margin-top: -10px;">' + textLoading + '</p>',
            css: {
                border: 'nome',
                width: '200',
                height: '100',
                background: 'none'
            },
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.5,
            }
        });
    }
    function unblockScreen(elementBlock) {
        if (elementBlock === undefined)
            elementBlock = '.page-content';
        window.$jQuery(elementBlock).unblock();
    }
    function addText(element, text) {
        
        if( text === undefined )
            return getElement(element).text();
        getElement(element).text(text);
    }
    function html(element, html) {
        if (html !== undefined)
            getElement(element).html(html);
        return getElement(element).html();
    }
    function displayBlock(element) {
        addCss(element, 'display', 'block');
    }
    function displayNone(element) {
        addCss(element, 'display', 'none');
    }
    function fadeIn(element) {
        window.$jQuery(element).fadeIn();
    }
    function fadeOut(element) {
        window.$jQuery(element).fadeOut();
    }
    function addPopover(element, optionsObj) {
        window.$jQuery(element).webuiPopover(optionsObj);
    }
    function attr(element, attr, value) {
        if (value !== undefined)
            getElement(element).attr(attr, value);
        else
            return getElement(element).attr(attr);
    }
    function bodyOverflow(status) {
        var overflow = (!status) ? 'hidden' : '';
        addCss('body', 'overflow', overflow);
    }
    function addSelect2(element, dataArray, searchField) {
        var options = {};
        if (dataArray)
            options.data = dataArray;
        if (!searchField)
            options.minimumResultsForSearch = -1;
        window.$jQuery(element).select2(options);
    }
    function findElement(targetElement, parentElement) {
        if (parentElement === undefined)
            parentElement = 'body';
        return getElement(parentElement).find(targetElement);
    }
    function val(element, value) {
        if (value === undefined)
            return getElement(element).val();
        getElement(element).val(value);
    }
    function quillEditor(element) {
        if (element === undefined)
            throw 'Quill is element undefined.';
        return {
            add: function(options) {
                var quillOptions = {
                    theme: 'snow'
                };
                if (options !== undefined) {
                    if (options.toolbar !== undefined)
                        quillOptions.modules = {
                            toolbar: options.toolbar
                        };
                    if (options.placeholder !== undefined)
                        quillOptions.placeholder = options.placeholder;
                }
                var quill = new window.Quill(element, quillOptions);
                if (options !== undefined)
                    if (options.events !== undefined)
                        if (options.events.textChange !== undefined)
                            quill.on('text-change', options.events.textChange);
                quill.focus();
            },
            setContent: function(content) {
                html(element + ' .ql-editor', content);
            },
            getContent: function() {
                return html(element + ' .ql-editor');
            }
        };
    }
    function isEmail(email) {
        var atpos = email.indexOf("@");
        var dotpos = email.lastIndexOf(".");
        if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length)
            return false;
        return true;
    }
    function getParent(element, level) {
        var i = 0;
        if (level === undefined)
            level = 1;
        while (i < level) {
            element = getElement(element).parent();
            i++;
        }
        return element;
    }
    /**
     * Filtro para reverter a ordem de um NgRepeat
     * @param   {object} items Lista de Objetos
     * @returns {object} Lista de Objetos reversa
     */
    function reverse(items) {
        return items.slice().reverse();
    }
    function addDropzone(configObj) {
        var options = {};
        if (configObj === undefined)
            throw 'Dropzone configuration undefined.';
        if (!configObj.element || configObj.element === undefined)
            throw 'Dropzone element is undefined.';
        if (!configObj.url || configObj.url === undefined)
            throw 'Dropzone url is undefined.';
        if (!configObj.url.url || configObj.url.url === undefined)
            throw 'Dropzone url is undefined.';
        if (configObj.options !== undefined) {
            if (!configObj.options.name)
                throw 'Dropzone options name is undefined.';
            if (configObj.options.previewTemplate !== undefined)
                options.previewTemplate = document.getElementById(configObj.options.previewTemplate).innerHTML;
            if (configObj.options.previewsContainer !== undefined)
                options.previewsContainer = document.getElementById(configObj.options.previewsContainer);
            if (configObj.options.createImageThumbnails !== undefined)
                options.createImageThumbnails = configObj.options.createImageThumbnails;
            if (configObj.options.maxFilesize !== undefined)
                options.maxFilesize = configObj.options.maxFilesize;
            if (configObj.options.init !== undefined)
                options.init = function() {
                    if (configObj.options.init.dropData !== undefined)
                        dropData = configObj.options.init.dropData;
                    if (configObj.options.init.addedfile !== undefined)
                        this.on('addedfile', configObj.options.init.addedfile);
                    if (configObj.options.init.success !== undefined)
                        this.on('success', configObj.options.init.success);
                    if (configObj.options.init.queuecomplete !== undefined)
                        this.on('queuecomplete', configObj.options.init.queuecomplete);
                    if (configObj.options.init.maxfilesexceeded !== undefined)
                        this.on('error', configObj.options.init.maxfilesexceeded);
                };
            if (configObj.options.maxFiles !== undefined)
                options.maxFiles = configObj.options.maxFiles;
                Dropzone.options[configObj.options.name] = options;
                Dropzone.options.headers = {
                'Authorization': 'Bearer ' + window.localStorage.getItem('ngStorage-token')
            };
        }
        var myDropzone = new Dropzone(configObj.element, configObj.url);
    }
    function addFullPathUserPhoto(array, prop) {
        if (array.length > 0) {
            array.forEach(function(objUser, index) {
                if (objUser[prop])
                    objUser[prop] = baseUrlAPI() + objUser[prop];
                else
                    objUser[prop] = false;
            });
        }
        return array;
    }
    function removeAttr(element, attr) {
        getElement(element).removeAttr(attr);
    }
    function removeElement(element) {
        getElement(element).remove();
    }
    function scrollPageslideBriefing($timeout, element) {
        
        element = ( element === undefined ) ? '.janela-profissional-briefing' : element;
        $timeout(function() {
            var height = window.$jQuery(element).height();
            window.$jQuery('.pageslide-900').animate({
                scrollTop: height
            }, 1000);
        }, 500, false);
    }
   /**
    * @name extractAlphanumeric
    * @description Helper que simplifica uma string removendo todos caractere não alfanumérico inclusive espaços em branco
    * @return {string} - String tratada
    * @version 1.1
    * @author Leandro Pitta v1, Danilo Verde v1.1
    */
    function extractAlphanumeric( string ) {
      return string.replace(/\W+/g, " ");
    }
    return {
        baseUrl: baseUrl,
        baseUrlAPI: baseUrlAPI,
        removeObjectFromArray: removeObjectFromArray,
        removeItemFromArray: removeItemFromArray,
        removeItemFromArrayByIndex: removeItemFromArrayByIndex,
        searchObjectInArray: searchObjectInArray,
        searchTextInObjectArray: searchTextInObjectArray,
        getPropertyValueFromObjectArray: getPropertyValueFromObjectArray,
        getElement: getElement,
        removeClass: removeClass,
        addClass: addClass,
        addCss: addCss,
        getClickedElement: getClickedElement,
        addStarRating: addStarRating,
        getFirstAndLastWord: getFirstAndLastWord,
        asScrollable: asScrollable,
        slideToggle: slideToggle,
        toggleClass: toggleClass,
        cortarTexto: cortarTexto,
        blockScreen: blockScreen,
        unblockScreen: unblockScreen,
        addText: addText,
        html: html,
        displayBlock: displayBlock,
        displayNone: displayNone,
        fadeIn: fadeIn,
        fadeOut: fadeOut,
        addPopover: addPopover,
        attr: attr,
        bodyOverflow: bodyOverflow,
        addSelect2: addSelect2,
        findElement: findElement,
        val: val,
        quillEditor: quillEditor,
        isEmail: isEmail,
        getParent: getParent,
        addDropzone: addDropzone,
        getLastElementFromArray: getLastElementFromArray,
        addFullPathUserPhoto: addFullPathUserPhoto,
        removeAttr: removeAttr,
        removeElement: removeElement,
        scrollPageslideBriefing: scrollPageslideBriefing,
        fixDate: fixDate,
        projetos: true,
        reverse: reverse,
        extractAlphanumeric: extractAlphanumeric,
        searchStarRating: searchStarRating
    };
}