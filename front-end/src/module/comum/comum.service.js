comumService.$inject = [
  "$http",
  "comumConfig",
  "$rootScope",
  "publicService",
  "$localStorage",
  "toaster",
  "$interval"
];

function comumService(
  $http,
  comumConfig,
  $rootScope,
  publicService,
  $localStorage,
  toaster,
  $interval
) {
  var self = this;

  // Public
  $rootScope.projetos = comumConfig.projetos;
  $rootScope.setUser = setUser;
  $rootScope.getProjetos = [];
  $rootScope.getUser = getUser;
  $rootScope.getUserPlan = getUserPlan;

  self.init = init;

  self.getCategorias = getCategorias;
  self.getPaises = getPaises;
  self.getEstados = getEstados;
  self.getCidades = getCidades;
  self.getSegmentos = getSegmentos;
  self.getDisponibilidades = getDisponibilidades;
  self.getSkills = getSkills;
  self.redirectTo = redirectTo;
  self.setUser = setUser;
  self.getProfile = getProfile;
  //self.redirectTo = redirectTo;
  self.isProspect = false;

  self.criaProjeto = $rootScope.criaProjeto;

  self.getUser = getUser;
  self.setUserCargo = setUserCargo;
  self.setUserPhoto = setUserPhoto;
  self.setLogo = setLogo;
  self.loadMenu = loadMenu;
  self.loadImagemPerfil = loadImagemPerfil;
  self.getBriefings = getBriefings;
  self.getBriefing = getBriefing;
  self.sendMessageToCustomer = sendMessageToCustomer;
  self.readMessage = readMessage;
  self.getProjetos = getProjetos;
  self.getProjeto = getProjeto;
  self.redirectTo = redirectTo;
  //window.redirectTo = redirectTo;
  self.getProjetosArquivados = getProjetosArquivados;
  self.getProjetoArquivado = getProjetoArquivado;
  self.dateTrim = dateTrim;
  self.orderObjectBy = orderObjectBy;

  self.getNotificationProfessional = getNotificationProfessional;
  self.getNotification = getNotification;
  self.getUserPlan = getUserPlan;
  self.hasPlan = hasPlan;
  self.getPlanIdByName = getPlanIdByName;
  self.getPlanValueByName = getPlanValueByName;
  self.readSystemMessage = readSystemMessage;

  self.plans = ["FREE", "START", "PRO", "ENTERPRISE"];

  self.percentageCrowd = 8;

  $rootScope.inputProjeto = [];

  // Private
  var usuario = {};
  var baseUrl = comumConfig.baseUrl();
  var baseUrlAPI = comumConfig.baseUrlAPI();
  var _userPlan = null;

  console.debug('123', usuario);
  $rootScope.usuario = usuario;
  console.debug('ÓLAR2', $rootScope.usuario);


  // Functions
  function getCategorias() {
    return publicService.getCategories();
  }

  function getPaises() {
    return publicService.getCountries();
  }

  function getEstados() {
    return publicService.getStates();
  }

  /**
   * @deprecated
   */
 /* function redirectTo() {
    if (usuario) {
      console.debug('loggedUser', usuario);
      $rootScope.getProjetos = [];
      console.log($rootScope.getProjetos);
    }
    return usuario;
  } */

  function getCidades(idEstado) {
    return publicService.getCities(idEstado);
  }

  function getSegmentos() {
    return publicService.getSegments();
  }

  function getDisponibilidades() {
    return publicService.getAvailabilities();
  }

  function getSkills() {
    return publicService.getSkills();
  }

  function setUser(userData) {
    console.debug('DEBUG', userData);
    usuario.id = userData.Id;
    usuario.IdFreelancer = userData.IdFreelancer;
    usuario.userId = userData.Id;
    usuario.email = userData.Email;
    usuario.nome = userData.Name;
    usuario.Name = userData.Name;
    usuario.Role = userData.Role;
    usuario.cargo = userData.RoleCompany;
    usuario.imagem_perfil = comumConfig.baseUrlAPI() + userData.Photo;
    usuario.logotipo = comumConfig.baseUrlAPI() + userData.CustomerLogo;
    usuario.empresa = userData.Customer;
    usuario.Photo = userData.Photo;
    usuario.IdCustomer = userData.IdCustomer;
    usuario.loggedUser = userData;
    usuario.CustomerPlan = userData.CustomerPlan;
    usuario.Paid = userData.Paid;

    _setUserPlan(userData.CustomerPlan);
  }

  function getUser() {
    return usuario;
  }
  
 /* function redirectTo() {
    var currentUrl = window.location.href;

    if (usuario.loggedUser.Prospect == true && currentUrl.indexOf('company-profile') === -1) {
      self.isProspect = true;
     // $state.go('')
    }
    if (usuario.loggedUser.Prospect == true && currentUrl.indexOf('company-profile') > -1) {
      self.isProspect = true;
    }

    console.log(self.isProspect);
  } */

  function setUserCargo(cargo) {
    usuario.cargo = cargo;
    $localStorage.user.RoleCompany = cargo;
  }

  function setUserPhoto(photo) {
    usuario.imagem_perfil = comumConfig.baseUrlAPI() + photo;
    usuario.Photo = photo;
    $localStorage.user.Photo = photo;
  }

  function setLogo(logo) {
    usuario.logotipo = comumConfig.baseUrlAPI() + logo;
  }

  /**
   * @deprecated O webservice não retornará mais este menuData durante o login, deve ficar aqui até final da replicação do cache
   */
  function loadMenu(menuData) {
    var user = getUser();

    if (user.Role === 2) {
      //NOTE: ROTA DO FREELANCER É DIFERENTE DAS OUTRAS
      var report = [
        {
          href: "freelancerReports",
          iconClass: "fa-line-chart",
          id: 5,
          title: "Relatórios",
        },
      ];
      var menuList = menuData.concat(report);
      $rootScope.menuPrincipal = menuList;
      console.log(menuList);
    } else {
      var report = [
        {
          href: "companyReports",
          iconClass: "fa-line-chart",
          id: 5,
          title: "Relatórios",
        },
      ];
      var menuList = menuData.concat(report);
      $rootScope.menuPrincipal = menuList;
      console.log(menuList);
    }
  }

  function redirectTo() {
    var currentUrl = window.location.href;
    var lastAttempt = window;

    console.debug('window', lastAttempt);
    
    if ($rootScope.usuario.loggedUser.Prospect == true && !$rootScope.usuario.loggedUser.IdFreelancer && currentUrl.indexOf('company-profile') == -1) {
      $rootScope.isProspect = true;
      $state.go('company-profile');
    }
    if ($rootScope.usuario.loggedUser.Prospect == true && !$rootScope.usuario.loggedUser.IdFreelancer && currentUrl.indexOf('company-profile') > -1) {
     $rootScope.isProspect = true;
    }
    if ($rootScope.usuario.loggedUser.Prospect == true && $rootScope.usuario.loggedUser.IdFreelancer && currentUrl.indexOf('profile') == -1) {
       $rootScope.isProspect = true;
       $state.go('profile');
     }
    if ($rootScope.usuario.loggedUser.Prospect == true && $rootScope.usuario.loggedUser.IdFreelancer && currentUrl.indexOf('profile') > -1) {
       $rootScope.isProspect = true;
    }
    if ($rootScope.usuario.loggedUser.Prospect == false) {
       $rootScope.isProspect = false;
    }
  }

  function loadImagemPerfil() {
    if (usuario.Photo)
      comumConfig.attr("#imagem-perfil-logado", "src", usuario.imagem_perfil);
  };

  function getBriefings() {
    var user = getUser();
    var id = user.Role === 2 ? user.userId : user.id;
    return $http.post(baseUrlAPI + "/Briefing/List?user_id=" + id);
  }

  function getBriefing(briefingId) {
    var user = getUser();
    var id = user.Role === 2 ? user.userId : user.id;

    var config = {
      params: {
        userId: id,
        briefingId: briefingId,
      },
    };

    return $http.get(baseUrlAPI + "/Briefing/Get", config);
  }

  function sendMessageToCustomer(messageData, task) {
    if (task)
      var data = {
        FreelancerId: messageData.professionalId,
        TaskId: messageData.briefingId,
        Text: messageData.message,
        Price: messageData.Price,
        DeadlineDays: messageData.DeadlineDays,
      };
    else
      var data = {
        FreelancerId: messageData.professionalId,
        BriefingId: messageData.briefingId,
        Text: messageData.message,
        Price: messageData.Price,
        DeadlineDays: messageData.DeadlineDays,
      };

    if (task) return $http.post(baseUrlAPI + "/Task/AddMessage", data);
    else return $http.post(baseUrlAPI + "/Briefing/AddMessage", data);
  }

  /**
 * Realiza a marcação de mensagens dentro do BriefingDetails como lidas
 * @description É chamado dentro do loop que prepara as mensagens dentro do pageslide
 * @param {string} messageId Id da mensagem que vai recerber a marcação como lida
 * @param {boolean} task Condicional que troca o endpoint da API
 * @return {object} retorna objeto do webservice
 */
  function readMessage(messageId, task) {
    var userId = usuario.userId;
    if (task) {
      var data = {
        user_id: userId,
        message_id: messageId,
      };

      return $http.post(baseUrlAPI + "/Task/ReadMessage?user_id="+ data.user_id+ "&message_id="+ data.message_id, data);
    } else {
      var data = {
        user_id: userId,
        message_id: messageId,
      };

      return $http.post(baseUrlAPI + "/Briefing/ReadMessage?user_id="+ data.user_id+ "&message_id="+ data.message_id, data);
    }
  }

  /**
     * GET webservice lista de projetos para o MENU
     * @param   {String} userId [ID do User]
     * @param   {Array} data [Array de configurações]
     * @returns {Object} [JSON contendo todos os projetos do usuário]
     */
  function getProjetos(userId, data) {
    // var config = {};
    if (!userId) userId = usuario.userId;
    if (data !== undefined)
      var config = {
        params: data,
      };
    return $http
      .get(baseUrlAPI + "/Project/List?userId=" + userId + "&status=true")
      .success(function callBack(response) {
        $rootScope.getProjetos = response;
        return response;
      });
  }
  /**
     * GET projeto único do banco
     * @param   {string} Id ID do projeto a ser requisitado
     * @returns {object} Objeto completo do Webservice
     */
  function getProjeto(Id) {
    return $http
      .get(baseUrlAPI + "/Project/Get?projectId=" + Id)
      .success(function callBack(response) {
        $rootScope.Projeto = response;
      });
  }

  /**
     * GET webservice lista de projetos Arquivados para o MENU
     * @param   {String} userId [ID do User]
     * @param   {Array} data [Array de configurações]
     * @returns {Object} [JSON contendo todos os projetos do usuário]
     */
  function getProjetosArquivados(userId, data) {
    // var config = {};
    if (!userId) userId = usuario.userId;
    if (data !== undefined)
      var config = {
        params: data,
      };
    return $http
      .get(baseUrlAPI + "/Project/List?userId=" + userId + "&status=false")
      .success(function callBack(response) {
        $rootScope.getProjetosArquivados = response;
      });
  }

  /**
     * GET projeto arquivado único do banco
     * @param   {string} Id ID do projeto a ser requisitado
     * @returns {object} Objeto completo do Webservice
     */
  function getProjetoArquivado(Id) {
    return $http
      .get(baseUrlAPI + "/Project/Get?projectId=" + Id)
      .success(function callBack(response) {
        $rootScope.Projeto = response;
      });
  }

  /**
     * Método em loop atualizando as notificações em 2.5s
     * @param {string} userId Id do usuário
     */
  function getNotification(userId) {
    return $http
      .get(baseUrlAPI + "/Briefing/Message/Count?user_id=" + userId)
      .success(function callBack(response) {
        $rootScope.briefingNotifica = response.total;
      })
      .error(function callBack(response) {
        console.log("Não foi possível obter as notificações!", response);
      });

    /*$rootScope.menuNotification(function callBack() {

        });*/
  }

  /**
     * POST de Projeto vindo do input na lateral do menu
     * @param {object} element [Objeto completo do input]
     * @param {object} briefing [Objeto completo do briefing]
     * @returns {object} [Limpa o valor do input se gravou]
     */
  $rootScope.criaProjeto = function(element, briefing) {
    if (!element) {
      element = {};
      element.target = angular.element(
        document.querySelector("#inputProjeto")
      )[0];
    }

    if (briefing) {
      element.target = angular.element(
        document.querySelector("#inputBriefingProjeto")
      )[0];
    }

    var user = getUser();
    var name = element.target.value;
    var id = user.Role === 2 ? user.userId : user.id;

    if (!name) {
      toaster.pop("error", "", "Preencha o nome do projeto");
      angular.element(document.querySelector("#inputProjeto"))[0].focus();
      return false;
    }

    // PREVINE INSERIR DUAS VEZES
    element.target.disabled = true;

    // PODER SER QUE VOCÊ ESTEJA FAZENDO UM HUNTING?
    hunting = JSON.parse(window.localStorage.getItem('USR_HTG'));

    if (hunting) {
      var data = {
        Id: 0,
        Name: name,
        IdUser: hunting[0].Id, // ESTOU GRAVANDO PARA O USUÁRIO CORRETO
      };

    } else { // OU TALVEZ NÃO SEJA HUNTING
      var data = {
        Id: 0,
        Name: name,
        IdUser: id,
      };
    }

    return $http
      .post(baseUrlAPI + "/Project/Add", data)
      .success(function callBack(response) {
        self.getProjetos(usuario.userId).success(function callBack(response) {
          $rootScope.getProjetos = response;
          if (briefing) {
            setTimeout(function() {
              angular
                .element(
                  document.querySelector(
                    ".selecionar-projeto-briefing li:nth-child(2)"
                  )
                )
                .triggerHandler("click");
              angular
                .element(
                  document.querySelector(
                    ".selecionar-projeto-briefing li:nth-child(2)"
                  )
                )
                .trigger("click");
            }, 300);
          }
        });
        element.target.value = "";
        element.target.disabled = false;
        element.target.blur();

        $rootScope.$broadcast("event:getProjetos", $rootScope);
      });
  };

  /**
     * Função para utilizar os 5 primeiros dígitos das datas DD/MM
     * @param {string} date Data vinda do WS sendo tratada através da View
     */
  function dateTrim(date) {
    if (date) return date.substring(0, 5);
    else return false;
  }

  /**
     * Ordena objetos dentro de um NgRepeat
     * @example projeto in orderObjectBy(getProjetos, 'Id', true) track by projeto.Id
     * @param   {object} items   Input do objeto
     * @param   {string} field   Nome do campo pelo qual será ordenado
     * @param   {boolean} reverse Reverte ou não a sequência
     * @returns {object} Output do objeto agora ordenado
     **/
  function orderObjectBy(items, field, reverse) {
    var filtered = [];
    angular.forEach(items, function(item) {
      filtered.push(item);
    });
    filtered.sort(function(a, b) {
      return a[field] > b[field] ? 1 : -1;
    });
    if (reverse) filtered.reverse();
    return filtered;
  }

  function getNotificationProfessional(userId, data) {
    // var config = {};
    if (!userId) userId = usuario.userId;
    if (data !== undefined)
      var config = {
        params: data,
      };

    return $http
      .get(baseUrlAPI + "/Professional/Tasks/Count?user_Id=" + userId)
      .success(function callBack(response) {
        $rootScope.getNotificationProfessional = response;
      });
  }

  function getProfile(professionalId) {
    return $http.get(
      baseUrlAPI + "/User/Get?code=" + encodeURIComponent(professionalId)
    );
  }

  function init() {
    setUser($localStorage.user);
    loadImagemPerfil();
    if($localStorage.menu != undefined) {
      loadMenu($localStorage.menu);
    }    

    var user = getUser();

    if (user.Role !== 2) {


        //NOTE: LOOP DAS NOTIFICAÇÕES      
        window.idleTime = 0;
        jQuery(document).ready(function () { //COMEÇA QUANDO O DOM ESTIVER PRONTO
            //INCREMENTA DENTRO DE UM INTERVALO
            window.idleInterval = $interval(timerIncrement, 30000); //MEIO MINUTO

            //ZERA O INTERVALO SE HOUVER INTERAÇÕES NA PLATAFORMA
            jQuery(this).mousemove(function (e) {
                window.idleTime = 0;
            });
            jQuery(this).keypress(function (e) {
                window.idleTime = 0;
            });
        });

        function timerIncrement() {
            window.idleTime++
            if (window.idleTime < 5) { //SE FOR MENOR DE 2 MINUTOS E MEIO
              getNotification(usuario.userId); //VERIFICA NOTIFICAÇÕES              
              getProjetos(usuario.userId); //VERIFICA NOTIFICAÇÕES
            }
        }

        getNotification(usuario.userId); //VERIFICA NOTIFICAÇÕES              
      
      $rootScope.getProjetos = self
        .getProjetos(usuario.userId)
        .then(function callBack(response) {
          return response;
        });
      // $rootScope.getProjetosArquivados = self
      //   .getProjetosArquivados(usuario.userId)
      //   .then(function callBack(response) {
      //     return response;
      //   });
    
  
    } else { //NOTE: USUÁRIO É FREELANCER
      
      $rootScope.getNotificationProfessional = self
        .getNotificationProfessional(usuario.userId)
        .then(function callBack(response) {
          return response;
        });
      
        //NOTE: LOOP DAS NOTIFICAÇÕES   
        window.idleTime = 0;
        jQuery(document).ready(function () { //COMEÇA QUANDO O DOM ESTIVER PRONTO
            //INCREMENTA DENTRO DE UM INTERVALO
            window.idleInterval = $interval(timerIncrement, 30000); //MEIO MINUTO

            //ZERA O INTERVALO SE HOUVER INTERAÇÕES NA PLATAFORMA
            jQuery(this).mousemove(function (e) {
                window.idleTime = 0;
            });
            jQuery(this).keypress(function (e) {
                window.idleTime = 0;
            });
        });

        function timerIncrement() {
          window.idleTime++
          if (window.idleTime < 5) { //SE FOR MENOR DE 2 MINUTOS E MEIO
            getNotification(usuario.userId); //VERIFICA NOTIFICAÇÕES
            $rootScope.getNotificationProfessional = self
              .getNotificationProfessional(usuario.userId)
              .then(function callBack(response) {
                return response;
            });

          }
        }
    }

    $rootScope.orderObjectBy = orderObjectBy;
  }

  function _setUserPlan(plan) {
    _userPlan = plan === undefined || !plan ? null : plan;
  }

  function getUserPlan(argument) {
    return _userPlan;
  }

  function hasPlan(plan) {
    return _userPlan === plan;
  }

  function getPlanIdByName(planId) {
    switch (planId) {
      case self.plans[0]:
        return 1;
      case self.plans[1]:
        return 2;
      case self.plans[2]:
        return 3;
      case self.plans[3]:
        return 4;
    }
  }

  function getPlanValueByName(plan) {
    switch (plan) {
      case self.plans[0]:
        return 0;
      case self.plans[1]:
        return 150;
      case self.plans[2]:
        return 500;
      case self.plans[3]:
        return 1500;
    }
  }

  function readSystemMessage(message_id) {
    var user_id = getUser()["userId"];

    return $http.post(
      baseUrlAPI +
        "/SystemMessage/Read?user_id=" +
        user_id +
        "&message_id=" +
        message_id
    );
  }
  if (usuario.loggedUser) {
    console.debug('loggedUser', usuario);
    $rootScope.getProjetos = [];
    //redirectTo();
    console.log($rootScope.getProjetos);
  }

}
