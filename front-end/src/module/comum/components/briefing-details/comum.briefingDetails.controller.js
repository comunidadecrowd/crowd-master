comumBriefingDetailsController.$inject = [
  "comumService",
  "$scope",
  "$rootScope",
  "comumConfig",
  "scBriefingService",
  "scProfissionalService",
  "$timeout",
  "toaster",
  "$state",
  "comumProjectService",
];

function comumBriefingDetailsController(
  comumService,
  $scope,
  $rootScope,
  comumConfig,
  scBriefingService,
  scProfissionalService,
  $timeout,
  toaster,
  $state,
  comumProjectService
) {
  var self = this;

  // Private
  var user = {};

  //comumConfig.unblockScreen('.janela-profissional-briefing');

  // Public
  self.isProfessional = false;
  self.error = false;
  self.showMessage = false;
  self.errorMessage = null;
  self.percentageCrowd = null;
  self.taskNameIsEditing = false;
  self.taskId = null;

  self.user = {};
  self.professional = {};
  self.customer = {};
  self.message = {
    Price: null,
    DeadlineDays: null,
  };

  self.baseUrl = comumConfig.baseUrlAPI();

  self.getUserName = getUserName;
  self.getUserPhoto = getUserPhoto;
  self.sendMessage = sendMessage;
  self.getProfile = $rootScope.getProfile;
  self.hireProfessional = hireProfessional;
  self.slideOpenProfile = slideOpenProfile;
  self.visualizaPerfil = visualizaPerfil;
  self.hireButton = hireButton;
  self.showPrice = showPrice;
  self.mostraOrcamento = mostraOrcamento;
  self.checkBriefingStatus = checkBriefingStatus;

  self.apply = function () {
    return $scope.apply();
  };

  // self.professional.Photo =

  // self.isOpenProfile = false;
  self.showFieldsPriceAndDays = showFieldsPriceAndDays;

  //  self.isOpen = {};

  self.dateTrim = comumService.dateTrim;
  self.fixDate = comumConfig.fixDate;
  self.calculateCrowdPercentage = calculateCrowdPercentage;
  self.saveTaskName = saveTaskName;
  // Functions
  function INIT() {
    self.user = comumService.getUser();
    self.isProfessional = self.user.Role === 2 ? true : false;

    // Adicionando o quill dentro do settimeout para não correr o risco de carregar o JS antes da view.
    $timeout(
      function () {
        initQuillEditor();
      },
      1000,
      false
    );
    $timeout(
      function () {
        checkBriefingStatus();
      },
      1300,
      false
    );
  }

  function getUserName(user) {
    if (!user) return self.professional.Name;

    return user.Name;
  }

  function getUserPhoto(user) {
    if (!user) return self.professional.Photo;

    return user.Photo;
  }

  function initQuillEditor() {
    var options = {
      toolbar: ["bold", "italic", "underline"],
      placeholder: "Escreva uma mensagem...",
    };
    comumConfig.quillEditor("#editor")
      .add(options);
    setTimeout(function () {
      window.$jQuery(".msg-preco:not(:last)")
        .hide();
      window.$jQuery(".bt-contratar:not(:last)")
        .hide();
      // console.log("jdjf");
    }, 6000);
  }

  function getMessage() {
    return comumConfig.quillEditor("#editor")
      .getContent();
  }

  function clearMessage() {
    comumConfig.quillEditor("#editor")
      .setContent("");
    self.error = false;
    self.message.Price = null;
    self.message.DeadlineDays = null;
    self.percentageCrowd = null;
  }

  /**
   * Envia mensagem pelo Webservice
   * @description Envia a mensagem e retorna
   * @param   {boolean} task Variação se é Task ou Briefing
   * @returns {boolean} Success ou Error
   */
  function sendMessage(task, message) {

    var percent = 8;
    var factor = 1 + percent / 100;

    var professional = $rootScope.getUser();

    if (!validateMessage()) return false;

    task = typeof task !== "undefined" ? task : false;

    var userId = professional.id;
    var promisse = {};

    self.message.briefingId = self.briefing.Id;
    self.message.professionalId = self.briefing.professionalId;
    self.message.uesrId = userId;
    self.message.message = function () {
      //NOTE: HOTFIX PARA BUG-161

      var message = getMessage();

      if (message == "<p><br></p>") { //NOTE: O QUILL EDITOR COLOCA ISSO EM CAMPOS VAZIOS
        message = "<em>*Segue minha proposta.*</em>";
      }
      return message;

    }();
    self.message.Price = self.message.Price; //FIX-ME: CAPITÃO ÓBVIO? self.message.Price *= factor : null;

    if (self.message.DeadlineDays && self.message.Price) {
      window.Intercom('trackEvent', 'enviou_orçamento', message);
    }

    //console.log(self.message);
    if (self.isProfessional)
      promisse = comumService.sendMessageToCustomer(self.message, task);
    else
      promisse = scBriefingService.sendMessageToProfessional(
        self.message,
        task
      );

    comumConfig.blockScreen("", ".campo-enviar-mensagem");

    promisse
      .success(function (data, status) {
        if (status != 200) {
          toaster.pop("error", "Bad Server", "Sua mensagem não foi enviada.");
          window.Rollbar.critical(
            "comum.briefingDetails.controller > sendMessage",
            data
          );
          return false;
        }

        addUserMessage(data);
        comumConfig.unblockScreen(".campo-enviar-mensagem");
        clearMessage();
        window.Intercom('trackEvent', 'trocou_mensagens');
      })
      .error(function (data) {
        //ROLLBAR
        window.Rollbar.critical(
          "comum.briefingDetails.controller > sendMessage",
          data
        );
        toaster.pop("error", "Bad Server", "Sua mensagem não foi enviada.");
        comumConfig.unblockScreen(".campo-enviar-mensagem");
      });
  }

  function commonDataMessage() {
    return {
      BriefingId: self.briefing.Id,
      CreatedAt: null,
      DeadlineDays: null,
      Freelancer: null,
      FreelancerId: self.professional.Id,
      FreelancerCode: self.professional.Code,
      Id: null,
      Price: null,
      Read: false,
      Text: null,
      User: null,
      UserId: null,
    };
  }

  /**
   * Controla a abertuar e fechamento da lapela com detalhes
   * @param {object}   parent Escopo parente
   * @param {object} attr   Atributos sobre o evento
   */
  function isOpen(parent, attr) {
    $scope.$ctrl.isOpen = attr;
  }

  function getProfessionalMessage(data) {
    var professionalMessage = commonDataMessage();

    professionalMessage.Price = data.model.Price;
    professionalMessage.DeadlineDays = data.model.DeadlineDays;
    professionalMessage.Name = self.user.nome;
    professionalMessage.Text = getMessage();

    return professionalMessage;
  }

  function getCustomerMessage(data) {
    var customerMessage = commonDataMessage();

    customerMessage.CreatedAt = data.model.CreatedAt;
    customerMessage.Id = data.model.Id;
    customerMessage.UserId = data.model.UserId;
    customerMessage.User = self.user.loggedUser;
    customerMessage.Text = getMessage();

    return customerMessage;
  }

  function addUserMessage(data) {
    if (self.isProfessional)
      self.briefing.offers.Messages.push(getProfessionalMessage(data));
    else self.briefing.offers.Messages.push(getCustomerMessage(data));

    if (data.model.Price && data.model.DeadlineDays) {
      mostraOrcamento();
      self.professional.hasOffer = true;
      self.professional.Price = data.model.Price;
      self.professional.DeadlineDays = data.model.DeadlineDays;
    }
  }

  function validateMessage() {
    if (!self.isProfessional) {
      if (messageIsEmpty()) {
        self.error = true;
        self.errorMessage = "Preencha a mensagem.";
        return false;
      }
    } else {
      if (
        messageIsEmpty() &&
        (!self.message.Price || !self.message.DeadlineDays)
      ) {
        self.error = true;
        self.errorMessage =
          "Você deve enviar um Valor e Prazo para esse Briefing, ou uma Mensagem.";
        return false;
      }
    }

    return true;
  }

  function messageIsEmpty() {
    return getMessage() == "<p><br></p>" ? true : false;
  }

  /**
   * Abre o painel com perfil do freelancer
   * @description Esta função é utilizada tanto em gestão de briefing quando projetos
   * @param {object} profile  Objeto contento perfil do cliente
   * @param {string} freelaId ID do Freelancer que será utilizado no Service
   * @param {object} parent   Escopo para alcanÃ§ar a variável isOpenProfile
   */

  function visualizaPerfil(profile, freelaId, parent, param2) {
    var getId;

    for (var i = 0; i < self.briefing.Messages.length; i++) {
      if (self.briefing.Messages[i].FreelancerId == freelaId) {
        console.log(self.briefing.Messages[i]);
        if (!self.briefing.Messages[i].FreelancerCode) {
          getId = self.briefing.Messages[i].Freelancer.Code;
        } else {
          getId = self.briefing.Messages[i].FreelancerCode;
        }
      } else {
        getId = freelaId;
      }
    }
    // self.perfil.profissional.imagemPerfil = comumConfig.attr('#imagem-perfil-' + idProfissional, 'src');
    comumService
      .getProfile(getId)
      .success(function (response) {
        console.log(response);
        // comumConfig.fadeIn('.mask-content-pageslide');
        $rootScope.$broadcast("event:visualizar-perfil", response);

        parent.$parent.$parent.$parent.$ctrl.isOpenProfile = true;
        parent.$parent.$parent.$parent.$ctrl.hideSelectButton = true;

        $rootScope.$broadcast("event:visualizar-perfil", response);
      })
      .error(function callback(data) {
        window.Rollbar.critical("visualizaPerfil", data);
        console.log(data);
      });
  }

  function checkBriefingStatus(briefing) {
    if (briefing.Status == 4) {
      document.getElementsByClassName("enviar-mensagem-briefing")[0].style =
        "display: none";
      console.log(4);
    } else {
      document.getElementsByClassName("enviar-mensagem-briefing")[0].style =
        "display: block";
      console.log(0);
    }
  }

  function slideOpenProfile() {
    //  comumConfig.fadeOut('.mask-content-pageslide');
    self.isOpenProfile = false;
    self.isOpen = true;
  }

  function configShowMesage() {

    // Adicionando o quill dentro do settimeout para não correr o risco de carregar o JS antes da view.
    $timeout(
      function () {
        initQuillEditor();
      },
      1000,
      false
    );
    $timeout(
      function () {
        checkBriefingStatus();
      },
      1300,
      false
    );


    if (self.briefing.Bulletin) {
      self.showMessage = false;
      return false;
    } else if (self.briefing.Contracted) {
      if (self.briefing.SelectedPropose) {
        if (
          self.isProfessional &&
          self.briefing.SelectedPropose.Freelancer.Id !== self.user.id
        ) {
          self.showMessage = false;
          return false;
        }
      }
    }

    $timeout(
      function () {
        initQuillEditor();
      },
      1000,
      false
    );
  }

  function hireButton(userType) {
    var proposeId = getProposeId();

    if (
      self.isProfessional ||
      userType ||
      self.briefing.Contracted ||
      !proposeId
    )
      return false;

    return true;
  }

  function getProposeId() {
    var proposeId = self.briefing.offers.Messages[0];

    return proposeId.ProposeId;
  }

  function hireProfessional(parent) {
    var proposeId = getProposeId();
    var contractData = {
      ProposeId: proposeId,
      FreelancerId: self.professional.Id,
      BriefingId: self.briefing.Id,
    };

    var html = "<figure>";
    html +=
      '<div style="margin: 0 auto; float: none; width: 80px; padding-bottom: 80px;" class="img-profile-50">';
    html +=
      '<div style="background:url(' +
      comumConfig.baseUrlAPI() +
      self.professional.Photo +
      ');background-size: 100% 100%"></div>';
    html +=
      '<p style="color: #90BFF1; width: 400px; margin-left: -160px;">' +
      self.professional.Name +
      "</p>";
    html += "</div>";
    html += "</figure>";

    //NOTE: ALERTA DE CONFIRMAÇÃO DE CONTRATAÇÃO
    window
      .swal({
        title: "Deseja contratar esse profissional?",
        html: html,
        showCancelButton: true,
        cancelButtonText: "Cancelar",
        confirmButtonColor: "#46BE8A",
        confirmButtonText: "Contratar",
        showLoaderOnConfirm: true,
        allowOutsideClick: false,
        reverseButtons: true,
        preConfirm: function () {
          comumConfig.blockScreen("", ".janela-profissional-briefing"); //BLOQUEIA A TELA PARA NÃO REALIZAR NOVAS AÇÕES
          return new Promise(function (resolve, reject) {
            scBriefingService
              .hireProfessional(contractData)
              .success(function (data, status) {
                if (status != 200) { //O SERVER DÊ UM PROBLEMA?
                  window.Rollbar.critical(
                    "comum.briefingDetails.controller > hireProfessional",
                    data
                  );
                  reject("Profissional não contratado.");
                  return false;

                } else { //O SERVER NÃO RESPONDEU COM PROBLEMAS, SEGUE COM A CONTRATAÇÃO

                  var selectedPropose = {
                    Freelancer: {
                      Photo: self.professional.Photo,
                      Name: self.professional.Name,
                    },
                    Price: self.professional.Price,
                    DeadlineDays: self.professional.DeadlineDays,
                  };
                  self.briefing.addHiredProfessional(selectedPropose);
                  self.briefing.isOpen = false;
                  self.briefing.selectedBriefing.Contracted = true;
                  
                  window.ga("send", "event", "Empresa", "Contratou");
                  window.Intercom('trackEvent', 'Contratou');
                  window.fbq('track', 'CompleteRegistration', { value: 'contratou freelancer', });

                  resolve(); //CALLBACK DO PROMISSE DO SWAL
                }

              })
          });
        },
      })
      .then(
        function (data) { // CONFIRMADA A CONTRATAÇÃO
          comumConfig.unblockScreen(".janela-profissional-briefing");
          toaster.pop(
            "success",
            "Parabéns!",
            "Profissional contratado com sucesso!"
          );
          $state.go("project", {
            id: self.briefing.IdProject,
          });
        },
        function (dismiss) { //DEU RUIM NA CONTRATAÇÃO
          comumConfig.unblockScreen(".janela-profissional-briefing");
        }
      );
  }

  function showPrice(user) {
    if (self.briefing.Contracted) return false;

    if (self.professional.Price != undefined && user.User == null) {
      return true;
    }
  }

  function mostraOrcamento() {
    setTimeout(function () {
      window.$jQuery(".msg-preco:not(:last)")
        .hide();
      window.$jQuery(".bt-contratar:not(:last)")
        .hide();
    }, 100);

    if (self.briefing.offers.Messages) {
      setTimeout(function () {
        if (
          window.$jQuery(".msg-preco:visible, .bt-contratar:visible")
          .length ==
          0 &&
          self.briefing.Status == 1
        ) {
          window.$jQuery("#labelAprovaOrcamento, #btnAvancaTask")
            .hide();
        } else {
          window.$jQuery("#labelAprovaOrcamento, #btnAvancaTask")
            .show();
        }
      }, 200);
    }
  }

  function showFieldsPriceAndDays() {
    if (!self.isProfessional || self.briefing.Contracted) return false;

    return true;
  }

  function calculateCrowdPercentage() {
    self.percentageCrowd = Math.abs(
      self.message.Price * comumService.percentageCrowd / 100 -
      self.message.Price
    );
  }

  $scope.$on("event:briefing-details", function (event, data) {
    self.briefing = null;
    self.professional = null;
    self.titleIsEditing = false;
    self.briefing = data;
    self.professional = data.offers.Freelancer;
    self.taskId = self.briefing.Id;

    
    $timeout(function  (){
      var taskName = self.briefing.Title;  
      comumConfig.addText("#taskName", taskName);
      
    }, 500);
    
    window.localStorage.setItem("BF_INFO", JSON.stringify(self.briefing));

    checkBriefingStatus(self.briefing);


    if (self.professional != null) {
      if (
        self.professional.Price == undefined &&
        self.professional.DeadlineDays == undefined
      ) {
        if (data.SelectedPropose != null) {
          self.professional.Price = data.SelectedPropose.Price;
          self.professional.DeadlineDays = data.SelectedPropose.DeadlineDays;
        }
      }
    }
  });

  function saveTaskName(isEditing) {
    if (isEditing !== undefined) self.taskNameIsEditing = isEditing;

    var taskName = comumConfig.addText("#taskName");
    comumProjectService.saveTaskName(self.taskId, taskName);
    comumConfig.addText("#task-orcamento-" + self.taskId, taskName);

    var names = {
      Id: self.taskId,
      prevName: self.briefing.Title,
      newName: taskName,
    };

    $rootScope.$broadcast("TASK:NAME_EDITED", names);
  }

  //Calls
  INIT();
}
