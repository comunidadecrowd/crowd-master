comumProjectComponentController.$inject = [
  "comumProjectService",
  "$scope",
  "$rootScope",
  "comumService",
  "comumConfig",
  "publicService",
  "scBriefingService",
  "scProfissionalService",
  "$timeout",
  "$stateParams",
  "toaster",
  "$state",
  "orderByFilter",
  "$sessionStorage",
  "ngDialog",
  "scFirstAccessService",
];

function comumProjectComponentController(
  comumProjectService,
  $scope,
  $rootScope,
  comumService,
  comumConfig,
  publicService,
  scBriefingService,
  scProfissionalService,
  $timeout,
  $stateParams,
  toaster,
  $state,
  orderByFilter,
  $sessionStorage,
  ngDialog,
  scFirstAccessService
) {
  "use strict";
  // Private
  var self = this;

  var currentPage = 1;
  var itensPerPage = 30;
  var totalPage = 0;
  var minPrice = 0;
  var maxPrice = 2000;
  var orderType = 2;

  var isPagination = false;
  var orderByRatingAndPrice = false;

  var order = null;
  var currentOrderCriterio = null;

  var categoriasSelecionadas = [];
  var categoriasSelecionadasIds = [];
  var skills = [];

  comumConfig.blockScreen();

  // Public
  self.projectController = true;

  self.pageslideOnOpen = pageslideOnOpen;
  self.pageslideOnClose = pageslideOnClose;

  self.selectedBriefing = null;
  self.isOpen = false;
  self.query = '';
  self.initSearch = initSearch;
  self.taskUserSearch = '';
  self.taskUser = {};
  self.noResults = false;
  self.isOpenProfile = false;
  self.loadingPros = false;

  self.mensagem = "";

  self.isProfessional = {};
  self.error = false;
  self.hasResults = false;
  self.openInput = false;
  self.showMessage = false;
  self.errorMessage = null;
  self.hideList = hideList;

  self.allOffers = [];

  self.baseUrl = comumConfig.baseUrlAPI();

  self.user = $rootScope.getUser();
  self.briefing = {};
  self.selectProfissional = selectProfissional;
  self.professional = {};
  self.customer = {};
  self.taskId = taskId;
  self.message = {
    Price: null,
    DeadlineDays: null,
  };

 self.setInputOpen = function() {
   console.log(self.openInput);
   self.openInput = !self.openInput;
  // $scope.$apply();
 }

  self.modelOptions = {
    debounce: {
      default: 500,
      blur: 250,
    },
    getterSetter: true,
  };

  self.getProjetos = $rootScope.getProjetos;
  self.Projeto = false;
  self.projectId = $stateParams.id !== undefined ? $stateParams.id : undefined;
  self.Arquivado = false;

  self.getProjetos = $rootScope.getProjetos;
  self.Projeto = false;
  self.projectId = $stateParams.id;
  self.Arquivado = false;
  self.getProfissionais = getProfissionais;

  self.typeOptions = {
    display: "pref",
  };
  self.selectedProfessional = {};

  self.getUserName = getUserName;
  self.getUserPhoto = getUserPhoto;
  self.getProfile = getProfile;
  self.hireProfessional = hireProfessional;
  self.hireButton = hireButton;
  self.showPrice = showPrice;
  self.showFieldsPriceAndDays = showFieldsPriceAndDays;

  self.profissionais = [];
  self.criarTarefa = criarTarefa;
  self.profissionais = [];
  self.adicionado = false;
  self.getMessagesGroupByProfessionalId = getMessagesGroupByProfessionalId;

  self.slideOpenProfile = slideOpenProfile;
  self.slideOpen = slideOpen;
  self.slideTaskStatus = {};

  // para criar uma tarefa
  self.taskProjeto = [];
  self.taskName = [];

  self.avancaTask = avancaTask;
  self.devolveTask = devolveTask;
  self.shortBy = shortBy;
  self.propertyName = "";
  self.reverse = true;

  self.arquivaProjeto = arquivaProjeto;

  self.dateTrim = comumService.dateTrim;

  self.orderBy = [];

  self.hasTask = false;

  self.goToPageSaveBriefing = goToPageSaveBriefing;
  self.openDialogCongratulations = openDialogCongratulations;
  self.deleteTaks = deleteTaks;
  self.confirmDeleteTask = confirmDeleteTask;
  self.cancelDeleteTask = cancelDeleteTask;
  self.saveProjectName = saveProjectName;

  // Functions

  /**
     * Função inicializadora do controller
     */
  function INIT() {
    //self.briefings = self.getBriefings.data.ativos;
    //    self.user = $rootScope.getUser();
    self.projectId = $stateParams.id;
    self.isProfessional = self.user.Role === 2 ? true : false;

    //    self.user.id = (self.user.Role === 2) ? self.user.userId : self.user.id;

    //Popula as promessas com valores
    getProjetos(self.projectId);
    getOrcamentos(self.projectId);
    getExecucao(self.projectId);
    getAprovacao(self.projectId);
    getEntregues(self.projectId);
  }

  /**
     * GET de profissionais no Webservice para o Typeahead.ui
     * @param   {string} data [Valor preenchido pelo usuário no input]
     * @returns {object} [Objeto que preenche o template #customTemplate.html]
     */
  function getProfissionais(data) {
    var config = {};
    if (data !== undefined)
      config = {
        params: {
          "filter.terms": data,
          pageSize: 5,
        },
      };

    return comumProjectService.getProfissionais(config);
  }

  /**
     * Promessas para os cards dos projetos
     */
  function getProjetos(project) {
    if (project) {
      comumService
        .getProjeto(project)
        .success(function(data) {
          console.log(data);
          if (
            data.ExceptionMessage ==
            "Object reference not set to an instance of an object."
          ) {
            $state.go("projects");
          }
          if (!data.Active) self.Arquivado = true;
          self.Projeto = data;
          self.getProjetos = false;
        })
        // ROLLBAR
        .error(function(data) {
          window.Rollbar.critical("comumService.getProjetos", data);
        });
    } else {
      if (self.user.Role != 2) {
        comumService
          .getProjetos(self.user.userId)
          .success(function (data) {
            self.getProjetos = data;
            self.Projeto = false;
          })
          // ROLLBAR
          .error(function (data) {
            window.Rollbar.critical("comumService.getProjetos", data);
          });
      }
      
    }
  }

  /**
     * Promessas para os cards dos Orçamentos
     */
  function getOrcamentos(project, add) {
    // if (!project) project = null;
    var config = {
      params: {
        IdUser: self.user.userId,
        IdProject: self.projectId,
        Status: 1,
      },
    };
    comumProjectService
      .getOrcamentos(config)
      .success(function callBack(response, status) {
        if (status == 403) {
          //NOTE: VOCÊ NÃO DEVERIA ESTAR VENDO ESTE PROJETO
          $state.go("projects");
          return false;
        }

        // if (status == 412) {
        //  NOTE: USUÁRIO TEM PENDÊNCIAS NO PREENCHIMENTO DO PERFIL
        //  scFirstAccessService.openDialog();
        // }

        if (add) self.adicionado = true;
        if (response[0] != null || add) self.getOrcamentos = response;

        if (
          self.getEntregues ||
          self.getAprovacao ||
          self.getExecucao ||
          self.getOrcamentos
        )
          self.hasTask = true;
        $timeout(function() {
          self.adicionado = false;
        });
      })
      // ROLLBAR
      .error(function(data) {
        window.Rollbar.critical("comumProjectService.getOrcamentos", data);
      });
  }

  /**
     * Promessas para os cards de tarefas em execução
     */
  function getExecucao(project, add) {
    //        if (!project) project = null;
    var config = {
      params: {
        IdUser: self.user.userId,
        IdProject: project,
        Status: 2,
      },
    };
    comumProjectService
      .getExecucao(config)
      .success(function callBack(response) {
        if (add) self.adicionado = true;
        if (response[0] != null || add) self.getExecucao = response;

        var projDone = self.getExecucao.map(function(x) {
          var b = x.Status === 4;
        });

        if (
          self.getEntregues ||
          self.getAprovacao ||
          self.getExecucao ||
          self.getOrcamentos
        )
          self.hasTask = true;
        $timeout(function() {
          self.adicionado = false;
        }, 1000);
      })
      // ROLLBAR
      .error(function(data) {
        window.Rollbar.critical("comumProjectService.getExecucao", data);
      });
  }

  /**
     * Promessas para os cards de tarefas em aprovação
     */
  function getAprovacao(project, add) {
    //        if (!project) project = undefined;
    var config = {
      params: {
        IdUser: self.user.userId,
        IdProject: project,
        Status: 3,
      },
    };
    comumProjectService
      .getAprovacao(config)
      .success(function callBack(response) {
        if (add) self.adicionado = true;
        if (response[0] != null || add) self.getAprovacao = response;

        if (
          self.getEntregues ||
          self.getAprovacao ||
          self.getExecucao ||
          self.getOrcamentos
        )
          self.hasTask = true;
        $timeout(function() {
          self.adicionado = false;
        }, 1000);
      })
      // ROLLBAR
      .error(function(data) {
        window.Rollbar.critical("comumProjectService.getAprovacao", data);
      });
  }

  /**
     * Promessas para os cards de tarefas entregues
     */
  function getEntregues(project, add) {
    // if (!project) project = null;
    var config = {
      params: {
        IdUser: self.user.userId,
        IdProject: project,
        Status: 4,
      },
    };
    comumProjectService
      .getEntregues(config)
      .success(function callBack(response) {
        comumConfig.unblockScreen();
        if (response[0] != null || add) {
          self.getEntregues = response;
        }

        // Armazena e busca o numero de tarefas entregues pelo profissional para garantir
        // que o modal de parabéns só aparecerá da primeira vez que ele concluir uma tarefa
        window.localStorage.setItem("PJ_PID", self.getEntregues.length);
        var preDeliveredNum = window.localStorage.getItem("PJ_PID");

        if (preDeliveredNum == 0 && self.getEntregues.length == 1) {
          window
            .swal({
              title:
                "<h6 style='font-size: 20px'>Parabéns, seu primeiro projeto foi finalizado!</h6>",
              html:
                "<div>" +
                "<div>" +
                "<p>A política de pagamento dos trabalhos entregues e aprovados na plataforma é a mesma para todas as empresas e agências. Entenda quando você receberá o pagamento pelos jobs:</p>" +
                "</div>" +
                "<div class='row' style='margin-top:20px'>" +
                "<div class='col-sm-6'>" +
                "<h5 class='header-freela'>Jobs finalizados entre:</h5>" +
                "<div style='width: 230px;border: 2px solid #e6e6e6;margin: 0 auto;height: 50px; border-radius: 5px;text-align: center; line-height: 20px;'><h4>16 e 31 do mês</h4></div>" +
                "<div style='width: 230px;border: 2px solid #e6e6e6;margin: 0 auto;height: 50px; border-radius: 5px;text-align: center; position: relative; top: 5px;'><h4>1 e 15 do mês</h4></div>" +
                "</div>" +
                "<div class='col-sm-6' style='margin-left: -15px'>" +
                "<h5 class='header-freela'>Pagamento:</h5>" +
                "<div style='width: 230px;border: 2px solid #e6e6e6;margin: 0 auto;height: 50px;border-radius: 5px;text-align: center; line-height: 20px;background-color: #e6e6e6'><h4 style='font-weight: 600'>Dia 10</h4></div>" +
                "<div style='width: 230px;border: 2px solid #e6e6e6;margin: 0 auto;height: 50px;border-radius: 5px;text-align: center; position: relative; top: 5px; background-color: #e6e6e6'><h4 style='font-weight: 600'>Dia 25</h4></div>" +
                "</div>" +
                "</div>" +
                "<div class='row' style='margin-top:10px'>" +
                "<div class='col-sm-12'>" +
                "<h5 style='font-size: 13px; font-weight: 600'> Fique tranquilo que mandaremos a cobrança por email</h5>" +
                "</div>" +
                "</div>" +
                "</div>",              
              imageUrl: "../assets/img/payment-icon.png",
              customClass: "modal-payment",
              showCloseButton: true,
              showConfirmButton: false,
            })
            .then(
              function() {},
              function(dismiss) {
                // dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
              }
            );
        }

        if (
          self.getEntregues ||
          self.getAprovacao ||
          self.getExecucao ||
          self.getOrcamentos
        )
          self.hasTask = true;
      })
      // ROLLBAR
      .error(function(data) {
        window.Rollbar.critical("comumProjectService.getEntregues", data);
      });
  }

  function getUserName(user) {
    if (!user) return self.professional.Name;
    return user.Name;
  }

  function getUserPhoto(user) {
    if (!user) return self.professional.Photo;
    return user.Photo;
  }

  function arrangeProfessionalMessages(messagesGroupArray) {
    messagesGroupArray.forEach(function(messagesObj, index) {
      var messagesProfessional = getProfessionalMessges(messagesObj.Messages);
      var lastProfessionalMessage = comumConfig.getLastElementFromArray(
        messagesProfessional
      );

      if (lastProfessionalMessage !== undefined) {
        messagesObj.Freelancer.ShortText = lastProfessionalMessage.Text
          ? comumConfig.cortarTexto(lastProfessionalMessage.Text, 125)
          : "";
        messagesObj.Freelancer.CreatedAt = lastProfessionalMessage.CreatedAt;
        messagesObj.Freelancer.hasOffer =
          lastProfessionalMessage.Price && lastProfessionalMessage.DeadlineDays
            ? true
            : false;
        messagesObj.Freelancer.Price = lastProfessionalMessage.Price;
        messagesObj.Freelancer.DeadlineDays =
          lastProfessionalMessage.DeadlineDays;
        messagesObj.Freelancer.DeliveryAt = lastProfessionalMessage.DeliveryAt;
      } else {
        messagesObj.Freelancer.ShortText = null;
        messagesObj.Freelancer.CreatedAt = null;
        messagesObj.Freelancer.hasOffer = false;
        messagesObj.Freelancer.Price = null;
        messagesObj.Freelancer.DeadlineDays = null;
        messagesObj.Freelancer.DeliveryAt = null;
      }

      messagesObj.offerId = messagesObj.Messages[0].BriefingId + "-" + index;
    });

    return messagesGroupArray;
  }

  function addClassBriefingAtivo(element) {
    var elements = comumConfig.findElement(".ativo", "#listagem-briefing");

    angular.forEach(elements, function(elem, index) {
      comumConfig.removeClass(elem, "ativo");
    });

    comumConfig.addClass(element, "ativo");
  }

  function sortMessages(messagesGroup) {
    if (messagesGroup.length !== 0) {
      messagesGroup.forEach(function(messagesObj, index) {
        messagesObj.Messages.reverse();
      });

      return messagesGroup;
    }

    return [];
  }

  function getBriefingOffers(event, briefingId, status) {
    blockScreen();
    addClassBriefingAtivo(event.currentTarget);

    var briefings = status ? self.briefingsAtivos : self.briefingsArquivados;

    comumConfig.displayBlock(".page-content");
    comumConfig.displayNone("#briefing-arquivado");

    scBriefingService
      .getBriefingOffers(briefingId)
      .success(function(data) {
        self.selectedBriefing = comumConfig.searchObjectInArray(
          briefings,
          "id",
          briefingId
        );
        var messagesGroup = data.messagesGroup;

        if (messagesGroup.length !== 0) {
          self.allOffers = sortMessages(messagesGroup);
          //self.allOffers = messagesGroup;
          self.offers = arrangeProfessionalMessages(messagesGroup);
          //addImageFullPath();
          if (self.selectedBriefing.Contracted)
            self.addHiredProfessional(self.selectedBriefing.SelectedPropose);
        } else {
          self.offers = [];
        }

        comumConfig.unblockScreen();
      })
      // ROLLBAR
      .error(function(data) {
        window.Rollbar.critical("scBriefingService.getBriefingOffers", data);
      });
  }

  /**
     * NgCLick - Controller arquivaProjeto
     * @param {string} id ID do projeto a ser arquivado
     */

  function arquivaProjeto(id) {
    comumProjectService
      .arquivaProjeto(id)
      .success(function callBack(response, status) {
        if (status != 200) {
          toaster.pop(
            "error",
            "Bad Server",
            "Não consegui arquivar seu projeto"
          );
          window.Rollbar.critical(
            "comum.project.controller > arquivaProjeto",
            response
          );
          return false;
        }

        toaster.pop("success", "", "Projeto arquivado!");
        comumService.getProjetos(self.user.userId); // ATUALIZA O MENU
        comumService.getProjetosArquivados(self.user.userId); // ATUALIZA O MENU
        $state.go("projects");
      })
      .error(function callBack(response) {
        // ROLLBAR
        toaster.pop(
          "error",
          "Bad Server",
          "Não consegui arquivar seu projeto"
        );
        window.Rollbar.critical(
          "comum.project.controller > arquivaProjeto",
          response
        );
        return false;
      });
  }

  /**
     * NgClick, POST via webservice a nova task criada na view de projetos
     * @param {String} tarefaName    [Valor do input]
     * @param {String} tarefaProject [Valor do Select]
     * @param {String} tarefaUser    [Valor Id do Freelancer, fornecido pelo autocomplete]
     * @fires criarTarefa#[service]
     */
  function criarTarefa(tarefaName, tarefaProject, tarefaFreela) {
    if (!tarefaName || !tarefaFreela) {
      toaster.pop("error", "", "Preencha todos os campos");
      return false;
    }

    if (!tarefaProject) tarefaProject = self.projectId;

    var data = {
      Id: 0,
      Title: tarefaName,
      Description: "",
      IdUser: self.user.userId,
      IdProject: tarefaProject,
      IdFreelancer: tarefaFreela,
      Status: 1,
    };

    comumProjectService
      .criarTarefa(data)
      .success(function callBack(data, status) {
        if (status != 200) {
          toaster.pop(
            "error",
            "Bad Server",
            "Não consegui retornar seu job"
          );
          window.Rollbar.critical(
            "comum.project.controller > criarTarefa",
            data
          );
          Intercom('showNewMessage', 'Olá, tive um problema tentando criar uma tarefa');

          return false;
        }

        toaster.pop("success", "", "Tarefa criada!");
        getOrcamentos(self.projectId);
        self.taskName = [];
        self.taskProjeto = [];
        self.taskUser = [];
        window.$jQuery("#adicionarProfissional").css("background", "");
      })
      // ROLLBAR
      .error(function(data) {
        toaster.pop(
          "error",
          "Bad Server",
          "Não consegui retornar seu job"
        );
        window.Rollbar.critical("comum.project.controller > criarTarefa", data);
        Intercom('showNewMessage', 'Olá, tive um problema tentando criar uma tarefa');

        return false;
      });
  }

  /**
     * @desc NgClick aprovando orçamento, realiza um POST através do webservice
     * @param {object} user  [Objeto com informações sobre usuário: UserId]
     * @param {object} task  [Objeto com informações sobre task: TaskId, CurrentStatus]
     */
  function avancaTask(task, obj) {
    obj.disabled = true;

    var data = {
      UserId: self.user.userId,
      TaskId: task.Id,
      NewStatus: task.Status + 1,
      CurrentStatus: task.Status,
    };

    comumProjectService
      .serviceAprovaOrcamento(data)
      /**
         * @desc Este callback também cuida do envio da mensagem automática
         * @param {object} data Objeto organizado para a API
         */
      .success(function callBack(response, status) {
        if (status != 200) {
          toaster.pop(
            "error",
            "Bad Server",
            "Não consegui trocar o status do job"
          );
          window.Rollbar.critical(
            "comum.project.controller > avancaTask",
            response
          );
          Intercom('showNewMessage', 'Olá, tive um problema com a aprovação de uma tarefa');

          return false;
        }

        self.isOpen = false;

        if (task.Status + 1 === 2) {
          toaster.pop("success", "", "Orçamento aprovado com sucesso!");
          sendMessage(
            task.Id,
            "<em style='color: lightslategrey;'>*Orçamento aprovado!*</em>"
          );
          $state.reload();
          //     getOrcamentos(self.projectId, true);
          //     getExecucao(self.projectId, true);
        }
        if (task.Status + 1 === 3) {
          toaster.pop("success", "", "Tarefa enviada para aprovação!");
          sendMessage(
            task.Id,
            "<em style='color: lightslategrey;'>*Tarefa pronta para aprovação.*</em>"
          );
          $state.reload();
          //     getExecucao(self.projectId, true);
          //     getAprovacao(self.projectId, true);
        }
        if (task.Status + 1 === 4) {
          toaster.pop("success", "Tarefa concluída com sucesso!");
          sendMessage(
            task.Id,
            "<em style='color: lightslategrey;'>*Tarefa finalizada.*</em>"
          );
          $state.reload();
          //     getAprovacao(self.projectId, true);
          //     getEntregues(self.projectId, true);
        }

        obj.disabled = false;
      })
      /**
             * @desc Este callback reporta ao Rollbar problema com a API
             * @param {object} data de resposta do Webservice
             */
      .error(function callBack(data) {
        // ROLLBAR
        window.Rollbar.critical("comumProjectService.criarTarefa", data);
        toaster.pop("error", "", "Você não pode fazer isso!");
        Intercom('showNewMessage', 'Olá, tive um problema com a aprovação de uma tarefa');

        obj.disabled = false;
      });
  }

  /**
 * Envia mensagem pelo Webservice
 * @description Envia a mensagem e retorna
 * @param   {boolean} task Variação se é Task ou Briefing
 * @returns {boolean}  Success ou Error
 */
  function sendMessage(task, message) {
    var percent = 8;
    var factor = 1 + percent / 100;

    var professional = $rootScope.getUser();

    task = typeof task !== "undefined" ? task : false;

    var userId = professional.id;
    var promisse = {};

    self.message.briefingId = self.briefing.Id;
    self.message.professionalId = self.briefing.professionalId;
    self.message.uesrId = userId;
    self.message.message = message ? message : getMessage();
    self.message.Price = self.message.Price
      ? (self.message.Price *= factor)
      : null;

    if (self.isProfessional)
      promisse = comumService.sendMessageToCustomer(self.message, task);
    else
      promisse = scBriefingService.sendMessageToProfessional(
        self.message,
        task
      );

    comumConfig.blockScreen("", ".campo-enviar-mensagem");

    promisse
      .success(function(data, status) {
        if (status != 200) {
          toaster.pop(
            "error",
            "Bad Server",
            "Não consegui registrar sua mensagem"
          );
          window.Rollbar.critical(
            "comum.project.controller > sendMessage",
            data
          );
          comumConfig.unblockScreen(".campo-enviar-mensagem");
          return false;
        }

        addUserMessage(data);
        comumConfig.unblockScreen(".campo-enviar-mensagem");
        clearMessage();
      })
      .error(function(data) {
        // ROLLBAR
        window.Rollbar.critical("comum.project.controller > sendMessage", data);
        toaster.pop(
          "error",
          "Bad Server",
          "Não consegui registrar sua mensagem"
        );
        comumConfig.unblockScreen(".campo-enviar-mensagem");
      });
  }

  /**
     * NgClick reprovando produção, realiza um POST através do webservice
     * @name Devolve Task
     * @param {object} user  [Objeto com informações sobre usuário: UserId]
     * @param {object} task  [Objeto com informações sobre task: TaskId, CurrentStatus]
     */
  function devolveTask(task, obj) {
    obj.disabled = true;

    var data = {
      UserId: self.user.userId,
      TaskId: task.Id,
      NewStatus: task.Status - 1,
      CurrentStatus: task.Status,
    };

    comumProjectService
      .serviceAprovaOrcamento(data)
      .success(function callBack(response, status) {
        if (status != 200) {
          toaster.pop(
            "error",
            "Bad Server",
            "Não consegui retornar seu job"
          );
          window.Rollbar.critical(
            "comum.project.controller > devolveTask",
            response
          );
          Intercom('showNewMessage', 'Olá, tive um problema retornando uma tarefa em aprovação');

          return false;
        }

        self.isOpen = false;

        if (task.Status - 1 === 2) {
          toaster.pop(
            "success",
            "",
            "Tarefa retornada ao profissional."
          );
          sendMessage(
            task.Id,
            "<em style='color: lightslategrey;'>*Tarefa não foi aprovada.*</em>"
          );
          $state.reload();
        }

        obj.disabled = false;
      })
      .error(function callBack(response) {
        // ROLLBAR
        toaster.pop(
          "error",
          "Bad Server",
          "Não consegui retornar seu job"
        );
        window.Rollbar.critical(
          "comum.project.controller > devolveTask",
          response
        );
        Intercom('showNewMessage', 'Olá, tive um problema com a aprovação de uma tarefa');

        obj.disabled = false;
      });
  }

  function shortBy(propertyName) {
    self.reverse = self.propertyName === propertyName ? !self.reverse : false;
    self.propertyName = propertyName;
  }

  function initQuillEditor() {
    var options = {
      toolbar: ["bold", "italic", "underline"],
      placeholder: "Escreva uma mensagem...",
    };
    comumConfig.quillEditor("#editor").add(options);
  }

  function getMessage() {
    return comumConfig.quillEditor("#editor").getContent();
  }

  function clearMessage() {
    comumConfig.quillEditor("#editor").setContent("");
    self.error = false;
    self.message.Price = null;
    self.message.DeadlineDays = null;
  }

  function pageslideOnOpen() {
    comumConfig.fadeIn(".mask-content-pageslide");
  }

  function pageslideOnClose() {
    comumConfig.fadeOut(".mask-content-pageslide");
  }

  function commonDataMessage() {
    return {
      BriefingId: self.briefing.Id,
      CreatedAt: null,
      DeadlineDays: null,
      Freelancer: null,
      FreelancerId: self.professional.Id,
      Id: null,
      Price: null,
      Read: false,
      Text: null,
      User: null,
      UserId: null,
    };
  }

  function getProfessionalMessage(data) {
    var professionalMessage = commonDataMessage();
    professionalMessage.CreatedAt = data.CreatedAt;
    professionalMessage.CreatedAt = data.CreatedAt;
    professionalMessage.Text = getMessage();
    return professionalMessage;
  }

  function getCustomerMessage(data) {
    var customerMessage = commonDataMessage();
    customerMessage.CreatedAt = data.CreatedAt;
    customerMessage.Id = data.Id;
    customerMessage.UserId = data.UserId;
    customerMessage.User = self.briefing.User;
    customerMessage.Text = getMessage();
    return customerMessage;
  }

  function getDataProfessional(freelancer, professionalId) {
    return {
      Photo: freelancer.Photo,
      Name: freelancer.Nome,
      Id: freelancer.Id,
    };
  }

  function getCustomerMessages(messagesArray) {
    if (messagesArray.length !== 0) {
      return messagesArray.filter(function(messagesObg) {
        if (self.user.Role != 2)
          return !messagesObg.UserId && !messagesObg.User;
        if (self.user.Role == 2) return messagesObg.UserId && messagesObg.User;
      });
    }

    return [];
  }
  /**
     * Envia id do projeto selecionado na criação de task para a service
     * @param {string} id [Valor selecionado no .dropdown-menu da View]
     */
  function taskId(id) {
    comumProjectService.taskId = id;
  }

  function addUserMessage(data) {
    if (self.isProfessional)
      self.briefing.offers.Messages.push(getProfessionalMessage(data));
    else self.briefing.offers.Messages.push(getCustomerMessage(data));
  }

  function validateMessage() {
    if (!self.isProfessional) {
      if (messageIsEmpty()) {
        self.error = true;
        self.errorMessage = "Preencha a mensagem.";
        return false;
      }
    } else {
      if (
        messageIsEmpty() &&
        (!self.message.Price || !self.message.DeadlineDays)
      ) {
        self.error = true;
        self.errorMessage =
          "Você deve enviar um Valor e Prazo para esse Briefing, ou uma Mensagem.";
        return false;
      }
    }
    return true;
  }

  function isOpen(parent, attr) {
    parent.$parent.$ctrl.isOpen = attr;
  }

  function blockScreen() {
    comumConfig.blockScreen("", ".page-content");
  }

  function slideOpen(briefingData, professionalId, $event) {
    if ($event !== undefined)
      if (comumConfig.getClickedElement($event) != "DIV") return false;

    if (briefingData.Id) {
      comumProjectService
        .getTask(briefingData.Id)
        .success(function(data, status) {
          if (status != 200) {
            toaster.pop(
              "error",
              "Bad Server!",
              "Desculpe. Não consegui carregar sua tarefa."
            );
            window.Rollbar.critical(
              "comum.projectController slideOpen()",
              data
            );
            Intercom('showNewMessage', 'Olá, tentei ver uma tarefa mas ela não carregou.');

            return false;
          }

          self.isOpen = !self.isOpen;

          // PEGA AS MENSAGENS
          var briefingMessages = data.Messages;

          //NOTE: ENVIA FUNÇÕES PARA A VIEW
          var briefing = data;
          briefing.projectController = self.projectController;
          briefing.isProfessional = self.isProfessional;
          briefing.professionalId = professionalId;
          briefing.avancaTask = self.avancaTask;
          briefing.devolveTask = self.devolveTask;
          briefing.projectId = self.projectId;

          if (data.Status == 1) briefing.Contracted = false;
          else briefing.Contracted = true;

          briefing.offers = {
            Messages: briefingMessages,
            Freelancer: getDataProfessional(data.Freelancer, professionalId),
          };

          $scope.$broadcast("event:briefing-details", briefing);
          self.isOpen = true;
          comumConfig.scrollPageslideBriefing(
            $timeout,
            ".janela-profissional-projeto"
          );

          if (professionalId)
            var customerMessages = getCustomerMessages(briefingMessages);
          else
            var professionalMessages = getProfessionalMessges(briefingMessages);

          if (customerMessages) {
            var messagesId = customerMessages.filter(function(messagesObj) {
              return messagesObj.Read === false;
            });

            messagesId = comumConfig.getPropertyValueFromObjectArray(
              messagesId,
              "Id"
            );

            if (messagesId.length !== 0) {
              messagesId.forEach(function(messageId) {
                comumService
                  .readMessage(messageId, true)
                  .success(function(data) {
                    if (comumService.getUser().Role !== 2) {
                      comumService.getNotification(self.user.userId);
                      comumService.getProjetos(self.user.userId);                    
                    } else {
                      comumService.getNotificationProfessional(
                        self.user.userId
                      );
                    }

                    comumConfig.removeElement("#unread-task-" + briefing.Id);
                  })
                  .error(function callback(data) {
                    //ROLLBAR
                    window.Rollbar.critical("comumService.readMessage", data);
                  });
              });
            }
          }

          if (professionalMessages) {
            var messagesId = professionalMessages.filter(function(messagesObj) {
              return messagesObj.Read === false;
            });

            messagesId = comumConfig.getPropertyValueFromObjectArray(
              messagesId,
              "Id"
            );

            if (messagesId.length !== 0)
              messagesId.forEach(function(messageId) {
                comumService
                  .readMessage(messageId, true)
                  .success(function(data) {
                    if (comumService.getUser().Role !== 2) {
                      comumService.getProjetos(self.user.userId);                      
                      comumService.getNotification(self.user.userId);
                    } else {
                      comumService.getNotificationProfessional(
                        self.user.userId
                      );
                    }

                    comumConfig.removeElement("#unread-task-" + briefing.Id);
                  })
                  .error(function callback(data) {
                    //ROLLBAR
                    window.Rollbar.critical("comumService.readMessage", data);
                  });
              });
            else comumConfig.removeElement("#unread-task-" + briefing.Id);
          }
        })
        .error(function callback(data) {
          //ROLLBAR
          window.Rollbar.critical("comum.project.controller > slideOpen", data);
          Intercom('showNewMessage', 'Olá, tentei ver uma tarefa mas ela não carregou.');
        });
    } else {
      self.briefing = briefingData;
      self.briefing.offers = {
        Messages: [],
        Freelancer: {},
      };
      $scope.$broadcast("event:briefing-details", self.briefing);
      self.isOpen = true;
    }
  }

  function getProfessionalMessges(messagesArray) {
    if (messagesArray.length !== 0) {
      return messagesArray.filter(function(messagesObj) {
        return !messagesObj.UserId && !messagesObj.User;
      });
    }

    return [];
  }

  function messageIsEmpty() {
    return getMessage() == "<p><br></p>" ? true : false;
  }

  function getProfile(parent, user) {
    if (user) return false;

    parent.$parent.$ctrl.getProfile(self.professional.Id);
    parent.$parent.$ctrl.isOpen = false;
    parent.$parent.$ctrl.isOpenProfile = true;
  }

  function configShowMesage() {

    // Adicionando o quill dentro do settimeout para não correr o risco de carregar o JS antes da view.
    $timeout(
      function () {
        initQuillEditor();
      },
      1000,
      false
    );
    $timeout(
      function () {
        checkBriefingStatus();
      },
      1300,
      false
    );

    if (self.briefing.Bulletin) {
      self.showMessage = false;
      return false;
    } else if (self.briefing.Contracted) {
      if (self.briefing.SelectedPropose) {
        if (
          self.isProfessional &&
          self.briefing.SelectedPropose.Freelancer.Id !== self.user.id
        ) {
          self.showMessage = false;
          return false;
        }
      }
    }

    self.showMessage = true;

    $timeout(
      function() {
        initQuillEditor();
      },
      1000,
      false
    );
  }

  function hireButton(message) {
    if (self.isProfessional || !message.Price || self.briefing.Contracted)
      return false;
    return true;
  }

  function hireProfessional(parent) {
    var proposeId = self.briefing.offers.Messages[0];
    proposeId = proposeId.ProposeId;
    var contractData = {
      ProposeId: proposeId,
      FreelancerId: self.professional.Id,
      BriefingId: self.briefing.Id,
    };
    var html = "<figure>";
    html +=
      '<div style="margin: 0 auto; float: none; width: 80px; padding-bottom: 80px;" class="img-profile-50">';
    html +=
      '<div style="background:url(' +
      comumConfig.baseUrlAPI() +
      self.professional.Photo +
      ');background-size: 100% 100%"></div>';
    html +=
      '<p style="color: #90BFF1; width: 400px; margin-left: -160px;">' +
      self.professional.Name +
      "</p>";
    html += "</div>";
    html += "</figure>";
    window.swal(
      {
        title: html,
        text: "Deseja aprovar este orçamento?",
        html: true,
        showCancelButton: true,
        cancelButtonText: "Cancelar",
        confirmButtonColor: "#46BE8A",
        confirmButtonText: "Contratar",
      },
      function() {
        comumConfig.blockScreen("", ".janela-profissional-briefing");
        scBriefingService
          .hireProfessional(contractData)
          .success(function(data, status) {
            if (status != 200) {
              toaster.pop(
                "error",
                "Bad Server",
                "Não consegui contratar este profissional"
              );
              window.Rollbar.critical(
                "comum.project.controller > hireProfessional",
                data
              );
              Intercom('showNewMessage', 'Olá, não consegui aprovar um Orçamento da tarefa ' + self.briefing.Id);
              return false;
            }

            var selectedPropose = {
              Freelancer: {
                Photo: self.professional.Photo,
                Name: self.professional.Name,
              },
              Price: null,
              DeadlineDays: null,
            };
            $window.ga("send", "event", "Empresa", "Contratou");
            window.Intercom('trackEvent', 'Contratou');            
            parent.$parent.$ctrl.addHiredProfessional(selectedPropose);
            parent.$parent.$ctrl.isOpen = false;
            parent.$parent.$ctrl.selectedBriefing.Contracted = true;
            comumConfig.unblockScreen(".janela-profissional-briefing");
          })
          .error(function(data) {
            //ROLLBAR
            toaster.pop(
              "error",
              "Bad Server",
              "Não consegui contratar este profissional"
            );
            window.Rollbar.critical(
              "comum.project.controller > hireProfessional",
              data
            );
            Intercom('showNewMessage', 'Olá, não consegui aprovar um Orçamento da tarefa ' + self.briefing.Id);
            comumConfig.unblockScreen(".janela-profissional-briefing");
          });
      }
    );
  }

  function getMessagesGroupByProfessionalId(professionalId) {
    var messageGroup = [];

    if (self.allOffers.length !== 0) {
      self.allOffers.forEach(function(messagesObj, index) {
        if (messagesObj.Freelancer.Id === professionalId)
          messageGroup = messagesObj;
      });
    }

    return messageGroup;
  }

  function slideOpenProfile() {
    self.isOpen = true;
    self.isOpenProfile = false;
    //  comumConfig.fadeOut('.mask-content-pageslide');
  }

  function getProfile(professionalId) {
    self.isOpenProfile = true;
    scProfissionalService
      .getPerfil(professionalId)
      .success(function(data) {
        data.hideSelectButton = true;
        $scope.$broadcast("event:visualizar-perfil", data);
      })
      .error(function(data) {
        //ROLLBAR
        window.Rollbar.critical("scProfissionalService.getPerfil", data);
      });
  }

  function showPrice(user) {
    if (self.professional.hasOffer && !user) return true;
    return false;
  }

  function orderBy(criterio) {
    if (criterio) {
      orderByRatingAndPrice = true;

      order = criterio == "raiting" ? 2 : 1;

      if (currentOrderCriterio != criterio) {
        orderType = 2;
        currentOrderCriterio = criterio;
      } else {
        orderType = orderType == 1 ? 2 : 1;
      }

      _getProfissionais();
    } else {
      return false;
    }
  }
 

  function selectProfissional(profissional) {
    self.taskUser = profissional;
    // console.debug(self.taskUser);
    hideList();
  }

  function hideList() {
      self.profissionais = [];
      self.openInput = false;
  }

  function initSearch() {
    self.loadingPros = true;
    var engine = algoliasearch('Z4V86RWKZ2', '3f7e6ad21e56af31bcbe84205f1dda59');
      self.isSearch = false;
      return engine.initIndex('freelancers')
        .search(self.taskUserSearch, function (success, content) {
          self.hasResults = true;
          self.profissionais = content.hits;
        //  self.taskUser = content.hits;

        if (self.profissionais[0].Name) {
          self.hasResults = true;
          self.noResults = false;
        } else {
          self.noResults = true;
          self.hasResults = false;
        }
          self.loadingPros = false;
          $scope.$apply();
        //  comumConfig.unblockScreen();
          return self.profissionais;
        });
  };

  function getDataEndereco(objProfissional) {
    return {
      estado:
        objProfissional.State.UF == "null" ? "" : objProfissional.State.UF,
      cidade:
        objProfissional.City.Name == "null" ? "" : objProfissional.City.Name,
    };
  }

  function _getProfissionais() {
    comumConfig.blockScreen("", ".busca-avancada, .page-content");

    scProfissionalService
      .getProfissionais(getFilter())
      .success(function(data) {
        initProfissionaisList(data);
        comumConfig.unblockScreen(".busca-avancada, .page-content");
        resetStatusPagination();
      })
      .error(function(data) {
        //ROLLBAR
        window.Rollbar.critical("scProfissionalService.getProfissionais", data);

        comumConfig.unblockScreen(".busca-avancada, .page-content");
        resetStatusPagination();
      });
  }

  function goToPageSaveBriefing() {
    delete $sessionStorage.briefing;
    delete $sessionStorage.fromBriefing;
    $state.go("salvar-briefing");
  }

  function getRelatedSkills(relatedSkills) {
    var skills = [];

    relatedSkills.forEach(function(skill, index) {
      skills.push(skill.Name);
    });

    return skills;
  }

  function initProfissionaisList(profissionais) {
    if (!isPagination) {
      self.profissionais = [];
      totalPage = Math.ceil(profissionais.totalReturned / itensPerPage);
      self.totalRecords = profissionais.totalReturned;
      self.tagsRelacionadas = getRelatedSkills(
        profissionais.response.relatedSkills
      );
    }

    function configDataProfissionais(dataProfissionais) {
      var profissional = {};

      dataProfissionais.forEach(function(objProfissional, index) {
        profissional.id = objProfissional.Id;
        profissional.nome = objProfissional.Name;
        profissional.profissao = objProfissional.Title;
        profissional.profissaoCurto = comumConfig.cortarTexto(
          objProfissional.Title,
          25
        );
        profissional.descricao = objProfissional.Description;
        profissional.valor_hora = objProfissional.Price;
        profissional.portfolio = getPortifolio(objProfissional.Portfolio);
        profissional.code = objProfissional.Code;
        profissional.tags = getStringFromSkillsArray(objProfissional.Skills);
        profissional.imagem_perfil =
          comumConfig.baseUrlAPI() + objProfissional.Photo;
        profissional.endereco = getDataEndereco(objProfissional);
        profissional.uf = profissional.endereco.estado;
        profissional.starRating = objProfissional.Rating;
        profissional.rating = {
          quality: objProfissional.QualityRating,
          responsability: objProfissional.ResponsabilityRating,
          agility: objProfissional.AgilityRating,
        };
        profissional.disponibilidade = objProfissional.Availability;
        profissional.Code = objProfissional.Code;
        //profissional.segmento        = objProfissional.Availability;

        self.profissionais.push(profissional);
        profissional = {};
      });
    }

    configDataProfissionais(profissionais.response.results);

    addStar();
    addNomeCurto();
    addResumoDescricao();
  }

  function addResumoDescricao() {
    self.profissionais.forEach(function(profissional, index) {
      profissional.descricao =
        profissional.descricao == "null" ? "" : profissional.descricao;
      profissional.resumoDescricao = comumConfig.cortarTexto(
        profissional.descricao,
        140
      );
    });
  }

  function addNomeCurto() {
    self.profissionais.forEach(function(profissional, index) {
      profissional.nomeCurto = comumConfig.getFirstAndLastWord(
        profissional.nome
      );
    });
  }

  function addStar() {
    self.profissionais.forEach(function(objProfissional, index) {
      var quality = objProfissional.rating.quality;
      var responsability = objProfissional.rating.responsability;
      var agility = objProfissional.rating.agility;

      objProfissional.star = comumConfig.addStarRating(
        quality,
        responsability,
        agility
      );
    });
  }

  

  function resetStatusPagination() {
    if (isPagination) {
      self.isLoading = false;
      isPagination = false;
    }
  }

  function showFieldsPriceAndDays() {
    if (!self.isProfessional || self.briefing.Contracted) return false;

    return true;
  }

  $scope.$on("event:briefing-details", function(event, data) {
    self.briefing = data;
    self.professional = data.offers.Freelancer;
    configShowMesage();
  });

  $rootScope.$on("event:getProjetos", function(event, data) {
    $rootScope = data;

    self.getProjetos = $rootScope.getProjetos;
 

    getProjetos();
  });

  function openDialogCongratulations() {
    ngDialog.open({
      template: "./templates/project/view/congratulations.modal.html",
      width: 800,
    });
  }

  function deleteTaks($event) {
    var card = comumConfig.getParent($event.currentTarget, 4);
    comumConfig.addClass(card, "active-del");
  }

  function confirmDeleteTask(taskId, $event) {
   
   // $event.messageId;

    var messageId = $event.messageId;

    comumProjectService
      .getTask(taskId)
      .success(function (task, status) {
        if (status != 200) {
          toaster.pop(
            "error",
            "Bad Server!",
            "Desculpe. Não consegui apagar esta tarefa."
          );
          window.Rollbar.critical(
            "comum.projectController confirmDeleteTask()",
            data
          );
          Intercom('showNewMessage', 'Olá, não consegui apagar a tarefa ' + taskId);
          return false;
        }

        if (self.user.Role == 2)
          var customerMessages = getCustomerMessages(briefingMessages);
        else
          var professionalMessages = getProfessionalMessges(briefingMessages);

        if (customerMessages) {
          var messagesId = customerMessages.filter(function (messagesObj) {
            return messagesObj.Read === false;
          });

          messagesId = comumConfig.getPropertyValueFromObjectArray(
            messagesId,
            "Id"
          );

          if (messagesId.length !== 0) {
            messagesId.forEach(function (messageId) {
              comumService
                .readMessage(messageId, true)
                .success(function (data) {
                  if (comumService.getUser().Role !== 2) {
                    comumService.getNotification(self.user.userId);
                    comumService.getProjetos(self.user.userId);
                  } else {
                    comumService.getNotificationProfessional(
                      self.user.userId
                    );
                  }

                  comumConfig.removeElement("#unread-task-" + briefing.Id);
                })
                .error(function callback(data) {
                  //ROLLBAR
                  window.Rollbar.critical("comumService.readMessage", data);
                });
            });
          }
        }


        var messagesId = professionalMessages.filter(function (messagesObj) {
          return messagesObj.Read === false;
        });

        messagesId = comumConfig.getPropertyValueFromObjectArray(
          messagesId,
          "Id"
        );

        if (messagesId.length !== 0)
          messagesId.forEach(function (messageId) {
            comumService
              .readMessage(messageId, true)
              .success(function (data) {
                if (comumService.getUser().Role !== 2) {
                  comumService.getProjetos(self.user.userId);
                  comumService.getNotification(self.user.userId);
                } else {
                  comumService.getNotificationProfessional(
                    self.user.userId
                  );
                }

                comumConfig.removeElement("#unread-task-" + briefing.Id);
              })
              .error(function callback(data) {
                //ROLLBAR
                window.Rollbar.critical("comumService.readMessage", data);
              });
          });
        else comumConfig.removeElement("#unread-task-" + briefing.Id);
      });
    
    comumService.readMessage(messageId, true)
     .success(function (data) {
         // CHAMAR TODAS AS MENSAGENS NESSA TASKID E E COM UM LOOP PASSAR U
      })
    
  comumProjectService
   .deleteTask(taskId)
   .success(function(data) {
    getOrcamentos();
    toaster.pop("success", "", "Task excluída com sucesso!");
    var card = comumConfig.getParent($event.currentTarget, 5);
    comumConfig.removeElement(card);
   })
   .error(function(data) {
    //ROLLBAR
    Rollbar.critical("scProfissionalService.getPerfil", data);
    toaster.pop("error", "Bad Server!", "Erro ao excluir a Task.");
   });
 }

  function showField() {
    return self.briefing;
  }

  function cancelDeleteTask($event) {
    var card = comumConfig.getParent($event.currentTarget, 4);
    comumConfig.removeClass(card, "active-del");
  }

  function saveProjectName($event) {
    var newName = $event.currentTarget.innerText;
    comumConfig.addText("#menu-project-" + self.projectId, newName);
    comumProjectService.saveProjectName(self.projectId, newName);
  }

  //Calls
  INIT();
}
