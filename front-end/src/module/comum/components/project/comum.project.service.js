comumProjectService.$inject = [
  "$http",
  "comumConfig",
  "$localStorage",
  "$rootScope",
];

function comumProjectService($http, comumConfig, $localStorage, $rootScope) {
  var self = this;
  // Public
  //this.getProjetos = getProjetos;
  self.getOrcamentos = getOrcamentos;
  self.getExecucao = getExecucao;
  self.getAprovacao = getAprovacao;
  self.getEntregues = getEntregues;
  self.getProfissionais = getProfissionais;
  self.criarTarefa = criarTarefa;
  self.getTask = getTask;
  self.user = $rootScope.setUser;
  self.taskId = {};
  self.serviceAprovaOrcamento = serviceAprovaOrcamento;
  self.arquivaProjeto = arquivaProjeto;
  self.deleteTask = deleteTask;
  self.saveTaskName = saveTaskName;
  self.saveProjectName = saveProjectName;

  // Private
  var baseUrl = comumConfig.baseUrl();
  var baseUrlAPI = comumConfig.baseUrlAPI();
  var usuario = {};

  function INIT() {
    // setUser($localStorage.user);
  }

  /**
   * LIST webservice dos orçamentos
   * @param   {String} project [ID do Projeto]
   * @returns {Object}   [JSON contendo toda os orçamentos do usuário]
   */
  function getOrcamentos(data) {
    return $http.get(baseUrlAPI + "/Task/List", data);
  }
  /**
   * LIST webservice de terefas em Execução
   * @param   {Array} data [Array de configurações]
   * @returns {Object}   [JSON contendo toda os orçamentos do usuário]
   */
  function getExecucao(data) {
    return $http.get(baseUrlAPI + "/Task/List", data);
  }
  /**
   * LIST webservice das tarefas em Aprovação
   * @param   {Array} data [Array de configurações]
   * @returns {Object}   [JSON contendo toda os orçamentos do usuário]
   */
  function getAprovacao(data) {
    return $http.get(baseUrlAPI + "/Task/List", data);
  }
  /**
   * LIST webservice das tarefas Entregues
   * @param   {Array} data [Array de configurações]
   * @returns {Object}   [JSON contendo toda os orçamentos do usuário]
   */
  function getEntregues(data) {
    return $http.get(baseUrlAPI + "/Task/List", data);
  }

  /**
   * GET de profissionais no webservice para o autocomplete
   * @param   {Array} data [Array de configurações]
   * @returns {JSON} [Objeto com profissionais]
   */
  function getProfissionais(config) {
    return $http
      .get(baseUrlAPI + "/Professional/Search", config)
      .then(function successCallback(response) {
        console.log(response.data.response.results);
        return response.data.response.results;
      });
  }

  /**
   * GET de uma unica TASK no webservice
   * @param   {string} taskId [Id da Task]
   * @returns {object} [JSON completo da Task]
   */
  function getTask(taskId, userId) {
    var user = $rootScope.getUser();
    var id = usuario.id;

    var config = {
      params: {
        IdUser: user.userId,
        IdTask: taskId,
      },
    };

    console.log("id", id);
    console.log("user", user);

    return $http.get(baseUrlAPI + "/Task/Get", config);
  }

  function serviceAprovaOrcamento(data) {
    // console.log(data);
    //return $http.post(baseUrlAPI + '/ChangeStatus', data);
    return $http.post(
      baseUrlAPI +
      "/Task/ChangeStatus?TaskId=" +
      data.TaskId +
      "&UserId=" +
      data.UserId +
      "&CurrentStatus=" +
      data.CurrentStatus +
      "&NewStatus=" +
      data.NewStatus,
      data
    );
  }

  /**
   * POST da task no webervice. Retorna nova task na tela de projetos
   * @param {String} name    [Valor passado pelo controller criarTarefa]
   * @param {String} project [Valor passado pelo controller criarTarefa]
   * @param {String} user    [Valor passado pelo controller criarTarefa]
   */
  function criarTarefa(data) {
    // console.log(data);
    return $http.post(baseUrlAPI + "/Task/Add", data);
  }

  /**
   * POST arquiva projeto. Retorna sucesso ou não.
   * @deprecated Backend está com problemas, este é um post que precisa de parametros
   * @param   {string} id ID do projeto a ser arquivado
   * @returns {object} Resposta do WS sobre o arquivamento
   */
  function arquivaProjeto(id) {
    var data = {
      projectId: id,
      status: false,
    };
    return $http.post(
      baseUrlAPI + "/Project/ChangeStatus?projectId=" + id + "&status=false"
    );
  }

  function setUser(userData) {
    console.log("MUDOU", userData);
    usuario.id =

      userData.Role == 2 ? userData.IdFreelancer :
      userData.Id;
    usuario.userId = userData.Id;
    usuario.IdFreelancer = userData.IdFreelancer;
    usuario.email = userData.Email;
    usuario.nome = userData.Name;
    usuario.Role = userData.Role;
    usuario.cargo = userData.RoleCompany;
    usuario.imagem_perfil = comumConfig.baseUrlAPI() + userData.Photo;
    usuario.logotipo = comumConfig.baseUrlAPI() + userData.CustomerLogo;
    usuario.empresa = userData.Customer;
    usuario.Photo = userData.Photo;
    usuario.IdCustomer = userData.IdCustomer;
  }

  function getUser() {
    // console.log("MUDOU", userData);
    return usuario;
  }

  function deleteTask(taskId) {
    return $http.post(baseUrlAPI + "/Task/RemoveTask?task_id=" + taskId);
  }

  function saveTaskName(taskId, taskName) {
    $http.post(
      baseUrlAPI + "/Task/EditTitle?task_id=" + taskId + "&title=" + taskName
    );
  }

  function saveProjectName(projectId, projectName) {
    $http.post(
      baseUrlAPI +
      "/Project/EditName?project_id=" +
      projectId +
      "&name=" +
      projectName
    );
  }

  INIT();
}
