/*jshint laxcomma:true*/
function comumProjectComponent() {
    return {
        templateUrl: "../templates/project/view/visualizar-project-completo.html",
        controller: comumProjectComponentController,
        bindings: {
            getProfissionais: "<"
        }
    };
}
