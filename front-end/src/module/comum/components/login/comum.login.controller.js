comumLoginController.$inject = ['comumLoginService', '$http', '$state'];

function comumLoginController(comumLoginService, $http, $state) {
  var self = this;

  self.user = {};
  self.error = false;

  self.login = function () {

    comumLoginService.login(self.user, function (data) {
      if (data.success) {

        if (!data.Name) window.Rollbar.critical("Login Error", data);
        
        var token = data.token;
        window.sessionStorage.setItem('token', token);
        $http.defaults.headers.common.Authorization = 'Bearer ' + token;
        $state.go('profissional');

      } else {
        window.Rollbar.critical("Login Error", data);
        self.error = true;
      }
    });

  };
}
