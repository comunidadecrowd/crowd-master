/*jshint scripturl:true*/

comumVisualizarPerfilProfissionalController.$inject = [
  "comumVisualizarPerfilProfissionalService",
  "$scope",
  "comumService",
  "publicService",
  "comumConfig",
  "$state",
  "$localStorage",
  "$timeout",
];

function comumVisualizarPerfilProfissionalController(
  comumVisualizarPerfilProfissionalService,
  $scope,
  comumService,
  publicService,
  comumConfig,
  $state,
  $localStorage,
  $timeout
) {
  var self = this;

  // Private
  var user = {};

  // Public
  self.notVisible = true;
  self.hideBg = true;
  self.hasWorkExperience = false;
  self.hideSelectButton = true;
  self.disponibilidade = null;
  self.showSlide = false;

  self.perfil = {};
  self.professional = {};
  self.listPortfolio = [];

  self.openPublicProfile = openPublicProfile;

  // Functions
  function init() {
    user = comumService.getUser();
    self.notVisible =

        user.Role === 2 ? true :
        false;
    self.showSlide =

        $state.current.name === "profissional" ? false :
        true;

    console.log($scope);
  }

  function openPublicProfile() {
    var OpenProfile = window.open(
      "https://crowd.br.com/" + self.professional.PortfolioOnline + "/Full"
    );
    OpenProfile.blur();
    window.focus();
  }

  /**
   * Condicionaliza a visualização ou registra a interação com o usuário
   * @param {object} professional [objeto contendo o freelancer]
   */
  function trackAndValidate(professional) {
    if (validateHistory(professional.id)) {
      // O Customer já visualizou este freelancer nesta sessão, então não precisa clicar novamente
      return (professional.show = true);
    } else {
      // Registra a visualização dos dados de contato deste Freelancer ao Customer
      return trackView(professional.id);
    }
  }

  /**
   * Checa se os dados de contato deste freelancer já foram visto nesta sessão
   * @param   {string} arguments [ID do Freelancer, proveniente do objeto profissional]
   * @returns {boolean} [True ou False]
   */
  function validateHistory(id) {
    // Retorna true ou false caso esxista registro local
    return $localStorage.get("vp-" + self.professional.id);
  }

  /**
   * Função que registra a vizualização das informações de contato do Freela, 
   * e registra localmente para não precisar realizar novamente
   * @param   {string} id [ID do Freelancer. Pertence ao objeto professional.id]
   * @returns {boolean}  [True ou False para continuar]
   */
  function trackView(id) {
    comumVisualizarPerfilProfissionalService
      .trackView(id)
      .then(function success(data) {
        return $localStorage.set("vp-" + id);
      })
      .catch(function error(message) {
        console.log("Tracking Error:", message);
        return false;
      });
  }

  // self.openGaleria = openGaleria;

  // Functions
  function init() {
    user = comumService.getUser();
    self.notVisible =

        user.Role === 2 ? true :
        false;
    self.showSlide =

        $state.current.name === "profissional" ? false :
        true;
  }

  /**
   * COndicionaliza a visualização ou registra a interação com o usuário
   * @param {object} professional [objeto contendo o freelancer]
   */
  function trackAndValidate(professional) {
    if (validateHistory(professional.id)) {
      // O Customer já visualizou este freelancer nesta sessão, então não precisa clicar novamente
      return (professional.show = true);
    } else {
      // Registra a visualização dos dados de contato deste Freelancer ao Customer
      return trackView(professional.id);
    }
  }

  /**
   * Checa se os dados de contato deste freelancer já foram visto nesta sessão
   * @param   {string} arguments [ID do Freelancer, proveniente do objeto profissional]
   * @returns {boolean} [True ou False]
   */
  function validateHistory(id) {
    // Retorna true ou false caso esxista registro local
    return $localStorage.get("vp-" + professional.id);
  }

  /**
   * Função que registra a vizualização das informações de contato do Freela, 
   * e registra localmente para não precisar realizar novamente
   * @param   {string} id [ID do Freelancer. Pertence ao objeto professional.id]
   * @returns {boolean}  [True ou False para continuar]
   */
  function trackView(id) {
    comumVisualizarPerfilProfissionalService
      .trackView(id)
      .then(function success(data) {
        return $localStorage.set("vp-" + id);
      })
      .catch(function error(message) {
        console.log("Tracking Error:", message);
        return false;
      });
  }

  function addSocialNetwork(socialNetwork) {
    var twitter = addLinkAndClassSocialNetwork(socialNetwork.twitter).replace(
      "socialNetwork",
      "fa-twitter"
    );
    var facebook = addLinkAndClassSocialNetwork(socialNetwork.facebook).replace(
      "socialNetwork",
      "fa-facebook"
    );
    var dribbble = addLinkAndClassSocialNetwork(socialNetwork.dribbble).replace(
      "socialNetwork",
      "fa-dribbble"
    );
    var instagram = addLinkAndClassSocialNetwork(
      socialNetwork.instagram
    ).replace("socialNetwork", "fa-instagram");
    var linkedin = addLinkAndClassSocialNetwork(socialNetwork.linkedin).replace(
      "socialNetwork",
      "fa-linkedin"
    );
    var behance = addLinkAndClassSocialNetwork(socialNetwork.behance).replace(
      "socialNetwork",
      "fa-behance"
    );

    var redesSociais =
      twitter + facebook + dribbble + instagram + linkedin + behance;

    return redesSociais;
  }

  function addLinkAndClassSocialNetwork(hasLink) {
    var baseLink =
      '<a href="link" target="_blank"><i class="icon socialNetwork actve" aria-hidden="true"></i></a>';
    var isValid =

        !hasLink ||
        (hasLink && hasLink.length < 7) ||
        hasLink == "http://" ? false :
        true;
    var link =
      isValid ? hasLink :
      "javascript:void(0);";
    var classe =
      isValid ? "actve" :
      "redes-sociais-inativo";

    baseLink = baseLink.replace("link", link);
    baseLink = baseLink.replace("actve", classe);

    return baseLink;
  }

  function hasRating() {
    if (
      (!self.professional.Comments ||
        self.professional.Comments.length === 0) &&
      !self.professional.AgilityRating &&
      !self.professional.QualityRating &&
      !self.professional.ResponsabilityRating
    )
      return false;

    return true;
  }

  function configAvailability(availa) {
    if (!availa) return false;

    var availabilities = publicService.getAvailabilities();
    var availability = comumConfig.searchObjectInArray(
      availabilities,
      "Id",
      availa
    );

    return availability.Name;
  }

  function configSegments(segments) {
    return comumConfig
      .getPropertyValueFromObjectArray(segments, "Name")
      .join(", ");
  }

  function configSkills(skills) {
    if (skills.length === 0) return false;

    if (skills[0].Name !== undefined)
      return comumConfig.getPropertyValueFromObjectArray(skills, "Name");

    return comumConfig.getPropertyValueFromObjectArray(skills, "text");
  }

  var weburl = new RegExp(
    "^" +
      // protocol identifier
      "(?:(?:https?|ftp)://)" +
      // user:pass authentication
      "(?:\\S+(?::\\S*)?@)?" +
      "(?:" +
      // IP address exclusion
      // private & local networks
      "(?!(?:10|127)(?:\\.\\d{1,3}){3})" +
      "(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})" +
      "(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})" +
      // IP address dotted notation octets
      // excludes loopback network 0.0.0.0
      // excludes reserved space >= 224.0.0.0
      // excludes network & broacast addresses
      // (first & last IP address of each class)
      "(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])" +
      "(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}" +
      "(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))" +
      "|" +
      // host name
      "(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)" +
      // domain name
      "(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*" +
      // TLD identifier
      "(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))" +
      // TLD may end with dot
      "\\.?" +
      ")" +
      // port number
      "(?::\\d{2,5})?" +
      // resource path
      "(?:[/?#]\\S*)?" +
      "$",
    "i"
  );

  $scope.$on("event:visualizar-perfil", function(event, data) {
    var photo = data.Photo;
    self.showSlide =

        data.showSlide !== undefined ? data.showSlide :
        true;
    self.professional = data;
    self.infoField = "";
    self.isPremium = false;
    self.freemiumAlert = function() {
      swal({
        title: "Contato com o freelancer",
        text:
          "Tenha acesso ao email, telefone, Whatsapp e Skype do freelancer antes de contratá-lo, para que possam discutir o briefing. Este contato garante ainda mais tranquilidade na hora de contratar!",
        confirmButtonClass: "confirm sweet-buttonswrapper",
        confirmButtonText:
          "<a style='color:white; padding: 16px 20px' ui-sref='company-profile' href='#company-profile'>Anualmente por R$ 499,<small>00</small></a>",
        imageUrl: "../assets/img/freemium-perfil-restrito.png",
        showCloseButton: true,
      }).then(
        function() {},
        function(dismiss) {
          // dismiss can be 'cancel', 'overlay',
          // 'close', and 'timer'
          console.log("dismiss:", dismiss);
        }
      );
    };

    self.showUserInfo = function(data, field) {
      self.isPremium = true;

      var userInfo = JSON.parse(localStorage.getItem("ngStorage-user"));

      var trackInfo = {
        FreelancerId: data.Id,
        UserId: userInfo.Id,
        fieldRequested: field,
      };
      console.log(trackInfo);
      publicService.freelancerInfoOnClick(trackInfo);
    };

    self.professional.Portfolio =
      weburl.test(data.Portfolio) ? data.Portfolio :
      null;

    if (self.professional.Portfolio) {
      if (!self.professional.Portfolio.match(/^[a-zA-Z]+:\/\//)) {
        self.professional.Portfolio = "https://" + self.professional.Portfolio;
      }
    }

    //console.log('self.professional.Portfolio:', self.professional.Portfolio);
    self.professional.firstName = self.professional.Name.split(" ")[0];
    self.professional.photo =

        !photo ||
        photo.indexOf("base64") > -1 ||
        photo.indexOf("comunidadecrowd") > -1 ? photo :
        comumConfig.baseUrlAPI() + photo;
    self.perfil.redesSociais = addSocialNetwork({
      twitter: self.professional.TwitterUrl,
      facebook: self.professional.FacebookUrl,
      dribbble: self.professional.DribbbleUrl,
      instagram: self.professional.InstagramUrl,
      linkedin: self.professional.LinkedinUrl,
      behance: self.professional.BehanceUrl,
    });
    self.disponibilidade = configAvailability(self.professional.Availability);
    self.professional.segments = configSegments(self.professional.Segments);
    self.professional.tags = configSkills(self.professional.Skills);

    if (!self.notVisible) {
      self.hideBg =
        !hasRating() ? false :
        true;
      self.hideSelectButton = self.professional.hideSelectButton;
    }

    self.hasWorkExperience =

        self.professional.Experiences.length !== 0 ? true :
        false;
  });

  //Calls
  init();
}
