comumVisualizarPerfilProfissionalService.$inject = ['$http', 'comumConfig'];
function comumVisualizarPerfilProfissionalService($http, comumConfig) {
	// Public
	this.setDataPerfil = setDataPerfil;
	this.getDataPerfil = getDataPerfil;
	this.getListPortfolio = getListPortfolio;

	// Private
	var dataPerfil = {};
	var service = this;

	service.setDataPerfil = setDataPerfil;
	service.getDataPerfil = getDataPerfil;

	// Private
	function setDataPerfil(data) {
		dataPerfil = data;
	}

	function getDataPerfil() {
		return dataPerfil;
	}

	function getListPortfolio() {
		// return $http.get( comumConfig.baseUrlAPI + '/Professional/ListPortfolio' ); //NOTE: ISSO AQUI ESTÁ SOBRANDO
	}
}