comumJustNumberDirective.$inject = [];
function comumJustNumberDirective()
{
	return {
		restric: 'A',
		require: "ngModel",
		link: function($scope, element, elemAttr, ctrl){
			
			var justNumber = function( input ){
				return input.replace(/[^0-9]+/g, "");
			};

			element.bind("keyup", function () {
				ctrl.$setViewValue(justNumber(ctrl.$viewValue));
				ctrl.$render();
			});				
		}
	};
}