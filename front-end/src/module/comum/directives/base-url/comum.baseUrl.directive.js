comumBaseUrlDirective.$inject = ['comumConfig'];

function comumBaseUrlDirective(comumConfig) {
  var baseUrl = comumConfig.baseUrlAPI();

  return {
    restric: 'A',
    link: function ($scope, element, elemAttr) {

      var tagName = element.context.tagName;

      if (tagName === 'A')
        elemAttr.$observe('href', function () {
          comumConfig.attr(element, 'href', baseUrl + elemAttr.href);
        });
      else
        elemAttr.$observe('src', function () {
          comumConfig.attr(element, 'src', baseUrl + elemAttr.src);
        });
    }
  };
}
