comumProfileImageDirective.$inject = [ 'comumConfig' ];
function comumProfileImageDirective( comumConfig )
{
	var baseUrl = comumConfig.baseUrlAPI();

	return {
		restric: 'E',
		scope: {
			src: '='
		},
		template: [
			'<div class="img-profile-50">',
			'<div style="background:url('+baseUrl+'{{src}});background-size: 100% 100%;"></div>',
			'</div>'
		].join('')
	};
}