comumRun.$inject = ['$rootScope', '$http', '$state', '$location', '$localStorage', 'comumService'];

function comumRun($rootScope, $http, $state, $location, $localStorage, comumService) {
   window.moment.locale('pt-br');

    console.log("comumRun");
    console.debug('px', $rootScope.usuario);
    console.debug($rootScope);
    $rootScope.usuario = comumService.getUser();
    $rootScope.isProspect = false; 
    console.debug($rootScope);

    function redirectTo() {
        var currentUrl = window.location.href;
        var lastAttempt = window;

            console.debug('window', lastAttempt);
            
            if ($rootScope.usuario.loggedUser.Prospect == true && !$rootScope.usuario.loggedUser.IdFreelancer && currentUrl.indexOf('company-profile') == -1) {
              $rootScope.isProspect = true;
              $state.go('company-profile');
            }
            if ($rootScope.usuario.loggedUser.Prospect == true && !$rootScope.usuario.loggedUser.IdFreelancer && currentUrl.indexOf('company-profile') > -1) {
              $rootScope.isProspect = true;
            }
            if ($rootScope.usuario.loggedUser.Prospect == true && $rootScope.usuario.loggedUser.IdFreelancer && currentUrl.indexOf('profile') == -1) {
                $rootScope.isProspect = true;
                $state.go('profile');
              }
            if ($rootScope.usuario.loggedUser.Prospect == true && $rootScope.usuario.loggedUser.IdFreelancer && currentUrl.indexOf('profile') > -1) {
                $rootScope.isProspect = true;
            }
            if ($rootScope.usuario.loggedUser.Prospect == false) {
                $rootScope.isProspect = false;
            }
            console.debug('prospect?', $rootScope);

    }


    setTimeout(function(){
      redirectTo();
    }, 500)
    


    if ($localStorage.user && $localStorage.token) {
        $http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.token;
        $http.defaults.headers.common.State = $state.current.name;
        comumService.init();
    }

    $rootScope.$on('$locationChangeStart', function(event, next, current) {
        $rootScope.hasProject = false;

        $rootScope.location = $location.path();

        if (!$localStorage.user || !$localStorage.token) {
            console.log("!$localStorage.user || !$localStorage.token")
            window.location = "https://crowd.br.com/login/";
        }
    });

    $rootScope.$on('$locationChangeSuccess', function() {
        $rootScope.location = $location.path();
        if (!$rootScope.usuario.Prospect) {
            window.Intercom("update", { page: $location.path() });            
        }
    })

    $rootScope.$on('unauthorized', function() {
        window.location = "/login";
    });

    var interceptor = ['$rootScope', '$q', "Base64", function (scope, $q, Base64) {
        function success(response) {
            return response;
        }

        function error(response) {
            var status = response.status;
            if (status == 401) {
                window.location = "/account/login?redirectUrl=" + Base64.encode(document.URL);
                return;
            }
            // otherwise
            return $q.reject(response);
        }
        return function(promise) {
            return promise.then(success, error);
        }
    }];


}
