scProfissionalConfig.$inject = ['tagsInputConfigProvider'];

function scProfissionalConfig(tagsInputConfigProvider) {
  tagsInputConfigProvider.setDefaults('tagsInput', {
    placeholder: '',
    addOnEnter: true,
    addFromAutocompleteOnly: false,
    enableEditingLastTag: true,
    addOnBlur: true
  });

  tagsInputConfigProvider.setActiveInterpolation('tagsInput', {
    placeholder: true
  });
}
