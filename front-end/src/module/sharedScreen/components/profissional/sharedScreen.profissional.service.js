sharedScreenProfissionalService.$inject = ['$http', 'comumConfig'];

function sharedScreenProfissionalService($http, comumConfig) {

  var service = this;
  // Public
  service.getProfissionais = getProfissionais;
  service.getPerfil = getPerfil;
  service.searchUser = searchUser;
  service.saveSearch = saveSearch;

  // Private	
  var baseUrl = comumConfig.baseUrl();
  var baseUrlAPI = comumConfig.baseUrlAPI();

  // Functions
  function getProfissionais(data) {
    var confif = {};

    if (data !== undefined)
      var config = {
        params: data
      };

    return $http.get(baseUrlAPI + '/Professional/Search', config);
  }

  function saveSearch(data) {
    return $http.post(baseUrlAPI + '/Search?tags=' + data);
  }

  function getPerfil(idProfissional, state) {
    if (state) {
      return $http.get(baseUrlAPI + '/User/Get?code=' + encodeURIComponent(idProfissional) + '&state=' + state);

    } else {
      return $http.get(baseUrlAPI + '/User/Get?code=' + encodeURIComponent(idProfissional));
    }
  }

  function searchUser() {
    return new AlgoliaSearch('JQ3HDF3RLD', '51659cf0f5e5ca2101a23bef5952b92f')
      .initIndex('freelancers');
  };

}
