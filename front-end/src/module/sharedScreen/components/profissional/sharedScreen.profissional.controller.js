/*jshint scripturl:true*/

sharedScreenProfissionalController.$inject = ['$http', 'scProfissionalService', 'comumConfig', 'scBindBriefingProfissionalService', '$state', '$stateParams', '$timeout', 'comumVisualizarPerfilProfissionalService', '$scope', 'comumService', '$sessionStorage', 'ngDialog', 'scFirstAccessService', '$rootScope', '$analytics'];

function sharedScreenProfissionalController($http, scProfissionalService, comumConfig, scBindBriefingProfissionalService, $state, $stateParams, $timeout, comumVisualizarPerfilProfissionalService, $scope, comumService, $sessionStorage, ngDialog, scFirstAccessService, $rootScope, $analytics) {
  var self = this;

  // Private
  var currentPage = 0;
  /** @deprecated */ var currentRoll = 12;
  var itensPerPage = 30;
  var totalPage = 0;
  var minPrice = 0;
  var maxPrice = 500;
  var orderType = 2;
  var _totalRegistered = 0;

  var isPagination = false;
  var orderByRatingAndPrice = false;

  var order = null;
  var currentOrderCriterio = null;

  var categoriasSelecionadas = [];
  var categoriasSelecionadasIds = [];
  var skills = [];
  var faceHits = Object();

   self.indexName = 'freelancers';

  var page = 1;
  var _ = window._;

  // controlador do hover
  var timer;

  // Public
  self.qtdMaximaDeProfissionais = 10;
  self.qtdProfissionaisSelecionados = 0;
  self.getCities = getCities;
  self.totalRecords = 0;
  self.totalRegistered = 0;
  self.cityResults = '';
  self.totalPage = 0;
  self.showDescription = showDescription;
  self.openDescription = {};
  self.criterioOrdenacao = false;
  self.reverseOrder = false;
  self.limiteAtingido = false;
  self.isOpen = false;
  self.isClicking = false;
  self.hasData = false;
  self.isLoading = false;
  self.isSearch = false;
  self.buttonSendType = null;
  self.viewType = "card";
  self.buscaAvancada = true;
  self.queryCidade = "";
  self.advancedFilter = "";
  self.baseUrlAPI = comumConfig.baseUrlAPI();
  self.profissionaisSelecionados = [];
  self.allProfissionais = [];
  self.profissionais = [];
  self.categorias = [];
  self.tagsRelacionadas = [];
  self.gettingCities = false;
  self.skills = [];
  self.estados = [];
  self.cidades = [];
  //self.hideList = hideList;
  self.segmentos = [];
  self.disponibilidades = [];
  self.categoryFilter = categoryFilter;
  self.cityQuery = cityQuery;
  self.perfil = {};
  self.filter = {};
  self.slideRange = {};
  self.query = "";
  self.saveSearch = saveSearch;

  self.setCity = setCity;

  self.getMatchingExp = getMatchingExp;
  self.orderBy = orderBy;
  self.onAddedTag = onAddedTag;
  self.onRemovedTag = onRemovedTag;
  self.addProfissional = addProfissional;
  self.changeViewType = changeViewType;
  self.removeProfissional = removeProfissional;
  self.selecionarCategoria = selecionarCategoria;
  self.addTagRelacionada = addTagRelacionada;
  self.slideOpenProfile = slideOpenProfile;
  self.enviarBriefing = enviarBriefing;
  self.loadCities = loadCities;
  self.loadSkills = loadSkills;
  self.cleanFilter = cleanFilter;
  self.advancedSearch = advancedSearch;
  self.openDialogFreelancers5Stars = openDialogFreelancers5Stars;
  self.freemiumAlert = freemiumAlert;
  self.initSearch = initSearch;
  self.buscaAvancada2 = buscaAvancada2;
  self.initRun = true;
  self.searchStarRating = function (media) {
    return comumConfig.searchStarRating(media);
  }
  self.baseUrlAPI = comumConfig.baseUrlAPI();

  // Functions
  function init() {

    // if (self.getProfissionais.status != 200) {
    //     window.Rollbar.critical("sharedScreenProfissionalController > getProfissionais", self.getProfissionais.data);
    // }

    self.categorias = self.getCategorias.data;
    self.buttonSendType = ($sessionStorage.briefing === undefined) ? 'Pedir' : 'Editar';
    skills = _getSkills(self.getSkills.data);

    // initProfissionaisList(self.getProfissionais.data);
    addEventScrollPage();
    checkSeUsuarioVeioDeBriefing();
    configDataFilter();
    configSlideRange();
    //addScrollable();

    engine.initIndex(self.indexName)
      .search("", {
        attributesToRetrieve: null
      }, function (success, content) {
        self.totalRegistered = content.nbHits;
      });

  }
  




  var engine = algoliasearch('Z4V86RWKZ2', '3f7e6ad21e56af31bcbe84205f1dda59'); 
  //.initIndex('freelancers');

  var placeholderText = [
    "Desenvolvedor, php, .net, programador, wordpress...",
    "Designer, UX, Photoshop, Logotipo, Branding...",
    "Google Adwords, Facebook ads, mídias sociais, Email marketing...",
    "Tradutor, Frances, Inglês, Espanhol....",
    "Editor Vídeo, Motion, Finalcut, Locutor...",
    "Redator, Jornalista, Saúde, Carros, Alimentação..",
    "Planejamento de Campanha, Branded Content, Estratégia de comunicação..."
  ];

  $('#search-input')
    .placeholderTypewriter({
      text: placeholderText
    });

  function setCity(event) {
    console.log("setCity ", event)
    console.log("self.filter.cidade ", self.filter.cidade)
    self.initSearch();
  }

  function initSearch(isBasic) {

    $timeout.cancel(timer);
    faceHits = {};
    value = [];
    count = [];

     self.advancedFilter = getFilter();
    

      if (self.query.length < 0) {
        self.query = "";
      }
    
    if (self.advancedFilter) var mappedFilter = _.map(self.advancedFilter);
    
      var string = [];

      for (i = 0; i < mappedFilter.length; i++) {
        string.push(mappedFilter[i].Category + mappedFilter[i].Name)
      }
      if (mappedFilter.length > 1) {
        self.isSearch = true;
      }
      if (self.query.length > 1) {
        self.isSearch = true;
      }

      var queryString = string.join('');
      engine.clearCache();

    

      return engine.initIndex('freelancers')
        .search(self.query, {
          highlightPreTag: '<em class="search-highlight">',
          attributesToSnippet: ['Description:15', 'Experiences:15'],
          hitsPerPage: 20,
          filters: queryString,
          facets: ['City.Name','State.UF', 'Segments', 'Category', 'Availability'],
          removeStopWords: ["pt-br", "pt", "en", "es"]
        }, function (success, content) {
          self.totalPage = content.nbPages;
          self.totalRecords = content.nbHits;
          self.profissionais = content.hits;
          timer = $timeout(function () {
            self.saveSearch();
          }, 600);
          value = _.keysIn(content.facets["City.Name"]);
          count = _.valuesIn(content.facets["City.Name"]);

          for (var index = 0; index < count.length; index++) {
            faceHits[index] = {
              "value": value[index],
              "count": count[index]
            };
            self.cityResults = faceHits
          }
          
          $scope.$apply();
          comumConfig.unblockScreen();
        })
    
  };

  function getCities(event) {
   // event.preventDefault();
   // event.stopPropagation();

    self.gettingCities = true;
    var typingTimer;                //timer identifier
    var doneTypingInterval = 4000;  //time in ms, 5 second for example
    var $input = window.jQuery('#city-input');
    
    //on keyup, start the countdown
    $input.on('keyup', function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });
    
    //on keydown, clear the countdown 
    $input.on('keydown', function () {
      console.debug('debug');
      clearTimeout(typingTimer);
    });
    
    //user is "finished typing," do something
    function doneTyping () {
        if (self.isClicking === true) {
          return false;
        }
        self.isClicking = true;
        /* window.jQuery('#city-input').autocomplete({
          showNoSuggestionNotice: true,
          noSuggestionNotice: 'Nenhum resultado encontrado',
          minChars: 4,
          serviceUrl: comumConfig.baseUrlAPI() + '/Common/Cities?city=' + self.queryCidade,
          transformResult: function(result) {
           // console.debug('query');
            self.gettingCities = false; 
            return {
              suggestions: window.jQuery.map(JSON.parse(result), function(dataItem) {
                return { 
                  value: dataItem.Name, 
                  id: dataItem.StateId
                }
              })
            }
          },
          onSelect: function(suggestion) {
            console.debug(suggestion);
            self.filter = {
              cidade: {
                Name: suggestion.value.match(/-([\s\S]*)$/)[1]
              }
            };
            initSearch();
          }
        }) */
        self.isClicking = false;
    }
  }

  function buscaAvancada2() {
    self.buscaAvancada = !self.buscaAvancada;

    if (self.buscaAvancada === false) {
      document.getElementById('profissionais-selecionados')
        .style.top = "130px"
    } else {
      document.getElementById('profissionais-selecionados')
        .style.top = "210px"
    }
  };

  function cityQuery() {

    console.log("self.filter.cidade", self.filter.cidade);

    if (self.filter.cidade) {
      engine.initIndex('freelancers')
        .searchForFacetValues({
          facetName: 'City.Name',
          facetQuery: self.filter.cidade
        }, function (success, content) {
          self.cityResults = {};
          self.cityResults = content.facetHits.length ? content.facetHits : null;
          self.hasData = true;
        })
    } else {
      self.filter.cidade = undefined;
      initSearch();
    }

  }

  function getMatchingExp(snippets) {
    var snippet = Object(snippets);
    var filter = Array();
    var subtitulo = String();
    var descricao = String();
    var index = Number();


    /**
     * @description Algoritmo de busca dentro do resultado para highlight adequado
     * @example Primeiro validar caso há um 'matchLevel full' para considerálo o highlight
     */
    if (!snippet.Experiences) {

      subtitulo = 'Sobre:';
      descricao = snippet.Description.value ? snippet.Description.value : '';

    } else {

      if (_.some(snippet.Experiences.Companies, {
          'matchLevel': 'full'
        })) { // ENCONTREI UMA EMPRESA 

        temp = _.find(snippet.Experiences.Companies, {
          'matchLevel': 'full'
        });
        subtitulo = "Trabalhou na: " + temp.value ? temp.value : '';

        for (i = 0; i < snippet.Experiences.Companies.length; i++) {
          if (snippet.Experiences.Companies[i].matchLevel == 'full') {
            index = i;
            break;
          }
        }

        descricao = snippet.Experiences.Descriptions[index].value ? snippet.Experiences.Descriptions[index].value : '';

      }

      if (_.some(snippet.Experiences.Descriptions, {
          'matchLevel': 'full'
        })) { // ENCONTREI UMA EXPERIÊNCIA PROFISSIONAL

        temp = _.find(snippet.Experiences.Descriptions, {
          'matchLevel': 'full'
        });
        descricao = temp.value ? temp.value : '';

        for (i = 0; i < snippet.Experiences.Descriptions.length; i++) {
          if (snippet.Experiences.Descriptions[i].matchLevel == 'full') {
            index = i;
            break;
          }
        }

        subtitulo = "Trabalhou na: " + snippet.Experiences.Companies[index].value ? snippet.Experiences.Companies[index].value : ''

      }

      if (_.some(snippet.Experiences.Roles, {
          'matchLevel': 'full'
        })) { // ENCONTREI UM TÍTULO PROFISSIONAL

        temp = _.find(snippet.Experiences.Roles, {
          'matchLevel': 'full'
        });
        subtitulo = "Experiente como: " + temp.value ? temp.value : '';

        for (i = 0; i < snippet.Experiences.Roles.length; i++) {
          if (snippet.Experiences.Roles[i].matchLevel == 'full') {
            index = i;
            break;
          }
        }

        descricao = snippet.Experiences.Descriptions[index].value ? snippet.Experiences.Descriptions[index].value : '';

      }

      if (subtitulo.length == 0 || descricao.length == 0) { // CASO HOUVE MATCHLEVEL MAS NÃO ERA FULL
        subtitulo = 'Sobre:';
        descricao = snippet.Description.value;
      }

    }

    filter[0] = subtitulo;
    filter[1] = descricao;

    return filter;
  };

  function showDescription(id) {
    if (id) {
      timer = $timeout(function () {
        self.openDescription.Id = id;
      }, 300);
    } else {
      $timeout.cancel(timer);
      self.openDescription.Id = undefined;
    }
  }

  /**
   * Executa o alerta avisando sobre a limitação Freemium para ver os contatos do freelancer
   */
  function freemiumAlert() {
    window.swal({
        title: "Contato com o freelancer",
        text: "Tenha acesso ao email, telefone, Whatsapp e Skype do freelancer antes de contratá-lo, para que possam discutir o briefing. Este contato garante ainda mais tranquilidade na hora de contratar!",
        confirmButtonClass: "confirm sweet-buttonswrapper",
        confirmButtonText: "<a style='color:white; padding: 16px 20px' ui-sref='company-profile' href='#company-profile'>Anualmente por R$ 499,<small>00</small></a>",
        imageUrl: '../assets/img/freemium-perfil-restrito.png',
        showCloseButton: true
      })
      .then(function () {},
        function (dismiss) {
          console.log('dismiss:', dismiss);

        });
  }

  function saveSearch() {
    var prevQuery = self.query;
    self.isLoading = false;
    
    setTimeout(function () {
      if (self.query == prevQuery) {
        window.Intercom('trackEvent', 'busca', self.query);
        window.fbq('track', 'Search', {
          search_string: self.query,
        });
        return scProfissionalService.saveSearch(self.query)
      } else {
        return false;
      }
    }, 2000)
  };

  function initProfissionaisList(profissionais) {
    if (!isPagination) {
      self.profissionais = [];
      console.log(profissionais);
      totalPage = Math.ceil(profissionais.totalReturned / itensPerPage);
      self.totalRecords = profissionais.totalReturned;
      self.tagsRelacionadas = getRelatedSkills(profissionais.response.relatedSkills);
      //$scope.$digest();
    }

    configDataProfissionais(profissionais.response.results);

    //addStar();
    // addNomeCurto();
    // addResumoDescricao();
  }

  function addStar() {
    self.profissionais.forEach(function (objProfissional, index) {
      var quality = objProfissional.rating.quality;
      var responsability = objProfissional.rating.responsability;
      var agility = objProfissional.rating.agility;

      objProfissional.star = comumConfig.addStarRating(quality, responsability, agility);
    });
  }

  function addNomeCurto() {
    self.profissionais.forEach(function (profissional, index) {
      profissional.nomeCurto = comumConfig.getFirstAndLastWord(profissional.nome);
    });
  }

  function addResumoDescricao() {
    self.profissionais.forEach(function (profissional, index) {
      profissional.descricao = profissional.Description == 'null' ? '' : profissional.Description;
      profissional.resumoDescricao = comumConfig.cortarTexto(profissional.Description, 140);
    });
  }

  function loadMoreProfissionais() {
    self.advancedFilter = getFilter();

    console.log("getFilter()", getFilter());

    if (self.isLoading) {
      return false;

    } else {

      self.isLoading = true;
      isPagination = true;
      var string = [];

      console.debug('self.advancedFilter.length', self.advancedFilter.length);
      console.debug('self.advancedFilter', self.advancedFilter);

      if (self.advancedFilter) {
        var mappedFilter = _.map(self.advancedFilter);

        for (i = 0; i < mappedFilter.length; i++) {
          string.push(mappedFilter[i].Category + mappedFilter[i].Name)
        }
      }

        var queryString = string.join('');
        engine.clearCache();

        engine.initIndex(self.indexName)
          .search(self.query, {
          
            filters: queryString,
            page: currentPage,
            attributesToSnippet: ['Description:15', 'Experiences:15'],
            hitsPerPage: 20,
            removeStopWords: ["pt-br", "pt", "en", "es"],
          
          }, function (success, content) {
            
            if (content.nbHits == 0) { // nenhum resultado novo
              comumConfig.unblockScreen();
              self.isLoading = true; // para evitar loop de requisições 
              return;
            }

            var prevList = self.profissionais;
            var content = content.hits;
            self.profissionais = prevList.concat(content);
            self.totalRecords = content.nbHits;
            
            $timeout(function () {
              comumConfig.unblockScreen();
              self.isLoading = false;
            }, 1000);

            $scope.$apply();

          })

    /*} else {

        self.isLoading = true;
        isPagination = true; //NOTE: uso não percebido em lugar nenhum?

        engine.initIndex(self.indexName)
          .search({
            
            query: self.query,
            page: currentPage,
            attributesToSnippet: ['Description:15', 'Experiences:15'],
            hitsPerPage: 20,
            removeStopWords: ["pt-br", "pt", "en", "es"]
          
          }, function (success, content) { // busca recebida com sucesso
            
            if (content.nbHits == 0) {
              comumConfig.unblockScreen();
              self.isLoading = true;
              return;
            }

            var prevList = self.profissionais;
            var content = content.hits;
            self.profissionais = prevList.concat(content);
            currentPage = currentPage + 1;
            
            $timeout(function () { //NOTE: o que este código faz?
              comumConfig.unblockScreen();
              self.isLoading = false;
            }, 1000);

            $scope.$apply();

          });
      } */
    }
  }

  /**
   * @name nextPage
   * @desc condicional que valida e inicia a requisição de uma proxima página da busca
   * @returns false quando isLoading é true
   */
  function nextPage() {

    if (self.isLoading) {
      comumConfig.unblockScreen();
      return false;
   // } if (self.advancedFilter) {
  //      comumConfig.unblockScreen();
  //      return false;
    } else {
        if (currentPage < self.totalPage) {
          currentPage = currentPage + 1;
          loadMoreProfissionais();
        }

    }

  }

  /**
   * @name addEventScrollPage
   * @desc adiciona o evento 'scroll' que assiste e dispara o carregamento em 900px para terminar e segura o usuário em 20px com uma tela de loading.
   * @requires comumConfig.blockScreen
   * @requires window.jQuery
   */
  function addEventScrollPage() {

    window.$jQuery(window)
      .bind('scroll', function () {
        if ($state.current.name === 'profissional') {
          if (jQuery(window)
            .scrollTop() + jQuery(window)
            .height() > jQuery(document)
            .height() - 900) {
            nextPage();
          }
          if (jQuery(window)
            .scrollTop() + jQuery(window)
            .height() > jQuery(document)
            .height() - 0) {
            comumConfig.blockScreen();
            $timeout(function () {
              comumConfig.unblockScreen();
            }, 1300);
          }
        }
      });

  };

  function addProfissional(profissional, event, closeSlide) {
    console.log(profissional);
    var selectedProfessional = '';
    $analytics.eventTrack('Profissional_selecionado', {
      category: 'Site',
      label: profissional.email
    });

    if (selectedProfessional == profissional.Id) {
      return false;
    } else {
      selectedProfessional = profissional.Id;
    }

    var user = JSON.parse(window.localStorage.getItem('ngStorage-user'));
    console.log(user);

    // NOTE: popup com o alerta sobre não ser permitido selecionar profissional de 4 ou mais estrelas
    if (profissional.Rating >= 4 && user.CustomerPlan == 'FREE' || profissional.starRating >= 4 && user.customerPlan == 'FREE') {
      window.swal({
          title: "Freelancer 4 ou 5 estrelas",
          text: "Contrate profissionais classificados com 4 ou 5 estrelas - os mais qualificados de nossa rede. Eles são avaliados pela qualidade do trabalho, comprimetimento com o projeto e agilidade na entrega.",
          confirmButtonClass: "confirm-freelancer",
          confirmButtonText: "<a style='color:white' ui-sref='company-profile' href='#company-profile'>Pagamendo anual de R$ 499,<small>00</small></a>",
          customClass: "alert-freelancer",
          imageUrl: '../assets/img/alert-cinco-estrelas.png',
          showCloseButton: true
        }, function () {
          window.location = "/";
        })
        .then(function success() {
          //
        });

      return event.stopPropagation();

    }
    if (profissional.Rating >= 4 && user.CustomerPlan == 'FREE' || profissional.starRating >= 4 && user.customerPlan == 'FREE') {
      window.swal({
          title: "Freelancer 4 ou 5 estrelas",
          text: "Contrate profissionais classificados com 4 ou 5 estrelas - os mais qualificados de nossa rede. Eles são avaliados pela qualidade do trabalho, comprimetimento com o projeto e agilidade na entrega.",
          confirmButtonClass: "confirm-freelancer",
        confirmButtonText: "<a style='color:white' ui-sref='company-profile' href='#company-profile'>Pagamendo anual de R$ 499,<small>00</small></a>",
          customClass: "alert-freelancer",
          imageUrl: '../assets/img/alert-cinco-estrelas.png',
          showCloseButton: true
        }, function () {
          window.location = "/";
        })
        .then(function success() {
          //
        });

      return event.stopPropagation();

    }


    if (profissional.Name !== undefined) {
      profissional = {
        id: profissional.Id,
        nome: profissional.Name,
        nomeCurto: comumConfig.getFirstAndLastWord(profissional.Name),
        imagem_perfil: comumConfig.baseUrlAPI() + profissional.Photo,
        profissao: profissional.Title,
        Code: profissional.Code
      };
    }

    var clickedElement = comumConfig.getClickedElement(event);

    if (event && clickedElement !== 'A') {

      // NOTE: popup com o alerta sobre não ser permitido selecionar profissional de 4 ou mais estrelas
      if ($rootScope.getUser()
        .CustomerPlan == 'FREE' && profissional.starRating >= 4) {
        window.swal({
            title: "Freelancer 4 ou 5 estrelas",
            text: "Contrate profissionais classificados com 5 estrelas - os mais qualificados de nossa rede. Eles são avaliados pela qualidade do trabalho, comprimetimento com o projeto e agilidade na entrega.",
            confirmButtonClass: "confirm sweet-buttonswrapper",
          confirmButtonText: "<a style='color:white' ui-sref='company-profile' href='#company-profile'>Anualmente por R$ 499,<small>00</small></a>",
            imageUrl: '../assets/img/alert-cinco-estrelas.png',
            showCloseButton: true
          })
          .then(function () {},
            function (dismiss) {
              // dismiss can be 'cancel', 'overlay',
              // 'close', and 'timer'
              console.log('dismiss:', dismiss);

            });

        return event.stopPropagation();

      }

      if ($rootScope.getUser().CustomerPlan == 'FREE' && profissional.starRating >= 4) {
        window.swal({
            title: "Freelancer 4 ou 5 estrelas",
            text: "Contrate profissionais classificados com 5 estrelas - os mais qualificados de nossa rede. Eles são avaliados pela qualidade do trabalho, comprimetimento com o projeto e agilidade na entrega.",
            confirmButtonClass: "confirm sweet-buttonswrapper",
          confirmButtonText: "<a style='color:white' ui-sref='company-profile' href='#company-profile'>Anualmente por R$ 499,<small>00</small></a>",
            imageUrl: '../assets/img/alert-cinco-estrelas.png',
            showCloseButton: true
          })
          .then(function () {},
            function (dismiss) {
              // dismiss can be 'cancel', 'overlay',
              // 'close', and 'timer'
              console.log('dismiss:', dismiss);

            });

        return event.stopPropagation();

      }      


      var idProfissional = profissional.Code;
      console.log(self.profissionaisSelecionados);

      if (!comumConfig.searchObjectInArray(self.profissionaisSelecionados, 'Code', idProfissional)) {
        if (self.qtdProfissionaisSelecionados >= self.qtdMaximaDeProfissionais && $rootScope.getUser().Role != 1) {
          //NOTE: Master pode adicionar quantos profissionais quiser
          self.limiteAtingido = true;
        } else {
          comumConfig.addClass('#box-' + idProfissional, 'box-ativo');
          self.profissionaisSelecionados.unshift(profissional);
          self.qtdProfissionaisSelecionados++;
          self.limiteAtingido = false;
          addLabelProfissional(idProfissional);
          window.$jQuery('#profissionais-selecionados')
            .asScrollable("update");
        }
      } else {
        self.removeProfissional(profissional);
        window.$jQuery('#profissionais-selecionados')
          .asScrollable("update");
      }

    }

    if (closeSlide) {
      self.isOpen = false;
      comumConfig.bodyOverflow(true);
      comumConfig.fadeOut('.mask-content-pageslide');
    }

  }

  function addLabelProfissional(idProfissional) {
    window.$jQuery('#label-profissional-' + idProfissional)
      .fadeToggle("slow");
    setTimeout(function () {
      window.$jQuery('#label-profissional-' + idProfissional)
        .fadeToggle("slow");
    }, 2000);
  }

  function changeViewType(viewType) {
    self.viewType = viewType ? "card" : "list";
    var element = comumConfig.getElement('.page-content');

    if (!viewType) {
      comumConfig.addClass(element, 'lista-profissional');
      comumConfig.removeClass(element, 'card-profissional');
    } else {
      comumConfig.removeClass(element, 'lista-profissional');
      comumConfig.addClass(element, 'card-profissional');
    }

    var grid = comumConfig.getElement('.view-type-grid');
    var list = comumConfig.getElement('.view-type-list');

    if (!viewType) {
      grid.css('color', '#ccc');
      grid.css('cursor', 'pointer');
      list.css('color', '#83b9ed');
      list.css('cursor', 'default');
    } else {
      grid.css('color', '#83b9ed');
      grid.css('cursor', 'default');
      list.css('color', '#ccc');
      list.css('cursor', 'pointer');
    }
  }

  function removeProfissional(profissional) {
    if (!checkIfProfessionalMayBeRemoved(profissional))
      return false;

    var idProfissional = profissional.Code;
    comumConfig.removeClass('#box-' + idProfissional, 'box-ativo');
    self.profissionaisSelecionados = comumConfig.removeObjectFromArray(self.profissionaisSelecionados, 'Code', idProfissional);
    self.qtdProfissionaisSelecionados--;
  }

  function checkIfProfessionalMayBeRemoved(professionalObj) {
    var professional = comumConfig.searchObjectInArray(self.profissionaisSelecionados, 'Code', professionalObj.Code);
    if (professional)
      if (professional.disableRemove !== undefined)
        if (professional.disableRemove)
          return false;

    return true;
  }

  function orderBy(criterio) {
    orderByRatingAndPrice = true;
    if (self.indexName === criterio) {
      self.indexName = 'freelancers';
    } else {
      self.indexName = criterio;
    }
     self.advancedFilter = getFilter();
     var string = [];

     if (self.advancedFilter) {
       var mappedFilter = _.map(self.advancedFilter);

       for (i = 0; i < mappedFilter.length; i++) {
         string.push(mappedFilter[i].Category + mappedFilter[i].Name)
       }
     }

    var queryString = string.join('');
    engine.clearCache();

    engine.initIndex(self.indexName)
      .search(self.query, {

        filters: queryString,
        page: currentPage,
        attributesToSnippet: ['Description:15', 'Experiences:15'],
        hitsPerPage: 20,
        removeStopWords: ["pt-br", "pt", "en", "es"],

      }, function (success, content) {

        if (content.nbHits == 0) { // nenhum resultado novo
          comumConfig.unblockScreen();
          self.isLoading = true; // para evitar loop de requisições 
          return;
        }

        var prevList = self.profissionais;
        var content = content.hits;
        self.profissionais = content;
        self.totalRecords = content.nbHits;

        $timeout(function () {
          comumConfig.unblockScreen();
          self.isLoading = false;
        }, 1000);

        $scope.$apply();

      });
  }

  function onAddedTag() {
    addAndRemoveClassInputTags();
    setTimeout(function () {
      window.$jQuery(".tag-list")
        .scrollLeft(500);
    }, 100);

    _getProfissionais();
  }

  function onRemovedTag() {
    addAndRemoveClassInputTags();
    _getProfissionais();
  }

  function addAndRemoveClassInputTags() {
    var input = comumConfig.getElement('.ng-valid');
    var classe = 'no-placeholder';

    if (self.skills.length > 0)
      comumConfig.addClass(input[1], classe);
    else
      comumConfig.removeClass(input[1], classe);
  }

  function _getProfissionais() {
    comumConfig.blockScreen('', '.busca-avancada, .page-content');
    self.isSearch = false;

    scProfissionalService.getProfissionais(getFilter())
      .success(function (data, status) {

        if (status != 200) {
          toaster.pop(
            "error",
            "Bad Server",
            "Sobrecarga no servidor"
          );
          window.Rollbar.critical(
            "scProfissionalService.getProfissionais > _getProfissionais",
            data
          );
          return false;
        }

        initProfissionaisList(data);
        console.log("data", data);
        comumConfig.unblockScreen('.busca-avancada, .page-content');
        resetStatusPagination();
        self.totalRegistered = _totalRegistered;
        self.isSearch = true;

      })
      .error(function (data) {
        comumConfig.unblockScreen('.busca-avancada, .page-content');
        resetStatusPagination();
      });
  }

  function resetStatusPagination() {
    if (isPagination) {
      self.isLoading = false;
      isPagination = false;
      currentPage = 0;
    }
  }

  function categoryFilter(event, categoria) {
    var filter = {
      Category: "Category.Name: ",
      Name: "'" + categoria.Name + "'"
    }
    return engine.initIndex(self.indexName)
      .search(self.query, {
        attributesToSnippet: ['Description:15', 'Experiences:15'],
        filters: filter.Category + filter.Name,
        removeStopWords: ["pt-br", "pt", "en", "es"],
      }, function (success, content) {
        console.log(engine);

        console.log(content);
        self.profissionais = content.hits;
        self.isSearch = true;
        self.totalRecords = content.nbHits;

        $scope.$apply();
        comumConfig.unblockScreen();

      })
  }

  function selecionarCategoria(event, categoria) {
    var element = comumConfig.getElement(event.currentTarget);

    if (!element.hasClass('cat-check')) {
      comumConfig.removeClass('.cat-check', 'cat-check');
      categoriasSelecionadas = [];
      comumConfig.addClass(element, 'cat-check');
      categoriasSelecionadas.push(categoria);
    } else {
      comumConfig.removeClass('.cat-check', 'cat-check');
      categoriasSelecionadas = [];
      initSearch(true);
    }

    var countCategoriasSelecionadas = comumConfig.findElement('.cat-check')
      .length;
    var labelElement = '#selecione-categoria';
    if (countCategoriasSelecionadas == 1) {
      var catName = categoriasSelecionadas[0];
      comumConfig.addText(labelElement, catName.Name);
      categoryFilter(event, categoria);
    } else if (countCategoriasSelecionadas > 1) {
      comumConfig.addText(labelElement, 'Profissional [' + countCategoriasSelecionadas + ']');
    } else {
      comumConfig.addText(labelElement, 'Profissional');
      initSearch(true);
    }
    console.log(categoria);

    categoriasSelecionadasIds = comumConfig.getPropertyValueFromObjectArray(categoriasSelecionadas, 'Id');
    console.log(categoriasSelecionadasIds);
  }

  function addTagRelacionada(tag) {
    tag = {
      text: tag
    };
    self.skills.push(tag);
    self.onAddedTag();
  }

  function slideOpenProfile(event, idProfissional, state) {
    console.log(idProfissional);
    $analytics.eventTrack('Profissional_visualizado', {
      category: 'Perfil_profissional',
      label: idProfissional
    });
    comumConfig.blockScreen('', '#perfil-profissional');
    $scope.$broadcast('event:visualizar-perfil', clearProfile());

    self.isOpen = !self.isOpen;

    window.$jQuery('#company-content')
      .toggleClass('fixed-pos');

    if (idProfissional && self.isOpen) {

      self.perfil.profissional = comumConfig.searchObjectInArray(self.profissionais, 'Code', idProfissional);
      self.perfil.profissional.imagemPerfil = comumConfig.attr('#imagem-perfil-' + idProfissional, 'src');
      // self.perfil.profissional.imagemPerfil = comumConfig.attr('#imagem-perfil-' + 'kkkk', 'src');

      console.log(self.perfil.profissional);

      event.stopPropagation();

      scProfissionalService.getPerfil(idProfissional, state)
        .success(function (data, status) {

          if (status != 200) {
            window.Rollbar.critical("sharedScreen.profissional.controller > getPerfil", data);
          }
          comumConfig.fadeIn('.mask-content-pageslide');
          data.showSlide = true;
          $scope.$broadcast('event:visualizar-perfil', data);
          comumConfig.unblockScreen('#perfil-profissional');
        })
        .error(function (data) {
          window.Rollbar.critical("sharedScreen.profissional.controller > getPerfil", data);
        });
    } else {
      comumConfig.fadeOut('.mask-content-pageslide');
    }
  }

  function clearProfile() {
    return {
      Name: '',
      Photo: null,
      Segments: [],
      Skills: [],
      Availability: null,
      Experiences: []
    };
  }

  function enviarBriefing() {
    if (self.profissionaisSelecionados) {

      var params = {};
      var professionais = [];

      self.profissionaisSelecionados.forEach(function (professionalObj) {
        var photo = professionalObj.imagem_perfil;
        professionais.push({
          Id: professionalObj.id,
          Name: professionalObj.nome,
          Photo: photo.substr(photo.indexOf('/Content')),
          Title: professionalObj.profissao,
          Code: professionalObj.Code
        });
      });

      if ($sessionStorage.briefing !== undefined) {
        $sessionStorage.briefing.Freelancers = professionais;
        $state.go('salvar-briefing', {
          id: $sessionStorage.briefing.Id,
          hunting: $sessionStorage.briefing.Hunting
        });

      } else {
        $sessionStorage.briefing = {
          Freelancers: professionais,
          Text: null,
          Title: null,
          Excerpt: null,
          Attachs: []
        };
      }
    }
  }

  function checkSeUsuarioVeioDeBriefing() {
    if ($sessionStorage.fromBriefing) {

      var profissionais = [];
      $sessionStorage.briefing.Freelancers.forEach(function (freelancerObj) {
        profissionais.push({
          id: freelancerObj.Id,
          nome: freelancerObj.Name,
          nomeCurto: freelancerObj.shourtName,
          imagem_perfil: comumConfig.baseUrlAPI() + freelancerObj.Photo,
          profissao: freelancerObj.Title,
          disableRemove: ((freelancerObj.complement !== undefined) ? false : true),
          Code: freelancerObj.Code
        });
      });

      self.profissionaisSelecionados = profissionais;
      self.qtdProfissionaisSelecionados = self.profissionaisSelecionados.length;

      self.profissionaisSelecionados.forEach(function (profissional, index) {
        if (comumConfig.searchObjectInArray(self.profissionais, 'Code', profissional.Code))
          $timeout(function () {
            comumConfig.addClass('#box-' + profissional.Code, 'box-ativo');
          }, 0, false);
      });
    }
    
  }

  function configDataFilter() {
    self.estados = [{
      Id: null,
      UF: null,
      Name: "Carregando estados..."
    }];
    self.cidades = [{
      Id: null,
      Name: "Selecione o estado...",
      StateId: null
    }];
    self.segmentos = [{
      Id: null,
      Name: 'Carregando segmentos...'
    }];
    self.disponibilidades = [{
      Id: 1,
      Name: 'Integral'
    }, {
      Id: 2,
      Name: 'Parcial'
    }];

    comumService.getEstados()
      .success(function (data) {
        self.estados = data;
      })
      .error(function (data) {
        alert('Oooops!\nErro ao carregar os estados.');
        console.error(data);
      });

    comumService.getSegmentos()
      .success(function (data) {
        self.segmentos = data;
      })
      .error(function (data) {
        alert('Oooops!\nErro ao carregar os segmentos.');
        console.error(data);
      });
  }

  function loadCities(estado) {
    if (!estado || !estado.Id)
      return false;

    self.filter.cidade = undefined;
    self.cidades = [{
      Id: null,
      Name: "Carregando cidades...",
      StateId: null
    }];

    comumService.getCidades(estado.Id)
      .success(function (data) {
        self.cidades = data;
      })
      .error(function (data) {
        alert('Ooops!\nErro ao carregar cidades.');
      });

    self.initSearch();
  }

  function configDataProfissionais(dataProfissionais) {
    var profissional = {};
    console.log(dataProfissionais);

    dataProfissionais.forEach(function (objProfissional, index) {

      profissional.id = objProfissional.Id;
      profissional.nome = objProfissional.Name;
      profissional.profissao = objProfissional.Title;
      profissional.profissaoCurto = comumConfig.cortarTexto(objProfissional.Title, 25);
      profissional.descricao = objProfissional.Description;
      profissional.valor_hora = objProfissional.Price;
      profissional.portfolio = getPortifolio(objProfissional.Portfolio);
      profissional.code = objProfissional.Code;
      profissional.tags = getStringFromSkillsArray(objProfissional.Skills);
      profissional.imagem_perfil = comumConfig.baseUrlAPI() + objProfissional.Photo;
      profissional.endereco = getDataEndereco(objProfissional);
      profissional.uf = objProfissional.State.UF;
      profissional.starRating = objProfissional.Rating;
      profissional.disponibilidade = objProfissional.Availability;
      profissional.Code = objProfissional.Code;
      //profissional.segmento        = objProfissional.Availability;
      profissional.rating = objProfissional.Rating;

      self.profissionais.push(profissional);
      profissional = {};
    });
  }

  function getPortifolio(portfolio) {
    if (portfolio === null || portfolio == "null")
      return false;

    var url = portfolio.replace('http://', '');
    url = url.replace('https://', '');

    return '//' + url;
  }

  function getStringFromSkillsArray(dataSkills) {
    var skills = [];

    dataSkills.forEach(function (objSkill, index) {
      skills.push(objSkill.Name);
    });

    return skills;
  }

  function getDataEndereco(objProfissional) {
    return {
      estado: objProfissional.State.UF == 'null' ? '' : objProfissional.State.UF,
      cidade: objProfissional.City.Name == 'null' ? '' : objProfissional.City.Name
    };
  }

  function configDataPerfilProfissional(dataPerfilProfissional) {
    var perfilProfissional = {

      profissional: {
        disponibilidade: getStringDisponibilidade(dataPerfilProfissional.Availability),
        email: dataPerfilProfissional.Email,
        segmento: getStringFromSegmentsArray(dataPerfilProfissional.Segments),
        skype: dataPerfilProfissional.Skype,
        telefone: dataPerfilProfissional.Phone,
        categoria: dataPerfilProfissional.Category.Name,
        portfolio: getPortifolio(dataPerfilProfissional.Portfolio)
      },

      redes_sociais: {
        twitter: dataPerfilProfissional.Twitter,
        facebook: dataPerfilProfissional.Facebook,
        dribbble: dataPerfilProfissional.Dribbble,
        instagram: dataPerfilProfissional.Instagram,
        linkedin: dataPerfilProfissional.Linkedin
      },

      experiencia_profissional: dataPerfilProfissional.Experiences

    };

    return perfilProfissional;
  }

  function getStringDisponibilidade(disponibilidade) {
    if (disponibilidade == 1)
      return 'Integral';
    else
      return 'Parcial';
  }

  function getStringFromSegmentsArray(segmentsArray) {
    if (segmentsArray.length)
      return segmentsArray[0].Name;

    return '';
  }

  function getRelatedSkills(relatedSkills) {
    var skills = [];

    relatedSkills.forEach(function (skill, index) {
      skills.push(skill.Name);
    });

    return skills;
  }

  function _getSkills(dataSkills) {
    var arraySkills = [];

    dataSkills.forEach(function (skill, index) {
      arraySkills.push({
        id: skill.Id,
        text: skill.Name
      });
    });

    return arraySkills;
  }

  function loadSkills(query) {
    return comumConfig.searchTextInObjectArray(skills, 'text', query);
  }

  function advancedSearch() {
    initSearch(String(comumService.getElement('#search-input')
      .value));
  }

  function cleanFilter() {
    self.filter.estado = undefined;
    self.filter.cidade = undefined;
    self.cidades = undefined;
    self.filter.segmento = undefined;
    self.filter.categoria = undefined;
    self.filter.disponibilidade = undefined;
    self.slideRange.minPrice = 0;
    self.slideRange.maxPrice = 500;

    window.isSearchClean = true;

    initSearch(true);
    $scope.$apply();

  }

  function getFilter() {

    var filter = {};
    var State = {
      Category: 'State.UF:',
      Name: self.filter.estado ? self.filter.estado.UF + ' AND ' : ""
    };
    var City = {
      Category: 'City.Name:',
      Name: self.filter.cidade ? "'" + self.filter.cidade + "'" + ' AND ' : ""
    }
    var Categoria = {
      Category: 'Category.Name:',
      Name: self.filter.categoria ? "'" + self.filter.categoria.Name + "'" + ' AND ' : ""
    }
    var Segments = {
      Category: 'Segments:',
      Name: self.filter.segmento ? "'" + self.filter.segmento.Name + "'" + ' AND ' : ""
    }
    var Availability = {
      Category: "Availability:",
      Name: self.filter.disponibilidade ? "'" + self.filter.disponibilidade.Id + "'" + ' AND ' : "'" + 2 + "'" + ' AND '
    }
    var Price = {
      Category: "Price: ",
      Name: self.slideRange.minPrice + " TO " + self.slideRange.maxPrice + ""
    }

    //currentPage = isPagination ? currentPage : 1;

    // if (currentPage != 1) {
    //     filter.page = currentPage;
    //     filter.pageSize = itensPerPage;
    // }


    // if (self.slideRange.maxPrice != maxPrice)
    //     filter['filter.maxPrice'] = self.slideRange.maxPrice;

    // if (self.skills.length > 0)
    //     filter['filter.terms'] = comumConfig.getPropertyValueFromObjectArray(self.skills, 'text');

    // if (categoriasSelecionadasIds.length > 0)
    //     filter['filter.segmentsType'] = categoriasSelecionadasIds;

    if (self.filter.categoria !== undefined)
      filter['Categoria'] = Categoria;

    if (self.filter.estado !== undefined)
      filter['State'] = State;

    if (self.filter.cidade !== undefined)
      filter['City'] = City;

    if (self.filter.segmento !== undefined)
      filter['Segments'] = Segments;

    if (self.filter.disponibilidade !== undefined)
      filter['Availability'] = Availability;

    if (self.slideRange.minPrice !== undefined)
      filter['Price'] = Price;

    return filter;
  }

  function configSlideRange() {
    self.slideRange = {
      minPrice: minPrice,
      maxPrice: maxPrice,
      options: {
        floor: minPrice,
        ceil: maxPrice,
        step: 1,
        translate: function (value) {
          return 'R$ ' + value;
        },
        hideLimitLabels: true,
        onEnd: function () {
          initSearch();
        }
      },
    };
  }

  function addScrollable() {
    comumConfig.asScrollable('#profissionais-selecionados')
      .add();
    setTimeout(function () {
      comumConfig.asScrollable('#profissionais-selecionados')
        .update();
    }, 2000);
  }

  function openDialogFreelancers5Stars() {
    ngDialog.open({
      template: './../templates/profissional/view/freelancers-5stars.modal.html',
      width: 800
    });
  }


  // Call's
  init();
  // initSearch(true);

}
