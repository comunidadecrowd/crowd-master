function sharedScreenProfissionalComponent()
{
	return {
		templateUrl: '../templates/profissional/view/profissional.html',
		controller: sharedScreenProfissionalController,		
		bindings: {
			getProfissionais: '<',
			getCategorias: '<',			
			getSkills: '<'			
		}
	};
}