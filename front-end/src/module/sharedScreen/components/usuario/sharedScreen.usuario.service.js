sharedScreenUsuarioService.$inject = ["$http", "comumConfig", "comumService"];
function sharedScreenUsuarioService($http, comumConfig, comumService) {
  var service = this;
  // Public
  service.getUsuarios = getUsuarios;
  service.salvarUsuario = salvarUsuario;
  service.statusUpdate = statusUpdate;
  service.showActions = showActions;

  // Private
  var baseUrl = comumConfig.baseUrl();
  var baseUrlAPI = comumConfig.baseUrlAPI();
  var usuario = comumService.getUser();

  function getUsuarios() {
    return $http.get(
      baseUrlAPI + "/Customer/GetCustomerUser?customerId=" + usuario.IdCustomer
    );
  }

  function salvarUsuario(dataUser) {
    var user = comumService.getUser();
    dataUser.IdCustomer = user.IdCustomer;
    dataUser.Role = 1;

    return $http.post(baseUrlAPI + "/Customer/AddUser", dataUser);
  }

  function statusUpdate(dataUser) {
    return $http.post(
      baseUrlAPI +
        "/Customer/ChangeStatusUser?userId=" +
        dataUser.userId +
        "&active=" +
        dataUser.active
    );
  }

  function showActions(role) {
    if (role !== 1 && role !== 4) return true;

    return false;
  }
}
