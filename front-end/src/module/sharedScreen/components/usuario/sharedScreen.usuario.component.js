function sharedScreenUsuarioComponent()
{
	return {
		templateUrl: '../templates/usuario/view/usuario.html',
		controller: sharedScreenUsuarioController,		
		bindings: {
			getUsuarios: '<'
		}
	};
}