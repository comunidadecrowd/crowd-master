sharedScreenUsuarioController.$inject = [
  "comumConfig",
  "scUsuarioService",
  "NgTableParams",
  "$scope",
  "toaster",
  "ngDialog",
  "comumService",
  "$state",
];

function sharedScreenUsuarioController(
  comumConfig,
  scUsuarioService,
  NgTableParams,
  $scope,
  toaster,
  ngDialog,
  comumService,
  $state
) {
  var self = this;

  // Private
  //...

  // Public
  self.tableParams = null;
  self.totalUsers = 0;
  self.error = false;
  self.errorMessage = null;
  self.isEditing = false;
  self.title = null;

  self.usuario = {};

  self.salvar = salvar;
  self.editar = editar;
  self.statusUpdate = statusUpdate;
  self.showActions = scUsuarioService.showActions;

  $scope.gotToCompanyProfile = gotToCompanyProfile;

  // Functions
  function init() {
    var usuarios = comumConfig.addFullPathUserPhoto(
      self.getUsuarios.data,
      "Photo"
    );
    self.totalUsers = self.getUsuarios.data.length;
    self.tableParams = new NgTableParams({
      count: 10
    }, {
      counts: [],
      dataset: usuarios
    });

    setTitle();
    window.$jQuery(window)
      .unbind("scroll");
  }

  function salvar() {
    /* if (!comumService.hasPlan(comumService.plans[2]) &&
      !comumService.hasPlan(comumService.plans[3])
    ) {
      _blockAddUserModal();
      return false;
    } */// NOTE: NINGUÉM SE IMPORTA MAIS COM SEU PLANO

    if (!userValidate()) return false;

    comumConfig.blockScreen();

    scUsuarioService
      .salvarUsuario(self.usuario)
      .success(function (data) {
        if (data.model) {
          if (!self.isEditing) {
            self.tableParams.settings()
              .dataset.unshift(data.model);
            self.tableParams.reload();
          }

          self.totalUsers++;
          self.error = false;
          self.errorMessage = "";
          self.isEditing = false;
          self.usuario = {};

          setTitle();
        }

        if (data == "Your plan allow only 5 users.") {
          toaster.pop("error", "", "Você ultrapassou o limite de usuários!");
          comumConfig.unblockScreen();
        } else {
          toaster.pop("success", "", "Usuário salvo!");
          comumConfig.unblockScreen();
        }
      })
      .error(function (data, status) {
        self.error = true;
        self.errorMessage = data;
        comumConfig.unblockScreen();
      });
  }

  function editar(user) {
    self.isEditing = true;
    self.usuario = user;

    setTitle();
  }

  function userValidate() {
    if (!self.usuario.Name) {
      self.errorMessage = "Preencha o campo nome!";
      self.error = true;
      return false;
    }

    if (!self.usuario.Email) {
      self.errorMessage = "Preencha o campo email!";
      self.error = true;
      return false;
    }

    if (!self.usuario.RoleCompany) {
      self.errorMessage = "Preencha o campo cargo!";
      self.error = true;
      return false;
    }

    if (!comumConfig.isEmail(self.usuario.Email)) {
      self.errorMessage = "Preencha um email válido!";
      self.error = true;
      return false;
    }

    if (!self.usuario.Password && !self.isEditing) {
      self.errorMessage = "Preencha o campo senha!";
      self.error = true;
      return false;
    }

    return true;
  }

  function statusUpdate(user) {
    var row = "#row-" + user.Id;
    var dataUser = {
      userId: user.Id,
      active: user.Active,
    };

    comumConfig.blockScreen("", row);

    scUsuarioService
      .statusUpdate(dataUser)
      .success(function (data) {
        comumConfig.unblockScreen(row);
      })
      .error(function (data) {
        comumConfig.unblockScreen(row);
      });
  }

  function setTitle() {
    if (self.isEditing) self.title = "Editar usuário";
    else self.title = "Adicionar novo usuário";
  }

  function addFullPathUserPhoto(array) {
    if (array.length > 0) {
      array.forEach(function (objUser, index) {
        if (objUser.Photo)
          objUser.Photo = comumConfig.baseUrlAPI() + objUser.Photo;
      });
    }

    return array;
  }

  function _blockAddUserModal() {
    ngDialog.open({
      template: "../templates/usuario/view/block-add-user.modal.html",
      width: 600,
      scope: $scope,
    });
  }

  function gotToCompanyProfile() {
    $state.go("company-profile");
    ngDialog.close();
  }

  // Call's
  init();
}
