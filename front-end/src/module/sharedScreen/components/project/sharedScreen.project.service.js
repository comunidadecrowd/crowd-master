sharedScreenProjectService.$inject = [ '$http', 'comumConfig' ];
function sharedScreenProjectService( $http, comumConfig )
{


	var service = this;
	// Public
	service.add  = add;	
	service.edit = edit;
	service.get  = get;
	service.list = list;

	// Private	
	var baseUrl    = comumConfig.baseUrl();
	var baseUrlAPI = comumConfig.baseUrlAPI();

	// Functions
	function add( projectData )
	{
		//...
	}	

	function edit( projectId )
	{
		//...
	}

	function get( projectId )
	{
		//...
	}

	function list()
	{
		//...
	}

}
