sharedScreenGestaoBriefingController.$inject = [
  "comumConfig",
  "scBriefingService",
  "comumService",
  "$timeout",
  "$state",
  "$scope",
  "$rootScope",
  "scProfissionalService",
  "$filter",
  "toaster",
  "$sessionStorage",
  "scFirstAccessService"
];

function sharedScreenGestaoBriefingController(
  comumConfig,
  scBriefingService,
  comumService,
  $timeout,
  $state,
  $scope,
  $rootScope,
  scProfissionalService,
  $filter,
  toaster,
  $sessionStorage,
  scFirstAccessService
) {
  var self = this;

  // Private
  //...

  // Public

  self.projetos = comumConfig.projetos;

  self.selectedBriefing = null;
  self.buscaBriefing = null;
  self.isOpen = false;
  self.isOpenProfile = false;
  self.mensagem = "";
  self.error = false;
  self.visualizaPerfil = visualizaPerfil;

  self.pageslideOnOpen = pageslideOnOpen;
  self.pageslideOnClose = pageslideOnClose;

  self.briefingsAtivos = [];
  self.briefingsArquivados = [];
  self.briefingsRascunhos = [];
  self.offers = [];
  self.allOffers = [];

  self.usuario = {};
  self.briefingDetalhes = {};

  self.showBriefingsAtivos = showBriefingsAtivos;
  self.showBriefingsArquivados = showBriefingsArquivados;
  self.getBriefingOffers = getBriefingOffers;
  self.showBriefingsRascunho = showBriefingsRascunho;
  self.changeStatus = changeStatus;
  self.slideOpen = slideOpen;
  self.slideOpenByContracted = slideOpenByContracted;
  self.editarBriefing = editarBriefing;
  self.editarHunting = editarHunting;
  self.getProfile = getProfile;
  self.slideOpenProfile = slideOpenProfile;
  self.addHiredProfessional = addHiredProfessional;
  self.buscaArquivados = buscaArquivados;
  self.goToPageSaveBriefing = goToPageSaveBriefing;
  self.continuarBriefing = continuarBriefing;
  self.hasBriefing = $sessionStorage.briefing;

  // Functions
  function init() {
    if (self.getBriefings.status == 412) {
      console.log(self.selectedBriefing);
    }

    self.usuario = comumService.getUser();
    configDataBriefing(self.getBriefings.data);
    console.debug('rascunhos', self.getBriefings.data);
    getFirstBriefing(self.briefingsAtivos);

    console.log(self.selectedBriefing);
  }

  function getFirstBriefing(dataBriefings) {
    if (!dataBriefings || dataBriefings.length === 0) return false;

    var firstBriefing = dataBriefings[0];
    var briefingId = firstBriefing.id;

    $timeout(
      function () {
        var element = {
          currentTarget: comumConfig.findElement(
            ".list-group-item",
            "#list-briefings-ativos"
          )[0]
        };
        getBriefingOffers(element, briefingId, 1);
      },
      1000,
      false
    );
  }

  function continuarBriefing() {
    var params = {};
    params.id =

      $sessionStorage.briefing.Id !== undefined ? $sessionStorage.briefing
      .Id :
      $sessionStorage.briefing.BriefingId;
    $state.go("salvar-briefing", params);
  }

  function showBriefingsAtivos() {
    comumConfig.toggleClass("#briefings-ativos-title", "aberto");
    showBriefinfList("#list-briefings-ativos");
  }

  function showBriefingsArquivados() {
    comumConfig.toggleClass("#briefings-arquivados-title", "aberto");
    showBriefinfList("#list-briefings-arquivados");
  }

  function showBriefinfList(element) {
    comumConfig.slideToggle(element, "slow");
  }

  function showBriefingsRascunho() {
    comumConfig.toggleClass("#briefings-rascunhos-title", "aberto");
    showBriefinfList("#list-briefings-rascunhos");
  }


  function getBriefingOffers(event, briefingId, status) {
    
    console.log("event",event);
    console.log("briefingId",briefingId);
    console.log("status",status);
    
    blockScreen();
    addClassBriefingAtivo(event.currentTarget);
    self.offers = [];

    var briefings = [];
    
    if (status === 1) {
      briefings = self.briefingsAtivos;
    }
    if (status === 2) {
      briefings = self.briefingsArquivados;
    }
    if (status === 3) {
      briefings = self.briefingsRascunhos;
    } 
    
    console.debug(status, briefings);

    comumConfig.displayBlock(".page-content");
    comumConfig.displayNone("#briefing-arquivado");

    scBriefingService.getBriefingOffers(briefingId)
      .success(function (data) {
        console.debug('broefomgs', briefings);
        self.selectedBriefing = comumConfig.searchObjectInArray(
          briefings,
          "id",
          briefingId
        );
        console.debug('broefomgs2', self.selectedBriefing);
        var messagesGroup = data.messagesGroup;

        if (messagesGroup.length !== 0) {
          self.allOffers = sortMessages(messagesGroup);
          self.offers = arrangeProfessionalMessages(messagesGroup);

        if (self.selectedBriefing.Contracted)
            self.addHiredProfessional(self.selectedBriefing.SelectedPropose);
        } else {
          self.offers = self.selectedBriefing;
        }

        if (self.briefingsRascunhos == briefings) {
          self.offers = briefings;
        }

        unblockScreen();
      });
  }

  /* function getBriefingOffers(event, briefingId, status)  {
   // blockScreen();
    addClassBriefingAtivo(event.currentTarget);
    self.offers = [];
    self.selectedBriefing = [];


      var briefings = '';

      if (status === 1) {
        briefings = self.briefingsAtivos;
      }
      if (status === 2) {
        briefings = self.briefingsArquivados;
      }
      if (status === 3) {
        briefings = self.briefingsRascunhos;
      } 

      console.debug(status, briefings);

    comumConfig.displayBlock(".page-content");
    comumConfig.displayNone("#briefing-arquivado");

    scBriefingService.getBriefingOffers(briefingId)
    .success(function (data) {
      console.debug('broefomgs', briefings);
      self.selectedBriefing = comumConfig.searchObjectInArray(
        briefings,
        "id",
        briefingId
      );
      console.debug('broefomgs2', self.selectedBriefing);
      var messagesGroup = data.messagesGroup;

      if (messagesGroup.length !== 0) {
        self.allOffers = sortMessages(messagesGroup);
        //self.allOffers = messagesGroup;
        self.offers = arrangeProfessionalMessages(messagesGroup);
        //console.log("self.offers", self.offers);

        if (self.selectedBriefing.Contracted)
          self.addHiredProfessional(self.selectedBriefing.SelectedPropose);
      } else {
        self.offers = self.selectedBriefing;
      }
        console.debug('óffer', self.offers);
        console.debug('óffer', self.selectedBriefing);

       if (self.briefingsRascunhos == briefings) {
         self.offers = briefings;
       }

        unblockScreen();
      });
  } */

  function messageIsEmpty() {
    return getMessage() == "<p><br></p>" ? true : false;
  }


  function visualizaPerfil(profile, freelaId, parent, param2) {
    // self.perfil.profissional.imagemPerfil = comumConfig.attr('#imagem-perfil-' + idProfissional, 'src');
    param2.stopPropagation();
    param2.preventDefault();
    comumService
      .getProfile(freelaId)
      .success(function (response) {
        console.log(response);
        // comumConfig.fadeIn('.mask-content-pageslide');
        $rootScope.$broadcast("event:visualizar-perfil", response);

        parent.$ctrl.isOpenProfile = true;
        parent.$ctrl.hideSelectButton = true;
        self.isOpen = false;

        $rootScope.$broadcast("event:visualizar-perfil", response);
      })
      .error(function callback(data) {
        window.Rollbar.critical("visualizaPerfil", data);
        console.log(data);
      });
  }


  function addClassBriefingAtivo(element) {
    var elements = comumConfig.findElement(".ativo", "#listagem-briefing");

    angular.forEach(elements, function (elem, index) {
      comumConfig.removeClass(elem, "ativo");
    });

    comumConfig.addClass(element, "ativo");
  }

  function changeStatus(briefing) {
    console.log("OLAR3");
    blockScreen();

    var status = !briefing.Active;
    var briefingId = briefing.id;

    scBriefingService
      .changeStatus(briefingId, status)
      .success(function (data) {
        if (!status) {
          var briefing = comumConfig.searchObjectInArray(
            self.briefingsAtivos,
            "id",
            briefingId
          );
          self.briefingsAtivos = comumConfig.removeObjectFromArray(
            self.briefingsAtivos,
            "id",
            briefingId
          );
          briefing.Active = status;
          self.briefingsArquivados.unshift(briefing);
          toaster.pop("success", "", "Briefing arquivado com sucesso!");
        } else {
          var briefing = comumConfig.searchObjectInArray(
            self.briefingsArquivados,
            "id",
            briefingId
          );
          self.briefingsArquivados = comumConfig.removeObjectFromArray(
            self.briefingsArquivados,
            "id",
            briefingId
          );
          briefing.Active = status;
          self.briefingsAtivos.unshift(briefing);
          toaster.pop("success", "", "Briefing ativado com sucesso!");
        }

        //comumConfig.displayNone('.page-content');
        //comumConfig.displayBlock('#briefing-arquivado');
        unblockScreen();

        $timeout(
          function () {
            getFirstBriefing(self.briefingsAtivos);
          },
          2000,
          false
        );
      })
      .error(function (data) {
        //...
      });
  }

  function unblockScreen() {
    comumConfig.unblockScreen(".page-content");
  }

  function blockScreen() {
    comumConfig.blockScreen("", ".page-content");
  }

  function slideOpenByContracted(briefingId, professionalId) {
    var offerId = getOfferIdByProfessional(professionalId);
    self.slideOpen(briefingId, professionalId, offerId);
  }

  function slideOpen(briefingId, professionalId, offerId, event) {
    event.preventDefault();
    setTimeout(function () {
      window.$jQuery(".msg-preco:not(:last)")
        .hide();
      window.$jQuery(".bt-contratar:not(:last)")
        .hide();
      console.log("jdjf");
    }, 1000);

    function initQuillEditor() {
      var options = {
        toolbar: [
          "bold",
          "italic",
          "underline"
        ],
        placeholder: "Escreva uma mensagem..."
      };
      comumConfig.quillEditor("#editor")
        .add(options);
    }

    function getMessage() {
      return comumConfig.quillEditor("#editor")
        .getContent();
    }

    function clearMessage() {
      comumConfig.quillEditor("#editor")
        .setContent("");
      self.error = false;
      self.message.Price = null;
      self.message.DeadlineDays = null;
      self.percentageCrowd = null;
    }

    function validateMessage() {
      if (!self.isProfessional) {
        if (messageIsEmpty()) {
          self.error = true;
          self.errorMessage = "Preencha a mensagem.";
          return false;
        }
      } else {
        if (
          messageIsEmpty() &&
          (!self.message.Price || !self.message.DeadlineDays)
        ) {
          self.error = true;
          self.errorMessage =
            "Você deve enviar um Valor e Prazo para esse Briefing, ou uma Mensagem.";
          return false;
        }
      }

      return true;
    }

    /**
     * Envia mensagem pelo Webservice
     * @description Envia a mensagem e retorna
     * @param   {boolean} task Variação se é Task ou Briefing
     * @returns {boolean}  Success ou Error
     */
    function sendMessage(task, message) {
      var percent = 8;
      var factor = 1 + percent / 100;

      var professional = $rootScope.getUser();
      console.log("professional", professional);

      if (!validateMessage()) return false;

      task = typeof task !== "undefined" ? task : false;

      var userId = professional.id;
      var promisse = {};

      self.message.briefingId = self.briefing.Id;
      self.message.professionalId = self.briefing.professionalId;
      self.message.uesrId = userId;
      self.message.message =
        message ? message :
        getMessage();
      self.message.Price = self.message.Price; //? self.message.Price *= factor : null;

      comumConfig.blockScreen("", ".campo-enviar-mensagem");

      self.message.briefingId = self.briefing.Id;
      self.message.professionalId = self.briefing.professionalId;
      self.message.uesrId = userId;
      self.message.message =
        message ? message :
        getMessage();
      self.message.Price = self.message.Price; //? self.message.Price *= factor : null;

      //console.log(self.message);
      if (self.isProfessional)
        promisse = comumService.sendMessageToCustomer(self.message, task);
      else
        promisse = scBriefingService.sendMessageToProfessional(
          self.message,
          task
        );

      function commonDataMessage() {
        return {
          BriefingId: self.briefing.Id,
          CreatedAt: null,
          DeadlineDays: null,
          Freelancer: null,
          FreelancerId: self.professional.Id,
          // FreelancerCode: self.professional.Code, APARENTEMENTE ISSO NÃO DEVERIA ESTAR AQUI
          Id: null,
          Price: null,
          Read: false,
          Text: null,
          User: null,
          UserId: null
        };
      }
    }

    function configShowMesage() {
      if (self.briefing.Bulletin) {
        self.showMessage = false;
        return false;
      } else if (self.briefing.Contracted) {
        if (self.briefing.SelectedPropose) {
          if (
            self.isProfessional &&
            self.briefing.SelectedPropose.Freelancer.Id !== self.user.id
          ) {
            self.showMessage = false;
            return false;
          }
        }
      }

      // function configShowMesage() {

      self.showMessage = true;

      initQuillEditor();
    }

    function getDataProfessional(freelancer, professionalId) {
      return {
        Photo: freelancer.Photo,
        Name: freelancer.Nome,
        Id: freelancer.Id
      };
    }

    function getCustomerMessages(messagesArray) {
      if (messagesArray.length !== 0) {
        return messagesArray.filter(function (messagesObg) {
          if (self.user.Role != 2)
            return !messagesObg.UserId && !messagesObg.User;
          if (self.user.Role == 2)
            return messagesObg.UserId && messagesObg.User;
        });
      }

      return [];
    }

    $timeout(
      function () {
        initQuillEditor();
      },
      1000,
      false
    );

    if (briefingId) {
      //TODO: MELHORAR O LIVEUPDATE
      //window.setInterval(function liveUpdate() {

      scBriefingService
        .getDetailsBriefing(briefingId, professionalId)
        .success(function (data) {
          self.isOpen = !self.isOpen;

          var briefing = data;
          console.log(self.selectedBriefing);
          var briefingMessages = getMessagesGroupByProfessionalId(
            professionalId
          );

          console.debug(briefing);
          briefing.offers = briefingMessages;
          briefing.addHiredProfessional = self.addHiredProfessional;
          briefing.isOpen = self.isOpen;
          briefing.selectedBriefing = self.selectedBriefing;
          briefing.professionalId = professionalId;

          console.log(self.selectedBriefing);

          $scope.$broadcast("event:briefing-details", briefing);
          //console.info(briefing);
          self.isOpen = true;
          comumConfig.scrollPageslideBriefing($timeout);

          var professionalMessages = getProfessionalMessges(
            briefingMessages.Messages
          );

          if (professionalMessages.length !== 0) {
            var messagesId = professionalMessages.filter(function (messagesObj) {
              return messagesObj.Read === false;
            });

            messagesId = comumConfig.getPropertyValueFromObjectArray(
              messagesId,
              "Id"
            );

            var briefingsAll = Object.assign(
              self.briefingsAtivos,
              self.briefingArquivados
            );
            var foundBriefing = comumConfig.searchObjectInArray(
              briefingsAll,
              "id",
              briefingId
            );
            var totalMessages = foundBriefing.interacoes - messagesId.length;

            foundBriefing.interacoes = totalMessages;

            if (messagesId.length !== 0)
              messagesId.forEach(function (messageId) {
                comumService.readMessage(messageId)
                  .success(function (data) {
                    comumConfig.removeElement("#message-alert-" + offerId);
                    comumService.getProjetos(self.usario.userId);
                    if (self.usario.Role !== 2) {
                      comumService.getNotification(self.usario.userId);
                    } else {
                      comumService.getNotificationProfessional(
                        self.usario.userId
                      );
                    }
                  });
              });
          }
        });
      //}, 2000); LIVE UPDATE
      /*.error(function(){
       //...
   });*/
    }
  }

  function configDataBriefing(dataBriefings) {
    var briefingRascunhos = {};
    var briefingAtivos = {};
    var briefingArquivados = {};

    // Briefings ativos
    dataBriefings.ativos.forEach(function (objBriefing, index) {
      briefingAtivos.id = objBriefing.Id;
      briefingAtivos.offerId = objBriefing.Id;
      briefingAtivos.briefing = objBriefing.Title;
      briefingAtivos.interacoes = objBriefing.MessagesCount;
      briefingAtivos.Contracted = objBriefing.Contracted;
      briefingAtivos.SelectedPropose = objBriefing.SelectedPropose;
      briefingAtivos.Active = objBriefing.Active;
      briefingAtivos.Hunting = objBriefing.Hunting;
      briefingAtivos.Sended = objBriefing.Sended;
      briefingAtivos.Freelancers = objBriefing.Freelancers;
      briefingAtivos.Project = objBriefing.Project;
      
      self.briefingsAtivos.push(briefingAtivos);
      briefingAtivos = {};
    });

    // Briefings arquivados
    dataBriefings.arquivados.forEach(function (objBriefing, index) {
      briefingArquivados.id = objBriefing.Id;
      briefingArquivados.offerId = objBriefing.Id;
      briefingArquivados.briefing = objBriefing.Title;
      briefingArquivados.interacoes = objBriefing.MessagesCount;
      briefingArquivados.Contracted = objBriefing.Contracted;
      briefingArquivados.SelectedPropose = objBriefing.SelectedPropose;
      briefingArquivados.Active = objBriefing.Active;
      briefingArquivados.Hunting = objBriefing.Hunting;
      briefingArquivados.Sended = objBriefing.Sended;
      briefingArquivados.Freelancers = objBriefing.Freelancers;
      briefingArquivados.Project = objBriefing.Project;

      self.briefingsArquivados.push(briefingArquivados);
      briefingArquivados = {};
    });

    // Briefings rascunho ou hunting
    dataBriefings.rascunhos.forEach(function (objBriefing, index) {
      briefingRascunhos.id = objBriefing.Id;
      briefingRascunhos.offerId = objBriefing.Id;
      briefingRascunhos.briefing = objBriefing.Title;
      briefingRascunhos.interacoes = objBriefing.MessagesCount;
      briefingRascunhos.Contracted = objBriefing.Contracted;
      briefingRascunhos.SelectedPropose = objBriefing.SelectedPropose;
      briefingRascunhos.Hunting = objBriefing.Hunting;
      briefingRascunhos.Sended = objBriefing.Sended;
      briefingRascunhos.Freelancers = objBriefing.Freelancers;
      briefingRascunhos.Active = objBriefing.Active;
      briefingRascunhos.Project = objBriefing.Project;

      self.briefingsRascunhos.push(briefingRascunhos);
      briefingRascunhos = {};
    });

  }

  function editarBriefing() {
    sessionStorage.clear();
    setTimeout(function () {
      $state.go("salvar-briefing", {
        id: self.selectedBriefing.id
      });
    }, 300);
  }
  function editarHunting() {
    sessionStorage.clear();
    setTimeout(function () {
      $state.go("salvar-briefing", {
        id: self.selectedBriefing.id,
        hunting: true
      });
    }, 300);
  }

  function getProfessionalMessges(messagesArray) {
    if (messagesArray.length !== 0) {
      return messagesArray.filter(function (messagesObj) {
        return !messagesObj.UserId && !messagesObj.User;
      });
    }

    return [];
  }

  function arrangeProfessionalMessages(messagesGroupArray) {
    messagesGroupArray.forEach(function (messagesObj, index) {
      var messagesProfessional = getProfessionalMessges(messagesObj.Messages);
      var lastProfessionalMessage = comumConfig.getLastElementFromArray(
        messagesProfessional
      );

      if (lastProfessionalMessage !== undefined) {
        messagesObj.Freelancer.ShortText =
          lastProfessionalMessage.Text ? comumConfig.cortarTexto(
            lastProfessionalMessage.Text,
            125
          ) :
          "";
        messagesObj.Freelancer.CreatedAt = lastProfessionalMessage.CreatedAt;
        messagesObj.Freelancer.hasOffer =

          lastProfessionalMessage.Price &&
          lastProfessionalMessage.DeadlineDays ? true :
          false;
        messagesObj.Freelancer.Price = lastProfessionalMessage.Price;
        messagesObj.Freelancer.DeadlineDays =
          lastProfessionalMessage.DeadlineDays;
        messagesObj.Freelancer.DeliveryAt = lastProfessionalMessage.DeliveryAt;
      } else {
        messagesObj.Freelancer.ShortText = null;
        messagesObj.Freelancer.CreatedAt = null;
        messagesObj.Freelancer.hasOffer = false;
        messagesObj.Freelancer.Price = null;
        messagesObj.Freelancer.DeadlineDays = null;
        messagesObj.Freelancer.DeliveryAt = null;
      }

      messagesObj.offerId = messagesObj.Messages[0].BriefingId + "-" + index;
    });

    return messagesGroupArray;
  }

  function sortMessages(messagesGroup) {
    if (messagesGroup.length !== 0) {
      messagesGroup.forEach(function (messagesObj, index) {
        messagesObj.Messages.reverse();
      });

      return messagesGroup;
    }

    return [];
  }

  function getMessagesGroupByProfessionalId(professionalId) {
    var messageGroup = [];

    if (self.allOffers.length !== 0) {
      self.allOffers.forEach(function (messagesObj, index) {
        if (messagesObj.Freelancer.Id === professionalId)
          messageGroup = messagesObj;
      });
    }

    return messageGroup;
  }

  function getProfile(professionalId) {
    self.isOpenProfile = true;
    scProfissionalService.getPerfil(professionalId)
      .success(function (data) {
        data.hideSelectButton = true;
        $scope.$broadcast("event:visualizar-perfil", data);
      });
  }

  function slideOpenProfile() {
    //  comumConfig.fadeOut('.mask-content-pageslide');
    self.isOpenProfile = false;
   // self.isOpen = true;
  }

  function addHiredProfessional(selectedPropose) {
    comumConfig.addCss(
      ".img-professional-hired",
      "background",
      'url("' +
      comumConfig.baseUrlAPI() +
      selectedPropose.Freelancer.Photo +
      '")'
    );
    comumConfig.addCss(
      ".img-professional-hired",
      "background-size",
      "100% 100%"
    );
    comumConfig.addText(
      ".name-professional-hired",
      selectedPropose.Freelancer.Name
    );
    comumConfig.addText(
      ".professional-hired-price",
      $filter("currency")(selectedPropose.Price)
    );
    comumConfig.addText(
      ".professional-hired-deadlinedays",
      window
      .moment(selectedPropose.DeliveryAt, "DD/MM/YYYY HH:mm")
      .format("DD/MM/YYYY")
    );
  }

  function buscaArquivados() {
    var resultSearch = comumConfig.searchTextInObjectArray(
      self.briefingsArquivados,
      "briefing",
      self.buscaBriefing
    );
    var element = "#list-briefings-arquivados";

    if (resultSearch.length > 0) comumConfig.displayBlock(element);
    else comumConfig.displayNone(element);
  }

  function pageslideOnOpen() {
    comumConfig.fadeIn(".mask-content-pageslide");
  }

  function pageslideOnClose() {
    comumConfig.fadeOut(".mask-content-pageslide");
  }

  function goToPageSaveBriefing() {
    delete $sessionStorage.briefing;
    delete $sessionStorage.fromBriefing;
    $state.go("salvar-briefing");
  }

  function getOfferIdByProfessional(professionalId) {
    var offer = self.offers.filter(function (offerObj) {
      return offerObj.Freelancer.Id === professionalId;
    });

    if (offer.length > 0) return offer[0].offerId;

    return null;
  }

  // Call's
  init();
}
