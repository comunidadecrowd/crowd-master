sharedScreenBriefingService.$inject = ["$http", "comumConfig", "comumService"];

function sharedScreenBriefingService($http, comumConfig, comumService) {
 
 var service = this;
 // Public
 service.getBriefing = getBriefing;
 service.getBriefings = getBriefings;
 service.getBriefingOffers = getBriefingOffers;
 service.changeStatus = changeStatus;
 service.getDetailsBriefing = getDetailsBriefing;
 service.sendMessageToProfessional = sendMessageToProfessional;
 service.salvarBriefing = salvarBriefing;
 service.briefingUpdate = briefingUpdate;
 service.hireProfessional = hireProfessional;
 service.deleteAttachment = deleteAttachment;

 // Private
 var baseUrlAPI = comumConfig.baseUrlAPI();
 var user = comumService.getUser();

 function getBriefing(idBriefing) {
  var config = {
   params: {
    userId: user.id,
    briefingId: idBriefing,
   },
  };

  return $http.get(baseUrlAPI + "/Briefing/Get", config);
 }

 function getBriefings() {
  return $http.post(baseUrlAPI + "/Briefing/List?user_id=" + user.id);
 }

 function getBriefingOffers(idBriefing) {
  var config = {
   params: {
    user_id: user.id,
    briefing_id: idBriefing,
   },
  };

  return $http.get(baseUrlAPI + "/Briefing/GetMessages", config);
 }

 function getDetailsBriefing(idBriefing, professionalId) {
  var config = {
   params: {
    userId: user.id,
				briefingId: idBriefing,
				freelancerId: professionalId
   },
  };

  return $http.get(baseUrlAPI + "/Briefing/Get", config);
 }

 function changeStatus(briefingId, status) {
  return $http.post(
   baseUrlAPI +
    "/Briefing/ChangeStatus?briefingId=" +
    briefingId +
    "&status=" +
    status
  );
 }

 function sendMessageToProfessional(messageData, task) {
  if (task)
   var data = {
    UserId: user.id,
    FreelancerId: messageData.professionalId,
    TaskId: messageData.briefingId,
    Text: messageData.message,
   };
  else
   var data = {
    UserId: user.id,
    FreelancerId: messageData.professionalId,
    BriefingId: messageData.briefingId,
    Text: messageData.message,
   };

  //console.log(data);
  if (task) return $http.post(baseUrlAPI + "/Task/AddMessage", data);
  else return $http.post(baseUrlAPI + "/Briefing/AddMessage", data);
 }

 function salvarBriefing(briefingData) {
    
   if (briefingData.Id) { // NOTE: NÃO TEM MODEL NO ENDPOINT É DIRETO NO BANCO
     var data = {
       Id: briefingData.Id,
       IdUser: briefingData.IdUser,
       Title: briefingData.Title,
       Excerpt: briefingData.Excerpt,
       Text: briefingData.Text,
       Freelancers: briefingData.Freelancers,
       Attachs: briefingData.Attachs,
       Hunting: briefingData.Hunting,
       Sended: briefingData.Sent,
       IdProject: briefingData.Project.Id,
     };
   } else {
     var data = {
       IdUser: briefingData.IdUser,
       Title: briefingData.Title,
       Excerpt: briefingData.Excerpt,
       Text: briefingData.Text,
       Freelancers: briefingData.Freelancers,
       Attachs: briefingData.Attachs,
       Hunting: briefingData.Hunting,
       Sended: briefingData.Sent,
       IdProject: briefingData.Project.Id,
     };
   }
   

  return $http.post(baseUrlAPI + "/Briefing/Add", data);
 }

 function briefingUpdate(complementData) {
  return $http.post(baseUrlAPI + "/Briefing/Update", complementData);
 }

 function configIdProfissionais(arrayId) {
  var profissionais = [];

  arrayId.forEach(function(idProfissional, index) {
   profissionais.push({
    Id: idProfissional,
   });
  });

  return profissionais;
 }

 function hireProfessional(contractData) {
  contractData.UserId = user.id;

  return $http.post(baseUrlAPI + "/Briefing/ContractFreelancer", contractData);
 }

 function deleteAttachment(filepath) {
  return $http.post(baseUrlAPI + "/Briefing/RemoveAttach?filepath=" + filepath);
 }
}
