sharedScreenSalvarBriefingController.$inject = [
  "comumConfig",
  "$rootScope",
  "scBindBriefingProfissionalService",
  "$state",
  "$stateParams",
  "$timeout",
  "comumService",
  "scBriefingService",
  "$sessionStorage",
  "toaster",
  "$localStorage",
  "$scope",
  "$window"
];

function sharedScreenSalvarBriefingController(
  comumConfig,
  $rootScope,
  scBindBriefingProfissionalService,
  $state,
  $stateParams,
  $timeout,
  comumService,
  scBriefingService,
  $sessionStorage,
  toaster,
  $localStorage,
  $scope,
  $window
) {
  var self = this;

  // Private
  var editor = "#editor";
  var briefingText = null;
  const professionalsLimit = 10;
  var hunting = Boolean(false);

  // Public

  self.projetos =
    $rootScope.getProjetos ? true :
    false;
  self.getProjetos = $rootScope.getProjetos;
  self.criaProjeto = $rootScope.criaProjeto;
  self.deleteAttachmentEdit = deleteAttachmentEdit;

  console.log("self.getBriefing", self.getBriefing);
  console.log("$sessionStorage.briefing", $sessionStorage.briefing);
  console.log("$sessionStorage.briefing?", $sessionStorage.briefing ? true : false);
  !$sessionStorage.fromBriefing && $stateParams.id ? $sessionStorage.briefing = self.briefingComplement = self.getBriefing = self.getBriefing.data : null;
  $sessionStorage.fromBriefing || $stateParams.hunting ? self.briefingComplement = self.getBriefing = self.getBriefing.data = $sessionStorage.briefing : null;

  self.idBriefing = null;
  self.errorMessage = null;
  self.anexo = 0;
  self.showIcon = false;
  self.newBriefing = true;
  self.cancelHunting = cancelHunting;
  self.briefingEnviado = false;
  self.hasProfessionals = false;
  self.anexoClick = false;
  self.addMoreProf = false;
  self.addMoreProfComp = false;
  self.errorComplement = false;
  self.redirectToCustomer = redirectToCustomer;

  self.attachs = [];

  self.user = {};
  self.briefing = {
    Freelancers: [],
  };

  self.taskProjeto = {};

  self.selecionaProjeto = selecionaProjeto;
  self.removeProfissional = removeProfissional;
  self.goToProfissionais = goToProfissionais;
  self.projeto = {};
  self.isHunting = $stateParams.hunting;
  self.save = save;
  self.baseUrlAPI = comumConfig.baseUrlAPI();
  self.briefingUpdate = briefingUpdate;
  self.visualizaPerfil = visualizaPerfil;
  self.isOpenProfile = false;
  self.isOpen = false;
  self.slideOpenProfile = slideOpenProfile;


  // Functions
  function init() {

    console.log("getBriefing", self.getBriefing);
    console.log("getBriefing", $sessionStorage.briefing);
    //console.log("self.getBriefing", self.getBriefing);

    console.log("$stateParams.hunting", $stateParams.hunting);

    /** É PRECISO SEPARAR ENTRE COMPLEMENTO E HUNTING ANTES DE INICIAR TUDO */
    if (!$stateParams.hunting && $stateParams.id) {
      /** @method this.briefingComplement guarda valores iniciais do estado */
      self.briefingComplement = {
        BriefingId: $stateParams.id,
        Text: $stateParams.hunting ? getText() : null,
        Freelancers: [],
        FreelancerIds: [],
        Attachs: [],
        Project: [],
      };
    }

    /* if ($stateParams.hunting) {
      /** @method this.editHunting guarda valores iniciais do estado 
      self.isHunting = $stateParams.hunting;
      self.editHunting = {
        BriefingId: $stateParams.id,
        Text: getText(),
        Freelancers: [],
        FreelancerIds: [],
        Attachs: [],
        Project: [],
      };
    }   */

    console.log("self.projetos", self.projetos);
    console.log("$rootScope.getProjetos", $rootScope.getProjetos);

    self.getProjetos = $rootScope.getProjetos;

    /** @method this.user objeto que contém informações de login completas */
    self.user = comumService.getUser();
    console.debug("user", self.user);

    configBriefing();
    initQuillEditor();
    configDropzone();
    addScrollable();
    configProfessionalShourtName();
    deleteAttachment();
    getProjetos();
    verifyHunting();
    vML(self.getBriefing);



    console.debug('briefing', self.briefing);

    console.debug('compare', hunting, self.user);
    // $scope.$apply();
    console.debug('POST-USER', self.user);

    // window.localStorage.setItem('USR_HTG', null);
    console.log("isHunting", $stateParams.hunting);

    console.log("self.briefingComplement", self.briefingComplement);

  }

  function selecionaProjeto(projeto) {
    self.briefing.Project = projeto;
  }

  function cancelHunting() {
    window.localStorage.setItem("USR_HTG", null);
    // window.sessionStorage.setItem("briefing", null);
    $state.reload();
  }

  /**
   * Esta função verifica se é um briefing eligível vindo de briefing.crowd.br
   * @param {object} b - getBriefing vindo do Machine Learning / master.route
   */
  function vML(b) {
    console.log("self.briefing.hasOwnProperty('Hunting')", self.briefing.hasOwnProperty('Hunting'));
    if (self.briefing.hasOwnProperty('Hunting')) {
      if (!$sessionStorage.fromBriefing) $sessionStorage.briefing = b;
      console.log("vML($sessionStorage.briefing)", $sessionStorage.briefing);
      return $sessionStorage.briefing;
    } else {
      return false;
    }
  }

  function verifyHunting() {
    hunting = JSON.parse(window.localStorage.getItem('USR_HTG'));
    if (hunting) {
      self.isHunting = true;

      console.log("hunting", hunting);

      self.user = {
        nome: hunting[0].Name,
        email: hunting[0].Email,
        imagem_perfil: hunting[0].Photo,
        cargo: hunting[0].RoleCompany,
        IdCustomer: hunting[1].Id,
        id: hunting[0].Id,
        IdUser: hunting[0].Id,
        empresa: hunting[1].Trade
      }

      self.briefing.Hunting = $stateParams.hunting;
      self.briefing.IdCustomer = hunting[0].IdCustomer;
    }
  }

  function localCustomer(b) {

    //FIXME: VERSÃO SERVICE vs. SESSIONSTORAGE
    if (self.isHunting) {
      self.user = {
        nome: b.User.Name,
        email: b.Customer ? b.Customer.Email : b.User.Email,
        imagem_perfil: b.Customer ? b.Customer.Photo : b.User.Photo,
        cargo: b.Customer ? b.Customer.RoleCompany : null,
        IdCustomer: b.Customer ? b.User.Id : b.User.IdCustomer,
        id: b.Customer ? b.Customer.Id : b.User.Id,
        IdUser: b.IdUser,
        empresa: b.User.Customer.Trade,
        logotipo: b.User.Customer.Logo
      }
      self.briefing.Hunting = $stateParams.hunting;
      self.briefing.IdCustomer = b.Customer ? b.User.Id : b.User.IdCustomer;

    }

  }


  /**
   * Promessa onde listamos os projetos a serem inseridos no briefing
   * @param {string} project OPCIONAL: ID de um projeto
   */
  function getProjetos(project) {
    if (project) {
      comumService.getProjeto(project)
        .success(function (data) {
          self.Projeto = data;
          self.Projetos = false;
        });
    } else {
      comumService.getProjetos(self.user.id)
        .success(function (data) {
          self.getProjetos = data;
          self.Projeto = false;
        });
    }
  }

  function redirectToCustomer() {
    console.debug(JSON.parse(window.localStorage.getItem("USER_HTG")));
    $state.go("cliente");
  }

  function visualizaPerfil(profile, freelaId, parent, param2) {
    param2.stopPropagation();
    param2.preventDefault();
    comumService
      .getProfile(freelaId)
      .success(function (response) {
        console.log(response);
        comumConfig.fadeIn('.mask-content-pageslide');
        $rootScope.$broadcast("event:visualizar-perfil", response);

        parent.$ctrl.isOpenProfile = true;
        parent.$ctrl.hideSelectButton = true;
        self.isOpen = false;

        $rootScope.$broadcast("event:visualizar-perfil", response);
      })
      .error(function callback(data) {
        window.Rollbar.critical("visualizaPerfil", data);
        console.log(data);
      });
  }

  function slideOpenProfile() {
    //  comumConfig.fadeOut('.mask-content-pageslide');
    self.isOpenProfile = false;
    // self.isOpen = true;
  }

  function configBriefing() {
    console.log("configBriefing(IF $sessionStorage.briefing)", $sessionStorage.briefing ? true : false);
    if ($sessionStorage.briefing) {
      console.log("configBriefing(self.getBriefing)", $sessionStorage.briefing);
      console.log("$sessionStorage.briefing", $sessionStorage.briefing)
      // console.log("$sessionStorage.briefing.Hunting", $sessionStorage.briefing.Hunting)

      self.briefing = $sessionStorage.briefing;
      $stateParams.hunting ? self.newBriefing = true : $stateParams.id && !$stateParams.hunting ? self.newBriefing = false : self.newBriefing = true;
      self.hasProfessionals = $sessionStorage.briefing.Freelancers ? true : false;

      self.briefing.Id ? self.briefingComplement.BriefingId = self.briefing.Id : null;

      if ($stateParams.id && !$stateParams.hunting) {
        configBriefingComplement();
      } else {
        setText(self.getBriefing.Text);
        var currentProfessionals;
        // self.getBriefing.data ? currentProfessionals = self.getBriefing.data.Freelancers : currentProfessionals = self.getBriefing.Freelancers;
        $stateParams.hunting ? currentProfessionals = [] : currentProfessionals = self.getBriefing.FreelancerIds;
        var newProfesseionals = [];
        self.briefingComplement = {};
        self.briefingComplement.Freelancers = [];

        $sessionStorage.briefing.Freelancers.forEach(function (freelancerObj) {
          if (!comumConfig.searchObjectInArray(
              currentProfessionals,
              "Id",
              freelancerObj.Id
            )) {
            freelancerObj.complement = true;
            self.briefingComplement.Freelancers.push(freelancerObj);
          }
        });

      }

      var image = "../images/user.png";
      if (self.briefing.hasOwnProperty('User')) {
        if (
          self.briefing.User.Photo != null &&
          self.briefing.User.Photo != "" &&
          self.briefing.User.Photo != "null"
        )
          self.briefing.User.Photo =
          comumConfig.baseUrlAPI() + self.briefing.User.Photo;
        else {
          self.briefing.User.Photo = image;
        }

      }

    } else if ($sessionStorage.briefing) {
      console.log("ELSE IF $sessionStorage.briefing", $sessionStorage.briefing ? true : false);

      self.briefing = $sessionStorage.briefing;
      self.attachs = self.briefing.Attachs;

      $sessionStorage.fromBriefing = false;

      if (self.briefing.Freelancers.length) self.hasProfessionals = true;
      else self.hasProfessionals = false;
    }

    var totalProfessionals = self.briefing.Freelancers.length;
    self.addMoreProf =
      self.newBriefing && totalProfessionals < professionalsLimit ? true :
      false;
    self.addMoreProfComp = !self.newBriefing && totalProfessionals < professionalsLimit ? true :
      false;

    if ($rootScope.getUser()
      .Role == 1) {
      // NOTE: ROLE 1 PODE ADICIONAR MAIS DE 10 PROFISSIONAIS
      if (self.briefing.Complement) self.addMoreProfComp = true;
      if (!self.briefing.Complement) self.addMoreProf = true;
    }
  }

  function configBriefingComplement() {
    var newProfesseionals = [];
    var currentProfessionals = [];

    self.getBriefing.Freelancers.forEach(function (freelancerObj) {
      if (!comumConfig.searchObjectInArray(
          currentProfessionals,
          "Id",
          freelancerObj.Id
        )) {
        freelancerObj.complement = true;
        newProfesseionals.push(freelancerObj);
      }
    });

    self.attachs = $sessionStorage.briefing.Attachs;
    self.briefingComplement.Attachs = $sessionStorage.briefing.Attachs;
    self.briefingComplement.Project = $sessionStorage.briefing.Project;
    self.briefingComplement.FreelancerIds = comumConfig.getPropertyValueFromObjectArray(
      newProfesseionals,
      "Id"
    );
    self.briefingComplement.Freelancers = newProfesseionals;

    // setText($sessionStorage.briefing.Text);
    console.log(self.attachs);
  }

  function removeProfissional(professionalId) {
    self.briefing.Freelancers = comumConfig.removeObjectFromArray(
      self.briefing.Freelancers,
      "Id",
      professionalId
    );
    self.briefingComplement.Freelancers = comumConfig.removeObjectFromArray(
      self.briefingComplement.Freelancers,
      "Id",
      professionalId
    );
    self.briefingComplement.FreelancerIds = comumConfig.removeItemFromArray(
      self.briefingComplement.FreelancerIds,
      professionalId
    );

    if (!self.getBriefing.hasOwnProperty('Hunting')) {
      $sessionStorage.briefing.Freelancers = self.briefing.Freelancers;
    } else {
      $sessionStorage.briefing.Freelancers = comumConfig.removeObjectFromArray(
        $sessionStorage.briefing.Freelancers,
        "Id",
        professionalId
      );
    }

    console.log(self.briefingComplement);
  }

  function addScrollable() {
    comumConfig.asScrollable("#profissionais-selecionados")
      .add();
    setTimeout(function () {
      comumConfig.asScrollable("#profissionais-selecionados")
        .update();
    }, 2000);
  }

  /**
   * botão final do formulário de briefing
   * 
   */
  function goToProfissionais() {

    if (self.newBriefing) {
      self.briefing.Attachs = self.attachs;
      self.briefing.Text = getText();
      $sessionStorage.briefing = self.briefing;
      //window.localStorage.setItem('USR_HTG', null);
    } else {
      $sessionStorage.briefing = {
        Id: self.briefing.Id,
        Attachs: self.attachs,
        Text: getText(),
        Project: self.briefing.Project,
      };
      $sessionStorage.briefing.Freelancers = self.briefing.Freelancers
    }

    $sessionStorage.fromBriefing = true;
    $state.go("profissional");
  }

  function buildBriefing(briefing) {
    if (briefing) {
      self.newBriefing = false;
      self.idBriefing = briefing.Id;
      if (briefing.Freelancers.length) self.hasProfessionals = true;
      else self.hasProfessionals = false;

      configDataBriefing(briefing);
      addScrollable();
    }
  }

  function initQuillEditor() {
    var options = {
      events: {
        textChange: function () {
          comumConfig.displayNone(".placeholder");
        },
      },
    };

    if (!self.newBriefing) editor = "#editor-complement";

    comumConfig.quillEditor(editor)
      .add(options);

    // $sessionStorage.briefing ? $sessionStorage.briefing.hasOwnProperty('Text') ? comumConfig.quillEditor(editor)
    //   .setContent($sessionStorage.briefing.Text) : null : null;

  }

  function getText() {
    return comumConfig.quillEditor(editor)
      .getContent();
  }

  function setText(text) {
    $timeout(
      function () {
        comumConfig.quillEditor(editor)
          .setContent(text);
      },
      1500,
      false
    );
  }

  function save() {
    comumConfig.blockScreen("Salvando Briefing", ".page-content");

    console.debug('isHuntingOnSave 1', self.isHunting); // NOTE: O SERVICE MONTA O MODELO



    self.briefing.Attachs = self.attachs;
    self.briefing.Text = getText();

    verifyHunting();


    console.debug('isHuntingOnSave 2', self.isHunting); // NOTE: O SERVICE MONTA O MODELO



    if (self.isHunting) {
      self.briefing.Hunting = true;
      self.briefing.IdUser = self.user.id;
      self.briefing.IdCustomer = self.user.IdCustomer;
      self.briefing.Sent = self.huntingSalvo;
    } else {
      self.briefing.IdUser = self.user.id;
      self.briefing.IdCustomer = self.user.IdCustomer;
      self.briefing.Hunting = false;
      self.briefing.IdUser = self.user.id;
      self.briefing.IdCustomer = self.user.IdCustomer;
    }

    if (self.briefing.Freelancers && self.isHunting) {
      self.briefing.Freelancers = $sessionStorage.briefing.Freelancers;
      // self.briefing.Sended = true;
    }

    console.debug('isHuntingOnSave 3', self.isHunting); // NOTE: O SERVICE MONTA O MODELO


    if (validateBriefingCreate()) {
      comumConfig.unblockScreen();
      toaster.pop("error", "", "Ooops! " + self.errorMessage);
      return false;
    }

    scBriefingService
      .salvarBriefing(self.briefing) // NOTE: O SERVICE MONTA O MODELO
      .success(function (data, status) {
        if (status != 200) {
          toaster.pop(
            "error",
            "Oh Não!",
            "Não foi possível gravar seu Orçamento"
          );
          window.Rollbar.critical(
            "sharedScreen.salvarBriefing.controller > salvarBriefing",
            data
          );
          Intercom(
            "showNewMessage",
            "Olá, tive um problema tentando publicar meu Orçamento"
          );
          return false;
        }
        window.Intercom('trackEvent', 'publicou briefing', true);

        if (status == 200) {

          if (!data.Hunting) {
            $window.ga('send', 'event', 'Customer', 'Publicou Briefing');
            toaster.pop("success", "Parabéns!", "Orçamento publicado com sucesso.");

            // briefingActionsAfterSave("Orçamento publicado com sucesso!");
            window.localStorage.setItem('USR_HTG', null);

            delete $sessionStorage.briefing;
            delete $sessionStorage.fromBriefing;

            $state.go("briefing");

          } else if (data.Hunting && data.Sended) {
            $window.ga('send', 'event', 'Customer', 'Hunting Publicado');
            toaster.pop("success", "Wow!", "Hunting enviado pelo cliente :D");
            window.localStorage.setItem('USR_HTG', null);
            delete $sessionStorage.briefing;
            delete $sessionStorage.fromBriefing;

            $state.go("briefing");

          } else if (data.Hunting && !data.Sended) {
            $window.ga('send', 'event', 'Customer', 'Salvou Hunting');
            toaster.pop("success", "Sucesso!", "Hunting salvo nos rascunhos do cliente.");
            self.huntingSalvo = true;

            self.briefing.Id = data.Id

            comumConfig.unblockScreen();

          }


        }
      })
      .error(function callBack(data) {
        toaster.pop("error", "", "Não foi possível gravar seu Briefing.");
        window.Rollbar.critical(
          "sharedScreen.salvarBriefing.controller > salvarBriefing",
          data
        );
      });
  }

  function briefingUpdate() {
    if (!validateBriefingComplement()) return false;

    if (textIsEmpty()) self.briefingComplement.Text = null;
    else self.briefingComplement.Text = getText();

    self.briefingComplement.Attachs = self.attachs;

    scBriefingService
      .briefingUpdate(self.briefingComplement)
      .success(function (data, status) {
        if (status != 200) {
          toaster.pop(
            "error",
            "Oh Não!",
            "Não foi possível gravar seu Briefing ):"
          );
          window.Rollbar.critical(
            "sharedScreen.salvarBriefing.controller > briefingUpdate()",
            data
          );
          Intercom(
            "showNewMessage",
            "Olá, tive um problema tentando atualizar meu Orçamento"
          );
          return false;
        }

        if (data) {
          delete $sessionStorage.briefing;
          delete $sessionStorage.fromBriefing;
          briefingActionsAfterSave("Briefing editado com sucesso!");
          $state.go("briefing");
        }
      })
      .error(function (data) {
        toaster.pop("error", "", "Ooops! Erro ao editar o briefing.");
        window.Rollbar.critical(
          "sharedScreen.salverBriefing.controller > briefingUpdate()",
          data
        );
        Intercom(
          "showNewMessage",
          "Olá, tive um problema tentando atualizar meu Orçamento"
        );
      });
  }

  function briefingActionsAfterSave(message) {
    toaster.pop("success", "", message);

    $timeout(function () {
      delete $sessionStorage.briefing;
      delete $sessionStorage.fromBriefing;
      $state.go("briefing");
    }, 2000);
  }

  function getDataEndereco(objProfissional) {
    return {
      estado:

        objProfissional.State.UF == "null" ? "" : objProfissional.State.UF,
      cidade:

        objProfissional.City.Name == "null" ? "" : objProfissional.City.Name,
    };
  }

  function configDataProfissionais(dataProfissionais) {
    var profissional = {};

    dataProfissionais.forEach(function (objProfissional, index) {
      profissional.id = objProfissional.Id;
      profissional.nome = objProfissional.Name;
      profissional.profissao = objProfissional.Title;
      profissional.profissaoCurto = comumConfig.cortarTexto(
        objProfissional.Title,
        25
      );

      profissional.descricao = objProfissional.Description;
      profissional.valor_hora = objProfissional.Price;
      profissional.portfolio = getPortifolio(objProfissional.Portfolio);
      profissional.code = objProfissional.Code;
      profissional.tags = getStringFromSkillsArray(objProfissional.Skills);
      profissional.imagem_perfil =
        comumConfig.baseUrlAPI() + objProfissional.Photo;
      profissional.endereco = getDataEndereco(objProfissional);
      profissional.uf = profissional.endereco.estado;
      profissional.starRating = objProfissional.Rating;
      profissional.rating = {
        quality: objProfissional.QualityRating,
        responsability: objProfissional.ResponsabilityRating,
        agility: objProfissional.AgilityRating,
      };
      profissional.disponibilidade = objProfissional.Availability;
      profissional.Code = objProfissional.Code;
      //profissional.segmento        = objProfissional.Availability;

      self.profissionais.push(profissional);
      profissional = {};
    });
  }

  function configDataBriefing(dataBriefing) {
    var briefing = {
      id: dataBriefing.Id,
      titulo: dataBriefing.Title,
      resumo: dataBriefing.Excerpt,
      descricao: dataBriefing.Text,
      profissionais: configDataProfissionais(dataBriefing.Freelancers),
      complemento: configDataComplementos(dataBriefing.Complements),
      anexos: configDataAnexos(dataBriefing.Attachs),
    };

    self.briefing = briefing;
  }

  function configProfessionalShourtName() {
    if (
      self.briefing.Freelancers !== undefined &&
      self.briefing.Freelancers.length >= 0
    ) {
      self.briefing.Freelancers.forEach(function (professionalObj) {
        professionalObj.shourtName = comumConfig.getFirstAndLastWord(
          professionalObj.Name
        );
      });
    }
  }

  function configDataComplementos(dataComplemento) {
    var complementos = {};

    if (dataComplemento.length) {
      dataComplemento.forEach(function (objComplemento, index) {
        var complemento = {};
        complemento.descricao = objComplemento.Text;
        complemento.anexos = [];
        if (objComplemento.Attachs) {
          objComplemento.Attachs.forEach(function (objAnexo, index) {
            var anexo = {};
            anexo.id = objAnexo.Id;
            anexo.file = objAnexo.attachUrl;

            complemento.anexos.push(anexo);
          });
        }
      });
    }

    return complementos;
  }

  function configDataAnexos(dataAnexos) {
    var anexos = [];

    if (dataAnexos.length) {
      dataAnexos.forEach(function (objAnexo, index) {
        var anexo = {};
        anexo.id = objAnexo.Id;
        anexo.path = objAnexo.attachUrl;
        anexo.name = objAnexo.attachName;

        anexos.push(anexo);
      });
    }

    return anexos;
  }

  function configDropzone() {
    var authorizationHeader = "Bearer " + $localStorage.token;

    var dropzone = {
      element: null,
      url: {
        headers: {
          Authorization: authorizationHeader,
        },
        url: comumConfig.baseUrlAPI() + "/Briefing/AttachFile",
        clickable: ".click-upload",
      },
      options: {
        name: null,
        init: {
          dropData: {
            imgTypeAllowed: [
              "image/jpg",
              "image/jpeg",
              "image/png",
              "image/gif",
            ],
            isImageAll: true,
            filesPath: [],
          },
          addedfile: function (file) {
            if (dropData.imgTypeAllowed.indexOf(file.type) < 0)
              dropData.isImageAll = false;
          },
          success: function (file, data) {
            var fileFullPath =
              comumConfig.baseUrlAPI() + data.folder + data.filename;

            if (dropData.isImageAll && !self.anexoClick)
              dropData.filesPath.push({
                fileFullPath: fileFullPath,
                fileName: file.name,
              });
            else
              self.attachs.push({
                attachUrl: data.folder + data.filename,
                attachName: file.name,
              });
          },
          maxfilesexceeded: function (file, data) {
            console.log(file);
            console.log(data);
            alert("O tamanho maximo permitido é de 10 MB");
            self.removeFile(file);
          },
          queuecomplete: function (file) {
            var previewElement =
              self.newBriefing ? "#dropzone-previews" :
              "#dropzone-previews-complement";
            var elementsToRemove = comumConfig.findElement(
              ".dz-preview",
              previewElement
            );

            if (dropData.isImageAll && !self.anexoClick) {
              dropData.filesPath.forEach(function (fileObj) {
                insertContent(fileObj.fileFullPath);
              });

              angular.forEach(elementsToRemove, function (element) {
                var fileName = getFileName(element);

                dropData.filesPath.forEach(function (fileObj) {
                  if (fileObj.fileName.indexOf(fileName) >= 0)
                    comumConfig.removeElement(element);
                });
              });
            } else {
              angular.forEach(elementsToRemove, function (element) {
                var fileName = getFileName(element);

                self.attachs.forEach(function (fileObj) {
                  if (fileObj.attachName.indexOf(fileName) >= 0) {
                    var elem = comumConfig.findElement(
                      ".dz-filename a",
                      element
                    );
                    comumConfig.attr(
                      elem,
                      "href",
                      comumConfig.baseUrlAPI() + fileObj.attachUrl
                    );

                    var elemI = comumConfig.findElement(
                      ".dz-filename i",
                      element
                    );
                    comumConfig.attr(elemI, "data-filepath", fileObj.attachUrl);
                  }
                });
              });
            }

            dropData.isImageAll = true;
            dropData.filesPath = [];
            self.anexoClick = false;

            deleteAttachment();
          },
        },
        maxFilesize: 10,
        createImageThumbnails: false,
        previewTemplate: null,
        previewsContainer: null,
      },
    };

    if (self.newBriefing) {
      dropzone.element = "#my-dropzone";
      dropzone.options.name = "myDropzone";
      dropzone.options.previewTemplate = "preview-template";
      dropzone.options.previewsContainer = "dropzone-previews";
    } else {
      dropzone.element = "#my-dropzone-complement";
      dropzone.options.name = "myDropzoneComplement";
      dropzone.options.previewTemplate = "preview-template-complement";
      dropzone.options.previewsContainer = "dropzone-previews-complement";
    }

    comumConfig.addDropzone(dropzone);
  }

  function insertContent(imagePath) {
    var currentContent = comumConfig.quillEditor(editor)
      .getContent();
    var addImageContent = '<img src="' + imagePath + '">';
    var newContent = currentContent + addImageContent;

    comumConfig.quillEditor(editor)
      .setContent(newContent);
  }

  function getFileName(element) {
    var elem = comumConfig.findElement(".dz-filename span", element);
    return comumConfig.html(elem);
  }

  function validateBriefingCreate() {
    var hasError = false;

    if (!self.briefing.Title) {
      self.errorMessage = "Preencha o título.";
      hasError = true;
    }

    if (!self.briefing.Excerpt) {
      self.errorMessage = "Preencha o resumo.";
      hasError = true;
    }

    if (textIsEmpty()) {
      self.errorMessage = "Preencha a descrição do briefing.";
      hasError = true;
    }

    if (self.briefing.Project === undefined) {
      self.errorMessage = "Selecione um projeto para seu briefing.";
      hasError = true;
    }

    return hasError;
  }

  function validateBriefingComplement() {
    var complement = self.briefingComplement;

    if (
      complement.FreelancerIds.length === 0 &&
      textIsEmpty() &&
      self.attachs.length === 0
    ) {
      self.errorMessage = "Complemente seu briefing.";
      self.errorComplement = true;
    }

    return !self.errorComplement;
  }

  function textIsEmpty() {
    if (getText() == "<p><br></p>") return true;

    return false;
  }

  function deleteAttachment() {
    window.$jQuery(".remover-anexo")
      .on("click", function () {
        var filepath = window.$jQuery(this)
          .data("filepath");
        var element = comumConfig.getParent(window.$jQuery(this), 3);
        //comumConfig.blockScreen( '', '#dropzone-previews' );
        scBriefingService
          .deleteAttachment(filepath)
          .success(function (data) {
            //comumConfig.removeElement(element);
          })
          .error(function (data) {});

        comumConfig.removeElement(element);
        self.attachs = comumConfig.removeObjectFromArray(
          self.attachs,
          "attachUrl",
          filepath
        );
      });
  }

  function deleteAttachmentEdit($event, key) {
    var filepath = comumConfig.attr($event.currentTarget, "data-filepath");
    var name = comumConfig.attr($event.currentTarget, "data-name");
    var element = comumConfig.getParent($event.currentTarget, 1);

    self.briefing.Attachs = self.briefing.Attachs.filter(function (
      value,
      index
    ) {
      return key !== index;
    });
    self.attachs = self.briefing.Attachs;

    scBriefingService
      .deleteAttachment(filepath)
      .success(function (data) {
        //self.briefing.Attachs = att;
        //self.attachs          = att;
        //console.log(self.briefing.Attachs);
      })
      .error(function (data) {});
  }

  $rootScope.$on("event:getProjetos", function (event, data) {
    console.log("event:getProjetos", data);
    $rootScope = data;

    console.log("$rootScope.getProjetos", $rootScope.getProjetos);

    self.getProjetos = $rootScope.getProjetos;

    getProjetos();
  });

  //Calls
  init();
}
