function sharedScreenGestaoBriefingComponent()
{
    return {
        templateUrl: '../templates/briefing/view/gestao-briefing.html',
        controller: sharedScreenGestaoBriefingController,       
        bindings: {
            getBriefings: '<'
        }
    };
}