function sharedScreenSalvarBriefingComponent()
{
    return {
        templateUrl: '../templates/briefing/view/salvar-briefing.html',
        controller: sharedScreenSalvarBriefingController,       
        bindings: {
            getBriefing: '=',
        }
    };
}