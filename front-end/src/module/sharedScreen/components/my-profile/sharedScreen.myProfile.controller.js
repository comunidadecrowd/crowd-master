sharedScreenMyProfileController.$inject = ['comumService', '$scope', 'ngDialog', 'comumConfig', 'scMyProfileService', 'toaster', '$localStorage'];

function sharedScreenMyProfileController(comumService, $scope, ngDialog, comumConfig, scMyProfileService, toaster, $localStorage) {
    var self = this;

    // Private
    //...

    // Public
    self.profileImage = false;
    self.error = false;
    self.errorMessage = null;
    self.usuario = {};
    self.user = {};

    self.openDialog = openDialog;
    self.save = save;
    self.baseUrlAPI = comumConfig.baseUrlAPI();

    $scope.closeDialog = closeDialog;

    
    comumService.redirectTo();
    // Functions
    function init() {
        self.user = comumService.getUser();


        console.log(self.user);
        console.log(self.user.Photo);
       // self.user.Photo = window.localStorage.getItem('userPic');
        setTimeout(function(){
            setProfileImage();
        }, 2000)
    }

    function openDialog() {
        ngDialog.open({
            template: './templates/view/dialog-image-profile.html',
            appendClassName: "ngdialog-custom",
            width: '50%',
            scope: $scope
        });
    }

    function closeDialog() {
        ngDialog.close();
        self.profileImage = true;
    }

    function setProfileImage() {
        if (self.user.Photo) {
        self.profileImage = true;

            var image = self.user.Photo;
            console.log(self.user.Photo);
            window.$jQuery('#profile-upload').css('background-image', "url:("+self.baseUrlAPI + image+");");
            document.getElementById('imagem-perfil-logado').setAttribute("src", self.baseUrlAPI + image);
        }
    }


    function save() {
        if (!validateProfile())
            return false;

        var photo = getImageProfile();
        
        console.log(photo);
        var dataProfile = {
            Id: self.user.id,
            Name: self.user.nome,
            Email: self.user.email,
            RoleCompany: self.user.cargo,
            Photo: (photo.Logo ? photo.Logo : null),
            PhotoBase64: (photo.LogoBase64 ? photo.LogoBase64 : null),
            PhotoExtension: (photo.LogoExtension ? photo.LogoExtension : null),
            Password: self.user.password
        };

        comumConfig.blockScreen();
        scMyProfileService.saveProfile(dataProfile)
            .success(function(data, status) {

                if(status != 200) {
                  toaster.pop('error', "Ops!", "Não conseguimos salvar seu perfil.");
                  window.Rollbar.critical("sharedScreen.myProfile.controller > saveProfile", data);
                  return false
                }


                if (data.loggedUser) {  
                  window.localStorage.setItem('ngStorage-token', JSON.stringify(data.token));
                  window.localStorage.setItem('ngStorage-user', JSON.stringify(data.loggedUser));
                }
                comumService.setUserCargo(data.RoleCompany);
                // comumService.setUserPhoto(data.Photo);
               // comumService.loadImagemPerfil();

                comumConfig.unblockScreen();
                console.log('data', data);
                console.log('user', self.user);
                window.localStorage.setItem('userPic', data.Photo)
                toaster.pop('success', "", "Perfil atualizado!");
            })
            .error(function(data) {
                comumConfig.unblockScreen();
                window.Rollbar.critical("sharedScreen.myProfile.controller > saveProfile", data);
            });
    }

    function validateProfile() {
        if (!self.user.nome)
            enableValidation('Nome');

        if (!self.user.email)
            enableValidation('Email');

        return !self.error;
    }

    function enableValidation(fieldName, message) {
        if (message === undefined)
            message = 'Preencha o campo ' + fieldName + '.';

        self.errorMessage = message;
        self.error = true;

        return false;
    }

    function getImageProfile() {
        var user = {};
        
        if (!window.valp) return false;

        var has = window.valp;
        has = has.substring(5, has.length - 2);
        if (/use.png/.test(has)) has = undefined;

        var imgSrc = window.valp;

        if (!imgSrc && !has) {
            user.Logo = undefined;
            user.LogoBase64 = undefined;
            user.LogoExtension = undefined;

        } else if (imgSrc) {
            user.Logo = imgSrc;
            user.LogoBase64 = imgSrc.replace(/^data:image\/[a-z]+;base64,/, "");

            var extension = String();
            var lowerCase = imgSrc.toLowerCase();
            if (lowerCase.indexOf("png") !== -1) extension = "png"
            else if (lowerCase.indexOf("jpg") !== -1 || lowerCase.indexOf("jpeg") !== -1)
                extension = "jpg"
            else if (lowerCase.indexOf("gif") !== -1)
                extension = "gif"
            else extension = "tiff";

            user.LogoExtension = extension;
        }

        return user;
    }



    // INIT
    init();
}
