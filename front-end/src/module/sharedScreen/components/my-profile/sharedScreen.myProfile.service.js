sharedScreenMyProfileService.$inject = [ '$http', 'comumConfig' ];
function sharedScreenMyProfileService( $http, comumConfig )
{

	var service = this;
	// Public
	service.saveProfile = saveProfile;		

	// Private
	var baseUrlAPI = comumConfig.baseUrlAPI();	

	function saveProfile( profileData )
	{
		return $http.post( baseUrlAPI + '/User/EditUser', profileData );
	}

}
