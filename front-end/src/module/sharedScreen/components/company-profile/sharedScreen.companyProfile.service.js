sharedScreenCompanyProfileService.$inject = [
  "$http",
  "comumConfig",
  "comumService",
  "$q",
];

function sharedScreenCompanyProfileService(
  $http,
  comumConfig,
  comumService,
  $q
) {
  var service = this;

  // Public
  service.changePlan = changePlan;
  service.getCustomer = getCustomer;
  service.postBoleto = postBoleto;
  service.postCard = postCard;
  service.iuguToken = String();

  // Private
  var baseUrlAPI = comumConfig.baseUrlAPI();

  // Functions...
  function changePlan(data) {
    return $http.post(
      baseUrlAPI +
      "/Customer/ChangePlan?userId=" +
      data.userId +
      "&planType=" +
      data.planType
    );
  }

  function postBoleto(transactionData, formData) {

    var req = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      url: baseUrlAPI + "/Payment",
      data: {
        idCustomer: transactionData.userId,
        Plan: transactionData.planId,
        PaymentType: 2,
      },
    };
    return $http(req);
  }

  function postCard(transactionData) {
    console.debug("transactionData",transactionData);
    console.debug("T_DATA", transactionData);

    var expiration =
      transactionData.expirationMonth + "/" + transactionData.expirationYear;
    transactionData.expirationYear = "20" + transactionData.expirationYear;

    var iuguCard = Iugu.CreditCard(
      transactionData.cardNumber,
      transactionData.expMonth,
      transactionData.expYear,
      transactionData.name,
      transactionData.lastName,
      transactionData.cvv
    );

    function delay(ms) {
      var ctr,
        rej,
        p = new Promise(function (resolve, reject) {
          ctr = setTimeout(resolve, ms);
          rej = reject;
        });
      p.cancel = function () {
        clearTimeout(ctr);
        rej(Error("Cancelled"));
      };
      return p;
    }

    var cardToken = function (creditCard) {
      return Iugu.createPaymentToken(creditCard, function (response) {
        var result = "";
        if (response.errors) {
          result = response;
          console.debug(response);
        } else {
          service.iuguToken = response.id;
        }
        return response;
      });
    };
    cardToken(iuguCard);
    console.debug("CARD", iuguCard);

    if (iuguCard.errors.length > 0) {
      console.debug("ERR", iuguCard.errors);
      return delay(3000)
        .then(function () {
          return {
            code: iuguCard.errors.number,
            message: "Seu cartão de crédito possui campos inválidos",
          };
        });
    } else {
      return delay(6000)
        .then(function () {
          var req = {
            method: "POST",
            url: baseUrlAPI + "/Payment", //NOTE: VERIFICAR SE URL ESTÁ CORRETA
            headers: {
              "Content-Type": "application/json",
              Accept: "application/json",
            },
            data: {
              idCustomer: transactionData.userId,
              Plan: transactionData.planId,
              creditCardToken: service.iuguToken,
              PaymentType: 1,
            },
          };
          return $http(req);
        });
    }
  }

  function getCustomer(customerId) {
    customerId = customerId || comumService.getUser()["IdCustomer"];

    return $http.get(baseUrlAPI + "/Customer/Get?customer_id=" + customerId);
  }
}
