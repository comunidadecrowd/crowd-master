sharedScreenCompanyProfileController.$inject = ['comumConfig', 'toaster', 'comumService', 'ngDialog', 'scCompanyProfileService', '$scope', 'masterCustomerService', '$window', '$state', '$timeout', '$http', '$rootScope'];

function sharedScreenCompanyProfileController(comumConfig, toaster, comumService, ngDialog, scCompanyProfileService, $scope, masterCustomerService, $window, $state, $timeout, $http, $rootScope) {
  var self = this;

  // Private
  var _customer = {};
  var _baseUrlAPI = comumConfig.baseUrlAPI();
 // self.state = {};
  self.city = {};
  self.paymentStatus = false;
  // Public
  var Public = $scope;
  self.buttonText = 'Contratar!';
  self.selectedPlan = false;
  self.planValue = 0;
  self.invalidField = "";
  self.logoPath = "";
  self.photoPath = "";
  self.someUser = {};
  self.isOrder = false;
  self.customerPlan = null;
  self.hasImagem = false;
  self.error = false;
  self.sending = false;
  self.isFree = false;
  self.getCities = getCities;
  self.queryCidade = "";
  self.plan = {};
  self.phase = 1;
  self.errorMessage = null;
  self.payDone = false;
  self.boletoSent = false;
  self.hasErrors = false;
  self.states = [];
  self.cities = [];
 // self.setCity = setCity;

  self.customer = {};
  self.logo = {};
  self.errors = null;
  self.isConfirmed = false;
  self.save = save;
  self.openDialog = openDialog;
  self.selectPlan = selectPlan;
  self.changePlan = changePlan;
  self.isEnterprise = false;
  self.loadCities = loadCities;
  self.openDialogEditImage = openDialogEditImage;
  window.getImageProfile = getImageProfile;

  $scope.closeDialog = closeDialog;

  var self = this;

  // Private

  // Public
  self.profileImage = false;
  self.error = false;
  self.errorMessage = null;
  self.usuario = {};
  
  /** @public {object} Puxa das configurações do servidor o objeto que forma o usuário */
  self.user = comumService.getUser();
  
  /** @public {boolean} Exibe as colunas com os planos quando for TRUE */
  self.iuguActive = Boolean(false);
  
  /** @public {boolean} Exibe o formulário quando for TRUE */
  self.formHide = Boolean(false);

  self.openDialog = openDialog;

  /** @public {function} Cuida de toda a lógica de salvar o formulário */
  self.save = save;

  /** @public {string} Puxa das configurações estáticas a base da URL do ambiente */
  self.baseUrlAPI = comumConfig.baseUrlAPI();

  /** @private {object} Puxa das configurações do servidor o objeto que forma o usuário */
  var customerData = comumService.getUser();

  $scope.closeDialog = closeDialog;

  function openDialog() {
    ngDialog.open({
      template: './templates/view/dialog-image-profile.html',
      appendClassName: "ngdialog-custom",
      width: '50%',
      scope: $scope
    });
  }

  function closeDialog() {
    ngDialog.close();
    self.profileImage = true;
  }

  function setProfileImage() {
    self.someUser = window.user.loggedUser;
    // if (self.someUser.Photo) {

    self.profileImage = true;

    var image = self.customer.Photo;
    window.$jQuery('#profile-upload').css('background-image', "url:(" + self.baseUrlAPI + image + ");");
    document.getElementById('imagem-perfil-logado').setAttribute("src", self.baseUrlAPI + image);
    //  }
  }

  self.setPaymentMethod = function (method) {
    var tpod = new Date()
      .getUTCDate();
    var month = new Date()
      .getMonth() + 1;
    var year = new Date()
      .getFullYear();
    var mili = new Date()
      .getMilliseconds();
    if (method == 'CC') {
      self.isCC = true;
      self.isBoleto = false;
    } else {
      self.isBoleto = true;
      self.isCC = false;
    }
  };

  self.validationOptions = {
    maxLength: 34,

  }

  self.payWithBoleto = function () {
    Iugu.setAccountID("183BC875106A495780F4297C42FA206C");
    // Iugu.setTestMode(true);
    Iugu.setup();
    
    scCompanyProfileService.postBoleto({
      name: customerData.Name,
      userId: customerData.IdCustomer,
      price: self.plan.price.data,
      email: customerData.Email,
      planId: self.plan.id,
    }, customerData)
      .then(function (data) {
        //TODO: LOGICA DO IUGU PARA AVISAR AO USUÁRIO QUE O BOLETO FOI ENVIADO POR EMAIL
        self.boletoSent = true;
        self.isConfirmed = true;
        self.phase = 3;
        // $scope.$apply();
      })
  };

  self.payWithCC = function (data) {
    // self.isConfirmed = true;
    self.sending = true;
    Iugu.setAccountID("183BC875106A495780F4297C42FA206C");
    // Iugu.setTestMode(true);
    Iugu.setup();

    function GetCardType(number) {
      // visa
      var re = new RegExp("^4");
      if (number.match(re) != null)
        return "Visa";

      // Mastercard
      re = new RegExp("^5[1-5]");
      if (number.match(re) != null)
        return "Mastercard";

      // AMEX
      re = new RegExp("^3[47]");
      if (number.match(re) != null)
        return "AMEX";

      // Discover
      re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
      if (number.match(re) != null)
        return "Discover";

      // Diners
      re = new RegExp("^36");
      if (number.match(re) != null)
        return "Diners";

      // Diners - Carte Blanche
      re = new RegExp("^30[0-5]");
      if (number.match(re) != null)
        return "Diners - Carte Blanche";

      // JCB
      re = new RegExp("^35(2[89]|[3-8][0-9])");
      if (number.match(re) != null)
        return "JCB";

      // Visa Electron
      re = new RegExp("^(4026|417500|4508|4844|491(3|7))");
      if (number.match(re) != null)
        return "Visa Electron";

      return "";
    }

    /** pegando os valores preenchidos no input da data de expiração */
    var expY = self.ccForm.expirationDate.split('/')[1];
    var expM = self.ccForm.expirationDate.split('/')[0];
    
    /** Deve-se enviar para o IUGU a data com formato MM/YY, com somente os dois ultimos dígitos */
    expY = expY.slice(-2);

    /** @private {string} preciso saber qual ano estamos em dois dígitos */
    var y = new Date();
    y = y.getFullYear();
    y = String(y);
    y = y.slice(-2);

    /** @private {number}  qual o mês atual? */
    var m = new Date().getMonth();
    m++ // a contagem dos meses começa pelo zero (Janeiro == 0)

    /** hora de validar a data de expiração 
     * expM {string} mês preenchido no formulário
     * expY {string} ano preenchido no formulário, só os dois últimos dígitos
     * m {number} mês atual
     * y {string} ano atual, só os dois últimos dígitos
     */
    if (expM <= m && expY <= y) {
      self.errors = "Ano inválido ou cartão expirado";
      self.hasErrors = true;
      return false;
    }


    var nameArray = customerData.Name.split(' ');
    var firstName = nameArray.shift();
    var lastName = nameArray.join(' ');

    scCompanyProfileService.postCard({
      name: firstName,
      lastName: lastName,
      userId: customerData.IdCustomer,
      price: self.plan.price.data,
      cpf: customerData.cpf,
      email: customerData.email,
      phone: customerData.phone,
      cardType: GetCardType(self.ccForm.number),
      cardNumber: self.ccForm.number,
      cvv: self.ccForm.cvv,
      expMonth: expM,
      expYear: expY,
      planId: self.plan.id,
      country: customerData.country,
      cep: customerData.cep
    }, customerData)
      .then(function (data) { //FIXME: NÃO USAR data QUANDO UTILIZAR O MÉTODO .then
        // FIXME: CONDICIONAL IUGU
        if (data.data.Description) {
          self.paymentStatus = false;
          self.errors = data.data.Description;
          self.hasErrors = true;
          window.Rollbar.critical("sharedScreen.companyProfile.controller > payWithCC()", data.data);
          Intercom('showNewMessage', 'Oi, tive problemas tentando pagar meu plano, poderia me ajudar?');
        } else {
          if (data.data.loggedUser) {
            var x = data.data.loggedUser;
            self.phase = 3;
            window.localStorage.setItem('ngStorage-user', JSON.stringify(data.data.loggedUser));
            self.hasErrors = false;
            self.paymentStatus = true;
            self.isConfirmed = true;
            $scope.$apply();
          }
        }
        self.sending = false;
      });
  };



  self.setPlan = function (plan) {
    self.phase = 2;

    if (plan == 1) {
      self.isFree = true;
      self.isEnterprise = false;
      self.plan = {
        name: 'FREE',
        id: 1,
        period: 'Anual',
        price: {
          ui: 'R$ 0',
          data: 0
        },
        details: {
          first: "1 usuário;",
          second: "Orce projetos em menos de 24hrs;",
          third: "Bloqueio das informações publicas de contato dos freelancers;",
          fourth: "Suporte por email e chat."
        }
      };
    }
    if (plan == 2) {
      self.isFree = false;
      self.isEnterprise = false;
      self.plan = {
        name: 'PRO',
        id: 2,
        period: 'Mensal',
        price: {
          ui: 'R$ 149/mês',
          data: 149
        },
        details: {
          first: "Tudo do plano FREE +;",
          second: "Contato dos profissionais;",
          third: "Profissionais Avaliados;",
          fourth: "Relatórios;",
          fifth: 'Alçadas*'
        }
      }
    }
    if (plan == 3) {
      self.isFree = false;
      self.isEnterprise = false;
      self.plan = {
        name: 'PRO',
        id: 3,
        period: 'ANUAL',
        price: {
          ui: 'R$ 499/ano',
          data: 499
        },
        details: {
          first: "Tudo do plano FREE+",
          second: "Contato dos profissionais;",
          third: "Profissionais Avaliados;",
          fourth: "Relatórios;",
          fifth: "Alçadas;"
        }
      }
    }
    if (plan == 4) {
      self.isFree = false;
      self.isEnterprise = true;
      self.plan = {
        name: 'ENTERPRISE',
        id: 4,
        period: 'SOB CONSULTA',
        price: {
          ui: 'SOB ORÇAMENTO',
          data: null
        },
        details: {
          first: "Tudo do plano PRO+;",
          second: "Usuários ilimitados;",
          third: "Condições especiais para pagamento;",
          fourth: "Personalizações jurídicas;"
        }
      }
    }
    self.isOrder = true;
  };

  function setCity(city) {
    if (city) {
      return $http.get(comumConfig.baseUrlAPI() + '/Common/Cities?city=' + city.Name)
        .then(function (data) {
          self.queryCidade = data.data[0].Name
        })
    } else {
      self.queryCidade = "";
    }
  } 

  function enableValidation(fieldName, message) {
    if (message === undefined)
      message = 'Preencha o campo ' + fieldName + '.';

    self.errorMessage = message;
    self.error = true;

    return false;
  }


  function getImageProfile() {

    var userLogo = window.val;
    var userPhoto = document.getElementById('imagem-perfil').value;

    if (userLogo) {
      self.customer.Logo = userLogo;
      self.customer.LogoBase64 = userLogo.replace(/^data:image\/[a-z]+;base64,/, "");
      var extension = String();
      var lowerCase = userLogo.toLowerCase();
      if (lowerCase.indexOf("png") !== -1) extension = "png"
      else if (lowerCase.indexOf("jpg") !== -1 || lowerCase.indexOf("jpeg") !== -1)
        extension = "jpg"
      else if (lowerCase.indexOf("gif") !== -1)
        extension = "gif"
      else extension = "tiff";
      self.customer.LogoExtension = extension;
    } else {
      self.customer.Logo = self.logoPath;
      self.customer.LogoBase64 = '';
      self.customer.LogoExtension = '';
    }

    if (userPhoto) {
      self.customer.Photo = userPhoto;
      self.customer.PhotoBase64 = userPhoto.replace(/^data:image\/[a-z]+;base64,/, "");
      var photoExtension = String();
      var lowerCase2 = userPhoto.toLowerCase();
      if (lowerCase2.indexOf("png") !== -1) photoExtension = "png"
      else if (lowerCase2.indexOf("jpg") !== -1 || lowerCase2.indexOf("jpeg") !== -1)
        photoExtension = "jpg"
      else if (lowerCase2.indexOf("gif") !== -1)
        photoExtension = "gif"
      else photoExtension = "tiff";
      self.customer.PhotoExtension = photoExtension;
    } else {
      self.customer.Photo = self.photoPath;
      self.customer.PhotoBase64 = '';
      self.customer.PhotoExtension = '';
    }
    
  /*  if (self.customer.Logo) {
      var imgSrc = window.val || comumConfig.baseUrlAPI() + self.customer.Logo;
    }
    
    if (document.getElementById('imagem-perfil').value) {
      var imgSrc2 = document.getElementById('imagem-perfil').value || self.customer.Photo;
    }

    if (!self.customer.Logo) {
      self.customer.Logo = imgSrc;
    }
    if (!self.customer.Photo) {
      self.customer.Photo = imgSrc2;
    }
    
    if (imgSrc2) {
      if (imgSrc2.indexOf('Content') > -1) {
        self.customer.PhotoBase64 = '';
      }
      else {
        self.customer.PhotoBase64 = imgSrc2.replace(/^data:image\/[a-z]+;base64,/, "");
        var photoExtension = String();
        var lowerCase2 = imgSrc2.toLowerCase();
        if (lowerCase2.indexOf("png") !== -1) photoExtension = "png"
        else if (lowerCase2.indexOf("jpg") !== -1 || lowerCase2.indexOf("jpeg") !== -1)
          photoExtension = "jpg"
        else if (lowerCase2.indexOf("gif") !== -1)
          photoExtension = "gif"
        else photoExtension = "tiff";
        self.customer.PhotoExtension = photoExtension;
      }
    }
    if (imgSrc) {
      self.customer.LogoBase64 = imgSrc.replace(/^data:image\/[a-z]+;base64,/, "");
      var extension = String();
      var lowerCase = imgSrc.toLowerCase();
      if (lowerCase.indexOf("png") !== -1) extension = "png"
      else if (lowerCase.indexOf("jpg") !== -1 || lowerCase.indexOf("jpeg") !== -1)
        extension = "jpg"
      else if (lowerCase.indexOf("gif") !== -1)
        extension = "gif"
      else extension = "tiff";
      self.customer.LogoExtension = extension;
      if (imgSrc.indexOf('Content') > -1) {
        self.customer.LogoBase64 = '';
        //self.customer.Logo = comumConfig.baseUrlAPI() + self.customer.Logo;
      }
    } */

  }




  // Functions
  function init() {
    self.states = self.getEstados.data;
    setImageProfile();



    self.customer = self.getCompanyProfile.data;
    console.debug('eita', self.customer)
    
    window.queryCidade = self.customer;
    
    if (JSON.parse(window.localStorage.getItem('ngStorage-user')).Photo) {
      self.customer.Photo = JSON.parse(window.localStorage.getItem('ngStorage-user')).Photo;
    }
    
    self.state = self.customer.State;
    self.queryCidade= self.customer.City || '';
    
    console.debug('ói', self.state);
    
    if (self.customer.Logo) {
      self.logo = {
        background: "url('" + _baseUrlAPI + self.customer.Logo + "')"
      };
    }
    
    self.user = comumService.getUser();

    console.debug('user', self.user);

    if (self.user.loggedUser.Prospect == false) {
       self.iuguActive = true;   
    }  
    console.debug('iuguActive', self.iuguActive);
    
    if (window.localStorage.getItem('userPic')) {
      self.user.Photo = window.localStorage.getItem('userPic');
    }

    _customer = self.customer;

    self.customerPlan = comumService.getUserPlan();

    _configState(self.customer.State);
    //getImageProfile();
    //loadCities();
    //configDropzone();
    // setTimeout(function() {
    //   configDropzone(); 
    // }, 500);
  }

  function setImageProfile() {
    if (self.customer.Photo) {
      self.photoPath = self.customer.Photo;
      self.customer.Photo = comumConfig.baseUrlAPI() + self.customer.Photo;
    } 
    if (self.customer.Logo) {
      self.logoPath = self.customer.Logo;
      self.customer.Logo = comumConfig.baseUrlAPI() + self.customer.Logo;
    }

    console.debug('logo', self.logoPath);
    console.debug('photo', self.photoPath);
  }  

  function openDialog() {
    ngDialog.open({
      template: './templates/company-profile/view/congratulations.modal.html',
      width: 600
    });

    $timeout(function () {
      ngDialog.close();
    }, 20000);
  }

  function selectPlan(plan) {
    self.selectedPlan = comumService.plans[plan];
    self.planValue = comumService.getPlanValueByName(self.selectedPlan);
  }

  function changePlan() {
    self.buttonText = 'Aguarde...';
    comumConfig.attr('#btnAgree', 'disabled', true);

    var data = {
      userId: comumService.getUser()['id'],
      planType: comumService.getPlanIdByName(self.selectedPlan)
    };


    if (self.selectedPlan == 'FREE') { //SE O PLANO FOR START ELE ESTÁ SENDO REDUZIDO
      window.swal({
        title: 'Você tem certeza que deseja limitar seu plano?',
        type: 'question',
        showCancelButton: true,
        customClass: 'modal-payment',
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Continuar com meu plano',
        cancelButtonText: 'Reduza meu plano!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: true
      })
        .then(function () { // CONFIRMOU A REDUÇÃO
          window.swal(
            'Reduzido!',
            'Seu plano foi migrado para ' + self.selectedPlan + '.',
            'success'
          )

          scCompanyProfileService.changePlan(data)
            .success(function (data) {

              // openDialog();
              if (data.indexOf('same') > -1) {
                toaster.pop({
                  type: 'error',
                  title: 'Ops!',
                  body: 'Vocês já está neste plano',
                  onShowCallback: function () {
                    setTimeout(function () {
                      window.location.reload(true);
                    }, 3000);
                  }
                });

                return false;
              }

              self.buttonText = "Plano trocado com sucesso!";
              comumConfig.attr("#btnAgree", "disabled", true);
              comumConfig.addCss("#btnAgree", "background", "#A9A9A9");
              comumConfig.removeClass("#payDo", "active");
              self.payDone = true;

              var currPlan = JSON.parse(window.localStorage.getItem("ngStorage-user"));
              currPlan.CustomerPlan = self.selectedPlan;
              var updatedPlanData = JSON.stringify(currPlan);
              window.localStorage.setItem("ngStorage-user", updatedPlanData);



              toaster.pop({
                type: "success",
                title: "Obrigado!",
                body: "Plano trocado com sucesso.",
                onShowCallback: function () {
                  setTimeout(function () {
                    window.location.reload(true);
                  }, 3000);
                },
              });

            })
            .error(function (data) {
              if (data.indexOf('same') > -1) {
                toaster.pop("error", "Ops!", "Vocês já está neste plano");
                return false;
              }

            });
        }, function (dismiss) {
          if (dismiss === 'cancel') {
            window.swal(
              'Alteração cancelada.',
              'Obrigado por continuar conosco.',
              'success'
            )
            return false;
          }
        });

    } else { //NOTE: O PLANO DELE NÃO ESTÁ SENDO REDUZIDO PARA START

      scCompanyProfileService.changePlan(data)
        .success(function (data) {
          // openDialog();
          if (data.indexOf('same') > -1) { // SE A API RESPONDER COM ERRO 400
            toaster.pop({
              type: 'error',
              title: 'Ops!',
              body: 'Vocês já está neste plano',
              onShowCallback: function () {
                setTimeout(function () {
                  window.location.reload(true);
                }, 3000);
              }
            });
            
          } else {

            self.buttonText = "Plano trocado com sucesso!";
            comumConfig.attr("#btnAgree", "disabled", true);
            comumConfig.addCss("#btnAgree", "background", "#A9A9A9");
            comumConfig.removeClass("#payDo", "active");
            self.payDone = true;

            var currPlan = JSON.parse(window.localStorage.getItem("ngStorage-user"));
            currPlan.CustomerPlan = self.selectedPlan;
            var updatedPlanData = JSON.stringify(currPlan);
            window.localStorage.setItem("ngStorage-user", updatedPlanData);

            toaster.pop({
              type: 'success',
              title: 'Obrigado!',
              body: 'Plano trocado com sucesso.',
              onShowCallback: function () {
                setTimeout(function () {
                  window.location.reload(true);
                }, 3000);
              }
            });

          }

        })
        .error(function (data) {
          window.Rollbar.critical("sharedScreen.companyProfile > changePlan", data);
          if (data.indexOf('same') > -1) {
            toaster.pop("error", "Ops!", "Vocês já está neste plano");
            return false;
          }

        });
    }

  }

  function openDialogEditImage() {
    ngDialog.open({
      template: '../templates/view/dialog-image-profile.html',
      appendClassName: "ngdialog-custom",
      width: '50%',
      scope: $scope
    });
  }

  function setLogotipoValue(img) {
    comumConfig.getElement("#hdLogotipo").val(img);
  }

  function getLogotipoValue() {
    return comumConfig.getElement("#hdLogotipo").val();
  }

  function setLogotipoImage(imgPath, status) {
    if (!status) {
      var imgHtml = '<img src="' + imgPath + '">';

      comumConfig.html(".logotipo-empresa", imgHtml);
      comumConfig.addClass(".placeholder-img", "none");
    } else {
      comumConfig.addClass(".placeholder-img", "block");
      comumConfig.html(".logotipo-empresa", "");
    }
  }

  function _getCustomer(customerId) {
    scCompanyProfileService.getCustomer(customerId)
      .success(function (data) {
        self.customer = data;
      })
      .error(function () {
      });
  }

  function loadCities() {
    // var city = self.customer.City;
  }

  function save() {
    if (!customerValidate())
      return false;
    
    console.debug('1', self.customer)
    comumConfig.blockScreen();

    getImageProfile();
    console.debug('2', self.customer)
    console.debug('5', self.queryCidade);
    
    if (self.queryCidade) {
      self.customer.City = {
        Name: self.queryCidade.originalObject.Name,
        Id: self.queryCidade.originalObject.Id
      }
    }

    if (self.state) {
      self.customer.State = {
       Name: self.state.Name,
       Id: self.state.Id
      }
    }

    masterCustomerService.save(self.customer)
      .then(function (data) { // FIXME: POR QUESTÕES SEMÂNTICAS ISTO DEVERIA SER 'RESPONSE' AO INVÉS DE 'DATA'

        console.debug('masterCustomerService.save(response): ', data);

        /** ALGUM CAMPO AINDA NECESSÁRIO NÃO FOI PREENCHIDO */
        if (data.status == 412) {
          var errMsg = data.data[0].Message;
          self.invalidField = data.data[0].Id;
          toaster.pop('success', "Falta pouco para começar a usar:", errMsg);
          comumConfig.unblockScreen();
          window.scrollTo(0,0);
          return false;
        } 

        /** ALGUM PROBLEMA GRAVE ACONTECEU */
        if (data.status != 200 && data.status != 400) {
          toaster.pop('error', "Bad Server", "Houve um problema no seu cadastro!");
          window.Rollbar.critical("sharedScreen.companyProfile.controller > save()", data);
          Intercom('showNewMessage', 'Oi, estou com dificuldades para concluir meu cadastro, poderia me ajudar?');
          comumConfig.unblockScreen();
          return false;
        }
        
        if (data.data) { // FIXME: data.data CONFUNDE MUITO 
          
          /** Se houve resposta do serviço, limpo os estados de erro no formulário */
          self.invalidField = "";
          self.error = false;
          self.errorMessage = null;
          
          /** @private {boolean} Guardo o estado atual do usuário se ele é um 'Lead' ou não */
          var actualProspect = JSON.parse(localStorage.getItem('ngStorage-user'))["Prospect"];
          
          /** @private {boolean} Guardo para comparar a resposta vinda do serviço com o resultado no $localStorage */
          var newProspect = data.data.loggedUser.Prospect;

          /** HOUVE ALTERAÇÃO NO ESTADO DE PROSPECT DO USUÁRIO
           *  ISSO ACONTECE QUANDO ELE SAI DO ESTADO 'LEAD' E PASSAR A SER UM USUÁRIO
           */
          if (actualProspect != newProspect && data.status == 200) {
            toaster.pop('success', "Bem-vindo!", "Seu acesso está liberado.");
            
            setTimeout(function () {
                
              /** Sendo então um 'Lead' que acabou de virar usuário, escondo o formulário para ele */
              self.formHide = true;

              /** Exibo os menus agora destrancados */
              $rootScope.isProspect = false;
              
              /** Trago os planos para ele escolher, entre gratuito e pagos */
              self.iuguActive = true; // FIXME: ATIVAR A EXIBIÇÃO DO IUGU QUANDO ESTIVER PRONTO
              
            }, 1000);
          
          } else {

            /** Ou ele só estava salvando o formulário */
            toaster.pop('success', "Sucesso!", "Alterações salvas.");
          }

          /** Com um novo loggedUser vindo do servidor, atualizo o $localStorage */
          if (data.data.loggedUser) {
            window.localStorage.setItem('ngStorage-user', JSON.stringify(data.data.loggedUser));
            window.localStorage.setItem('ngStorage-token', JSON.stringify(data.data.token));
          }
          
          comumConfig.unblockScreen();

        }

      })
      .catch(function (data, status) {
        toaster.pop('error', status, data);
        console.debug('stat',status, data);

        if (data.status == 409) {
          toaster.pop('error', '', data.data);
          comumConfig.unblockScreen();
        }

        if (data.status == 400) {
          toaster.pop('error', '', data.data);
          comumConfig.unblockScreen(); 
          console.debug('error', data);
        }

        self.error = true;
        self.errorMessage = data;
        comumConfig.unblockScreen();
      });
  }


  function _configState(state) {
    var stateObj = comumConfig.searchObjectInArray(self.states, 'Id', state);
    self.state = state;
  }

  function getCities(event) {
    // event.preventDefault();
    // event.stopPropagation();

    self.gettingCities = true;
    var typingTimer;                //timer identifier
    var doneTypingInterval = 4000;  //time in ms, 5 second for example
    var $input = window.jQuery('#city-input');

    //on keyup, start the countdown
    $input.on('keyup', function () {
      clearTimeout(typingTimer);
      typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });

    //on keydown, clear the countdown 
    $input.on('keydown', function () {
      clearTimeout(typingTimer);
    });

    //user is "finished typing," do something
    function doneTyping() {
      if (self.isClicking === true) {
        return false;
      }
      self.isClicking = true;
      /* window.jQuery('#city-input').autocomplete({
        showNoSuggestionNotice: true,
        noSuggestionNotice: 'Nenhum resultado encontrado',
        minChars: 4,
        serviceUrl: comumConfig.baseUrlAPI() + '/Common/Cities?city=' + self.queryCidade,
        transformResult: function (result) {
          self.gettingCities = false;
          return {
            suggestions: window.jQuery.map(JSON.parse(result), function (dataItem) {
              return {
                value: dataItem.Name,
                Id: dataItem.Id
              }
            })
          }
        },
        onSelect: function (suggestion) {
          self.customer.City = {
            Name: suggestion.value,
            Id: suggestion.Id
          };
        }
      }) */
      self.isClicking = false;
    }
  }

  function customerValidate() {


 //   if (!self.state || Object.keys(self.state)
 //     .length === 0) {
 //     return enableValidation('', 'Selecione o estado!');
 //   } else {
 //     //window._state = self.customer.State;
 //     //self.customer.State = self.state;
 //   }

    /* if (!self.city || Object.keys(self.city)
     .length === 0) {
     return enableValidation('', 'Selecione a cidade!');
    } else {
     window._city = self.customer.City;
     self.customer.City = self.city.Id;
    } */

    /* if (!self.city || Object.keys(self.city)
     .length === 0) {
     return enableValidation('', 'Selecione a cidade!');
    } else {
     window._city = self.customer.City;
     self.customer.City = self.city.Id;
    } */

    return true;
  }

  function enableValidation(fieldName, message) {
    if (message === undefined)
      message = 'Preencha o campo ' + fieldName + '.';

    self.errorMessage = message;
    self.error = true;

    return false;
  }


  //  function getImageProfile() {
  //   // var imgSrc = window.$jQuery('.dz-image').children()[0].src;

  //   if (!imgSrc) {
  //    self.customer.Logo = null;
  //    self.customer.LogoBase64 = null;
  //    self.customer.LogoExtension = null;
  //   } else {
  //    self.customer.Logo = imgSrc;
  //    self.customer.LogoBase64 = imgSrc.substr(22);
  //    self.customer.LogoExtension = 'jpeg';
  //   }
  //  }

  function closeDialog() {
    ngDialog.close();
  }



  // Call's
  init();
  setImageProfile();
}