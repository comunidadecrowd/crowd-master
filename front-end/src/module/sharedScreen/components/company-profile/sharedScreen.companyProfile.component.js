function sharedScreenCompanyProfileComponent()
{
	return {
		templateUrl: '../templates/company-profile/view/company-profile.html',
		controller: sharedScreenCompanyProfileController,		
		bindings: {
            getEstados: '<',
            getCompanyProfile: '<'
        }
	};
}