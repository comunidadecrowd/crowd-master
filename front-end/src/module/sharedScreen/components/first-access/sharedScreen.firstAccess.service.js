sharedScreenFirstAccessService.$inject = [ '$http', 'comumConfig', 'comumService', 'ngDialog' ];
function sharedScreenFirstAccessService( $http, comumConfig, comumService, ngDialog )
{

	var service = this;
	// Public
	service.openDialog = openDialog;
	service.save       = save;

	// Private
	var baseUrlAPI = comumConfig.baseUrlAPI();

	// Functions...
	function openDialog()
	{
		ngDialog.open({
           template: '<sc-first-access-component></sc-first-access-component>',
           width: 600,
           plain: true,
           closeByDocument: false,
           showClose: false           
        });
	}

	function save( dataUser )
	{
		var user     			= comumService.getUser();
			dataUser.IdCustomer = user.IdCustomer;
			dataUser.Role       = 1;
			dataUser.Id         = user.id;

		return $http.post( baseUrlAPI + '/Customer/AddUser', dataUser );
	}

}
