function sharedScreenFirstAccessComponent()
{
	return {
		templateUrl: '../templates/first-access/view/first-access.modal.html',
		controller: sharedScreenFirstAccessController,
		bindings: {}
	};
}
