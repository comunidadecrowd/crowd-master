sharedScreenFirstAccessController.$inject = [ 'comumConfig', 'comumService', 'ngDialog', '$scope', 'toaster', 'scFirstAccessService', '$timeout' ];
function sharedScreenFirstAccessController( comumConfig, comumService, ngDialog, $scope, toaster, scFirstAccessService, $timeout )
{
	var self = this;

	// Private
    var _customer     = {};
    var _ngDialogId   = 0;
    var _baseUrlAPI   = comumConfig.baseUrlAPI();
    var _errorMessage = null;
    var _loggedUser   = {};

    // Public
    self.nextStep            = true;
    self.logo                = {};
    self.user                = {
        Photo:           null,
        PhotoExtension:  null,
        PhotoBase64:     null,
        Name:            null,
        Email:           null,
        Phone:           null,
        Password:        null,
        ConfirmPassword: null,
        RoleCompany:     null,
        AcceptTerms:     null
    };

    self.openDialogEditImage = openDialogEditImage;
    self.save                = save;
    self.setStep             = setStep;

    $scope.closeDialog       = closeDialog;


    // Functions
    function init()
    {

        _loggedUser = comumService.getUser()['loggedUser'];

        self.user.Name        = _loggedUser.Name;
        self.user.Email       = _loggedUser.Email;
        self.user.Photo       = _loggedUser.Photo;
        self.user.RoleCompany = _loggedUser.RoleCompany;
        self.logo             = { background: "url('"+_baseUrlAPI+self.user.Photo+"')"};

        setImageProfile(self.user.Photo);
    }

    function save()
    {
        getImageProfile();

        if( !userValidate() ){
            toaster.pop('error', "", _errorMessage);
            return false;
        }

        delete self.user.ConfirmPassword;

        comumConfig.blockScreen('Aguarde...', '.modal-text-center');

        scFirstAccessService.save(self.user)
            .success(function() {
                toaster.pop('success', "", 'Perfil editado com successo');
                ngDialog.close();
                $timeout(function() {
                    location.reload();
                }, 5000);
            })
            .error(function() {
                toaster.pop('error', "", 'Erro ao editar o perfil. Tente novamente.');
            });

        return false;
    }

    function userValidate() {

        if (!self.user.Photo)
            return enableValidation(false, 'Você precisa adicionar uma imagem de perfil');

        if (!self.user.Name)
            return enableValidation('Nome');

        if (!self.user.Email)
            return enableValidation('Email');

        if (!comumConfig.isEmail(self.user.Email))
            return enableValidation(false, 'Email inválido');

        if (!self.user.Phone)
            return enableValidation('Telefone');

        if (!self.user.RoleCompany)
            return enableValidation('Cargo');

        if (!self.user.Password)
            return enableValidation('Senha');

        if (!self.user.ConfirmPassword)
            return enableValidation(false, 'Confirme a Senha.');

        if (self.user.Password !== self.user.ConfirmPassword)
            return enableValidation(false, 'Senha informada e sua confirmação não conferem.');

        if (!self.user.AcceptTerms)
            return enableValidation(false, 'Você precisa aceitar os Termos de uso.');

        return true;
    }

    function enableValidation(fieldName, message)
    {
        if (message === undefined)
            message = 'Preencha o campo ' + fieldName + '.';

        _errorMessage = message;

        return false;
    }

    function openDialogEditImage() {
        var properties = ngDialog.open({
            template: './templates/view/dialog-image-profile.html',
            appendClassName: "ngdialog-custom",
            width: '50%',
            scope: $scope
        });

        _ngDialogId = properties.id;
    }

    function setImageProfile(img)
    {
        var imgSrc = comumConfig.val('#imagem-perfil', img);
    }

    function getImageProfile()
    {
        var imgSrc = comumConfig.val('#imagem-perfil');

        if (!imgSrc) {
            self.user.Photo = null;
            self.user.PhotoBase64 = null;
            self.user.PhotoExtension = null;
        } else {
            self.user.Photo = imgSrc;
            self.user.PhotoBase64 = imgSrc.substr(23);
            self.user.PhotoExtension = 'jpeg';
        }
    }

    function closeDialog()
    {
        ngDialog.close(_ngDialogId);
    }

    function setStep( step )
    {
        self.nextStep = step || false;console.log(step);
    }


    // Call's
	init();
}
