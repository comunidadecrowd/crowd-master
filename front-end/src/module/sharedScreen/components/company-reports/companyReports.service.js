CompanyReportsService.$inject = ['$http', 'comumConfig'];

function CompanyReportsService($http, comumConfig) {


  var service = this;
  // Public
  service.getTasksDone = getTasksDone;
  service.getPerfil = getPerfil;
  service.getCommonValues = getCommonValues;
  service.getListTaksFortnight = getListTaksFortnight;
  service.getFile = getFile;

  // Private	
  var baseUrl = comumConfig.baseUrl();
  var baseUrlAPI = comumConfig.baseUrlAPI();

  // Functions
  function getTasksDone(init, end) {
    return $http.get(baseUrlAPI + '/Report/ListTasksDone?start=' + init + '&end=' + end);
  };

  function getCommonValues(init, end) {
    var queryUrl = '';
    if (!init || !end) {
      queryUrl = '/Report/GetCommonValues'
    } else {
      queryUrl = '/Report/GetCommonValues';
    }
    return $http.get(baseUrlAPI + queryUrl);
  }

  function getListTaksFortnight(init, end) {
    return $http.get(baseUrlAPI + '/Report/ListTaksFortnight?start=' + init + '&end=' + end);
  }

  function getFile(init, end, type) {
    if (type == '.csv') {
      return $http.get(baseUrlAPI + '/Report/GenerateSheet?start=' + init + '&end=' + end);
    } else {
      return $http.get(baseUrlAPI + '/Report/GeneratePDF?start=' + init + '&end=' + end);
    }
  }

  function getPerfil(idProfissional) {
    return $http.get(baseUrlAPI + '/User/Get?code=' + encodeURIComponent(idProfissional));
  }
}
