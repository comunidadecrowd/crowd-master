angular
  .module("SharedScreen")
  .controller("CompanyReportsController", CompanyReportsController);

CompanyReportsController.$inject = [
  "$rootScope",
  "$scope",
  "$window",
  "comumConfig",
  "NgTableParams",
  "CompanyReportsService",
];

function CompanyReportsController(
  $rootScope,
  $scope,
  $window,
  comumConfig,
  NgTableParams,
  CompanyReportsService
) {
  var vm = $scope;

  vm.tableData = {};
  vm.tableParams = {};
  vm.reportPeriodEnd = "";
  vm.reportInitDate = 0;
  vm.reportEndDate = 0;
  vm.selectedDateInit = undefined;
  vm.selectedDateEnd = "";
  vm.grandTotal = 0;
  vm.graphData = {};
  vm.reportTotalValue = 0;
  vm.selectedDate = "";
  vm.selectedDate2 = "";
  vm.selectedDate3 = "";
  vm.rangeMaxPick = "";
  vm.rangeMinPick = "";
  vm.fileUrl = undefined;
  vm.pdfUrl = undefined;
  vm.reportColor = "#5DA4E9";
  vm.ApiUrl = comumConfig.baseUrlAPI();
  vm.reportUpdatedAt = window.moment().locale('pt-br').format('L');
  function init() {
    getTableData();
    getCommonValues();
    renderGraph()
  }

  /** 
    * Variáveis de período de tempo padrão vigente nos relatórios
    * São invocadas quando os parametros init e end faltam nas funções que precisam deles
    * ALTERAR APENAS EM CASO DE MUDANÇA DE REGRA DE NEGÓCIO
  **/

  var defaultInit = window
    .window.moment()
    .subtract(3, "months")
    .toDate()
    .toISOString();
  var defaultEnd = window.moment().toDate().toISOString();

  /**
    * Busca dados do webservice para preencher a tabela
    * @description Envia dados e retorna
    * @param   {ISOString} init Data inicial da período
    * @param   {ISOString} end Data final do período
    * @returns {Object}  Contendo o Array de resultados e dados sobre a solicitação
  */

  function getTableData(init, end) {
    var tableInit = init ? init : defaultInit;
    var tableEnd = end ? end : defaultEnd;
    CompanyReportsService.getTasksDone(tableInit, tableEnd).then(function(
      data
    ) {
      vm.tableData = data.data;
      vm.reportPeriod = window.moment(tableInit)
        .locale("pt-br")
        .format("LL")
        .replace(/\d+\s[a-z]{0,2}/, "");
      vm.reportPeriodEnd = window.moment(tableEnd)
        .locale("pt-br")
        .format("LL")
        .replace(/\d+\s[a-z]{0,2}/, "");
      vm.tableParams = new NgTableParams({}, { dataset: vm.tableData, count: 25 });
      vm.reportInitDate = window.moment(tableInit).locale("pt-br").format("L").replace("/2017", "");
      vm.reportEndDate = window.moment(tableEnd).locale("pt-br").format("L");
      console.log("table", data);
    });
  }

  /**
    * Busca dados do webservice para os valores pagos pela empresa em um período
    * @description Caso os parametros init e end estejam vazios, o webservice retornará o valor total histórico
    * @param   {ISOString} init Data inicial da período
    * @param   {ISOString} end Data final do período
    * @returns {Object}  Contendo o Array de resultados e dados sobre a solicitação
    */

  function getCommonValues(init, end) {
    var init = init ? init : defaultInit;
    var end = end ? end : defaultEnd;
    return CompanyReportsService.getCommonValues(init, end).then(function(
      data
    ) {
      console.log("totalPaid", data);
      vm.reportTotalValue = data.data.totalPaid;
    });
  }

  /**
    * Filtra valores por período
    * @description Chamada pelos filtros de data na view utilizando os models vm.selectedDateInit e vm.selectedDateEnd
    * @returns Chama funções de renderização de dados utilizando o período selecionado
    */

  vm.filterByDate = function(period) {
    if (period == 3) {
      var queryInit = moment().subtract(3, 'months').toISOString();
      var queryEnd = moment().toISOString();

      renderGraph(queryInit, queryEnd);
      getCommonValues(queryInit, queryEnd);
      getTableData(queryInit, queryEnd);
    }

    if (period == 2) {
      var queryInit = window.moment().subtract(2, "months").toISOString();
      var queryEnd = window.moment().toISOString();

      renderGraph(queryInit, queryEnd);
      getCommonValues(queryInit, queryEnd);
      getTableData(queryInit, queryEnd);
    }
    if (vm.selectedDateInit == 0 || vm.selectedDateEnd == 0) {
      vm.rangeMinPick = window.moment(vm.selectedDateInit).add(2, "months");
      vm.rangeMaxPick = window.moment(vm.selectedDateInit).add(3, "months");
      console.log(vm.rangeMaxPick);
    } else {
      var queryInit = window.moment(vm.selectedDateInit).toISOString();
      var queryEnd = window.moment(vm.selectedDateEnd).toISOString();
      vm.rangeMinPick = window.moment(vm.selectedDateInit).add(2, "months");
      vm.rangeMaxPick = window.moment(vm.selectedDateInit).add(3, "months");

      renderGraph(queryInit, queryEnd);
      getCommonValues(queryInit, queryEnd);
      getTableData(queryInit, queryEnd);
    }
  };

    vm.getFile = function(init, end, type) {
        var init =  init ? init : defaultInit;
        var end = end ? end : defaultEnd;

        return CompanyReportsService.getFile(init, end, type)
            .then(function(data) {
                console.log(data);
                var blob = new Blob([data.data], { encoding:"UTF-8", type: 'application/xml;charset=UTF-8' }),
                    url = $window.URL || $window.webkitURL;
                console.log(blob);
                vm.fileUrl = url.createObjectURL(blob);
                CompanyReportsService.getFile(init, end, 'pdf')
                    .then(function(data){
                        var blob = new Blob([data.data], { encoding:"UTF-8", type: 'application/pdf;charset=UTF-8' }),
                            url = $window.URL || $window.webkitURL;
                        console.log(blob);
                        vm.pdfUrl = url.createObjectURL(blob);
                    })
            })
    }
    /** NÃO MEXA NESTE CARALHO
    /**
    * Busca dados do webservice para renderizar o gráfico utilizando-os
    * @description Caso os parametros init e end estejam vazios, o webservice retornará o valor dos últimos 6 meses
    * @param   {ISOString} init Data inicial da período
    * @param   {ISOString} end Data final do período
    * @returns Renderiza o gráfico
    **/

  function renderGraph(init, end) {
    var graphInit = init ? init : defaultInit;
    var graphEnd = end ? end : defaultEnd;
    var dataSeries = [];

    //console.log('lol');

    vm.getFile(graphInit, graphEnd, ".csv");

    CompanyReportsService.getListTaksFortnight(
      graphInit,
      graphEnd
    ).then(function(data) {
      console.log(">>", data);
      var isGraph = false;
      var tasksLength = data.data.map(function(x) {
        return x.Tasks.length > 0;
      });
      console.log(tasksLength);

      for (var i = 0; i < tasksLength.length; i++) {
        if (tasksLength[i] === true) {
          isGraph = true;
        }
      }

      console.log("end", isGraph);
      if (isGraph === false) {
        document.getElementById("report-chart").innerHTML =
          '<div class="col-xs-12 text-center">' +
          '<i class="fa fa-line-chart" style="font-size: 80px;color: #b7b7b7;"></i>' +
          '<h3 style="color: #b7b7b7"> Não há dados a serem exibidos para esse período</h3>' +
          "</div>";
      } else {
        console.log(data);
        var info = data.data;
        var categories = [];
        var qty = [];

        var maxTasks = window._.maxBy(info, function(d) {
          return d.Tasks.length;
        });

        var dataSeries = [];
        for (var j = 0; j < info.length; j++) {
          var currentTasks = info[j].Tasks;
          var oi = currentTasks.map(function(task, taskIndex) {
            var pack = { name: task.project, data: [] };
            for (var i = 0; i < info.length; i++) {
              pack.data.push(i === j ? task.price : null);
            }
            return pack;
          });
          dataSeries = dataSeries.concat(oi);
        }
        var colours = ['#5DA4E9', '#5DA4E9', '#5DA4E9', '#5DA4E9', '#5DA4E9', '#5DA4E9', '#5DA4E9', '#5DA4E9']

        window.Highcharts.chart("report-chart", {
          chart: {
            type: "column",
          },
          colors: colours,
          title: {
            text: "",
          },
          xAxis: {
            categories: info.map(function(d) {
              return d.Fortnight;
            }),
          },
          yAxis: {
            min: 0,
            title: {
              text: "Valores (em R$)",
            },
          },
          tooltip: {
            pointFormat:
              '<span style="color:{series.color}">{series.name}</span>: <b>R$ {point.y}</b><br/>',
            shared: true,
          },
          plotOptions: {
            column: {
              stacking: "normal",
            },
          },
          series: dataSeries,
        });
      }
    });
  }
  init();
  return vm;
}
