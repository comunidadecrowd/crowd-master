/**
 * SharedScreen Module
 *
 * Description
 */
angular
  .module("SharedScreen", [
    "ngTable",
    "angucomplete-alt",
  ])
  // Components
  .component("scProfissionalComponent", sharedScreenProfissionalComponent())
  .component("scGestaoBriefingComponent", sharedScreenGestaoBriefingComponent())
  .component("scSalvarBriefingComponent", sharedScreenSalvarBriefingComponent())
  .component("scUsuarioComponent", sharedScreenUsuarioComponent())
  .component("scMyProfileComponent", sharedScreenMyProfileComponent())
  .component("scCompanyProfileComponent", sharedScreenCompanyProfileComponent())
  .component("scFirstAccessComponent", sharedScreenFirstAccessComponent())
  .component("companyReportsComponent", companyReportsComponent())
  /*.component('perfilPage', perfilPage())
	.component('gestaoBriefingPage', gestaoBriefingPage())
	.component('salvarBriefingPage', salvarBriefingPage());*/

  // Services
  .service("scProfissionalService", sharedScreenProfissionalService)
  .service("scBriefingService", sharedScreenBriefingService)
  .service("scUsuarioService", sharedScreenUsuarioService)
  .service("scSalvarBriefingComponent", sharedScreenSalvarBriefingComponent)
  .service(
    "scBindBriefingProfissionalService",
    sharedScreenBindBriefingProfissionalService
  )
  .service("scMyProfileService", sharedScreenMyProfileService)
  .service("scCompanyProfileService", sharedScreenCompanyProfileService)
  .service("scFirstAccessService", sharedScreenFirstAccessService)
  .service("CompanyReportsService", CompanyReportsService)
  // Config to ngInputTag
  .config(scProfissionalConfig)
  .config([
    "$compileProvider",
    function($compileProvider) {
      $compileProvider.aHrefSanitizationWhitelist(
        /^\s*(https?|ftp|mailto|tel|file|blob):/
      );
    },
  ]);

angular.module("SharedScreen").filter("reduceSecondName", function() {
  return function(input) {
    var userInfo = JSON.parse(window.localStorage.getItem("ngStorage-user"));
    // var result
    if (userInfo.CustomerPlan == "FREE") {
      firstName = input.split(" ")[0];
      result =
        firstName + "&nbsp;" + input.charAt(input.indexOf(" ") + 1) + ".";
    } else {
      // var firstName = input.substring(0, input.indexOf(' ') + 1);
      result = input;
    }
    return result;
  };
});
