function sharedScreenBindBriefingProfissionalService() {
  // Private
  var profissionais = [];
  var profissional = false;
  var briefing = false;
  var idBriefing = null;

  var service = this;

  // Public
  service.getProfissionais = getProfissionais;
  service.setProfissionais = setProfissionais;
  service.setBriefing = setBriefing;
  service.isBriefing = isBriefing;
  service.getIdBriefing = getIdBriefing;
  service.setIdBriefing = setIdBriefing;
  service.setProfissional = setProfissional;
  service.isProfissional = isProfissional;

  // Functions
  function getProfissionais() {
    return profissionais;
  }

  function setProfissionais(arrayProfissionais) {
    profissionais = arrayProfissionais;
  }

  function setBriefing(value) {
    briefing = value;
  }

  function isBriefing() {
    return briefing;
  }

  function setProfissional(value) {
    profissional = value;
  }

  function isProfissional() {
    return profissional;
  }

  function getIdBriefing() {
    return idBriefing;
  }

  function setIdBriefing(id) {
    idBriefing = id;
  }
}
