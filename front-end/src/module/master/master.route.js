masterRoute.$inject = ["$stateProvider", "$urlRouterProvider"];

function masterRoute($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/index");

    $stateProvider

    // Página Busca de Profissionais
        .state("profissional", {
        url: "/profissionais",
        views: {
            "pageTitle": {
                template: "Profissionais"
            },
            "pageContent": {
                template: "<sc-profissional-component get-skills='$resolve.getSkills' get-profissionais='$resolve.getProfissionais' get-categorias='$resolve.getCategorias'></sc-profissional-component>"
            },
            "visualizar-perfil-profissional@profissional": {
                template: "<comum-visualizar-perfil-profissional-component></comum-visualizar-perfil-profissional-component>"
            }
        },
        resolve: {
            //FIX-ME: POSSÍVEIS REQUISIÇÕES DESNECESSÁRIAS ACONTECENDO, ESTÁ DUPLICADO. REVER NO CONTROLLER O USO.
            // getProfissionais: masterGetProfissionais,
            getCategorias: masterGetCategorias,
            getSkills: masterGetSkills,
            updateNotifications: updateNotifications,
            // intercom: window.Intercom("update");
        }
    })

    // Página Gestão de Briefing
    .state("briefing", {
        url: "/briefings",
        views: {
            "pageTitle": {
                template: "Briefings"
            },
            "pageContent": {
                template: "<sc-gestao-briefing-component get-briefings='$resolve.getBriefings'></sc-gestao-briefing-component>"
            },
            "briefing-details@briefing": {
                template: "<comum-briefing-details></comum-briefing-details>"
            },
            "visualizar-perfil-profissional@briefing": {
                template: "<comum-visualizar-perfil-profissional-component></comum-visualizar-perfil-profissional-component>"
            }
        },
        resolve: {
            getBriefings: masterGetBriefings,
            updateNotifications: updateNotifications,
            // intercom: window.Intercom("update")
        }
    })

    // Página Novo/Editar Briefing
    .state("salvar-briefing", {
        url: "/briefings/salvar/:id/:hunting/",
        views: {
            "pageTitle": {
                template: masterGetStatusBriefing
            },
            "pageContent": {
                template: "<sc-salvar-briefing-component get-briefing='$resolve.getBriefing'></sc-salvar-briefing-component>"
            },
            'visualizar-perfil-profissional@salvar-briefing': {
              template: '<comum-visualizar-perfil-profissional-component></comum-visualizar-perfil-profissional-component>'
            }
        },
        resolve: {
            getBriefing: masterGetBriefing,
            updateNotifications: updateNotifications,
            // intercom: window.Intercom("update")
        }
    })

    // Página Usuários
    .state("usuario", {
        url: "/usuarios",
        views: {
            "pageTitle": {
                template: "Usuários"
            },
            "pageContent": {
                template: "<sc-usuario-component get-usuarios='$resolve.getUsuarios'></sc-usuario-component>"
            }
        },
        resolve: {
            getUsuarios: masterGetUsuarios,
            updateNotifications: updateNotifications,
            // intercom: window.Intercom("update")
        }
    })

    // Página Usuários
    .state("my-profile", {
        url: "/my-profile",
        views: {
            "pageTitle": {
                template: "Perfil"
            },
            "pageContent": {
                template: "<sc-my-profile-component></sc-my-profile-component>"
            }
        }
    })

    // Página Clientes
    .state("cliente", {
        url: "/clientes",
        views: {
            "pageTitle": {
                template: "Clientes"
            },
            "pageContent": {
                template: "<master-customer-component get-customers='$resolve.getCustomers' get-estados='$resolve.getEstados'></master-customer-component>"
            }
        },
        resolve: {
            getCustomers: masterGetCustomers,
            getEstados: masterGetEstados,
            updateNotifications: updateNotifications,
            // intercom: window.Intercom("update")
        }
    })

    // Página Projetos
    .state("project", {
        url: "/project/:id",
        views: {
            "pageTitle": {
                template: "Projetos"
            },
            "pageContent": {
                template: "<comum-project-component></comum-project-component>"
            },
            "briefing-details@project": {
                template: "<comum-briefing-details ></comum-briefing-details>"
            },
            "visualizar-perfil-profissional@project": {
                template: "<comum-visualizar-perfil-profissional-component></comum-visualizar-perfil-profissional-component>"
            }
        },
        resolve: {
            getProfissionais: masterGetProfissionais,
            getCategorias: masterGetCategorias,
            getSkills: masterGetSkills,
            getUsuarios: masterGetUsuarios,
            getCustomers: masterGetCustomers,
            getEstados: masterGetEstados,
            updateNotifications: updateNotifications,
            // intercom: window.Intercom("update")
        }

    })

    // Página Projetos
    .state("projects", {
        url: "/projects",
        views: {
            "pageTitle": {
                template: "Projetos"
            },
            "pageContent": {
                template: "<comum-project-component></comum-project-component>"
            },
            "briefing-details@projects": {
                template: "<comum-briefing-details></comum-briefing-details>"
            },
            "visualizar-perfil-profissional@projects": {
                template: "<comum-visualizar-perfil-profissional-component></comum-visualizar-perfil-profissional-component>"
            }
        },
        resolve: {
            getProfissionais: masterGetProfissionais,
            getCategorias: masterGetCategorias,
            getSkills: masterGetSkills,
            getUsuarios: masterGetUsuarios,
            getCustomers: masterGetCustomers,
            getEstados: masterGetEstados,
            updateNotifications: updateNotifications,
            // intercom: window.Intercom("update")
        }

    })

    //Página relatórios
    .state("companyReports", {
        url: "/company-reports",
        views: {
            "pageTitle": {
                template: "Relatórios"
            },
            "pageContent": {
                template: "<company-reports-component></company-reports-component>"
            }
        }
    })
    // Perfil da Empresa
    .state("company-profile", {
        url: "/company-profile",
        views: {
            "pageTitle": {
                template: "Informações da Empresa"
            },
            "pageContent": {
                template: "<sc-company-profile-component  get-estados='$resolve.getEstados' get-company-profile='$resolve.getCompanyProfile'></sc-company-profile-component>"
            }
        },
        resolve: {            
            getCompanyProfile: getCompanyProfile,
            getEstados: masterGetEstados,
            // intercom: window.Intercom("update")
        }
    })

    .state("logout", {
        url: "/logout",
        views: {
            "pageLogout": {
                controller: comumController
            }
        },
        resolve: {
            // intercom: window.Intercom('shutdown')
        }
    });
}

/* ********** Route "profissional" ********** */
masterGetProfissionais.$inject = ["scProfissionalService"];

function masterGetProfissionais(scProfissionalService) {
    var defaultPage = 1;
    return scProfissionalService.getProfissionais(defaultPage);
}

masterGetCategorias.$inject = ["comumService"];

function masterGetCategorias(comumService) {
    return comumService.getCategorias();
}

masterGetSkills.$inject = ["comumService"];

function masterGetSkills(comumService) {
    return comumService.getSkills();
}


/* ********** Route "briefing" ********** */
masterGetBriefings.$inject = ["scBriefingService"];

function masterGetBriefings(scBriefingService) {
    window.Pace.restart;    
    return scBriefingService.getBriefings();
}



/* ********** Route "salvar-briefing" ********** */
masterGetBriefing.$inject = ["scBriefingService", "$stateParams"];

function masterGetBriefing(scBriefingService, $stateParams) {

    var idBriefing = $stateParams.id;
    console.log(idBriefing);

    if (idBriefing)
        return scBriefingService.getBriefing(idBriefing);

    return false;
}

masterGetStatusBriefing.$inject = ["$stateParams"];

function masterGetStatusBriefing($stateParams) {
    var idBriefing = $stateParams.id;

    if (idBriefing)
        return "Editar Briefing";

    return "Novo Briefing";
}


/* ********** Route "usuario" ********** */
masterGetUsuarios.$inject = ["scUsuarioService"];

function masterGetUsuarios(scUsuarioService) {
    return scUsuarioService.getUsuarios();
}


/* ********** Route "cliente" ********** */
masterGetCustomers.$inject = ["masterCustomerService"];

function masterGetCustomers(masterCustomerService) {
    return masterCustomerService.getAll();
}


getCompanyProfile.$inject = ["scCompanyProfileService"];
function getCompanyProfile(scCompanyProfileService) {
    return scCompanyProfileService.getCustomer();
}


/* ********** Todas as Routes ********** */
updateNotifications.$inject = ["comumService"];

function updateNotifications(comumService) {
    "use strict";

    if (comumService.getUser().Role == 2) {
        comumService.getNotificationProfessional();
    } else {
        comumService.getNotification(comumService.getUser().userId);
    }

    comumService.getProjetos();
    comumService.getProjetosArquivados();

}

masterGetEstados.$inject = ["comumService"];

function masterGetEstados(comumService) {
    return comumService.getEstados();
}
