masterCustomerService.$inject = ["$http", "comumConfig", "scUsuarioService"];
function masterCustomerService($http, comumConfig, scUsuarioService) {
  var service = this;
  // Public
  service.getAll = getAll;
  service.save = save;
  service.statusUpdate = statusUpdate;
  service.getUsers = getUsers;

  // Private
  var baseUrlAPI = comumConfig.baseUrlAPI();

  function getAll() {
    return $http.get(baseUrlAPI + "/Customer/GetCustomers");
  }

  function save(dataCustomer, isEdit) {
    if (isEdit) return $http.post(baseUrlAPI + "/Customer/Edit", dataCustomer);
    else return $http.post(baseUrlAPI + "/Customer/Add", dataCustomer);
    
  }

  function statusUpdate(dataCustomer) {
    return $http.post(
      baseUrlAPI +
        "/Customer/ChangeStatusCustomer?customerId=" +
        dataCustomer.customerId +
        "&active=" +
        dataCustomer.active
    );
  }

  function getUsers(idCustomer) {
    return $http.get(
      baseUrlAPI + "/Customer/GetCustomerUser?customerId=" + idCustomer
    );
  }
}
