masterCustomerController.$inject = [
  "comumConfig",
  "masterCustomerService",
  "NgTableParams",
  "scUsuarioService",
  "comumService",
  "toaster",
  "$window",
  "$state",
  "$scope",
];

function masterCustomerController(
  comumConfig,
  masterCustomerService,
  NgTableParams,
  scUsuarioService,
  comumService,
  toaster,
  $window,
  $state,
  $scope
) {
  var self = this;

  /** @private {boolean} controle de estado do formulário */
  var isEdit = false;

  // Public
  self.tableParams = null;
  self.totalCustomers = 0;
  self.error = false;
  self.hasData = false;
  self.errorMessage = null;
  self.showIcon = false;
  self.isOpen = false;
  self.hasImagem = false;
  self.profissionaisSelecionados = JSON.parse(window.localStorage.getItem('USER_HTG'));
  self.enviarBriefing = enviarBriefing;

  self.states = [];
  self.cities = [];
  self.queryCidade = "";

  self.customer = {};

  self.setCity = setCity;
  self.save = save;
  self.edit = edit;
  self.statusUpdate = statusUpdate;
  self.getUsers = getUsers;
  self.openForm = openForm;
  self.userStatusUpdate = userStatusUpdate;
  self.loadCities = loadCities;
  self.cityQuery = cityQuery;
  self.cancel = cancel;
  self.showActions = scUsuarioService.showActions;
  self.baseURL = comumConfig.baseUrlAPI();

  // Functions
  function init() {
    // configCustomers();

    self.states = self.getEstados.data;
    self.totalCustomers = self.getCustomers.data.length;
    self.tableParams = new NgTableParams({
      count: 10,
    }, {
      counts: [],
      dataset: self.getCustomers.data,
    });

    console.debug('table', self.tableParams);
    configDropzone();
  }

  var engine = algoliasearch('JQ3HDF3RLD', '51659cf0f5e5ca2101a23bef5952b92f');


  /**
   * O usuário selecionou um funcionário e agora é roteado para a tela de briefing para começar um hunting
   * Gravo no localStorage os valores que irão construir o objeto self.user no outro $state
   * @param {object} user 
   * @param {object} customer 
   * @param {boolean} isHunting 
   */
  function enviarBriefing(user, customer, isHunting) {
    window.localStorage.setItem('USR_HTG', JSON.stringify([user, customer, isHunting]));
    $state.go('salvar-briefing');
  }

/**
 * Esta função vai começar montando o modelo válido para o endpoint
 * 
 * @returns $state.reload()
 */
function save() {

  comumConfig.blockScreen();
  
  /** é na validação que se montam os objetos de estado e cidade */
  if (!customerValidate()) return false;

  // self.customer.Logo = getLogotipoValue(); FIXME: ENTENDER ISSO
  delete self.customer.users; // NÃO QUEREMOS ATUALIZAR OS FUNCIONÁRIOS

  /** PROBLEMAS CLÁSSICOS COM UPLOAD DE IMAGENS */
  self.customer.logo;
  
  console.log("self.customer: ", self.customer);

  /** A CHAMADA DO SERVICE PRECISA INFORMAR SE É EDIÇÃO POIS O ENDPOINT MUDA */
  masterCustomerService
    .save(self.customer, isEdit)
    .then(function (response, status) { // NOTE: UTILIZAR SEMPRE response QUANDO USADO O MÉTODO .then
      console.log("THEN");
      console.log("RESPONSE", response);
      console.log("status", status); // NÃO EXISTE ESTE PARÂMETRO

      if (response.status == 200) { // response.DATA É A MELHOR PRÁTICA
        if (!isEdit) {
          self.tableParams.settings()
            .dataset.unshift(response.data);
          self.totalCustomers++;
          toaster.pop("success", "Boa!", "Cliente cadastrado com sucesso!");

        } else {
          var currentUser = comumService.getUser();
          toaster.pop("success", "", "Cliente editado com sucesso!");

        }

        isEdit = false;
        self.isOpen = false;
        self.error = false;
        self.errorMessage = null;
        self.customer = {};
        self.tableParams.reload();
        return $state.reload();

      } else { // ERROR HANDLING
        console.log("ERROR", response);
        toaster.pop("error", response.statusText, response.data);
        self.error = true;
        self.errorMessage = response.data;
        // SEMPRE CHAMAR O ROLLBAR NOS ERROS
        window.Rollbar.critical(
          "master.customer.controller > save()",
          response.config
        );

      }

      comumConfig.unblockScreen();
    })
    .catch(function (data, status) {
      console.log("ERROR", data);
      toaster.pop("error", status, data);
      self.error = true;
      self.errorMessage = data;
      comumConfig.unblockScreen();
    });

    comumConfig.unblockScreen();
  }

  function cityQuery() {

    if (self.customer.City) {
      engine.initIndex('freelancers')
        .searchForFacetValues({
          facetName: 'City.Name',
          facetQuery: self.customer.City
        }, function (success, content) {
          self.cityResults = {};
          self.cityResults = content.facetHits.length ? content.facetHits : null;
          self.hasData = true;
        })
    } else {
      self.customer.City = undefined;
    }

  }

  function setCity(city) {
    if (city) {
      return $http.get(comumConfig.baseUrlAPI() + '/Common/Cities?city=' + city.Name)
        .then(function (data) {
          self.queryCidade = data.data[0].Name
        })
    } else {
      self.queryCidade = "";
    }
  } 

  function edit(customer) {
    /** MODEL SE COMPORTANDO ESTRANHO */
    self.customer = null;
    self.queryCidade = null;
    self.error = false;
    self.errorMessage = null;
    self.tableParams.reload();
    // $state.reload();
    
    isEdit = true;
    console.log("customer.EDIT",customer);
    console.log("customer.LOGO",customer.Logo);

    /** NOTE: https://ghiden.github.io/angucomplete-alt/#example16 */
    $scope.$broadcast('angucomplete-alt:changeInput', 'city-input', customer.City);
    
    jQuery(
      "#selectEstados option[value='" + customer.City.StateId + "']"
    ).attr("selected", "selected");
  
    if (customer.Logo) {
      self.hasImagem = true;
      var imgPath = customer.Logo;
      setLogotipoImage(imgPath);
      setLogotipoValue(
        comumConfig.getLastElementFromArray(customer.Logo.split("/"))
      );
    } else {
      self.hasImagem = false;
      setLogotipoValue("");
      setLogotipoImage("", false);
      comumConfig.removeClass(".placeholder-img", "none");
    }

    jQuery("#selectEstados")
      .find('option[value="' + customer.City.StateId + '"]')
      .prop('selected', true)
      .trigger('change');
    self.state = customer.State;

    openForm();

    window.$jQuery("html, body")
      .animate({
          scrollTop: 0,
        },
        "slow"
    );
    
    self.customer = customer;

  }

  function customerValidate() {
    if (!self.customer.Name) return enableValidation("Razão Social");

    if (self.customer.QtyPersons < 0)
      return enableValidation("Número de funcionários");

    if (!self.customer.CNPJ)
      return enableValidation("", "Preencha um CNPJ válido!");

    if (!self.customer.Phone) return enableValidation("Telefone");


    if (self.state) {
      self.customer.State = {
        Name: self.state.Name,
        Id: self.state.Id,
        UF: self.state.UF
      }
    } else {
      return enableValidation("", "Selecione o estado!");
    }

    if (self.queryCidade) {
      self.customer.City = {
        Name: self.queryCidade.originalObject.Name,
        Id: self.queryCidade.originalObject.Id,
        StateId: self.state.Id
      }
    } else {
      return enableValidation("", "Selecione a cidade!");
    }

    if (!self.customer.Address) return enableValidation("Rua");

    if (!self.customer.Number) return enableValidation("Nº");

    if (!self.customer.NameResponsible)
      return enableValidation("Nome responsável!");

    if (!self.customer.EmailResponsible) return enableValidation("Email");

    if (!comumConfig.isEmail(self.customer.EmailResponsible))
      return enableValidation("", "Preencha um email válido!");

    return true;
  }

  function enableValidation(fieldName, message) {
    if (message === undefined) message = "Preencha o campo " + fieldName + ".";

    self.errorMessage = message;
    self.error = true;

    return false;
  }

  function statusUpdate(customer) {
    var row = "#row-" + customer.Id;
    var dataCustomer = {
      customerId: customer.Id,
      active: customer.Active,
    };

    comumConfig.blockScreen("", row);

    masterCustomerService
      .statusUpdate(dataCustomer)
      .success(function (data) {
        // console.log(data);
        comumConfig.unblockScreen(row);
      })
      .error(function (data) {
        // console.log(data);
        comumConfig.unblockScreen(row);
      });
  }

  function getUserFromStorage() {
    return JSON.parse(window.localStorage.getItem('USER_HTG'));
  }


  function getUsers(customer) {
    var element = comumConfig.getElement("#users-table-" + customer.Id);
    var notFound = "#users-notfound-" + customer.Id;

    if (!element.hasClass("none")) {
      comumConfig.addClass(element, "none");
      comumConfig.addText(notFound, "");
    } else {
      comumConfig.blockScreen("", "#customer-list");
      comumConfig.removeClass(element, "none");
      masterCustomerService
        .getUsers(customer.Id)
        .success(function (data) {
          if (data.length > 0) {
            comumConfig.addText(notFound, "");
            customer.users = comumConfig.addFullPathUserPhoto(data, "Photo");
          } else {
            comumConfig.addText(notFound, "Nenhum usuário encontrado!");
          }

          comumConfig.unblockScreen("#customer-list");
        })
        .error(function (data) {
          // console.log(data);
          comumConfig.unblockScreen("#customer-list");
        });
    }
  }

  function openForm() {
    if (!self.isOpen) self.isOpen = true;
  }

  function addUsersEmptyArray() {
    self.getCustomers.data.forEach(function (objCustomer, index) {
      objCustomer.users = [];
    });
  }

  function userStatusUpdate(user) {
    var dataUser = {
      userId: user.Id,
      active: user.Active,
    };

    // FIXME: WTF DUPLICADO?
    //  function enviarBriefing() {
    //  //   if (self.profissionaisSelecionados) {
    //       $state.go('salvar-briefing');
    //   //  }
    //   }    

    comumConfig.blockScreen("", "#customer-list");
    scUsuarioService
      .statusUpdate(dataUser)
      .success(function (data) {
        comumConfig.unblockScreen("#customer-list");
      })
      .error(function (data) {
        comumConfig.unblockScreen("#customer-list");
      });
  }

  /** @deprecated esta função não executa nada, só precisa ver se não há dependências */
  function loadCities() {

  }

  function configDropzone() {
    var onInit = {
      onAdd: function (file) {
        console.log(file);
      },
    };

    var dropzone = {
      element: "#my-dropzone",
      url: {
        url: comumConfig.baseUrlAPI() + "/Customer/AttachFile",
        clickable: "#click-upload",
      },
      options: {
        headers: {
          Authorization: "Bearer " + window.localStorage.getItem("ngStorage-token"),
          State: $state.current.name,
        },
        maxFilesize: 2,
        name: "myDropzone",
        init: {
          addedfile: function (file) {},
          success: function (file, data) {
            data = data;
            var imgPath =
              comumConfig.baseUrlAPI() + data.folder + data.filename;
            setLogotipoImage(imgPath);
            self.customer.Logo = data.filename;
            setLogotipoValue(data.filename);
          },
        },
        createImageThumbnails: false,
        previewTemplate: "preview-template",
        previewsContainer: "dropzone-previews",
      },
    };

    comumConfig.addDropzone(dropzone);
  }
  
  function setLogotipoValue(img) {
    comumConfig.getElement("#hdLogotipo")
      .val(img);
  }

  function getLogotipoValue() {
    return comumConfig.getElement("#hdLogotipo")
      .val();
  }

  function setLogotipoImage(imgPath, status) {
    if (!status) {
      var imgHtml = '<img src="' + self.baseURL + '/Content/images/customer/' + imgPath + '">';
      comumConfig.html(".logotipo-empresa", imgHtml);
      comumConfig.addClass(".placeholder-img", "none");
    } else {
      comumConfig.addClass(".placeholder-img", "block");
      comumConfig.html(".logotipo-empresa", "");
    }
  }

  function cancel() {
    isEdit = false;
    self.isOpen = false;
    self.customer = {};
    setLogotipoImage("", true);
    self.error = false;
    self.errorMessage = null;
    self.tableParams.reload();
    $state.reload();
  }

  function configCustomers() {
    if (self.getCustomers.data.length > 0) {
      addUsersEmptyArray();
      self.getCustomers.data = comumConfig.addFullPathUserPhoto(
        self.getCustomers.data,
        "Logo"
      );
    }
  }

  // INIT
  init();
}
