function masterCustomerComponent()
{
	return {
		templateUrl: '../templates/customer/view/customer.html',
		controller: masterCustomerController,		
		bindings: {
			getCustomers: '<',
			getEstados: '<'
		}
	};
}