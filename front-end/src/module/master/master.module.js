/**
* Master Module
*
* Description
*/
angular.module('Master', ['Comum', 'SharedScreen'])

	// Route
	.config(masterRoute)

	// Components
	.component('masterCustomerComponent', masterCustomerComponent())

	// Services
	.service('masterCustomerService', masterCustomerService)