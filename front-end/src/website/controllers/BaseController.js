crowd.controller("BaseController", ["$scope", "$location", "$anchorScroll", "$timeout", "BaseFactory",
    function($scope, $location, $anchorScroll, $timeout, BaseFactory){
    $scope.srcLogo = 'https://crowd.br.com/wp-content/uploads/2018/04/Logo_crowd_branco.png';
    $scope.colorMenu = BaseFactory.colorMenu;
    $scope.changeColorMenu = function(color){
        setTimeout(function(){
            if(color == 'black'){
                $scope.srcLogo = './images/crowd-black.png';
                $jQuery('#link-menu').removeClass('white');
                $jQuery('#logo-crowd img').attr('src', $scope.srcLogo);

            }
            else{
                $scope.srcLogo = 'https://crowd.br.com/wp-content/uploads/2018/04/Logo_crowd_branco.png';
                $jQuery('#link-menu').removeClass('black');
                $jQuery('#logo-crowd img').attr('src', $scope.srcLogo);
            }
            $jQuery('#link-menu').addClass(color);
        }, 10);
        return $scope.colorMenu;
    };

    $scope.goToRegister = function(){
        $timeout(function() {
            jQuery('html,body').animate({scrollTop:
                jQuery('footer').offset().top
            },'slow');
        });
    }
}]);

crowd.factory("BaseFactory", function(){
    var base = {};
    base.colorMenu = 'white';

    return base;
});
