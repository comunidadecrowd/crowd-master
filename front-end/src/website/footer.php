
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script src="//unpkg.com/masonry-layout@4.1/dist/masonry.pkgd.min.js"></script>
<script src="./js/jquery.lazyload.min.js"></script>
<script src="//unpkg.com/imagesloaded@4.1/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" charset="utf-8">
 var $jQuery = $.noConflict();

</script>
<script type="text/javascript" charset="utf-8">
 $jQuery(window).scroll(function() {
  if ($jQuery(this).scrollTop() > 10) {
   $jQuery('header').fadeIn('slow', function() {
    $jQuery('header')
     .attr('style', 'opacity: 1;background: rgba(0, 0, 0, 1);top: 0;position: fixed;')
    angular.element('body').scope().changeColorMenu('white');
   });
  }
  /*else{
      jQuery('header')
           .attr('style', 'opacity: 1;background: none;top: 0;position: absolute;')
  }*/
 });

 $jQuery('.menu-mobile').click(function() {
  $jQuery('.menu-xs-mobile').toggle("slow");
  $jQuery('.login-mobile').hide("slow");
 });
 $jQuery('#cad_empresa').click(function() {
  $jQuery('html, body').animate({
   scrollTop: $jQuery('.texto-form-empresa').position().top
  }, 'slow');
 })
 $jQuery('#cad_profissional').click(function() {
  $jQuery('html, body').animate({
   scrollTop: $jQuery('.facaparte').position().top
  }, 'slow');
 })

 $jQuery('.segments-list').click(function(e) {
  e.stopPropagation();
 });
 $jQuery(window).load(function() {
  $jQuery('header').css('opacity', '1');
 });
 $jQuery('body').delegate('input.input', 'keyup', function() {
  var placeholder = $jQuery('input.input').hasClass('ng-not-empty');
  if (placeholder) {
   $jQuery('.tags').addClass('noafter');
  } else {
   $jQuery('.tags').removeClass('noafter');
  }
 });
 jQuery('body').delegate('.panel-heading>h4>a', 'click', function(e) {
  if (location.href.indexOf('faq') > -1) {
   e.stopPropagation();
   var div = jQuery(jQuery(this).attr('href'));
   if (jQuery(div).css('display') == 'none') {
    jQuery(div).slideDown();
   } else
    jQuery(div).slideUp();
   return false;
  }
 });
 $jQuery('.placeholder-habilidades').keypress(function() {
  var placeholder = $jQuery('input.input').hasClass('ng-not-empty');
  if (placeholder) {
   $jQuery('.tags').addClass('noafter');
  } else {
   $jQuery('.tags').removeClass('noafter');
  }
 });

</script>

<script> 
var $buoop = {
    vs: {i:20},  
    // Specifies browser versions to notify. Negative numbers specify how much versions behind current version to notify.
 
    // f:22 ---> Firefox <= 22
    // c:-5 ---> Chrome <= 35 if current Chrome version is 40.

    reminder: 0,                  
    // after how many hours should the message reappear
    // 0 = mostra o tempo todo

    reminderClosed: 1,           
    // if the user explicitly closes message it reappears after x hours

    onshow: function(infos){
        $jQuery("input").prop("disabled", true);
        $jQuery("button").prop("disabled", true);
        $jQuery("button").css("opacity", ".5");
    },     
    // onclick: function(infos){},
    // onclose: function(infos){},
    // callback functions after notification has appeared / was clicked / closed

    test: false,                    
    // true = sempre mostra a barra (para testes)

    text: "<center>Que pena! Nossa plataforma é muito instável no {brow_name}. Por favor faça seu login em outro navegador.</center>",                       
    // texto de notificação customizado (html)
    // Placeholders {brow_name} will be replaced with the browser name, {up_but} with contents of the update link tag and {ignore_but} with contents for the ignore link.
    // Exemplo: Your browser, {brow_name}, is too old: <a{up_but}>update</a> or <a{ignore_but}>ignore</a>.

    text_xx: "",                    
    // texto de notificação customizado para linguagem "xx"
                                  
    // ex.: text_de para alemão e text_it para italiano

    newwindow: false,                
    // abre o link em uma nova janela/aba

    url: 'https://crowd.br.com',                      
    // the url to go to after clicking the notification

    noclose:true,                  
    // Do not show the "ignore" button to close the notification

    nomessage: false,               
    // Do not show a message if the browser is out of date, just call the onshow callback function

    container: document.body, 
    // Element where the notification will be injected.

    unsecure:true,
    api:4
};
function $buo_f(){ 
 var e = document.createElement("script"); 
 e.src = "//browser-update.org/update.min.js"; 
 document.body.appendChild(e);
};
try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
catch(e){window.attachEvent("onload", $buo_f)}
</script>


</body>

</html>
