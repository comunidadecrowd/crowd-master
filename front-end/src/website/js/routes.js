// crowd.config(function($routeProvider, $locationProvider){
//     $routeProvider.when("/", {
//         title : "",
//         description : "",
//         templateUrl : "views/home.html",
//         controller: 'BaseController'
//     }).when("/manifesto", {
//         title : "Manifesto",
//         description : "",
//         templateUrl : "views/manifesto.html",
//         controller: 'BaseController'
//     }).when("/faq", {
//         title : "Faq",
//         description : "",
//         templateUrl : "views/faq.html",
//         controller: 'BaseController'
//     }).when("/quero-expandir-minha-empresa", {
//         title : "Quero expandir minha empresa",
//         description : "",
//         templateUrl : "views/quero-expandir-minha-empresa.html",
//         controller: 'publicCustomerController'
//     }).when("/quero-ser-profissional", {
//         title : "Quero ser profissional",
//         description : "",
//         templateUrl : "views/quero-ser-profissional.html",
//         controller: 'publicProfessionalController'
//     }).when("/recuperar-senha", {
//         title : "Recuperar senha",
//         description : "",
//         templateUrl : "views/recuperar-senha.html",
//         controller: 'publicController'
//     }).when("/login", {
//         title : "Login",
//         description : "",
//         templateUrl : "views/login.html",
//         controller: 'publicController'
//     }).when("/sobre", {
//         title : "Sobre",
//         description : "",
//         templateUrl : "views/sobre.html",
//         controller: 'BaseController'
//     }).otherwise({redirectTo: '/'});
//     $locationProvider.html5Mode(true);
// });
//
// crowd.run(['$rootScope', function($rootScope) {
//     $rootScope.page = {
//         setTitle: function(title) {
//             this.title = title;
//         },
//         setDescription: function(description){
//             this.description = description;
//         }
//     }
//
//     $rootScope.$on("$routeChangeStart",function(event, next, current){
//         if(next.templateUrl) {
//             ga('send', 'pageview', { page: next.templateUrl });
//         }
//     });
//
//     $rootScope.$on('$routeChangeSuccess', function(event, current, previous) {
//         jQuery('header')
//              .attr('style', 'opacity: 1;background: none;top: 0;position: absolute;');
//         //jQuery('html,body').animate({scrollTop: 0},'slow');
//         if(current.$$route != undefined){
//             $rootScope.page.setTitle(current.$$route.title || 'Crowd');
//             $rootScope.page.setDescription(current.$$route.description);
//         }
//     });
//
// }]);
