<?php include 'header.php' ?>
<section style="background:#fff; padding-top: 50px;">
 <div ng-class="changeColorMenu('black')"></div>
 <div class="container container-login container-cadastro" ng-app="Public" ng-controller="publicController as Public">
  <h2 style="margin-top:50px">Acesse Agora</h2>
  <div>
   <img ng-cloak style="margin-bottom:15px;max-width: 100px;max-height: 100px;" ng-if="!Public.hidePassword" class="img-profile-default radius100" ng-src="{{Public.Photo == null || Public.Photo == '' || Public.Photo == 'null' ? '/images/user.png' : 'http://api.comunidadecrowd.com.br' + Public.Photo}}">
   <br/>
   <strong ng-cloak ng-if="!Public.hidePassword">{{Public.Name}}</strong>&nbsp;<small ng-cloak><a href="" ng-if="!Public.hidePassword" ng-click="((Public.hidePassword = !Public.hidePassword) && (Public.user = undefined))">Não é você?</a></small>
  </div>
  <form name="formLogin" class="login fazer-login" style="margin-top:30px;position: relative;z-index:1000;" ng-submit="Public.login()">
   <div class="row">
    <div class="col-md-12">
     <input ng-if="Public.hidePassword" type="email" placeholder="E-mail" ng-model="Public.user.Email" style="margin-right: 0">

     <input ng-cloak ng-if="!Public.hidePassword" type="password" placeholder="Senha" ng-model="Public.user.Password" ng-required="true" ng-focus="!Public.hidePassword">
    </div>
   </div>
   <div class="row">
    <div class="col-md-12">
     <button type="submit" class="logar-sistema"><i class="fa fa-long-arrow-right"
       aria-hidden="true"></i></button>
    </div>
   </div>
   <span ng-cloak ng-if="!Public.hidePassword && !Public.forgotMessage" class="btn-esqueceu-senha">
    <a href="#"
     ng-click="Public.forgotPassword()">Esqueceu sua senha?</a>
   </span>
   <div ng-cloak ng-show="Public.forgotMessage" class="msg-erro-landing" style="width: 300px;border-color: #3875B2;">
    <span id="text-forgot"></span>
   </div>
   <div ng-cloak ng-show="Public.error" class="msg-erro-landing">
    <i class="fa fa-caret-up" aria-hidden="true"></i> {{Public.errorMessage}}
   </div>
  </form>

  <div class="row" style="display: block">
   <div class="col-md-12">
    <h3 style="font-weight: bold;margin:100px 0 10px 0;">
     Ainda não tem cadastro?
    </h3>
    <br>
    <a href="contratar-freelancer" class="btncadastrar btn-roxo btn-mobile-full">
    				Contratar Freelancers
    			</a>
    <a href="sou-freelancer" class="btncadastrar btncad btn-mobile-full ">
    				Sou Freelancer
    			</a>
   </div>
  </div>
 </div>
</section>
<?php include 'footer.php' ?>
