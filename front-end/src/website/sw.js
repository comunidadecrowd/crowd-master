importScripts('/lib/node_modules/sw-toolbox/sw-toolbox.js');

/**
 * Copyright 2015 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* eslint-env browser */
'use strict';

if ('serviceWorker' in navigator) {
  // Delay registration until after the page has loaded, to ensure that our
  // precaching requests don't degrade the first visit experience.
  // See https://developers.google.com/web/fundamentals/instant-and-offline/service-worker/registration
  window.addEventListener('load', function() {
    // Your service-worker.js *must* be located at the top-level directory relative to your site.
    // It won't be able to control pages unless it's located at the same level or higher than them.
    // *Don't* register service worker file in, e.g., a scripts/ sub-directory!
    // See https://github.com/slightlyoff/ServiceWorker/issues/468
    navigator.serviceWorker.register('service-worker.js').then(function(reg) {
      // updatefound is fired if service-worker.js changes.
      reg.onupdatefound = function() {
        // The updatefound event implies that reg.installing is set; see
        // https://w3c.github.io/ServiceWorker/#service-worker-registration-updatefound-event
        var installingWorker = reg.installing;

        installingWorker.onstatechange = function() {
          switch (installingWorker.state) {
            case 'installed':
              if (navigator.serviceWorker.controller) {
                // At this point, the old content will have been purged and the fresh content will
                // have been added to the cache.
                // It's the perfect time to display a "New content is available; please refresh."
                // message in the page's interface.
                console.log('New or updated content is available.');
              } else {
                // At this point, everything has been precached.
                // It's the perfect time to display a "Content is cached for offline use." message.
                console.log('Content is now available offline!');
              }
              break;

            case 'redundant':
              console.error('The installing service worker became redundant.');
              break;
          }
        };
      };
    }).catch(function(e) {
      console.error('Error during service worker registration:', e);
    });
  });
}

https://crowd.br.com/login/portal/#/profissionais
https://crowd.br.com/login/global/css/pace.flash.css
https://crowd.br.com/login/global/js/pace.min.js
https://crowd.br.com/login/assets/js/autotrack.js
https://crowd.br.com/login/global/css/bootstrap.min.css
https://crowd.br.com/login/global/css/bootstrap-extend.min.css
https://crowd.br.com/login/assets/css/site.css
https://crowd.br.com/login/global/vendor/asscrollable/asScrollable.css
https://crowd.br.com/login/global/vendor/switchery/switchery.css
https://crowd.br.com/login/global/vendor/intro-js/introjs.css
https://crowd.br.com/login/global/vendor/slidepanel/slidePanel.css
https://crowd.br.com/login/global/vendor/flag-icon-css/flag-icon.css
https://crowd.br.com/login/global/vendor/asrange/asRange.css
https://crowd.br.com/login/global/fonts/web-icons/web-icons.min.css
https://crowd.br.com/login/global/fonts/brand-icons/brand-icons.min.css
https://crowd.br.com/login/global/vendor/modernizr/modernizr.js
https://crowd.br.com/login/global/vendor/breakpoints/breakpoints.js
https://crowd.br.com/login/css/font/font-awesome.min.css
https://crowd.br.com/login/css/theme.min.css
https://crowd.br.com/login/css/main.min.css
https://crowd.br.com/login/portal/images/user.png
https://crowd.br.com/login/global/vendor/jquery/jquery.js
https://crowd.br.com/login/global/vendor/babel-external-helpers/babel-external-helpers.js
https://crowd.br.com/login/global/vendor/tether/tether.min.js
https://crowd.br.com/login/global/vendor/bootstrap/bootstrap.js
https://crowd.br.com/login/global/vendor/animsition/animsition.js
https://crowd.br.com/login/global/vendor/asscrollable/jquery.asScrollable.all.js
https://crowd.br.com/login/global/vendor/select2/select2.full.min.js
https://crowd.br.com/login/global/vendor/switchery/switchery.min.js
https://crowd.br.com/login/global/vendor/screenfull/screenfull.js
https://crowd.br.com/login/global/vendor/slidepanel/jquery-slidePanel.js
https://crowd.br.com/login/global/js/State.js
https://crowd.br.com/login/global/js/Component.js
https://crowd.br.com/login/global/js/Plugin.js
https://crowd.br.com/login/global/js/Base.js
https://crowd.br.com/login/global/js/Config.js
https://crowd.br.com/login/js/bootstrap.min.js
https://crowd.br.com/login/js/main.min.js
https://crowd.br.com/login/js/dropzone.min.js
https://crowd.br.com/login/assets/js/placeholderTypewriter.js
https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css
https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600
https://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic
https://cdnjs.cloudflare.com/ajax/libs/select2/3.4.5/select2.css
https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.8.5/css/selectize.default.css
https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js
https://cdn.jsdelivr.net/algoliasearch/3/algoliasearch.min.js
https://cdn.jsdelivr.net/npm/instantsearch.js@1/dist/instantsearch.min.js
https://cdn.jsdelivr.net/algoliasearch/3/algoliasearchLite.min.js
https://cdn.jsdelivr.net/lodash/4.17.4/lodash.min.js
https://cdnjs.cloudflare.com/ajax/libs/angulartics/1.4.0/angulartics.min.js
https://cdnjs.cloudflare.com/ajax/libs/angulartics/1.4.0/angulartics-intercom.min.js
https://crowd.br.com/login/images/crowd.png
https://fonts.gstatic.com/s/opensans/v14/cJZKeOuBrn4kERxqtaUH3ZBw1xU1rKptJj_0jans920.woff2
https://api.crowd.br.com/Content/images/user/90c76722-491a-41a2-a1d0-592927a500b5.jpeg
https://crowd.br.com/login/global/fonts/web-icons/web-icons.woff?v=0.2.3
https://crowd.br.com/login/fonts/fontawesome-webfont.woff?v=4.3.0
https://crowd.br.com/login/templates/profissional/view/profissional.html
https://crowd.br.com/login/images/slider-ico.jpg
https://cdn.jsdelivr.net/instantsearch.js/1/instantsearch.min.css
https://crowd.br.com/login/templates/visualizar-perfil-profissional/view/visualizar-perfil-profissional.html
https://cdnjs.cloudflare.com/ajax/libs/select2/3.4.5/select2.png
https://crowd.br.com/login/images/user.png