<?php include 'header.php' ?>
<div ng-class="changeColorMenu('black')"></div>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<section class="container container-login container-cadastro" ng-controller="publicController as Public">
	<div class="container" style="padding-top: 50px;">
		<div class="container-faq">
			<h1>Recuperar Senha</h1>
			<p>Digite seu email no campo abaixo para redefinir sua senha.</p>
		</div>

		<div class="container">
			<div class="col-sm-4" style="float:none;margin:auto; text-align:center;" ng-show="!Public.success">
				<form class="login fazer-login">
					<div class="row">
						<input class="form-control" type="email" name="email" ng-model="Public.email" placeholder="seu-email@email.com.br">
						<br>
						<input class="form-control" type="password" name="password" ng-model="Public.Password" ng-if="Public.newPassword" placeholder="Informe a nova senha">
						<button class="btn btn-primary" type="submit"
							name="validate" ng-click="Public.validadeHash()" ng-if="!Public.newPassword">Validar email</button>
						<button class="btn btn-primary" type="submit"
							name="recover" ng-click="Public.recoverPassword()" ng-if="Public.newPassword">Recuperar senha</button>
					</div>
				</form>
			</div>

			<div ng-show="Public.forgotError" class="col-sm-6" style="float:none;margin:auto; margin-top: 20px;">
                <div class="alert alert-danger">
                    <strong>Oooops...</strong> {{Public.forgotMessage}}
                </div>
            </div>

            <div class="col-sm-6" style="float:none;margin:auto; margin-top: 20px;">
                <div class="alert alert-success" ng-show="Public.success">
                    <strong>Prontinho...</strong> Agora basta acessar a plataforma com seus novos dados.
                </div>
            </div>

		</div>

	</div>
</section>
<?php include 'footer.php' ?>
