<?php
$useragent=$_SERVER['HTTP_USER_AGENT'];
$uriTitle = explode("/",$_SERVER["REQUEST_URI"])[2];
$uriName = explode("/",$_SERVER["REQUEST_URI"])[3];
if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) {
  $BASE_URL = "https://crowd.br.com/";
  $urlAMP = $BASE_URL . "portfolio-online/" . $uriTitle . "/" . $uriName . "/";
  header('Location: '.$urlAMP);
}

  $API_URL = "https://api.crowd.br.com/";

  /**
  * VALIDA A HASH E AUTENTICA O USUÁRIO
  */

  $url = $API_URL . "portfolio-online/" . $uriTitle . "/" . $uriName . "/full";

  $ch = curl_init();
  // Disable SSL verification
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  // Will return the response, if false it print the response
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  // Set the url
  curl_setopt($ch, CURLOPT_URL,$url);
  // Execute
  $result=curl_exec($ch);
  // Closing
  curl_close($ch);    

  $user = json_decode($result, true);
  
?>
<!DOCTYPE html>
<html ng-app="openProfile" xmlns:ng="http://angularjs.org">
  <head>
    <meta charset="utf-8">
    <title><?=$user['Name']?> - <?=$user['Title']?> - CROWD</title>
    <meta name="description" content="<?=$user['Description']?>">
    <base href="./" />
    <meta name="author" content="CROWD">
    <meta name="viewport" content="width=device-width,minimum-scale=1">
    <link rel="canonical" href="https://www.crowd.br.com/portfolio-online/<?=$uriTitle?>/<?=$uriName?>/Full">
    <link rel="amphtml" href="https://www.crowd.br.com/portfolio-online/<?=$uriTitle?>/<?=$uriName?>/">
    <!-- <link rel="stylesheet" href="css/styles.css?v=1.0"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
    <link rel="stylesheet" href="/portfolio-online/css/styles.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.8/angular.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/lodash@4.17.4/lodash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/1.0.3/angular-ui-router.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ng-meta/1.0.2/ngMeta.min.js"></script>
    <script>
      var user = <?=$result?>;
      var myApp = angular.module('openProfile', ['ui.router', 'ngMeta']);
    </script>
    <!--[if lt IE 9]>
      <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- <meta property="fb:app_id" content="302184056577324" />  -->
    <meta name="twitter:card" content="summary" />
    <meta property="og:type"   content="profile" /> 
    <meta property="og:url"    content="<?=$_SERVER["REQUEST_URI"]?>" /> 
    <meta property="og:title"  content="<?=$user['Name']?> - <?=$user['Title']?>" /> 
    <meta property="og:image"  content="https://api.crowd.br.com/<?=$user['Photo']?>" />
    <meta property="og:description" content="<?=$user['Description']?>" />

    <script type="application/ld+json">
          {
            "@context": "http://schema.org",
            "@type": "Person",
            "name": "<?=$user['Name']?>",
            "givenName": "<?=$user['Name']?>",
            "homeLocation": "<?=$user['City']?>",
            "jobTitle": "<?=$user['Title']?>",
            "estimatedSalary":"<?=$user['Price']?>",
            "pricerange":"<?=$user['Price']?>",
            "skills":[
              <?php
                $i = 0;
                $len = count($user['Skills']);
                foreach ($user['Skills'] as $item) {
                  if ($i == $len - 1) {
                    echo '"'.$item.'"';
                  } else {
                    echo '"'.$item .'"'.",";
                  }
                  $i++;
                }
              ?>
            ],
            "hasOccupation": [{"name":"<?=$user['Title']?>", "estimatedSalary":"<?=$user['Price']?>","skills":[
              <?php
                $i = 0;
                $len = count($user['Skills']);
                foreach ($user['Skills'] as $item) {
                  if ($i == $len - 1) {
                    echo '"'.$item.'"';
                  } else {
                    echo '"'.$item .'"'.",";
                  }
                  $i++;
                }
              ?>
              ]
            }],
            "worksFor": "<?=$user['Experiencies'][0]['Company']?>",
            "description": "<?=$user['Description']?>",
            "email": "<?=$user['Email']?>",
            "member": "CROWD",
    		    "url": "<?=$user['Canonical']?>",
            "offeredBy": "CROWD",
            "follows":[
              <?php
                $i = 0;
                $len = count($user['Segments']);
                foreach ($user['Segments'] as $item) {
                  if ($i == $len - 1) {
                    echo '"'.$item.'"';
                  } else {
                    echo '"'.$item .'"'.",";
                  }
                  $i++;
                }
              ?>  
            ],
            "image": "https://api.crowd.br.com/<?=$user['Photo']?>",
    		    "makesOffer":"<?=$user['Categoria']?>",
    		    "seeks": "freelancer",
            "reviewedBy": "CROWD",
            "ratingValue": "<?=$user['Rating']?>"
          }
        </script>

        <style>
          [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
            display: none !important;
          }
        </style>

  </head>
  <body ng-cloak ng-controller="OpenProfileController" itemscope="" itemtype="http://schema.org/Person">
     <main class="main-container🔥">
       <div class="container-center">
         <div class="logo-box">
            <a href="/"><img src="//crowd.br.com/wp-content/uploads/2017/03/logo-crowd-black-1.png" alt="Logo Crowd"></a>
         </div>
         <div class="left-col">
                 <div class="info-card">
                    <div class="img-profile-default">
                      <div style="background:url({{photos.user}});width: 100%!important;height: 80px!important;background-position: center!important;background-size: cover!important;"></div>
                    </div>
                    <div class="action-data">
                      <div class="star-rating" 
                      ng-class="{ 'rating-zero': user.Rating === 0, 'rating-five': user.Rating === 5, 'rating-four': user.Rating === 4, 'rating-three': user.Rating === 3, 'rating-two': user.Rating === 2, 'rating-one': user.Rating === 1 } "
                       style="background: url('/portfolio-online/regua-rating.svg'); background-size: cover;height: 17px;padding-right: 4px; background-color: #F3A64B; border: 1px solid white;">
                      </div>
                      <h1 itemprop="name" class="user-name">{{user.Name}}</h1>
                      <h2 class="user-role">{{user.Title}} </h2>
                    </div>
                    <a ga-on="click" ga-event-category="CTA" ga-event-action="Selecionar Profissional" class="primary-btn right-block" href="//crowd.br.com/contratar-freelancers/">
                      <i class="fa fa-check" aria-hidden="true"></i>
                      Contratar {{user.FirstName}}
                     </a>
                 </div>
                 <div class="text-block" style="white-space: pre-line; margin-bottom: 30px;">{{ user.Description }}</div>
                 <h3 class="section-subheading">
                    Atividades e Habilidades
                  </h3>
                  <div class="left-text">
                    <span class="skill-tag" ng-repeat="tag in user.Skills"> {{ tag }} </span>
                  </div>
                  <div class="left-bar">
                    <h4 class="info-category">
                      <i class="fa fa-map-marker" aria-hidden="true"></i>
                      {{user.State}}, {{user.City}}
                    </h4>
                    <h4 class="info-category">
                      Disp. {{user.Availability}}
                    </h4>
                    <h4 itemprop="pricerange" class="info-category">
                      R${{user.Price}}/h
                    </h4>
                  </div>
                  <div class="left-text section-block">
                      <h4 class="info-category no-bottom"><span class="category-title">Categoria: </span> {{user.Category}}</h4>
                      <h4 class="info-category no-bottom"><span class="category-title">Segmento: </span> Artes & Design</h4>
                      <h4 class="info-category no-bottom"><span class="category-title">Idiomas: </span><span ng-repeat="language in user.Languages"> {{language}},</span></h4>
                    </div>
                    <h3 class="section-subheading">
                      Experiência Profissional
                    </h3>
                    <div class="exp-block" ng-if="!user.Experiences[0].Company">
                      <h3 class="no-portfolio">O usuário ainda não cadastrou sua experiência.</h3>
                    </div>
                    <div class="exp-block" ng-if="user.Experiences" ng-repeat="experience in user.Experiences">
                      <div class="row">
                        <div class="left-block bottom-pad">
                           <h3 class="exp-role">{{experience.Role}}</h3>
                           <h3 class="exp-company">{{experience.Company}}</h3>
                        </div>
                        <div ng-show="experience.StartDate" class="left-block right-text">
                         <h3 class="exp-date">{{experience.StartDate |  date:'MM/yyyy' }} - 
                          <span ng-if="experience.EndDate">{{ experience.EndDate |  date:'MM/yyyy' }}</span>
                          <span ng-hide="experience.EndDate">Atual</span>
                        </h3>
                        </div>
                      </div>
                      <div class="text-block">{{ experience.Description }}</div>
                    </div>

         </div>
         <div class="right-col">
          <div class="row">
            <div class="left-block">
              <h3 class="section-subheading-2">
                Redes Sociais
              </h3>
            </div>
            <div itemscope itemtype="http://schema.org/Organization" class="left-block right-text">
              <h3 ng-show="user.Portfolio" class="section-subheading-2">
                Portfolio completo:
                  <a itemprop="url" ng-show="user.Portfolio" style="padding-left:3%" ng-href="{{user.Portfolio}}">{{user.Portfolio}}</a>
              </h3>
            </div>
          </div>
          <div itemscope itemtype="http://schema.org/Organization" class="redes-sociais-icones" style="">
            <a itemprop="sameAs" ng-href="{{ user.TwitterUrl }}" target="_blank"><i ng-class="{twitter: user.TwitterUrl}"  class="fa fa-twitter"></i></a>
            <a itemprop="sameAs" ng-href="{{ user.FacebookUrl }}" target="_blank"><i ng-class="{facebook: user.FacebookUrl}" class="fa fa-facebook"></i></a>
            <a itemprop="sameAs" ng-href="{{ user.DribbbleUrl }}" target="_blank"><i ng-class="{dribbble: user.DribbbleUrl}" class="fa fa-dribbble"></i></a>
            <a itemprop="sameAs" ng-href="{{ user.InstagramUrl }}" target="_blank"><i ng-class="{instagram: user.InstagramUrl}" class="fa fa-instagram "></i></a>
            <a itemprop="sameAs" ng-href="{{ user.LinkedinUrl }}" target="_blank"><i ng-class="{linkedin: user.LinkedinUrl}" class="fa fa-linkedin"></i></a>
            <a itemprop="sameAs" ng-href="{{ user.GithubUrl }}" target="_blank"><i ng-class="{github: user.GithubUrl}" class="fa fa-github"></i></a>
            <a itemprop="sameAs" ng-href="{{ user.BehanceUrl }}"><i ng-class="{behance: user.BehanceUrl}" class="fa fa-behance"></i></a>
          </div>
          <h3 class="section-subheading last-eval">
              Quem já avaliou:
          </h3>
          
          <div class="row row-rating" ng-if="user.Ratings">
           <div class="companies-block-mini">
            <img ng-if="uniq.Logo" ng-repeat="uniq in uniq" ng-src="{{BASE_URL}}{{uniq.Logo}}" class="company-logo">
            <h3 ng-if="!uniq" class="no-portfolio">Profissional ainda não recebeu avaliações.</h3>
           </div>
          </div>
          <h3 class="section-subheading last-eval" ng-if="!user.Ratings">
              O que estão dizendo:
          </h3>
          <div ng-if="rating.Description != ''" class="row row-rating" ng-if="user.Ratings" ng-repeat="rating in user.Ratings">
           <!-- <div class="left-block-mini">
            <img ng-if="rating.Logo" ng-src="{{BASE_URL}}{{rating.Logo}}" class="rating-company" alt="">
            <img ng-if="!rating.Logo" ng-src="https://crowd.br.com/images/user.png" class="rating-company" alt="">
           </div> -->
           <div itemprop="review" itemscope itemtype="http://schema.org/Review" class="left-block-mini-2">
             <h4 class="project-heading" ng-if="rating.taskTitle">Finalizou o projeto: <span itemprop="name" class="project-name">"{{rating.taskTitle}}"</span></h4>
             <h5 itemprop="author" class="person-name display-none">{{ rating.Company }}</h5>
             <div itemprop="description" class="text-block">{{rating.Description}}</div>
             <div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating" class="star-rating" 
                ng-class="{ 'rating-zero': rating.Rating === 0, 'rating-five': user.Rating === 5, 'rating-four': user.Rating === 4, 'rating-three': user.Rating === 3, 'rating-two': user.Rating === 2, 'rating-one': user.Rating === 1 } "
                style="background: url('/portfolio-online/regua-rating.svg'); background-size: cover;height: 17px;padding-right: 4px; background-color: #F3A64B; border: 1px solid white;">
              </div>
           </div>
           <div class="left-block-mini right-text">
              <h3 class="exp-date" ng-if="rating.TaskDate">
                <meta itemprop="datePublished" content="{{rating.TaskDate}}">
                <span class="date-border">{{rating.TaskDate}}</span>
              </h3>
              <!-- div class="star-rating rated-stars" style="background: url('regua-rating.svg'); background-size: cover;width: 130px;height: 17px;padding-right: 4px; background-color: #F3A64B; border: 1px solid white;"></div> -->
           </div>
          </div>
          <br>
         </div>   
       </div>
      </main>
      <footer class="footer">
        <div class="left-block left-pad">
          <ul>
            <li class="menu-item">
              <a href="//crowd.br.com/contratar-freelancers/">
                Contratar Freelancers
              </a>
            </li>
            <li class="menu-item">
              <a href="//crowd.br.com/contratar-a-crowd/">
                Contratar a Crowd
              </a>
            </li>
            <li class="menu-item">
              <a href="//crowd.br.com/sou-freelancer/">
                Sou Freelancer
              </a>
            </li>
            <li class="menu-item">
              <a href="//crowd.br.com/blog">
                 Blog
              </a>
            </li>
          </ul>
        </div>
        <div class="left-block right-text w-38 right-footer">
          <h5>© 2017 </h5>
          <a href="/"><img src="//crowd.br.com/wp-content/uploads/2017/03/logo-crowd-black-1.png" alt=""></a>
        </div>
      </footer>
      <script src="/portfolio-online/js/scripts.js" type="application/javascript"></script>
      
      <script data-cfasync="true" src="//www.google-analytics.com/analytics.js"></script>
      <script data-cfasync="true" src="/assets/js/autotrack.js"></script>
      <script>
        window.ga = window.ga || function () {
          (ga.q = ga.q || [])
            .push(arguments)
        };
        ga.l = +new Date;
        ga('create', 'UA-51170795-3', 'auto');

        // Replace the following lines with the plugins you want to use.
        ga('require', 'cleanUrlTracker', {
            stripQuery: true,
            trailingSlash: 'add',
            indexFilename: 'index.html'
        });
        ga('require', 'eventTracker', ['click', 'cadastra', 'busca', 'viu', 'abriu', 'respondeu']);
        ga('require', 'maxScrollTracker');
        ga('require', 'mediaQueryTracker');
        ga('require', 'outboundFormTracker');
        ga('require', 'outboundLinkTracker', {
          events: ['click', 'auxclick', 'contextmenu'],
            shouldTrackOutboundLink: function (link, parseUrl) {
              var href = link.getAttribute('href') || link.getAttribute('xlink:href');
              var url = parseUrl(href);
              return !/crowd?/.test(url.hostname);
            }
          });
        ga('require', 'pageVisibilityTracker', {
          sendInitialPageview: true,
          pageLoadsMetricIndex: 1,
          visibleMetricIndex: 2,
        });
        ga('require', 'urlChangeTracker');

        ga('send', 'pageview');

      </script>

  </body>
</html>
