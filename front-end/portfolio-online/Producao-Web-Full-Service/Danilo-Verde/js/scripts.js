 myApp
.factory('openProfileService', ['$http', '$state', '$stateParams', function($http, $state, $stateParams) {
  // Public
  this.getProfileData = getProfileData;
  console.debug($state);
  console.debug($stateParams);
    var self = this;
  
    // Private
    var API_URL = "https://api.crowd.br.com/";
    var SITE_URL = "https://crowd.br.com/";
    // var USER_ROLE = "/" + $stateParams.freelancerRole;
    // var USER_NAME = "/" + $stateParams.freelancerName;
    var titulo = window.location.href.split('/')[4];
    var nome = window.location.href.split('/')[5];
    // self.profileUrl = API_URL + "/portfolio-online" + USER_ROLE + USER_NAME;
  

  function getProfileData(professionalId) {
    return $http.get(API_URL + "portfolio-online/" + titulo + "/" + nome + "/full");
  }

  return this;
}]).controller('OpenProfileController', ['openProfileService', '$scope', '$state', function(OpenProfileService, $scope, $state) {
    $scope.user = {};
    $scope.photos = {};
    $scope.url = {
        role: window.location.href.split('/')[4],
        name: window.location.href.split('/')[5]
    };
    setTimeout(function() {
      console.debug('user', $scope.user);
    }, 600);
    // console.debug($state.current);
    // console.debug($state.params);
    $scope.profileUrl = OpenProfileService.profileUrl;    
    var BASE_URL = "https://api.crowd.br.com";
    $scope.BASE_URL = BASE_URL;
    

  function init() {

    
    $scope.user = window.user;
    $scope.photos = {
      user: BASE_URL + $scope.user.Photo
    };

    $scope.uniq = _.uniqBy(user.Ratings, 'Logo');

    }

    init();    

  }]);