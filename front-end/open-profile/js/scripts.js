myApp
.factory('openProfileService', ['$http', '$state', '$stateParams', function($http, $state, $stateParams) {
  // Public
  this.getProfileData = getProfileData;
  console.debug($state);
  console.debug($stateParams);
  var self = this;

  // Private
  var API_URL = "http://homolog.api.comunidadecrowd.com.br/";
  var SITE_URL = "https://crowd.br.com/";
  var USER_ROLE = "/" + $stateParams.freelancerRole;
  var USER_NAME = "/" + $stateParams.freelancerName;
  self.profileUrl = API_URL + "/portfolio-online" + USER_ROLE + USER_NAME;
  

  function getProfileData(professionalId) {
    return $http.get(self.profileUrl);
  }

  return this;
}]).controller('OpenProfileController', ['openProfileService', '$scope', function(OpenProfileService, $scope) {
    $scope.user = {};
    $scope.photos = {};
    $scope.profileUrl = OpenProfileService.profileUrl;    
    var BASE_URL = "http://homolog.api.comunidadecrowd.com.br";
     /* name: 'Anne Bione',
      role: 'Diretora de Arte',
      City: {
        Id: 1,
        Name: 'São Paulo'
      },
      State: {
        Id: 1,
        Name: 'São Paulo',
        UF: 'SP'
      },
      Availability: 'Integral',
      Price: 100,
      Category: 'Design',
      Skills: ['HTML5', 'CSS3', 'JavaScript', 'PHP', 'MySQL', 'JavaScript', 'PHP', 'MySQL'],
      Segments: ['Artes & Design'],
      Languages: ['Inglês', 'Espanhol'],
      Experiences: [
        {
          Company: 'AD.Dialeto',
          Role: 'Diretora de Arte',
          StartDate: 'Jun/2010',
          EndDate: 'Nov/2016',
          Description: 'Nullam eu dictum erat, sed dignissim sapien. Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..'
        },
        {
          Company: 'AD.Dialeto',
          Role: 'Diretora de Arte',
          StartDate: 'Jun/2010',
          EndDate: 'Nov/2016',
          Description: 'Nullam eu dictum erat, sed dignissim sapien. Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..'
        }
      ],
      Portfolio: 'https://github.com/umportfolio'
    }; */

    function init() {
     OpenProfileService.getProfileData()
       .then(function(data, status) {
         console.debug(data, status);
         $scope.user = data.data;
         $scope.photos = {
           user: BASE_URL + $scope.user.Photo
         }
         console.debug($scope.photos);
       })

       console.debug($scope.user);
    }

    init();    

  }]).config(function($stateProvider) {
    $stateProvider.state('user.profile', {
      url: "/portfolio-online/{freelancerRole}/{freelancerName}",
      views: {
        templateUrl: '../profile.html',
        controller: 'openProfileController'
      }
    })
   });
