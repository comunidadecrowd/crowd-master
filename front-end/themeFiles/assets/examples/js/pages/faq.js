/*!
 * Remark (http://getbootstrapadmin.com/remark)
 * Copyright 2016 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(angular, document, window, $) {
  angular.module('site').controller('FaqCtrl', function() {

    if ($('.faq-list').length) {
      $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        $(e.target).addClass('active').siblings().removeClass('active');
      });
    }
  });

})(angular, document, window, jQuery);
