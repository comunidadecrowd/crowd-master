/*!
 * Remark (http://getbootstrapadmin.com/remark)
 * Copyright 2016 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(angular, document, window, $) {
  angular.module('site').controller('HighlightController', function() {
    hljs.initHighlighting();
  });
})(angular, document, window, jQuery);
