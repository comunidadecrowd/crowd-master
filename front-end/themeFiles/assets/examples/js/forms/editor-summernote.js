/*!
 * Remark (http://getbootstrapadmin.com/remark)
 * Copyright 2016 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(angular, document, window, $) {
  angular.module('site').controller('SummernoteCtrl', function() {

    window.edit = function() {
      $('.click2edit').summernote();
    };
    window.save = function() {
      $('.click2edit').destroy();
    };
  });
})(angular, document, window, jQuery);
