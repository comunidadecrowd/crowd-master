## Iniciando o projeto (front-end): ##

```
#!bash

npm I -g yarn
npm I -g bower
npm I -g gulp
yarn
bower install
gulp build && gulp

Em outro terminal
cd dist/
php -S localhost:8080 (ou abrir outro servidor http simples)
```

**Abra o navegador em: localhost:8080**

### **Instalar** o git-flow, ou usar alguma GUI compatível *(ex.: GitKraken)* ###

## Estrutura das branches: ##

### Principais ###
*   **Master** - Produção
*   **Develop** - Homologação

### Gitflow ###
*   **Feature** - Desenvolvimento
*   **Hotfix** - Bugs em produção
*   **Release** - Versão RC/Beta

O fluxo é abrir de Master uma nova **feature/hotfix** branche somente para a tarefa atual, testar e concluir realizando um **merge** em uma **release branche** também de master, realizar um **teste de implementação** subindo para o ambiente de homologação, por fim enviar um **pull request** da sua **release branche**.
O desenvolvimento deve acontecer localmente em sua máquina, inclusive todos os testes que não forem de implementação da release.
Você pode subir no ambiente de homologação para testar sua branche quando em **momento de teste de implementação e Pull Request**.
**Todos** os envios para as branches Develop e Master são através de **Pull Request**.