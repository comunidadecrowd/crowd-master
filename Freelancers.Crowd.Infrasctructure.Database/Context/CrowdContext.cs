﻿using Freelancers.Crowd.Domain.Entities;
using Freelancers.Crowd.Infrasctructure.Database.Context.Configuration;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;

namespace Freelancers.Crowd.Infrasctructure.Database.Context
{
    public class CrowdContext : DbContext, IDisposable
    {
        public CrowdContext(): base("name=Connection")
        {}

        public static CrowdContext ConnectionInstance { get; set; }

        public DbSet<User> User { get; set; }
        public DbSet<User.ActionLog> ActionLog { get; set; }
        public DbSet<Customer> Customer { get; set; }
        public DbSet<Customer.Tracking> Tracking { get; set; }

        public DbSet<Freelancer> Freelancer { get; set; }
        public DbSet<Freelancer.Experience> Experience { get; set; }
        public DbSet<Freelancer.Category> Category { get; set; }
        public DbSet<Freelancer.Skill> Skill { get; set; }
        public DbSet<Freelancer.Segment> Segment { get; set; }
        public DbSet<Freelancer.RatingValue> Rating { get; set; }
        public DbSet<Freelancer.Company> Company { get; set; }
        public DbSet<Freelancer.Award> Award { get; set; }
        public DbSet<Freelancer.Portfolio> Portfolio { get; set; }
        public DbSet<Freelancer.Payment> Payment { get; set; }
        public DbSet<Freelancer.Link> Link { get; set; }
        public DbSet<Freelancer.Language> Language { get; set; }

        public DbSet<Briefing> Briefing { get; set; }
        public DbSet<Briefing.Attach> BriefingAttach { get; set; }
        public DbSet<Briefing.Message> Message { get; set; }
        public DbSet<Briefing.ReadBriefing> ReadBriefing { get; set; }
        public DbSet<Campaign> Campaign { get; set; }
        public DbSet<CampaignParticipation> CampaignParticipation { get; set; }
        public DbSet<State> State { get; set; }
        public DbSet<City> City { get; set; }
        public DbSet<Briefing.Complement> Complement { get; set; }
        public DbSet<ComplementAttach> ComplementAttach { get; set; }
        public DbSet<Project> Project { get; set; }
        public DbSet<Briefing.Propose> Propose { get; set; }
        public DbSet<Tasks> Tasks { get; set; }
        public DbSet<TasksAttach> TasksAttach { get; set; }
        public DbSet<MessageTasks> MessageTasks { get; set; }
        public DbSet<MessageTaskAttachs> MessageTaskAttachs { get; set; }

        public DbSet<SystemMessage> SystemMessage { get; set; }
        public DbSet<SystemMessage.ReadSystemMessage> ReadSystemMessage { get; set; }
        public DbSet<BePay> BePay { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            
            modelBuilder.Properties<string>()
                .Configure(p => p.HasColumnType("varchar"));

            modelBuilder.Configurations.Add(new CustomerConfiguration());
            modelBuilder.Configurations.Add(new TrackingConfiguration());

            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new ActionLogConfiguration());

            modelBuilder.Configurations.Add(new FreelancerConfiguration());
            modelBuilder.Configurations.Add(new ExperienceConfiguration());
            modelBuilder.Configurations.Add(new BriefingConfiguration());
            modelBuilder.Configurations.Add(new BriefingAttachConfiguration());
            modelBuilder.Configurations.Add(new CategoryConfiguration());
            modelBuilder.Configurations.Add(new SkillConfiguration());
            modelBuilder.Configurations.Add(new SegmentConfiguration());
            modelBuilder.Configurations.Add(new MessageConfiguration());
            modelBuilder.Configurations.Add(new RatingConfiguration());
            modelBuilder.Configurations.Add(new ReadBriefingConfiguration());

            modelBuilder.Configurations.Add(new CampaignConfiguration());
            modelBuilder.Configurations.Add(new CampaignParticipationConfiguration());

            modelBuilder.Configurations.Add(new StateConfiguration());
            modelBuilder.Configurations.Add(new CityConfiguration());


            modelBuilder.Configurations.Add(new ComplementConfiguration());
            modelBuilder.Configurations.Add(new ComplementAttachConfiguration());

            modelBuilder.Configurations.Add(new ProjectConfiguration());
            modelBuilder.Configurations.Add(new ProposeConfiguration());

            modelBuilder.Configurations.Add(new TasksConfiguration());
            modelBuilder.Configurations.Add(new TasksAttachConfiguration());
            modelBuilder.Configurations.Add(new MessageTasksConfiguration());
            modelBuilder.Configurations.Add(new MessageTasksAttachsConfiguration());

            modelBuilder.Configurations.Add(new AwardsConfiguration());
            modelBuilder.Configurations.Add(new PortfolioConfiguration());

            modelBuilder.Configurations.Add(new CompanyConfiguration());

            modelBuilder.Configurations.Add(new PaymentConfiguration());
            modelBuilder.Configurations.Add(new LinkConfiguration());
            modelBuilder.Configurations.Add(new LanguageConfiguration());

            modelBuilder.Configurations.Add(new SystemMessageConfiguration());
            modelBuilder.Configurations.Add(new ReadSystemMessageConfiguration());

            modelBuilder.Configurations.Add(new BePayConfiguration());

        }
    }
}
