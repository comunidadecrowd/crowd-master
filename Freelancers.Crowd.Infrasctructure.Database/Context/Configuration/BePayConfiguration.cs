﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Freelancers.Crowd.Domain.Entities;

namespace Freelancers.Crowd.Infrasctructure.Database.Context.Configuration
{
    public class BePayConfiguration : BaseEntityConfiguration<BePay>
    {
        public BePayConfiguration()
            : base("crowd_bepay")
        {
            HasRequired(x => x.Customer)
                .WithMany()
                .HasForeignKey(x => x.IdCustomer);

            HasRequired(x => x.User)
                .WithMany()
                .HasForeignKey(x => x.IdUser);

            Property(x => x.IdUser)
                .HasColumnName("id_user")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.IdCustomer)
                .HasColumnName("id_costumer")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.PlanType)
                .HasColumnName("plan_type")
                .HasColumnType("int");

            Property(x => x.BePayType)
                .HasColumnName("payment_type")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.Transaction)
                .HasColumnName("transactionId")
                .HasColumnType("varchar")
                .HasMaxLength(50)
                .IsRequired();

            Property(x => x.Token)
                .HasColumnName("token")
                .HasColumnType("varchar")
                .HasMaxLength(50);

            Property(x => x.ReadableLine)
                .HasColumnName("readableline")
                .HasColumnType("varchar")
                .HasMaxLength(50);

            Property(x => x.FinancialStatus)
                .HasColumnName("financial_status")
                .HasColumnType("varchar")
                .HasMaxLength(20);

            Property(x => x.AuthorizationNumber)
                .HasColumnName("authorization_number")
                .HasColumnType("varchar")
                .HasMaxLength(50);
        }
    }
}
