﻿using Freelancers.Crowd.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Freelancers.Crowd.Infrasctructure.Database.Context.Configuration
{
    public class StateConfiguration : BaseEntityConfiguration<State>
    {
        public StateConfiguration()
            :base("crowd_state")
        {
            Property(x => x.Name)
                .HasColumnName("name")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsRequired();

            Property(x => x.UF)
                .HasColumnName("uf")
                .HasColumnType("varchar")
                .HasMaxLength(2)
                .IsRequired();            
        }
    }
}
