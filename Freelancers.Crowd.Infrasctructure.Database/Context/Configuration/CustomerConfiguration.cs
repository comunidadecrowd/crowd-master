﻿using Freelancers.Crowd.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Freelancers.Crowd.Infrasctructure.Database.Context.Configuration
{
    public class CustomerConfiguration : BaseEntityConfiguration<Customer>
    {
        public CustomerConfiguration()
            : base("crowd_customers")
        {
            HasRequired(x => x.State)
                .WithMany()
                .HasForeignKey(x => x.StateId);

            HasRequired(x => x.City)
                .WithMany()
                .HasForeignKey(x => x.CityId);

            Property(x => x.Name)
                .HasColumnName("name")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsRequired();

            Property(x => x.Trade)
                .HasColumnName("trade")
                .HasColumnType("varchar")
                .HasMaxLength(255);

            Property(x => x.Logo)
                .HasColumnName("logo")
                .HasColumnType("varchar")
                .HasMaxLength(255);

            Property(x => x.CNPJ)
                .HasColumnName("cnpj")
                .HasColumnType("varchar")
                .HasMaxLength(20)
                .IsOptional();

            Property(x => x.QtyPersons)
                .HasColumnName("qty_persons")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.StateId)
                .HasColumnName("id_state")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.CityId)
                .HasColumnName("id_city")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.NameResponsible)
                .HasColumnName("name_responsible")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsRequired();

            Property(x => x.EmailResponsible)
                .HasColumnName("email_responsible")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsRequired();

            Property(x => x.InscricaoEstadual)
                .HasColumnName("inscricao_estadual")
                .HasColumnType("varchar")
                .HasMaxLength(255);

            Property(x => x.InscricaoMunicipal)
                .HasColumnName("inscricao_municipal")
                .HasColumnType("varchar")
                .HasMaxLength(255);

            Property(x => x.Address)
                .HasColumnName("address")
                .HasColumnType("varchar")
                .HasMaxLength(255);

            Property(x => x.Number)
                .HasColumnName("number")
                .HasColumnType("varchar")
                .HasMaxLength(50);

            Property(x => x.CEP)
                .HasColumnName("cep")
                .HasColumnType("varchar")
                .HasMaxLength(9);

            Property(x => x.Complement)
                .HasColumnName("complement")
                .HasColumnType("varchar")
                .HasMaxLength(255);

            Property(x => x.Prospect)
                .HasColumnName("prospect")
                .HasColumnType("bit")
                .IsRequired();

            Property(x => x.Phone)
                .HasColumnName("phone")
                .HasColumnType("varchar")
                .HasMaxLength(20)
                .IsOptional();

            Property(x => x.LastChangeActivation)
                .HasColumnName("last_change_activation");

            Property(x => x.PlanType)
                .HasColumnName("plan_type")
                .HasColumnType("int")
                .IsRequired();
        }
    }

    public class TrackingConfiguration : BaseEntityConfiguration<Customer.Tracking>
    {
        public TrackingConfiguration()
            : base("crowd_customers_tracking")
        {
            Property(x => x.FreelancerId)
                .HasColumnName("freelancer_id")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.UserId)
                .HasColumnName("user_id")
                .HasColumnType("int")
                .IsRequired();

            Property(x => x.FieldRequested)
                .HasColumnName("field_requested")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsRequired();

            Property(x => x.SearchTerm)
                .HasColumnName("search_terms")
                .HasColumnType("varchar")
                .HasMaxLength(255)
                .IsOptional();

            Property(x => x.PlanType)
                .HasColumnName("plan_type")
                .HasColumnType("int")
                .IsRequired();
        }
    }
}
