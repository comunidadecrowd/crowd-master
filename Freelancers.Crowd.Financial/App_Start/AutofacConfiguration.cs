﻿using Autofac;
using Autofac.Integration.Mvc;
using Freelancers.Crowd.Domain.Interfaces.Repositories;
using Freelancers.Crowd.Domain.Services;
using Freelancers.Crowd.Infrasctructure.Database.Context;
using Freelancers.Crowd.Infrasctructure.Database.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace Freelancers.Crowd.Financial.App_Start
{
    public static class AutofacConfiguration
    {
        public static void Init()
        {
            var builder = new ContainerBuilder();

            //builder.RegisterAssemblyModules(typeof(MvcApplication).Assembly);
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterType<CrowdContext>().InstancePerRequest();
            builder.RegisterGeneric(typeof(RepositoryBase<>)).As(typeof(IRepositoryBase<>)).InstancePerRequest();
            builder.RegisterGeneric(typeof(ServiceBase<>)).InstancePerRequest();
            builder.RegisterType(typeof(ServiceFreelancer)).InstancePerRequest();
            builder.RegisterType(typeof(ServiceUser)).InstancePerRequest();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}