﻿using Freelancers.Crowd.Domain.Entities;
using Freelancers.Crowd.Domain.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Freelancers.Crowd.Financial.Controllers
{
    public class HomeController : Controller
    {
        private readonly ServiceBase<Tasks> _serviceTasks;


        public HomeController(ServiceBase<Tasks> serviceTasks)
        {
            _serviceTasks = serviceTasks;
        }

        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                var tasks = _serviceTasks.GetAll(x => x.Status == Domain.Entities.Enum.TaskStatus.DONE).OrderBy(item => item.IdFreelancer).ToList();
                return View(tasks);
            }
            else
                return Redirect("/Account/Login/");
        }

        [HttpPost]
        public ActionResult Index(string from_date, string to_date)
        {
            var tasks = _serviceTasks.GetAll(x => x.Status == Domain.Entities.Enum.TaskStatus.DONE).OrderBy(item => item.IdFreelancer).ToList();
            
            DateTime start = DateTime.ParseExact(from_date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime end = DateTime.ParseExact(to_date, "dd/MM/yyyy", CultureInfo.InvariantCulture) + new TimeSpan(23,59,59);

            tasks = tasks.Where(x => x.UpdatedAt >= start && x.UpdatedAt <= end).ToList();
            return View("Index", tasks);
        }


    }
}