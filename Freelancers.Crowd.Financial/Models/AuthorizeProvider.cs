﻿using Freelancers.Crowd.Domain.Entities;
using Freelancers.Crowd.Domain.Entities.Enum;
using Freelancers.Crowd.Domain.Services;
using Freelancers.Crowd.Infrasctructure.Database.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace Freelancers.Crowd.Financial.Models
{
    public class AuthorizeProvider : RoleProvider
    {

        private readonly ServiceUser _serviceUser;

        public AuthorizeProvider()
        {
            _serviceUser = new ServiceUser(new RepositoryBase<User>(new Infrasctructure.Database.Context.CrowdContext()));
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetRolesForUser(string username)
        {

            var user = _serviceUser.GetByExpression(x => x.Email.Equals(username));
            if (user != null && user.Id > 0)
            {
                var nameRole = Enum.GetName(typeof(RoleType), user.Role);
                return new string[] { nameRole };
            }
            return new string[] { };
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}