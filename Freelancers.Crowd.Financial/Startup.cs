﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Freelancers.Crowd.Financial.Startup))]
namespace Freelancers.Crowd.Financial
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

        }
    }
}
