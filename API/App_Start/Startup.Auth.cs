﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.OAuth;
using Owin;
using API.Providers;
using API.Models;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security;
using API.Security;
using System.IdentityModel.Tokens;
using System.Web.Http;
using Microsoft.Owin.Cors;
using System.Security.Claims;
using System.Web.Http.Cors;

namespace API
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            OAuthAuthorizationServerOptions authServerOptions = new OAuthAuthorizationServerOptions()
            {
                //Em produção se atentar que devemos usar HTTPS
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/oauth2/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(2),
                Provider = new CustomOAuthProviderJwt(),
                AccessTokenFormat = new CustomJwtFormat("http://localhost:1741/")
            };
            app.UseOAuthAuthorizationServer(authServerOptions);
            //var issuer = "http://localhost:1741/";

            //var cors = new EnableCorsAttribute("*", "*", "*");
            app.UseCors(CorsOptions.AllowAll);
           

            app.UseJwtBearerAuthentication(
                new JwtBearerAuthenticationOptions
                {
                    AuthenticationMode = AuthenticationMode.Active,
                    AllowedAudiences = new[] { SecurityConstants.TokenAudience },
                    IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
                    {
                        new SymmetricKeyIssuerSecurityTokenProvider(SecurityConstants.TokenIssuer, SecurityConstants.KeyForHmacSha256)
                    }
                });
        }
    }
}
