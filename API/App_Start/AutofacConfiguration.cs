﻿using Autofac;
using Autofac.Integration.WebApi;
using Freelancers.Crowd.Domain.Interfaces.Repositories;
using Freelancers.Crowd.Domain.Interfaces.Services;
using Freelancers.Crowd.Domain.Services;
using Freelancers.Crowd.Domain.Util;
using Freelancers.Crowd.Infrasctructure.Database.Context;
using Freelancers.Crowd.Infrasctructure.Database.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;

namespace API.App_Start
{
    public static class AutofacConfiguration
    {
        public static void Init()
        {
            var builder = new ContainerBuilder();

            builder.RegisterApiControllers(typeof(Startup).Assembly);
            builder.RegisterType<CrowdContext>().InstancePerRequest();
            builder.RegisterGeneric(typeof(RepositoryBase<>)).As(typeof(IRepositoryBase<>)).InstancePerRequest();

            builder.RegisterGeneric(typeof(RepositoryBase<>)).As(typeof(IRepositoryBase<>)).InstancePerRequest();
            builder.RegisterGeneric(typeof(ServiceBase<>)).As(typeof(IServiceBase<>)).InstancePerRequest();
            builder.RegisterType(typeof(ServiceFreelancer)).As(typeof(IServiceFreelancer)).InstancePerRequest();
            builder.RegisterType(typeof(ServiceUser)).As(typeof(IServiceUser)).InstancePerRequest();
            builder.RegisterType(typeof(ServiceCampaign)).As(typeof(IServiceCampaign)).InstancePerRequest();
            
            // Add your registrations

            var container = builder.Build();

            //return container;

            // Set the dependency resolver for Web API.
            var webApiResolver = new AutofacWebApiDependencyResolver(container);
            GlobalConfiguration.Configuration.DependencyResolver = webApiResolver;
        }
    }
}