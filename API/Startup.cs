﻿#define DEBUG
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using API.App_Start;
using System.Web.Http;
using Autofac.Integration.WebApi;
using Microsoft.Owin.Cors;
using System.Web.Http.Cors;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Cors;

[assembly: OwinStartup(typeof(API.Startup))]

namespace API
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //var cors = new EnableCorsAttribute("http://homolog.comunidadecrowd.com.br", "*", "*");
            //GlobalConfiguration.Configuration.EnableCors(cors);
            GlobalConfiguration.Configuration.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;
            ConfigureAuth(app);
            AutofacConfiguration.Init();
            app.UseWebApi(GlobalConfiguration.Configuration);
        }
    }
}
