﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class BriefingAttachViewModel
    {
        public int Id { get; set; }
        public int briefingId { get; set; }
        public string attachUrl { get; set; }
        public string attachName { get; set; }
    }
}