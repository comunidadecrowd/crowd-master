﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class ComplementAttachViewModel
    {
        public int Id { get; set; }
        public int complementId { get; set; }
        public string attachUrl { get; set; }
        public string attachName { get; set; }
    }
}