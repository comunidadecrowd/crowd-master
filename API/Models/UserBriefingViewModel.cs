﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class UserBriefingViewModel
    {
        public int Id { get; set; }
        public int? IdCustomer { get; set; }
        public string Name { get; set; }

        public string Photo { get; set; }

        public CustomerViewModel Customer { get; set; }
    }
}