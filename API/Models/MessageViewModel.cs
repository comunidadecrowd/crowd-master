﻿using API.Models.Propose;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class MessageViewModel
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public string FreelancerId { get; set; }
        public Guid FreelancerCode { get; set; }
        public int BriefingId { get; set; }
        public string Text { get; set; }
        public bool Read { get; set; }
        public DateTime CreatedAt { get; set; }

        public int? ProposeId { get; set; }
        public decimal? Price { get; set; }
        public int? DeadlineDays { get; set; }
        public DateTime? DeliveryAt { get; set; }

        public FreelancerBriefingViewModel Freelancer { get; set; }
        public UserBriefingViewModel User { get; set; }

    }
}