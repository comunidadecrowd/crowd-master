﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.Report
{
    public class TaskFortnightViewModel
    {
        public string Fortnight { get; set; }
        public List<TaskFortnightGroupViewModel> Tasks { get; set; }
    }
}