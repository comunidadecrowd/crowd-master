﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class UpdateBriefingViewModel
    {
        
        public int BriefingId { get; set; }
        public string Text { get; set; }

        public int[] FreelancerIds { get; set; }

        public List<ComplementAttachViewModel> Attachs { get; set; }
    }

}