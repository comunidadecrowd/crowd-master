﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class CategoriesViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}