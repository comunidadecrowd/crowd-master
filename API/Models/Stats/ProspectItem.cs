﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.Stats
{
    public class ProspectItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}