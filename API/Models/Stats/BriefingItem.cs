﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.Stats
{
    public class BriefingItem
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Customer { get; set; }
        public int TotalFreelancers { get; set; }
        public int TotalFreelancersAnswer { get; set; }
    }
}