﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.Stats
{
    public class TaskItem
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public string FreelancerName { get; set; }
        public decimal Price { get; set; }
        public decimal Tax { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? CompletedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string TaskName { get; set; }
    }
}