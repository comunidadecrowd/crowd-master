﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.Stats
{
    public class FreelancerCategoryItem
    {
        public string Category { get; set; }
        public int Total { get; set; }
    }
}