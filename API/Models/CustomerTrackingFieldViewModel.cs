﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class CustomerTrackingFieldViewModel
    {
        public string FreelancerId { get; set; }
        public int UserId { get; set; }
        public string FieldRequested { get; set; }
        public string SearchTerm { get; set; }
    }
}