﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class UserEditViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string RoleCompany { get; set; }
        public string Photo { get; set; }
        public string PhotoBase64 { get; set; }
        public string PhotoExtension { get; set; }

        public string Password { get; set; }
    }
}