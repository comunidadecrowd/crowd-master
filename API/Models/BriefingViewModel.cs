﻿using API.Models.Projects;
using API.Models.Propose;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class BriefingViewModel
    {
        public int Id { get; set; }
        public int IdUser { get; set; }

        public UserBriefingViewModel User { get; set; }

        public string Title { get; set; }
        public string Excerpt { get; set; }
        public string Text { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

        public bool Active { get; set; }

        public List<FreelancerBriefingViewModel> Freelancers { get; set; }
        public List<MessageViewModel> Messages { get; set; }

        public List<ComplementViewModel> Complements { get; set; }

        public List<BriefingAttachViewModel> Attachs { get; set; }

        public int MessagesCount { get; set; }

        public bool Bulletin { get; set; }

        public ProjectViewModel Project { get; set; }
        public int IdProject { get; set; }
                
        public ProposeViewModel SelectedPropose { get; set; }
        public bool Contracted { get; set; }
        public bool Read { get; set; }
    }
}
