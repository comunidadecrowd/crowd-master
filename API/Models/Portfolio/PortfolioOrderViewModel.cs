﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.Portfolio
{
    public class PortfolioOrderViewModel
    {
        public int PortfolioId { get; set; }
        public int Order { get; set; }
    }
}