﻿using Freelancers.Crowd.Domain.Entities;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Freelancers.Crowd.Domain.Interfaces.Services;

namespace API.Controllers
{

    //[EnableCors("https://www.becrowd.co", "*", "*")]
    public abstract class BaseAPIController : ApiController
    {
        public IServiceBase<User> _serviceUser;

        protected BaseAPIController()
        {
                
        }

        protected BaseAPIController(IServiceUser serviceUser)
        {
            _serviceUser = serviceUser;
        }



        protected User RequestTokenUser()
        {
            var handler = new JwtSecurityTokenHandler();

            var jsonToken = handler.ReadToken(Request.Headers.Authorization.Parameter) as JwtSecurityToken;
            var unique_name = jsonToken.Claims.First(claim => claim.Type == "unique_name").Value;

            var requestUser = _serviceUser.GetByExpression(x => x.Email.Contains(unique_name));
            return requestUser;
        }

    }
}
