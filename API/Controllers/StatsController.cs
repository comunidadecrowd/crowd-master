using API.Models.Stats;
using Freelancers.Crowd.Domain.Entities;
using Freelancers.Crowd.Domain.Entities.Enum;
using Freelancers.Crowd.Domain.Interfaces.Services;
using Freelancers.Crowd.Domain.Services;
using Freelancers.Crowd.Infrasctructure.Database.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{

    public class StatsController : ApiController
    {
        private IServiceFreelancer _serviceFreelancer;
        private IServiceUser _serviceUser;
        private IServiceBase<Customer> _serviceCustomer;
        private IServiceBase<Briefing> _serviceBriefing;
        private IServiceBase<Tasks> _serviceTasks;

        private IServiceBase<Freelancer.Category> _serviceCategory;

        public StatsController()
        {
        }

        public StatsController(
                IServiceFreelancer serviceFreelancer,
                IServiceUser serviceUser,
                IServiceBase<Customer> serviceCustomer,
                IServiceBase<Briefing> serviceBriefing,
                IServiceBase<Tasks> serviceTasks,
                IServiceBase<Freelancer.Category> serviceCategory
            )
        {
            _serviceFreelancer = serviceFreelancer;
            _serviceUser = serviceUser;
            _serviceCustomer = serviceCustomer;
            _serviceBriefing = serviceBriefing;
            _serviceTasks = serviceTasks;
            _serviceCategory = serviceCategory;

        }

        [HttpGet]
        [Route("GetFreelancersCount")]
        public HttpResponseMessage GetFreelancersCount()
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var freelancers = _serviceFreelancer.GetAll(lazyLoadEnabled: false);
                var total = freelancers.Count();
                var now = DateTime.Now;
                var totalToday = freelancers.Where(x => x.CreatedAt >= new DateTime(now.Year, now.Month, now.Day)).Count();
                var totalCurrentMonth = freelancers.Where(x => x.CreatedAt.Month == now.Month && x.CreatedAt.Year == now.Year).Count();


                response = Request.CreateResponse(
                    new
                    {
                        total,
                        totalToday,
                        totalCurrentMonth
                    }
                );
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }

        [HttpGet]
        [Route("GetCustomersCount")]
        public HttpResponseMessage GetCustomersCount()
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var customers = _serviceCustomer.GetAll(x => x.Active && x.Id != 1);
                var total = customers.Count();
                var now = DateTime.Now;
                var totalToday = customers.Where(x => x.LastChangeActivation >= new DateTime(now.Year, now.Month, now.Day)).Count();
                var totalCurrentMonth = customers.Where(x => x.LastChangeActivation.Value.Month == now.Month).Count();


                response = Request.CreateResponse(
                    new
                    {
                        total,
                        totalToday,
                        totalCurrentMonth
                    }
                );
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }


        [HttpGet]
        [Route("GetBriefingsCount")]
        public HttpResponseMessage GetBriefingsCount()
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var briefings = _serviceBriefing.GetAll(x => x.User.IdCustomer != 1);
                var total = briefings.Count();
                var now = DateTime.Now;
                var totalToday = briefings.Where(x => x.CreatedAt >= new DateTime(now.Year, now.Month, now.Day)).Count();
                var totalCurrentMonth = briefings.Where(x => x.CreatedAt.Month == now.Month && x.CreatedAt.Year == now.Year).Count();


                response = Request.CreateResponse(
                    new
                    {
                        total,
                        totalToday,
                        totalCurrentMonth
                    }
                );
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }


        [HttpGet]
        [Route("GetProspects")]
        public HttpResponseMessage GetProspects()
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var prospects = _serviceCustomer.GetAll(x => x.Prospect && !x.Active, ascending: false);
                var total = prospects.Count();

                var items = new List<ProspectItem>();
                prospects.Take(20).ToList().ForEach(item =>
                {
                    items.Add(new ProspectItem()
                    {
                        Id = item.Id,
                        Name = item.Name
                    });
                });

                response = Request.CreateResponse(
                    new
                    {
                        total,
                        items
                    }
                );
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }


        [HttpGet]
        [Route("GetBriefings")]
        public HttpResponseMessage GetBriefings()
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var briefings = _serviceBriefing.GetAll(x => x.User.IdCustomer != 1).OrderByDescending(x => x.CreatedAt);

                var items = new List<BriefingItem>();
                briefings.Take(20).ToList().ForEach(item =>
                {
                    items.Add(new BriefingItem()
                    {
                        Id = item.Id,
                        Title = item.Title,
                        Customer = item.User.Customer.Trade,
                        TotalFreelancers = item.Freelancers.Count(),
                        TotalFreelancersAnswer = item.Messages.GroupBy(x => x.FreelancerId).Count()
                    });
                });

                response = Request.CreateResponse(
                    new
                    {
                        items
                    }
                );
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }


        [HttpGet]
        [Route("GetFreelancersCategory")]
        public HttpResponseMessage GetFreelancersCategory()
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var freelancers = _serviceFreelancer.GetAll(true).GroupBy(x => x.Categorys);

                var items = new List<FreelancerCategoryItem>();
                freelancers.ToList().ForEach(item =>
                {
                    items.Add(new FreelancerCategoryItem()
                    {
                        Category = item.Key.Name,
                        Total = item.Count()
                    });
                });
                response = Request.CreateResponse(
                    new
                    {
                        items
                    }
                );
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }


        [HttpGet]
        [Route("ListCustomersNotHire")]
        public HttpResponseMessage ListCustomersNotHire()
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                CrowdContext context = new CrowdContext();

                
                var customersNotHireButSent = context.Database.SqlQuery<CustomerItem>("select distinct id, trade, created_at as createdat from (SELECt distinct A.id, A.trade, A.created_at, max(t.id) as task, 0 brief, 0 as status FROM crowd_customers A inner join crowd_users c on c.id_customer = a.id inner join crowd_briefings b on c.id = b.id_user left join crowd_tasks t on b.id = t.id_briefing Where a.active = 1 group by A.id, A.trade, A.created_at having max(t.id) is null union SELECt A.id, A.trade, A.created_at, 0 as task, 0 brief, MAX(t.status) as status FROM crowd_customers A inner join crowd_users c on c.id_customer = a.id inner join crowd_tasks t on c.id = t.id_user Where a.active = 1 group by A.id, A.trade, A.created_at having MAX(t.status) = 1 ) as customers ORDER BY created_at DESC").ToList();

                var customersNotHireButSentTotal = customersNotHireButSent.Count();

                var itemsNotHireButSent = new List<CustomerItem>();
                customersNotHireButSent.Take(20).ToList().ForEach(item =>
                {

                    var briefings = _serviceBriefing.GetAll(x => x.User.Customer.Id == item.Id);
                    var tasks = _serviceTasks.GetAll(x => x.User.Customer.Id == item.Id);
                    itemsNotHireButSent.Add(new CustomerItem()
                    {
                        Id = item.Id,
                        CreatedAt = item.CreatedAt,
                        Trade = item.Trade,
                        QtyBriefings = briefings.Count() + tasks.Count()
                    });
                });

                var customersNotHire = context.Database.SqlQuery<CustomerItem>("SELECT A.id, A.trade, A.created_at as createdat FROM crowd_customers A INNER JOIN crowd_users B ON A.id = B.id_customer LEFT JOIN crowd_tasks C ON B.id = C.id_user LEFT JOIN crowd_briefings D ON B.id = D.id_user WHERE A.active = 1 GROUP BY A.id, A.trade, A.created_at HAVING COUNT(C.id) = 0 AND COUNT(D.id) = 0 ORDER BY A.created_at DESC ").ToList();

                var customersNotHireTotal = customersNotHire.Count();

                var itemsNotHire = new List<CustomerItem>();
                customersNotHire.Take(20).ToList().ForEach(item =>
                {
                    itemsNotHire.Add(new CustomerItem()
                    {
                        Id = item.Id,
                        CreatedAt = item.CreatedAt,
                        Trade = item.Trade
                    });
                });


                response = Request.CreateResponse(
                    new
                    {
                        itemsNotHireButSent,
                        itemsNotHire,
                        customersNotHireTotal,
                        customersNotHireButSentTotal
                    }
                );
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }



        [HttpGet]
        [Route("ListTasks")]
        public HttpResponseMessage ListTasks(TaskStatus Status)
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var tasks = _serviceTasks.GetAll(x => x.Status == Status && x.User.IdCustomer != 1 && x.Active, ascending: false);

                var items = new List<TaskItem>();
                tasks.Take(20).ToList().ForEach(item =>
                {
                    var freelancer = _serviceFreelancer.GetById(item.IdFreelancer);
                    items.Add(new TaskItem()
                    {
                        Id = item.Id,
                        TaskName = item.Title,
                        CustomerName = item.User.Customer.Trade,
                        FreelancerName = freelancer.Name,
                        Price = item.Price ?? 0,
                        Tax = Convert.ToDecimal((item.Price ?? 0) * (decimal)0.08),
                        CreatedDate = item.CreatedAt,
                        DueDate = item.DeliveryAt,
                        CompletedDate = item.UpdatedAt
                    });
                });

                response = Request.CreateResponse(
                    new
                    {
                        items
                    }
                );
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }


        [HttpGet]
        [Route("GetTasksCount")]
        public HttpResponseMessage GetTasksCount()
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var tasks = _serviceTasks.GetAll(x => x.User.IdCustomer != 1 && x.Status != TaskStatus.TO_BE_DONE);
                var total = tasks.Count();
                var now = DateTime.Now;
                var totalCurrentMonth = tasks.Where(x => x.CreatedAt.Month == now.Month && x.CreatedAt.Year == now.Year).Count();

                response = Request.CreateResponse(
                    new
                    {
                        total,
                        totalCurrentMonth
                    }
                );
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }


        [HttpGet]
        [Route("GetTicketAverage")]
        public HttpResponseMessage GetTicketAverage()
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var tasks = _serviceTasks.GetAll(x => x.Status == TaskStatus.DONE && x.User.IdCustomer != 1);
                var ticketAvg = tasks.Average(x => x.Price);
                var now = DateTime.Now;
                var tasksCurrentMonth = tasks.Where(x => x.CreatedAt.Month == now.Month && x.CreatedAt.Year == now.Year);
                var ticketAvgCurrentMonth = tasksCurrentMonth.Average(x => x.Price);

                response = Request.CreateResponse(
                    new
                    {
                        ticketAvg,
                        ticketAvgCurrentMonth
                    }
                );
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }


        [HttpGet]
        [Route("GetFreelancersCompletedProfileCount")]
        public HttpResponseMessage GetFreelancersCompletedProfileCount()
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var freelancers = _serviceFreelancer.GetAll(true)
                                        .Where(x => x.Experiences.Count != 0)
                                        .GroupBy(x => x.Id);
                var total = freelancers.Count();

                response = Request.CreateResponse(
                    new
                    {
                        total
                    }
                );
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }


        [HttpGet]
        [Route("GetCustomersHiredCount")]
        public HttpResponseMessage GetCustomersHiredCount()
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var tasks = _serviceTasks.GetAll(x => x.Status != TaskStatus.TO_BE_DONE).GroupBy(x => x.User.Customer);

                var total1x = tasks.Where(x => x.Count() == 1).Count();
                var total2x = tasks.Where(x => x.Count() > 1).Count();

                response = Request.CreateResponse(
                    new
                    {
                        total1x,
                        total2x
                    }
                );
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }


        [HttpGet]
        [Route("GetCustomersSentBriefingCount")]
        public HttpResponseMessage GetCustomersSentBriefingCount()
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var briefings = _serviceBriefing.GetAll(true).GroupBy(x => x.User.Customer);

                var total1x = briefings.Where(x => x.Count() == 1).Count();
                var total2x = briefings.Where(x => x.Count() > 1).Count();

                response = Request.CreateResponse(
                    new
                    {
                        total1x,
                        total2x
                    }
                );
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }


        [HttpGet]
        [Route("ListCategoryFreelancerHiredCount")]
        public HttpResponseMessage ListCategoryFreelancerHiredCount()
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var tasks = _serviceTasks.GetAll(x => x.Status != TaskStatus.TO_BE_DONE).GroupBy(x => x.Freelancer.Categorys);

                var items = new List<FreelancerCategoryItem>();
                tasks.ToList().ForEach(item =>
                {
                    items.Add(new FreelancerCategoryItem()
                    {
                        Category = item.Key.Name,
                        Total = item.Count()
                    });
                });

                response = Request.CreateResponse(
                    new
                    {
                        items
                    }
                );
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }


        [HttpGet]
        [Route("GetRevenueCount")]
        public HttpResponseMessage GetRevenueCount()
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var tasks = _serviceTasks.GetAll(x => x.Status == TaskStatus.DONE && x.User.IdCustomer != 1);
                var total = tasks.Sum(x => x.Price);
                var now = DateTime.Now;
                var totalCurrentMonth = tasks.Where(x => x.CreatedAt.Month == now.Month && x.CreatedAt.Year == now.Year).Sum(x => x.Price);



                response = Request.CreateResponse(
                    new
                    {
                        total,
                        totalCurrentMonth
                    }
                );
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }


        [HttpGet]
        [Route("GetTaxCount")]
        public HttpResponseMessage GetTaxCount()
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var tasks = _serviceTasks.GetAll(x => x.Status == TaskStatus.DONE && x.User.IdCustomer != 1);
                var total = tasks.Sum(x => x.Price) * (decimal)0.08;
                var now = DateTime.Now;
                var totalCurrentMonth = tasks.Where(x => x.CreatedAt.Month == now.Month && x.CreatedAt.Year == now.Year).Sum(x => x.Price) * (decimal)0.08;

                response = Request.CreateResponse(
                    new
                    {
                        total,
                        totalCurrentMonth
                    }
                );
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }


        [HttpGet]
        [Route("GetAnswersAvg")]
        public HttpResponseMessage GetAnswersAvg()
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var briefings = _serviceBriefing.GetAll(true).OrderByDescending(x => x.CreatedAt);
                var total = briefings.Count();
                var totalFreelancers = briefings.Sum(x => x.Freelancers.Count());
                var totalAnswers = briefings.Sum(x => x.Messages.Where(m => m.UserId == null).GroupBy(y => y.FreelancerId).Count());

                var now = DateTime.Now;
                var totalCurrentMonth = briefings.Where(x => x.CreatedAt.Month == now.Month && x.CreatedAt.Year == now.Year).Count();
                var totalFreelancersCurrentMonth = briefings.Where(x => x.CreatedAt.Month == now.Month && x.CreatedAt.Year == now.Year).Sum(x => x.Freelancers.Count());
                var totalAnswersCurrentMonth = briefings.Where(x => x.CreatedAt.Month == now.Month && x.CreatedAt.Year == now.Year).Sum(x => x.Messages.Where(m => m.UserId == null).GroupBy(y => y.FreelancerId).Count());

                var totalAvg = (float)totalAnswers / totalFreelancers;
                var totalCurrentMonthAvg = (float)totalAnswersCurrentMonth / totalFreelancersCurrentMonth;

                response = Request.CreateResponse(
                    new
                    {
                        totalAvg,
                        totalCurrentMonthAvg,
                        totalFreelancers,
                        totalAnswers,
                        totalFreelancersCurrentMonth,
                        totalAnswersCurrentMonth
                    }
                );
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }

    }
}
