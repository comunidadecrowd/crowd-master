using API.Models;
using API.Models.Propose;
using Freelancers.Crowd.Domain.Entities;
using Freelancers.Crowd.Domain.Entities.Enum;
using Freelancers.Crowd.Domain.Interfaces.Services;
using Freelancers.Crowd.Domain.Services;
using Freelancers.Crowd.Domain.Util.Network;
using Freelancers.Crowd.Domain.Util.Security;
using Newtonsoft.Json;
using RollbarDotNet;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.IdentityModel.Tokens;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;

namespace API.Controllers
{
    [RoutePrefix("Briefing")]
    public class BriefingController : BaseAPIController
    {
        private IServiceBase<Briefing> _serviceBriefing;
        private IServiceBase<Briefing.Attach> _serviceBriefingAttach;
        private IServiceFreelancer _serviceFreelancer;
        private IServiceUser _serviceUser;
        private IServiceBase<Briefing.Message> _serviceMessage;
        private IServiceBase<Briefing.Propose> _servicePropose;
        private IServiceBase<Briefing.ReadBriefing> _serviceReadBriefing  { get; set; }
        private IServiceBase<Tasks> _serviceTasks;

        private IServiceBase<SystemMessage> _serviceSystemMessages;

        public BriefingController(IServiceBase<Briefing> serviceBriefing, IServiceFreelancer serviceFreelancer,
            IServiceUser serviceUser, IServiceBase<Briefing.Attach> serviceBriefingAttach,
            IServiceBase<Briefing.Message> serviceMessage, IServiceBase<Briefing.Propose> servicePropose,
            IServiceBase<Tasks> serviceTasks, IServiceBase<Briefing.ReadBriefing> serviceReadBriefing,
            IServiceBase<SystemMessage> serviceSystemMessages)
        {
            _serviceBriefing = serviceBriefing;
            _serviceFreelancer = serviceFreelancer;
            _serviceUser = serviceUser;
            _serviceBriefingAttach = serviceBriefingAttach;
            _serviceMessage = serviceMessage;
            _servicePropose = servicePropose;
            _serviceTasks = serviceTasks;
            _serviceReadBriefing = serviceReadBriefing;
            _serviceSystemMessages = serviceSystemMessages;
        }


        [Route("Get")]
        public IHttpActionResult Get(int briefingId, int userId, int? freelancerId = null)
        {
            if (ModelState.IsValid)
            {
                var user = _serviceUser.GetById(userId);
                var model = new BriefingViewModel();
                var briefing = _serviceBriefing.GetById(briefingId);

                ProposeViewModel selectedPropose = null;

                model.Id = briefing.Id;
                model.Freelancers = new List<FreelancerBriefingViewModel>();

                if ((user.Role == RoleType.EMPLOYEE &&
                    user.Customer.Id != briefing.User.IdCustomer) ||
                    (user.Role == RoleType.FREELANCER &&
                    !briefing.Freelancers.ToList().Any(item => item.Users.Count() != 0 ? (item.Users.First().IdFreelancer == user.IdFreelancer) : false)))
                {

                    return  Content(HttpStatusCode.Unauthorized, "Não autorizado");
                }

                var proposes = _servicePropose.GetAll(x => x.BriefingId == briefing.Id).OrderByDescending(x => x.UpdatedAt).ToList();

                if (user.Role != RoleType.FREELANCER)
                {
                    model.Freelancers = new List<FreelancerBriefingViewModel>();

                    var freelancers = freelancerId != null ? briefing.Freelancers.Where(x => x.Id == freelancerId).ToList() : briefing.Freelancers.ToList();
                    
                    freelancers.ForEach(item =>
                    {
                        var freelancer_id = item.Id.ToString();

                        model.Freelancers.Add(new FreelancerBriefingViewModel()
                        {
                            Id = freelancer_id,
                            Name = item.Name,
                            Photo = item.Users.Count() != 0 ? item.Users.First().Photo : string.Empty,
                            Title = item.Title,
                            Code = item.Code
                        });
                    });
                    

                    model.Messages = new List<MessageViewModel>();
                    briefing.Messages.Where(x => x.Freelancers.Id == freelancerId).OrderBy(i => i.CreatedAt).ToList().ForEach(item =>
                    {
                        var freelancer_id = item.FreelancerId.ToString();
                        var messageViewModel = new MessageViewModel()
                        {
                            Id = item.Id,
                            BriefingId = briefing.Id,
                            FreelancerId = freelancer_id,
                            FreelancerCode = item.Freelancers.Code,
                            Read = item.Read,
                            Text = item.Text,
                            UserId = item.UserId,
                            CreatedAt = item.CreatedAt
                        };

                        if (item.User != null)
                        {
                            var userViewModel = new UserBriefingViewModel();
                            userViewModel.Name = item.User.Name;
                            userViewModel.Photo = item.User.Photo;
                            userViewModel.Customer = new CustomerViewModel()
                            {
                                Name = item.User.Customer.Name,
                                Logo = item.User.Customer.Logo
                            };

                            messageViewModel.User = userViewModel;
                        }

                        model.Messages.Add(messageViewModel);
                    });

                    var proposeByFreelancer = proposes.Where(x => x.Freelancer.Id == freelancerId).ToList();

                    if (proposeByFreelancer.Count() != 0)
                    {
                        var firstPropose = proposeByFreelancer.Last();
                        var freelancer_id = firstPropose.Freelancer.Id.ToString();
                        selectedPropose = new ProposeViewModel()
                        {
                            DeadlineDays = firstPropose.DeadlineDays,
                            Price = firstPropose.Price,
                            Id = firstPropose.Id,
                            Contracted = firstPropose.Contracted,
                            Freelancer = new FreelancerBriefingViewModel()
                            {
                                Id = freelancer_id,
                                Name = firstPropose.Freelancer.Name,
                                Photo = firstPropose.Freelancer.Users.First().Photo,
                                Code = firstPropose.Freelancer.Code
                            },
                            DeliveryAt = firstPropose.UpdatedAt.HasValue ?
                                firstPropose.UpdatedAt.Value.AddDays(firstPropose.DeadlineDays) : firstPropose.CreatedAt.AddDays(firstPropose.DeadlineDays)
                        };
                    }

                    
                    
                }
                else
                {
                    model.Messages = new List<MessageViewModel>();
                    briefing.Messages.Where(x => x.FreelancerId == user.IdFreelancer).OrderBy(i => i.CreatedAt).ToList().ForEach(item =>
                    {
                        var freelancer_id = item.FreelancerId.ToString();
                        var messageViewModel = new MessageViewModel()
                        {
                            Id = item.Id,
                            BriefingId = briefing.Id,
                            FreelancerId = freelancer_id,
                            FreelancerCode = item.Freelancers.Code,
                            Read = item.Read,
                            Text = item.Text,
                            UserId = item.UserId,
                            CreatedAt = item.CreatedAt
                        };

                        if (item.User != null)
                        {
                            var userViewModel = new UserBriefingViewModel();
                            userViewModel.Name = item.User.Name;
                            userViewModel.Photo = item.User.Photo;
                            userViewModel.Customer = new CustomerViewModel()
                            {
                                Name = item.User.Customer.Name,
                                Logo = item.User.Customer.Logo
                            };

                            messageViewModel.User = userViewModel;
                        }

                        model.Messages.Add(messageViewModel);
                    });
                    if (user.IdFreelancer != null &&  _serviceReadBriefing.GetByExpression(x => x.BriefingId == briefingId
                        && x.FreelancerId == user.IdFreelancer) == null) {
                        _serviceReadBriefing.Add(new Briefing.ReadBriefing()
                        {
                            BriefingId = briefingId, 
                            FreelancerId = user.IdFreelancer.Value,
                            Read = true
                        });
                    }

                    
                    briefing.Freelancers.Where(x => x.Id == user.Freelancers.Id).ToList().ForEach(item =>
                    {
                        var freelancer_id = item.Id.ToString();
                        model.Freelancers.Add(new FreelancerBriefingViewModel()
                        {
                            Id = freelancer_id,
                            Name = item.Name,
                            Photo = item.Users.Count() != 0 ? item.Users.First().Photo : null,
                            Title = item.Title,
                            Code = item.Code
                        });
                    });



                    var proposeByFreelancer = proposes.Where(x => x.FreelancerId == user.Freelancers.Id).ToList();
                    if (proposeByFreelancer.Count() != 0)
                    {
                        var firstPropose = proposeByFreelancer.Last();
                        var freelancer_id = firstPropose.Freelancer.Id.ToString();
                        selectedPropose = new ProposeViewModel()
                        {
                            DeadlineDays = firstPropose.DeadlineDays,
                            Price = firstPropose.Price,
                            Id = firstPropose.Id,
                            Contracted = firstPropose.Contracted,
                            Freelancer = new FreelancerBriefingViewModel()
                            {
                                Id = freelancer_id,
                                Name = firstPropose.Freelancer.Name,
                                Photo = firstPropose.Freelancer.Users.First().Photo,
                                Code = firstPropose.Freelancer.Code
                            },
                            DeliveryAt = firstPropose.UpdatedAt.HasValue ?
                                firstPropose.UpdatedAt.Value.AddDays(firstPropose.DeadlineDays) : firstPropose.CreatedAt.AddDays(firstPropose.DeadlineDays)
                        };
                    }
                }

                model.Excerpt = briefing.Excerpt;
                model.IdUser = briefing.IdUser;
                model.Text = briefing.Text;
                model.Title = briefing.Title;
                model.IdProject = briefing.IdProject.HasValue ? briefing.IdProject.Value : 0;
                model.Active = briefing.Active;

                if (briefing.Project != null)
                {
                    model.Project = new Models.Projects.ProjectViewModel()
                    {
                        Id = briefing.Project.Id,
                        Color = briefing.Project.Color,
                        Name = briefing.Project.Name
                    };
                }

                model.Attachs = new List<BriefingAttachViewModel>();
                briefing.Attachs.ToList().ForEach(item =>
                {
                    model.Attachs.Add(new BriefingAttachViewModel()
                    {
                        attachUrl = item.attachUrl,
                        briefingId = item.briefingId,
                        attachName = item.attachName,
                        Id = item.Id
                    });
                });

                model.Complements = new List<ComplementViewModel>();
                briefing.Complements.ToList().ForEach(item =>
                {
                    var complementAttachs = new List<ComplementAttachViewModel>();

                    item.Attachs.ToList().ForEach(attach =>
                    {
                        complementAttachs.Add(new ComplementAttachViewModel()
                        {
                            attachName = attach.attachName,
                            attachUrl = attach.attachUrl,
                            complementId = attach.complementId,
                            Id = attach.Id
                        });
                    });
                    model.Complements.Add(new ComplementViewModel()
                    {
                        BriefingId = briefing.Id,
                        Text = item.Text,
                        Attachs = complementAttachs
                    });
                });

                model.User = new UserBriefingViewModel()
                {
                    Name = briefing.User.Name,
                    Photo = briefing.User.Photo,
                    Customer = new CustomerViewModel()
                    {
                        Name = briefing.User.Customer.Name,
                        Logo = briefing.User.Customer.Logo
                    }
                };

                model.SelectedPropose = selectedPropose;
                model.Contracted = proposes.Count() == 0 ? false : proposes.FirstOrDefault(x => x.Contracted) != null ? true : false;
                

                return Ok(model);
            }

            return BadRequest(ModelState);
        }

        [HttpPost]
        [Route("Add")]
        public IHttpActionResult Add(Briefing briefing)
        {
            if (ModelState.IsValid)
            {
                var user = _serviceUser.GetById(briefing.IdUser);
                var handler = new JwtSecurityTokenHandler();

                var jsonToken = handler.ReadToken(Request.Headers.Authorization.Parameter) as JwtSecurityToken;
                var unique_name = jsonToken.Claims.First(claim => claim.Type == "unique_name").Value;

                var requestUser = _serviceUser.GetByExpression(x => x.Email.Contains(unique_name));

                briefing.Freelancers.ToList().ForEach(x =>
                {
                    briefing.Freelancers.Add(_serviceFreelancer.GetById(x.Id));
                    briefing.Freelancers.Remove(x);
                });

                if (requestUser.Customer.PlanType == PlanType.START && briefing.Freelancers.Where(x => x.Ratings.Any() && ((x.Ratings.Average(y => y.Agility) + x.Ratings.Average(y => y.Quality) + x.Ratings.Average(y => y.Responsability)) / 3) == 5).Count() > 0)
                {
                    return BadRequest("You can not add an 5 star freelancer. Please update your plan.");
                }

                _serviceBriefing.Add(briefing);

                var sendEmail = new Email();
                briefing.Freelancers.ToList().ForEach(x =>
                {
                    sendEmail.Send(x.Email, x.Name, "Novo Briefing: " + briefing.Title, "new-briefing",
                            new Dictionary<string, string> {
                                { "DE", briefing.User.Name },
                                { "EMPRESA", String.IsNullOrEmpty(briefing.User.Customer.Trade) ? briefing.User.Customer.Name :  briefing.User.Customer.Trade},
                                { "EXCERPT", briefing.Excerpt }
                            }
                    );
                });
                return Ok(briefing);
            }
            return BadRequest(ModelState);
        }

        [HttpPost]
        [Route("AddComplement")]
        public IHttpActionResult AddComplement(Briefing.Complement complement)
        {
            if (ModelState.IsValid)
            {
                var briefing = _serviceBriefing.GetById(complement.BriefingId);
                briefing.Complements.Add(complement);
                _serviceBriefing.Add(briefing);

                //Send e-mail 
                var sendEmail = new Email();
                briefing.Freelancers.ToList().ForEach(x =>
                {
                    sendEmail.Send(x.Email, x.Name, "Briefing Modificado: " + briefing.Title,
                        "new-briefing-complement",
                        new Dictionary<string, string> {
                                { "TITLE", briefing.Title },
                                { "CLIENT", String.IsNullOrEmpty(briefing.User.Customer.Trade) ? briefing.User.Customer.Name :  briefing.User.Customer.Trade },
                                { "COMPLEMENT", briefing.Complements.Last().Text }
                        });
                });

                return Ok(briefing);
            }

            return BadRequest(ModelState);
        }

        [HttpPost]
        [Route("AddFreelancers")]
        public IHttpActionResult AddFreelancers(int[] freelancerIds, int briefingId)
        {

            if (ModelState.IsValid)
            {
                var briefing = _serviceBriefing.GetById(briefingId);
                var freelancers = new List<Freelancer>();

                var handler = new JwtSecurityTokenHandler();

                var jsonToken = handler.ReadToken(Request.Headers.Authorization.Parameter) as JwtSecurityToken;
                var unique_name = jsonToken.Claims.First(claim => claim.Type == "unique_name").Value;

                var requestUser = _serviceUser.GetByExpression(x => x.Email.Contains(unique_name));

                for (var i = 0; i < freelancerIds.Length; i++)
                {
                    var freelancer = _serviceFreelancer.GetById(freelancerIds[i]);
                    double rate = 0;

                    if (freelancer.Ratings.Count() != 0)
                        rate = (freelancer.Ratings.Average(y => y.Agility) + freelancer.Ratings.Average(y => y.Quality) + freelancer.Ratings.Average(y => y.Responsability)) / 3;
                    if (requestUser.Customer.PlanType == PlanType.START && rate == 5)
                    {
                        return BadRequest("You can not add an 5 star freelancer. Please update your plan.");
                    }
                    freelancers.Add(freelancer);
                    briefing.Freelancers.Add(freelancer);
                }
                _serviceBriefing.Update(briefing);

                Email sendEmail = new Email();
                freelancers.ForEach(item =>
                {
                    sendEmail.Send(
                        item.Email,
                        item.Name,
                        "Novo Briefing: " + briefing.Title,
                        "new-briefing",
                        new Dictionary<string, string> {
                            { "DE", item.Users.First().Name },
                            { "EMPRESA", String.IsNullOrEmpty(briefing.User.Customer.Trade) ? briefing.User.Customer.Name :  briefing.User.Customer.Trade},
                            { "EXCERPT", briefing.Excerpt }
                    });
                });

                return Ok();
            }

            return BadRequest();
        }

        [HttpPost]
        [Route("Update")]
        public IHttpActionResult Update([FromBody]UpdateBriefingViewModel model)
        {
            if (ModelState.IsValid)
            {
                var briefing = _serviceBriefing.GetById(model.BriefingId);

                if (briefing == null)
                    return NotFound();

                var handler = new JwtSecurityTokenHandler();

                var jsonToken = handler.ReadToken(Request.Headers.Authorization.Parameter) as JwtSecurityToken;
                var unique_name = jsonToken.Claims.First(claim => claim.Type == "unique_name").Value;

                var requestUser = _serviceUser.GetByExpression(x => x.Email.Contains(unique_name));

                if (model.FreelancerIds.Count() != 0)
                {
                    var freelancers = new List<Freelancer>();
                    for (var i = 0; i < model.FreelancerIds.Length; i++)
                    {
                        var freelancer = _serviceFreelancer.GetById(model.FreelancerIds[i]);
                        double rate = 0;

                        if (freelancer.Ratings.Count() != 0)
                            rate = (freelancer.Ratings.Average(y => y.Agility) + freelancer.Ratings.Average(y => y.Quality) + freelancer.Ratings.Average(y => y.Responsability)) / 3;
                        if (requestUser.Customer.PlanType == PlanType.START && rate == 5)
                        {
                            return BadRequest("You can not add an 5 star freelancer. Please update your plan.");
                        }
                        freelancers.Add(freelancer);
                        briefing.Freelancers.Add(freelancer);
                    }
                    _serviceBriefing.Update(briefing);


                    Email sendEmail2 = new Email();
                    freelancers.ForEach(item =>
                    {
                        sendEmail2.Send(
                            item.Email,
                            item.Name,
                            "Novo Briefing: " + briefing.Title,
                            "new-briefing",
                            new Dictionary<string, string> {
                                { "DE", briefing.User.Name },
                                { "EMPRESA", String.IsNullOrEmpty(briefing.User.Customer.Trade) ? briefing.User.Customer.Name :  briefing.User.Customer.Trade},
                                { "EXCERPT", briefing.Excerpt }
                        });
                    });
                }
                //if (!string.IsNullOrEmpty(model.Text))
                //{
                    var complement = new Briefing.Complement()
                    {
                        BriefingId = model.BriefingId,
                        Text = model.Text
                    };
                    briefing.Complements.Add(complement);
                    _serviceBriefing.Update(briefing);

                    complement.Attachs = new List<ComplementAttach>();
                    model.Attachs?.ForEach(attach =>
                    {
                        complement.Attachs.Add(new ComplementAttach()
                        {
                            attachUrl = attach.attachUrl,
                            attachName = attach.attachName,
                            complementId = complement.Id
                        });
                    });

                    _serviceBriefing.Update(briefing);



                    Email sendEmail = new Email();
                    briefing.Freelancers.ToList().ForEach(item =>
                    {
                        sendEmail.Send(
                            item.Email,
                            item.Name,
                            "Briefing Modificado: " + briefing.Title,
                            "new-briefing-complement",
                            new Dictionary<string, string> {
                                { "TITLE", briefing.Title },
                                { "CLIENT", String.IsNullOrEmpty(briefing.User.Customer.Trade) ? briefing.User.Customer.Name :  briefing.User.Customer.Trade },
                                { "COMPLEMENT", complement.Text }
                            });
                    });

                    return Ok(model);
                //}
            }

            return BadRequest();
        }

        [HttpPost]
        [Route("ChangeStatus")]
        public IHttpActionResult ChangeStatus(int briefingId, bool status)
        {
            if (ModelState.IsValid)
            {
                var briefing = _serviceBriefing.GetById(briefingId);
                briefing.Active = !briefing.Active;

                _serviceBriefing.Update(briefing);

                return Ok();
            }

            return BadRequest(ModelState);
        }


        [Route("List")]
        public IHttpActionResult List(int user_id)
        {
            object response = null;
            var arquivados = new List<BriefingViewModel>();
            var ativos = new List<BriefingViewModel>();

            if (ModelState.IsValid)
            {
                var user = _serviceUser.GetById(user_id);

                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadToken(Request.Headers.Authorization.Parameter) as JwtSecurityToken;
                var unique_name = jsonToken.Claims.First(claim => claim.Type == "unique_name").Value;
                var requestUser = _serviceUser.GetByExpression(x => x.Email.Contains(unique_name));

                if (!requestUser.AcceptTerms)
                {
                    return Content(HttpStatusCode.PreconditionFailed, new { message = "You need to complete your profile before continue." });
                }

                var briefings = new List<Briefing>();

                if (user.Role == RoleType.MASTER)
                {
                    briefings = _serviceBriefing.GetAll(null, x => x.UpdatedAt.ToString(), ascending: false).OrderByDescending(x => x.CreatedAt).ToList();
                }
                else if (user.Role == RoleType.EMPLOYEE || user.Role == RoleType.CLIENT)
                {
                    briefings = _serviceBriefing.GetAll(x => x.User.Customer.Id == user.IdCustomer, x => x.UpdatedAt.ToString(), ascending: false).OrderByDescending(x => x.CreatedAt).ToList();
                }
                else if (user.Role == RoleType.FREELANCER)
                {
                    briefings = _serviceBriefing.GetAll(x => x.Freelancers.Any(y => y.Id == user.Freelancers.Id), x => x.UpdatedAt.ToString(), ascending: false).OrderByDescending(x => x.CreatedAt).ToList();

                }

                briefings.ForEach(item =>
                {
                    int messageCount = 0;
                    var messages = new List<Briefing.Message>();
                    var messagesViewModel = new List<MessageViewModel>();

                    if (user.Role == RoleType.MASTER)
                    {
                        messages = item.Messages.ToList();
                        messageCount = messages.Where(x => !x.Read && x.UserId != user.Id && x.Briefings.IdUser == user.Id).Count();
                    }
                    else if (user.Role == RoleType.CLIENT || user.Role == RoleType.EMPLOYEE)
                    {
                        messageCount = item.Messages.ToList().Where(x => !x.Read && x.UserId != user.Id && x.Briefings.IdUser == user.Id).Count();
                    }
                    else
                    {
                        messages = item.Messages.Where(x => x.FreelancerId == user.IdFreelancer).ToList();
                        messageCount = messages.Where(x => !x.Read && x.User != null).Count();
                    }


                    messages.Where(x => x.UserId == item.IdUser).OrderBy(o => o.CreatedAt).Take(1).ToList().ForEach(m =>
                    {
                        messagesViewModel.Add(new MessageViewModel()
                        {
                            Text = m.Text,
                            CreatedAt = m.CreatedAt,
                            FreelancerCode = m.Freelancers.Code
                        });
                    });

                    var propose = _servicePropose.GetAll(x => x.BriefingId == item.Id && x.Contracted, x => x.CreatedAt.ToString(), ascending: false).OrderByDescending(x => x.UpdatedAt);
                    ProposeViewModel selectedPropose = null;
                    if (propose.Count() != 0)
                    {
                        var freelancerId = Criptografy.Encrypt(propose.First().Freelancer.Id.ToString(), ConfigurationManager.AppSettings["cryptoPass"]);
                        selectedPropose = new ProposeViewModel()
                        {
                            DeadlineDays = propose.First().DeadlineDays,
                            Price = propose.First().Price,
                            Id = propose.First().Id,
                            Freelancer = new FreelancerBriefingViewModel()
                            {
                                Id = freelancerId,
                                Name = propose.First().Freelancer.Name,
                                Photo = propose.First().Freelancer.Users.First().Photo,
                                Code = propose.First().Freelancer.Code
                            },
                            DeliveryAt = propose.First().UpdatedAt.Value.AddDays(propose.First().DeadlineDays)
                        };
                    }
                    bool readBriefing = false; 
                    if (user.Role == RoleType.FREELANCER && item.Read != null)
                    {
                        var itemRead = item.Read.FirstOrDefault(x => x.FreelancerId == user.IdFreelancer);
                        readBriefing = itemRead != null ? itemRead.Read : false;
                    }

                    var brief = new BriefingViewModel()
                    {
                        Id = item.Id,
                        Excerpt = item.Excerpt,
                        Title = item.Title,
                        Active = item.Active,
                        MessagesCount = messageCount,
                        Messages = messagesViewModel,
                        CreatedAt = item.CreatedAt,
                        UpdatedAt = item.UpdatedAt,
                        User = new UserBriefingViewModel()
                        {
                            Name = item.User.Name,
                            Photo = item.User.Photo

                        },
                        Bulletin = false,
                        Contracted = propose.Count() == 0 ? false : true,
                        SelectedPropose = selectedPropose != null && user.Role != RoleType.FREELANCER ? selectedPropose : null,
                        Read = readBriefing
                    };

                    if (user.Role == RoleType.FREELANCER || item.Active)
                    {
                        ativos.Add(brief);
                    }
                    else
                    {
                        arquivados.Add(brief);
                    }
                });


                if (user.Role == RoleType.FREELANCER)
                {
                    var systemMessages = _serviceSystemMessages.GetAll(x => x.ToAllFreelancers || x.Users.Where(y => y.Id == user_id).Count() != 0);

                    systemMessages.ToList().ForEach(systemMessage =>
                    {
                        ativos.Add(new BriefingViewModel()
                        {
                            Id = systemMessage.Id,
                            CreatedAt = systemMessage.CreatedAt,
                            Active = true,
                            Text = systemMessage.Text.Replace("#NAME#", user.Name),
                            Title = systemMessage.Title,
                            Excerpt = systemMessage.Excerpt,
                            Bulletin = true,
                            Read = systemMessage.Read.Where(x => x.SystemMessageId == systemMessage.Id && x.UserId == user_id).Count() == 0 ? false : true,
                            User = new UserBriefingViewModel()                            
                            {
                                Name = systemMessage.UserSent.Name,
                                Photo = systemMessage.UserSent.Photo,
                                Customer = new CustomerViewModel()
                                {
                                    Name = systemMessage.UserSent.Customer.Name,
                                    Logo = "/Content/images/customer/ComunidadeCrowd.png"
                                }
                            }
                        });
                    });
                }

                ativos = ativos.OrderByDescending(x => x.CreatedAt).ToList();


                response = new { arquivados, ativos };
            }


            return Ok(response);
        }

        [HttpPost]
        [Route("AddMessage")]
        public IHttpActionResult AddMessage(MessageViewModel model)
        {
            if (ModelState.IsValid)
            {
                var freelancerId = int.Parse(Criptografy.Decrypt(model.FreelancerId, ConfigurationManager.AppSettings["cryptoPass"]));

                if (model.FreelancerId == null && freelancerId == 0)
                    return Content(HttpStatusCode.BadRequest, "Freelancer does not exist");
                var freelancer = _serviceFreelancer.GetById(freelancerId);
                if (freelancer == null)
                    return Content(HttpStatusCode.BadRequest, "Freelancer does not exist");

                if(model.DeadlineDays > 365)
                    return Content(HttpStatusCode.BadRequest, "Deadline days need to be less than 365");

                Email sendEmail = new Email();
                var briefing = _serviceBriefing.GetById(model.BriefingId);
                _serviceBriefing.Update(briefing);

                var message = new Briefing.Message()
                {
                    BriefingId = model.BriefingId,
                    UserId = model.UserId,
                    FreelancerId = freelancerId,
                    Text = model.Text,
                    Read = false,
                    CreatedAt = DateTime.Now
                };

                model.CreatedAt = message.CreatedAt;

                if (!string.IsNullOrEmpty(model.Text))
                    _serviceMessage.Add(message);

                if (model.Price != null && model.Price != 0 && model.DeadlineDays != null && model.DeadlineDays != 0)
                {
                    
                    var propose = new Briefing.Propose()
                    {
                        BriefingId = model.BriefingId,
                        FreelancerId = freelancerId,
                        DeadlineDays = (int)model.DeadlineDays,
                        Price = (decimal)model.Price
                    };
                    _servicePropose.Add(propose);

                    sendEmail.Send(
                        briefing.User.Email,
                        briefing.User.Name,
                        "Você recebeu uma nova proposta",
                        "new-propose",
                        new Dictionary<string, string> {
                        { "DE", freelancer.Name },
                        { "TITLE", briefing.Title }
                    });
                }

                if (model.UserId != 0 && model.UserId != null)
                {
                    var user = _serviceUser.GetById((int)model.UserId);

                    sendEmail.Send(
                        freelancer.Email,
                        freelancer.Name,
                        "Nova mensagem",
                        "new-message-freelancer",
                        new Dictionary<string, string> {
                            { "NOME", freelancer.Name },
                            { "DE", user.Name },
                            { "EMPRESA", String.IsNullOrEmpty(briefing.User.Customer.Trade) ? briefing.User.Customer.Name :  briefing.User.Customer.Trade},
                            { "TASK", briefing.Title }
                        });
                }
                else
                {

                    if (!string.IsNullOrEmpty(model.Text) && ((model.Price == null || model.Price == 0) && (model.DeadlineDays == null || model.DeadlineDays == 0)))
                    {
                        sendEmail.Send(
                            briefing.User.Email,
                            briefing.User.Name,
                            "Nova mensagem",
                            "new-message-client",
                            new Dictionary<string, string> {
                        { "NOME", briefing.User.Name },
                        { "DE", freelancer.Name },
                        { "TASK", briefing.Title }
                            });
                    }

                }

                return Ok(new { model = model });
            }

            return BadRequest(ModelState);
        }

        [HttpPost]
        [Route("ReadMessage")]
        public HttpResponseMessage ReadMessage(int user_id, int message_id)
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var message = _serviceMessage.GetById(message_id);
                var user = _serviceUser.GetById(user_id);

                if (user.Role == RoleType.FREELANCER)
                {
                    if (user.Id == message.Freelancers.Users.First().Id)
                    {
                        message.Read = true;
                        _serviceMessage.Update(message);
                        response = Request.CreateResponse(HttpStatusCode.OK);
                    }
                    else
                    {
                        response = Request.CreateResponse(HttpStatusCode.Unauthorized);
                    }
                }
                else
                {
                    if (user.Id == message.Briefings.IdUser)
                    {
                        message.Read = true;
                        _serviceMessage.Update(message);
                        response = Request.CreateResponse(HttpStatusCode.OK);
                    }
                    else
                    {
                        response = Request.CreateResponse(HttpStatusCode.BadRequest);
                    }
                }
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }


        [Route("GetMessages")]
        public HttpResponseMessage GetMessages(int briefing_id, int user_id)
        {
            HttpResponseMessage response = null;

            dynamic messagesGroup = null;

            if (ModelState.IsValid)
            {
                var user = _serviceUser.GetById(user_id);
                var briefing = _serviceBriefing.GetById(briefing_id);
                var briefMessages = briefing.Messages.OrderByDescending(x => x.CreatedAt).ToList();

                if (user.Role != RoleType.CLIENT && user.Role != RoleType.EMPLOYEE && user.Role != RoleType.MASTER)
                {
                    briefMessages = briefing.Messages.Where(x => x.FreelancerId == user.IdFreelancer).ToList();
                }

                messagesGroup = briefMessages.GroupBy(x => x.Freelancers, (key, g) =>
                {
                    var messages = new List<MessageViewModel>();

                    var unreadMessages = 0;

                    g.OrderByDescending(i => i.CreatedAt).ToList().ForEach(item =>
                    {
                        var propose = _servicePropose.GetAll(x => x.BriefingId == item.BriefingId && x.FreelancerId == item.FreelancerId).OrderByDescending(x => x.CreatedAt).ThenByDescending(x => x.Contracted);


                        if (user.Role == RoleType.MASTER)
                        {
                            unreadMessages = g.ToList().Where(x => !x.Read && x.UserId != user.Id && x.Briefings.IdUser == user.Id).Count();
                        }
                        else if (user.Role == RoleType.CLIENT || user.Role == RoleType.EMPLOYEE)
                        {
                            unreadMessages = g.ToList().Where(x => !x.Read && x.UserId != user.Id && x.Briefings.IdUser == user.Id).Count();
                        }
                        else
                        {
                            unreadMessages = g.ToList().Where(x => x.FreelancerId == user.IdFreelancer && !x.Read && x.User != null).Count();
                        }

                        var freelancerId = Criptografy.Encrypt(item.FreelancerId.ToString(), ConfigurationManager.AppSettings["cryptoPass"]);

                        var messageViewModel = new MessageViewModel()
                        {
                            Id = item.Id,
                            BriefingId = item.BriefingId,
                            FreelancerId = freelancerId,
                            FreelancerCode = item.Freelancers.Code,
                            Read = item.Read,
                            Text = item.Text,
                            UserId = item.UserId,
                            CreatedAt = item.CreatedAt
                        };

                        if (propose.Count() != 0)
                        {
                            messageViewModel.DeadlineDays = propose.First().DeadlineDays;
                            messageViewModel.Price = propose.First().Price;
                            messageViewModel.ProposeId = propose.First().Id;
                            if (propose.First().UpdatedAt.HasValue)
                                messageViewModel.DeliveryAt = propose.First().UpdatedAt.Value.AddDays(propose.First().DeadlineDays);
                        }

                        if (item.UserId != null)
                        {
                            var userMessage = _serviceUser.GetById(long.Parse(item.UserId.ToString()));
                            messageViewModel.User = new UserBriefingViewModel()
                            {
                                Id = userMessage.Id,
                                Name = userMessage.Name,
                                Photo = userMessage.Photo
                            };
                        }

                        messages.Add(messageViewModel);
                    });

                    return new
                    {
                        Freelancer = new
                        {
                            Id = Criptografy.Encrypt(key.Id.ToString(), ConfigurationManager.AppSettings["cryptoPass"]),
                            Name = key.Name,
                            Photo = key.Users.Count() != 0 ? key.Users.First().Photo : string.Empty,
                            Title = key.Title,
                            Code = key.Code
                        },
                        Messages = messages,
                        UnreadMessages = unreadMessages
                    };
                });

                response = Request.CreateResponse(new { messagesGroup });
            }


            return response;
        }

        [HttpPost()]
        [Route("AttachFile")]
        public HttpResponseMessage AttachFile()
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
            string folder = "/Content/images/briefing/";
            string sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Content/images/briefing/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            var path = "";
            var filename = "";

            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];
                if (hpf.ContentLength > 10485760)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "File size exceed limit of 10MB");
                var fileName = Guid.NewGuid().ToString();
                var fileExtenstion = Path.GetExtension(hpf.FileName);
                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!File.Exists(sPath + Path.GetFileName(fileName + fileExtenstion)))
                    {
                        // SAVE THE FILES IN THE FOLDER.
                        hpf.SaveAs(sPath + fileName + fileExtenstion);
                        filename = fileName + fileExtenstion;
                        path = sPath + fileName + fileExtenstion;
                        iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            if (iUploadedCnt > 0)
            {
                return Request.CreateResponse(new { folder, filename });
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Cannot upload file");
            }
        }

        [HttpPost()]
        [Route("AttachComplementFile")]
        public HttpResponseMessage AttachComplementFile()
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
            string folder = "/Content/images/briefing/complement/";
            string sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Content/images/briefing/complement/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            var path = "";
            var filename = "";

            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];
                if (hpf.ContentLength > 10485760)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "File size exceed limit of 10MB");

                var fileName = Guid.NewGuid().ToString();
                var fileExtenstion = Path.GetExtension(hpf.FileName);
                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!File.Exists(sPath + Path.GetFileName(fileName + fileExtenstion)))
                    {
                        // SAVE THE FILES IN THE FOLDER.
                        hpf.SaveAs(sPath + fileName + fileExtenstion);
                        filename = fileName + fileExtenstion;
                        path = sPath + fileName + fileExtenstion;
                        iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return Request.CreateResponse(new { folder, filename });
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Cannot upload file");
            }
        }

        [HttpPost()]
        [Route("RemoveAttach")]
        public HttpResponseMessage RemoveAttach(string filepath)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.InternalServerError, "Cannot remove file");
            var file = System.Web.Hosting.HostingEnvironment.MapPath(filepath);
            // Delete a file by using File class static method...
            if (File.Exists(file))
            {
                // Use a try block to catch IOExceptions, to
                // handle the case of the file already being
                // opened by another process.
                try
                {
                    File.Delete(file);
                    response = Request.CreateResponse(HttpStatusCode.OK);
                }
                catch (IOException)
                {
                    response = Request.CreateResponse(HttpStatusCode.InternalServerError, "Cannot remove file");
                }
            }

            return response;
        }


        [HttpPost]
        [Route("ContractFreelancer")]
        public HttpResponseMessage ContractFreelancer(ContractFreelancerViewModel model)
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var selectedPropose = _servicePropose.GetById(model.ProposeId);
                if (selectedPropose == null)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Not found");

                var freelancerId = int.Parse(Criptografy.Decrypt(model.FreelancerId, ConfigurationManager.AppSettings["cryptoPass"]));
                var freelancer = _serviceFreelancer.GetById(freelancerId);

                var handler = new JwtSecurityTokenHandler();

                var jsonToken = handler.ReadToken(Request.Headers.Authorization.Parameter) as JwtSecurityToken;
                var unique_name = jsonToken.Claims.First(claim => claim.Type == "unique_name").Value;

                var requestUser = _serviceUser.GetByExpression(x => x.Email.Contains(unique_name));

                double rate = 0;

                if (freelancer.Ratings.Count() != 0)
                    rate = (freelancer.Ratings.Average(y => y.Agility) + freelancer.Ratings.Average(y => y.Quality) + freelancer.Ratings.Average(y => y.Responsability)) / 3;
                if (requestUser.Customer.PlanType == PlanType.START && rate == 5)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "You can not add an 5 star freelancer. Please update your plan.");
                }

                if (selectedPropose.FreelancerId != freelancerId || selectedPropose.BriefingId != model.BriefingId)
                    return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Unauthorized");

                selectedPropose.Contracted = true;
                selectedPropose.UserResponsibleId = model.UserId;
                _servicePropose.Update(selectedPropose);

                Email sendEmail = new Email();
                sendEmail.Send(
                        selectedPropose.Freelancer.Email,
                        selectedPropose.Freelancer.Name,
                        "Sua Proposta Foi Selecionada!",
                        "selected-propose",
                        new Dictionary<string, string> {
                            { "DE", selectedPropose.UserResponsible.Name },
                            { "EMPRESA", String.IsNullOrEmpty(selectedPropose.UserResponsible.Customer.Trade) ? selectedPropose.UserResponsible.Customer.Name :  selectedPropose.UserResponsible.Customer.Trade },
                            { "TITLE", selectedPropose.Briefing.Title }
                        });


                selectedPropose.Briefing.Freelancers.Where(x => x.Id != freelancerId).ToList().ForEach(item =>
                {
                    sendEmail.Send(
                        item.Email,
                        item.Name,
                        "Seleção Encerrada",
                        "not-selected-propose",
                        new Dictionary<string, string> {
                            { "EMPRESA", selectedPropose.UserResponsible.Customer.Name },
                            { "TITLE", selectedPropose.Briefing.Title },
                            { "FREELAFOTO", selectedPropose.Freelancer.Users.First().Photo },
                            { "FREELANAME", selectedPropose.Freelancer.Name },
                            { "FREELATITLE", selectedPropose.Freelancer.Title }
                        });
                });
                var ret = new ProposeViewModel()
                {
                    DeadlineDays = selectedPropose.DeadlineDays,
                    Price = selectedPropose.Price,
                    Id = selectedPropose.Id,
                    Freelancer = new FreelancerBriefingViewModel()
                    {
                        Id = model.FreelancerId,
                        Name = selectedPropose.Freelancer.Name,
                        Photo = selectedPropose.Freelancer.Users.First().Photo
                    },
                    DeliveryAt = selectedPropose.UpdatedAt.Value.AddDays(selectedPropose.DeadlineDays)
                };

                //Create task
                var briefing = selectedPropose.Briefing;
                var task = new Tasks()
                {
                    DeliveryAt = ret.DeliveryAt,
                    Price = ret.Price,
                    Description = briefing.Text,
                    IdFreelancer = selectedPropose.FreelancerId,
                    IdBriefing = briefing.Id,
                    IdUser = briefing.IdUser,
                    Status = TaskStatus.DOING,
                    Title = briefing.Title,
                    Attachs =  new List<TasksAttach>(),
                    Complements = new List<Briefing.Complement>()
                };

                briefing.Attachs.ToList().ForEach(x => {
                    task.Attachs.Add(new TasksAttach()
                    {
                        Name = x.attachName,
                        URL = x.attachUrl,
                        CreatedAt = x.CreatedAt
                    });
                });

                if (briefing.IdProject.HasValue)
                {
                    task.IdProject = briefing.IdProject.Value;
                    _serviceTasks.Add(task);


                    var briefingToUpdate = _serviceBriefing.GetById(briefing.Id);

                    briefingToUpdate.Complements.ToList().ForEach(x =>
                    {
                        x.TaskId = task.Id;
                    });

                    _serviceBriefing.Update(briefingToUpdate);
                }

                response = Request.CreateResponse(HttpStatusCode.OK, new { ret });
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }


        [HttpGet]
        [Route("Message/Count")]
        public IHttpActionResult CountMessage(int user_id) {

            var user = _serviceUser.GetById(user_id);
            int total = 0;

            if (user.Role == RoleType.MASTER)
            {
                total = _serviceMessage.GetAll(x => !x.Read && x.Briefings.Active).Count();
            }
            else if (user.Role == RoleType.CLIENT || user.Role == RoleType.EMPLOYEE)
            {
                total = _serviceMessage.GetAll(x => !x.Read && x.UserId != user.Id && x.Briefings.IdUser == user.Id && x.Briefings.Active).Count();
            }
            else
            {
                total = _serviceMessage.GetAll(x => !x.Read && x.User != null && x.FreelancerId == user.IdFreelancer && x.Briefings.Active).Count();
            }

            return Ok(new { total = total});
        }

    }
}
