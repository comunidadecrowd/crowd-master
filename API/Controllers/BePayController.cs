﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API.Models;
using Freelancers.Crowd.Domain.Entities;
using Freelancers.Crowd.Domain.Entities.Enum;
using Freelancers.Crowd.Domain.Interfaces.Services;

namespace API.Controllers
{
    [Authorize]
    [RoutePrefix("BePay")]
    public class BePayController : BaseAPIController
    {
        private IServiceBase<Customer> _serviceCustomer;

        private IServiceBase<BePay> _serviceBePay;

        public BePayController()
        {

        }

        public BePayController(IServiceUser serviceUser,
            IServiceBase<Customer> serviceCustomer,
            IServiceBase<BePay> serviceBePay
        ) : base(serviceUser)
        {
            _serviceCustomer = serviceCustomer;
            _serviceBePay = serviceBePay;
        }
        // GET: api/BePay
        [Route("")]
        public IEnumerable<string> Get()
        {
            return new[] { "value1", "value2" };
        }

        // GET: api/BePay/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/BePay
        [HttpPost]
        [Route("")]
        public HttpResponseMessage Post(BePayViewModel model)
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var customer = _serviceCustomer.GetById(model.IdCustomer);
                var user = RequestTokenUser();

                if (customer == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Customer does not exist");

                if (user == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "User does not exist");

                var bepay = new BePay()
                {
                    IdCustomer = model.IdCustomer,
                    IdUser = user.Id,
                    PlanType = model.PlanType == 0 ? PlanType.ONE : model.PlanType,
                    Transaction = model.transactionId,
                    Token = model.creditCardToken,
                    ReadableLine = model.readableLine,
                    FinancialStatus = model.financialStatement.status,
                    AuthorizationNumber = model.financialStatement.authorizationDetails.number,
                    BePayType = (String.IsNullOrEmpty(model.creditCardToken) ? BePayType.Boleto : BePayType.Cartao)
                };

                _serviceBePay.Add(bepay);

                response = Request.CreateResponse(HttpStatusCode.OK);


            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }

        // PUT: api/BePay/5
        [HttpPut]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/BePay/5
        [HttpDelete]
        public void Delete(int id)
        {
        }
    }
}
