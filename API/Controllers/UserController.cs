using API.Models;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Text;
using Freelancers.Crowd.Domain.Services;
using Freelancers.Crowd.Domain.Entities;
using System.IdentityModel.Tokens;
using System.Security.Claims;
using API.Security;
using System.IdentityModel.Protocols.WSTrust;
using Freelancers.Crowd.Domain.Interfaces.Repositories;
using System.Web;
using System.IO;
using Freelancers.Crowd.Domain.Util;
using System.Drawing;
using Freelancers.Crowd.Domain.Entities.Enum;
using Freelancers.Crowd.Domain.Util.Security;
using Freelancers.Crowd.Domain.Util.Network;
using Freelancers.Crowd.Domain.Interfaces.Services;
using System.Configuration;
using System.Reflection;
using API.Models.Portfolio;
using API.Models.Tasks;

namespace API.Controllers
{
    [RoutePrefix("User")]
    public class UserController : BaseAPIController
    {

        private IServiceUser _serviceUser;
        private IServiceFreelancer _serviceFreelancer;
        private IServiceBase<Freelancer.Skill> _serviceSkill;
        private IServiceBase<Freelancer.Segment> _serviceSegment;
        private IServiceBase<City> _serviceCity;
        private IServiceBase<State> _serviceState;
        private IServiceBase<Freelancer.Category> _serviceCategory;
        private IServiceBase<Freelancer.Experience> _serviceFreelancerExperience;
        private IServiceBase<Freelancer.Award> _serviceFreelancerAwards;
        private IServiceBase<Freelancer.RatingValue> _serviceRating;
        private IServiceBase<Customer> _serviceCustomer;

        private IServiceBase<User.ActionLog> _serviceActionLog;

        private IServiceBase<Freelancer.Language> _serviceLanguage;

        private IServiceBase<Tasks> _serviceTasks;

        private IServiceBase<Freelancer.Portfolio> _servicePortfolio;

        public UserController()
        {
        }

        public UserController(
            IServiceUser serviceUser,
            IServiceFreelancer serviceFreelancer,
            IServiceBase<Freelancer.Skill> serviceSkill,
            IServiceBase<Freelancer.Segment> serviceSegment,
            IServiceBase<City> serviceCity,
            IServiceBase<State> serviceState,
            IServiceBase<Freelancer.Category> serviceCategory,
            IServiceBase<Freelancer.Experience> serviceFreelancerExperience,
            IServiceBase<Freelancer.RatingValue> serviceRating,
            IServiceBase<Freelancer.Award> serviceFreelancerAwards,
            IServiceBase<Customer> serviceCustomer,
            IServiceBase<Freelancer.Language> serviceLanguage,
            IServiceBase<User.ActionLog> serviceActionLog,
            IServiceBase<Tasks> serviceTasks,
            IServiceBase<Freelancer.Portfolio> servicePortfolio
        )
        {
            _serviceUser = serviceUser;
            _serviceFreelancer = serviceFreelancer;
            _serviceSkill = serviceSkill;
            _serviceSegment = serviceSegment;
            _serviceCity = serviceCity;
            _serviceState = serviceState;
            _serviceCategory = serviceCategory;
            _serviceFreelancerExperience = serviceFreelancerExperience;
            _serviceRating = serviceRating;
            _serviceFreelancerAwards = serviceFreelancerAwards;
            _serviceCustomer = serviceCustomer;
            _serviceLanguage = serviceLanguage;
            _serviceActionLog = serviceActionLog;
            _serviceTasks = serviceTasks;
            _servicePortfolio = servicePortfolio;
        }


        /// <summary>
        /// Login User
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Login")]
        public HttpResponseMessage Login(LoginViewModel model)
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var user = _serviceUser.GetByExpression(x => x.Email == model.Email);

                response = Request.CreateResponse(HttpStatusCode.Unauthorized, "User does not exist");

                if (user == null)
                {
                    var freelancer = _serviceFreelancer.GetByExpression(x => x.Email == model.Email);

                    if (freelancer != null)
                    {
                        response = Request.CreateResponse((HttpStatusCode)422, "User does not have credentials");

                        var password = System.Web.Security.Membership.GeneratePassword(8, 4);

                        var newUser = new User()
                        {
                            Active = true,
                            CreatedAt = DateTime.Now,
                            Email = freelancer.Email,
                            IdFreelancer = freelancer.Id,
                            Name = freelancer.Name,
                            Password = password,
                            Role = RoleType.FREELANCER
                        };

                        _serviceUser.Add(newUser);

                        Email sendEmail = new Email();
                        sendEmail.Send(
                            freelancer.Email,
                            freelancer.Name,
                            "Alterar a Senha",
                            "new-freelancer-password",
                            new Dictionary<string, string> {
                            { "PASSWORD",  password }
                        });
                    }
                }
                else
                {
                    if (!user.Active)
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, "User is inactive");

                    if (!string.IsNullOrEmpty(model.Email) && !string.IsNullOrEmpty(model.Password))
                    {
                        user = _serviceUser.Authentication(model.Email, model.Password);

                        if (user != null && user.Id > 0)
                        {
                            object loggedUser;
                            
                            var token = CreateJwtToken(user, out loggedUser);

                            _serviceActionLog.Add(new User.ActionLog()
                            {
                                Type = ActionLogType.LOGIN,
                                UserId = user.Id                                
                            });

                            var redirect = "";

                            if (user.Role == RoleType.FREELANCER)
                                redirect = ConfigurationManager.AppSettings["freelancerPath"];
                            else
                                redirect = ConfigurationManager.AppSettings["customerPath"];

                            var menu = new List<MenuViewModel>();

                            switch (user.Role)
                            {
                                case RoleType.MASTER:
                                    menu.Add(new MenuViewModel()
                                    {
                                        id = 1,
                                        title = "Clientes",
                                        href = "cliente",
                                        iconClass = "fa-users"
                                    });
                                    menu.Add(new MenuViewModel()
                                    {
                                        id = 2,
                                        title = "Usuários",
                                        href = "usuario",
                                        iconClass = "fa-user"
                                    });
                                    menu.Add(new MenuViewModel()
                                    {
                                        id = 3,
                                        title = "Profissionais",
                                        href = "profissional",
                                        iconClass = "fa-search"
                                    });
                                    menu.Add(new MenuViewModel()
                                    {
                                        id = 4,
                                        title = "Briefings",
                                        href = "briefing",
                                        iconClass = "icon fa-file-text"
                                    });
                                    break;

                                case RoleType.FREELANCER:
                                    menu.Add(new MenuViewModel()
                                    {
                                        id = 1,
                                        title = "Meu Perfil",
                                        href = "profile",
                                        iconClass = "fa-user"
                                    });
                                    menu.Add(new MenuViewModel()
                                    {
                                        id = 2,
                                        title = "Briefings",
                                        href = "briefing",
                                        iconClass = "icon fa-file-text"
                                    });
                                    break;
                                case RoleType.CLIENT:
                                    menu.Add(new MenuViewModel()
                                    {
                                        id = 1,
                                        title = "Usuários",
                                        href = "usuario",
                                        iconClass = "fa-user"
                                    });
                                    menu.Add(new MenuViewModel()
                                    {
                                        id = 2,
                                        title = "Profissionais",
                                        href = "profissional",
                                        iconClass = "fa-search"
                                    });
                                    menu.Add(new MenuViewModel()
                                    {
                                        id = 3,
                                        title = "Briefings",
                                        href = "briefing",
                                        iconClass = "icon fa-file-text"
                                    });
                                    break;

                                case RoleType.EMPLOYEE:
                                    menu.Add(new MenuViewModel()
                                    {
                                        id = 1,
                                        title = "Profissionais",
                                        href = "profissional",
                                        iconClass = "fa-search"
                                    });
                                    menu.Add(new MenuViewModel()
                                    {
                                        id = 2,
                                        title = "Briefings",
                                        href = "briefing",
                                        iconClass = "icon fa-file-text"
                                    });
                                    break;
                            }

                            response = Request.CreateResponse(HttpStatusCode.OK, new { loggedUser, token, redirect, menu });
                        }
                    }
                    else
                    {
                        response = Request.CreateResponse(HttpStatusCode.Found, new { Photo = user.Photo, Name = user.Name });
                    }
                }
            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }

        [HttpPost]
        [Route("Register")]
        public HttpResponseMessage Register(FreelancerViewModel model)
        {
            var httpRequest = HttpContext.Current.Request;
            var freelancerExist = _serviceFreelancer.GetByExpression(x => x.Email.Equals(model.Email));
            var userExist = _serviceUser.GetByExpression(x => x.Email.Equals(model.Email));

            if (!string.IsNullOrEmpty(model.Email))
            {
                var existEmail = _serviceFreelancer.GetAll(x => x.Email == model.Email);
                if (existEmail.Count() != 0)
                    return Request.CreateResponse(HttpStatusCode.Forbidden, "This email is used by another user");
            }
            
            if ((freelancerExist == null || freelancerExist.Id == 0) && userExist == null)
            {
                var freelancer = new Freelancer();
                if (freelancerExist != null)
                {
                    freelancer = freelancerExist;
                }
                else
                {
                    freelancer.Active = false;
                }

                freelancer.Availability = model.Availability;
                freelancer.Name = model.Name;
                freelancer.Email = model.Email;
                freelancer.Active = true;
                freelancer.CityId = model.City.Id;
                freelancer.StateId = model.State.Id;

                freelancer.City = _serviceCity.GetById(model.City.Id);
                freelancer.State = _serviceState.GetById(model.State.Id);

                freelancer.Country = model.Country;
                freelancer.Price = decimal.Parse(model.Price.ToString());
                freelancer.Phone = model.Phone;
                freelancer.Title = model.Title;
                freelancer.PortfolioURL = model.Portfolio;
                freelancer.Description = model.Description;
                freelancer.Skype = model.Skype;
                freelancer.CategoryId = model.CategoryId;
                freelancer.Categorys = _serviceCategory.GetById(model.CategoryId);

                freelancer.Birthday = model.Birthday;
                freelancer.EmailSecondary = model.EmailSecondary;

                freelancer.Code = Guid.NewGuid();


                Array.ForEach(model.SegmentsIds, x =>
                {
                    var segment = _serviceSegment.GetById(x);
                    if (freelancer.Segments.All(s => s.Id != segment.Id))
                        freelancer.Segments.Add(segment);
                });

                Array.ForEach(model.SkillsIds, x =>
                {
                    var skill = _serviceSkill.GetById(x);
                    if ( freelancer.Skills.All(s => s.Id != skill.Id))
                        freelancer.Skills.Add(skill);
                });

                if (freelancerExist == null)
                    _serviceFreelancer.Add(freelancer);
                else
                    _serviceFreelancer.Update(freelancer);


                var user = new User
                {
                    IdFreelancer = freelancer.Id,
                    Role = RoleType.FREELANCER,
                    Name = freelancer.Name,
                    Email = freelancer.Email,
                    Password = model.Password,
                    AcceptTerms = model.AcceptTerms,
                    AcceptTermsAt = DateTime.Now,
                    Active = false,
                    ConfirmEmail = Guid.NewGuid()
                };

                string folder = "/Content/images/user/";
                string path = HttpContext.Current.Server.MapPath("~/Content/images/user/");
                if (!string.IsNullOrEmpty(model.PhotoBase64))
                {
                    byte[] bytes = Convert.FromBase64String(model.PhotoBase64);
                    string filename = Guid.NewGuid().ToString();
                    path += filename + "." + model.PhotoExtension;
                    File.WriteAllBytes(path, bytes);
                    user.Photo = folder + filename + "." + model.PhotoExtension;
                }

                _serviceUser.Add(user);

                //Add Index
                var lucene = new LuceneSearchController();
                lucene.AddUpdateLuceneIndex(freelancer);

                Email sendEmail = new Email();
                //sendEmail.Send(model.Email, model.Name, "Seja muito bem vindo a Crowd!", "new-freelancer");
                sendEmail.Send(model.Email, model.Name, "Seja muito bem vindo a Crowd!", "confirm-email",
                    new Dictionary<string, string> {
                        { "CONFIRMEMAIL", user.ConfirmEmail.ToString() },
                        { "EMAIL", user.Email }
                    });
                               

                return Request.CreateResponse(HttpStatusCode.OK);

            }
            return Request.CreateResponse(HttpStatusCode.Unauthorized, "User already exist");
        }

        [Route("Get")]
        public FreelancerViewModel Get(string code)
        {
            var freelancer = _serviceFreelancer.GetByExpression(x => x.Code.ToString() == code);

            if (freelancer == null)
                throw new Exception("Freelancer does not exist");

            var handler = new JwtSecurityTokenHandler();

            var jsonToken = handler.ReadToken(Request.Headers.Authorization.Parameter) as JwtSecurityToken;
            var unique_name = jsonToken.Claims.First(claim => claim.Type == "unique_name").Value;

            var requestUser = _serviceUser.GetByExpression(x => x.Email.Contains(unique_name));

            var city = new CityViewModel();
            var state = new StateViewModel();

            if (freelancer.City != null)
            {
                city = new CityViewModel()
                {
                    Id = freelancer.City.Id,
                    Name = freelancer.City.Name
                };
            }

            if (freelancer.State != null)
            {
                state = new StateViewModel()
                {
                    Id = freelancer.State.Id,
                    UF = freelancer.State.UF
                };
            }

            var skills = new List<SkillsViewModel>();

            freelancer.Skills.ToList().ForEach(item =>
            {
                skills.Add(new SkillsViewModel()
                {
                    Id = item.Id,
                    Name = item.Name
                });
            });

            var segments = new List<SegmentsViewModel>();
            freelancer.Segments.ToList().ForEach(segement => segments.Add(
                new SegmentsViewModel()
                {
                    Id = segement.Id,
                    Name = segement.Name
                }
            ));

            var experiences = new List<FreelancerExperienceViewModel>();

            freelancer.Experiences.ToList().OrderByDescending(d=> d.StartDate).ToList().ForEach(item =>
            {
                experiences.Add(new FreelancerExperienceViewModel()
                {
                    Id = item.Id,
                    Company = item.Company,
                    Description = item.Description,
                    EndDate = item.EndDate,
                    FreelancerId = item.FreelancerId,
                    Role = item.Role,
                    StartDate = item.StartDate
                });
            });

            var awards = new List<FreelancerAwardsViewModel>();

            freelancer.Awards.ToList().ForEach(item =>
            {
                awards.Add(new FreelancerAwardsViewModel()
                {
                    Id = item.Id,
                    URL = item.URL,
                    Date = item.Date,
                    Title = item.Title,
                    FreelancerId = item.FreelancerId,
                    Type = item.Type
                });
            });

            var category = new CategoriesViewModel()
            {
                Id = freelancer.CategoryId,
                Name = freelancer.Categorys.Name
            };

            var ratings = freelancer.Ratings.Where(x => x.Active == true);
            var ratingsCount = ratings.Count();

            var comments = new List<string>();
            ratings.ToList().ForEach(rate =>
            {
                comments.Add(rate.Comment);
            });

            var lastTasks = new List<TasksViewModel>();
            var tasks = _serviceTasks.GetAll(x => x.IdFreelancer == freelancer.Id && x.Status == TaskStatus.DONE).OrderByDescending(x => x.UpdatedAt).Take(10);
            tasks.ToList().ForEach(item =>
            {
                lastTasks.Add(new TasksViewModel()
                {
                    DeliveryAt = item.DeliveryAt.HasValue ? String.Format("{0:s}", item.DeliveryAt.Value) : null,
                    Description = item.Description,
                    Id = item.Id,
                    Project = new Models.Projects.ProjectViewModel()
                    {
                        Id = item.Project.Id,
                        Color = item.Project.Color,
                        Name = item.Project.Name
                    },
                    Title = item.Title,
                    User = new UserBriefingViewModel()
                    {
                        Id = item.User.Id,
                        Name = item.User.Name,
                        Photo = item.User.Photo,
                        Customer = new CustomerViewModel()
                        {
                            Name = item.User.Customer.Name,
                            Trade = item.User.Customer.Trade,
                            Logo = item.User.Customer.Logo                            
                        }
                    }
                    
                });
            });

            var galeriaPortfolio = new List<PortfolioGaleriaViewModel>();
            int order = 0;
            freelancer.Portfolios.OrderByDescending(d => d.CreatedAt).Take(12).ToList().ForEach(item =>
            {
                order++;

                galeriaPortfolio.Add(new PortfolioGaleriaViewModel()
                {
                    Title = item.Title,
                    Type = item.Type,
                    URL = item.URL,
                    StartSlide = order
                });
            });

            var cover = new CoverViewModel();

            if (!String.IsNullOrEmpty(freelancer.Cover))
            {
                cover.Type = freelancer.CoverType ?? FileType.NOTSET;
                cover.URL = freelancer.Cover;
            }

                var response = new FreelancerViewModel()
            {
                Id = Criptografy.Encrypt(freelancer.Id.ToString(), ConfigurationManager.AppSettings["cryptoPass"]),
                Code = freelancer.Code.ToString(),
                Availability = freelancer.Availability,
                Name = freelancer.Name,
                Email = freelancer.Email,
                City = city,
                State = state,
                Country = freelancer.Country,
                Price = freelancer.Price,
                Phone = freelancer.Phone,
                Title = freelancer.Title,
                Portfolio = freelancer.PortfolioURL,
                Description = freelancer.Description,
                Skype = freelancer.Skype,
                CategoryId = freelancer.CategoryId,
                Skills = skills,
                Segments = segments,
                Category = category,
                FacebookUrl = freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Facebook) != null
                    ? freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Facebook).URL : String.Empty,
                TwitterUrl = freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Twitter) != null
                    ? freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Twitter).URL : String.Empty,
                DribbbleUrl = freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Dribbble) != null
                    ? freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Dribbble).URL : String.Empty,
                InstagramUrl = freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Instagram) != null
                    ? freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Instagram).URL : String.Empty,
                LinkedinUrl = freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Linkedin) != null
                    ? freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Linkedin).URL : String.Empty,
                BehanceUrl = freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Behance) != null
                    ? freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Behance).URL : String.Empty,
                GithubUrl = freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Github) != null
                    ? freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Github).URL : String.Empty,
                Experiences = experiences,
                Comments = comments,
                Photo = freelancer.Users.Count() != 0 ? freelancer.Users.First().Photo : "",
                Active = freelancer.Active,
                Languages = String.Join(",", freelancer.Languages.Select(x => x.Value).ToArray()),
                Available = freelancer.Status == StatusUserType.Active ? true : false,
                Awards = awards,
                Birthday = freelancer.Birthday,
                EmailSecondary = freelancer.EmailSecondary,
                LastTasks = lastTasks,
                GaleriaPortfolio = galeriaPortfolio,
                Cover = cover
            };


            response.QualityRating = ratingsCount == 0 ? 0 : Math.Round((double)ratings.Select(x => x.Quality).Sum() / ratingsCount);
            response.ResponsabilityRating = ratingsCount == 0 ? 0 : Math.Round((double)ratings.Select(x => x.Responsability).Sum() / ratingsCount);
            response.AgilityRating = ratingsCount == 0 ? 0 : Math.Round((double)ratings.Select(x => x.Agility).Sum() / ratingsCount);
            response.Rating = ratingsCount == 0 ? 0 : ((response.QualityRating + response.ResponsabilityRating + response.AgilityRating) / 3);
            
            if (requestUser.Customer != null && requestUser.Customer.PlanType == PlanType.START)
            {
                string[] removeProperties = new string[] {
                    "Email",
                    "Skype",
                    "Phone",
                    "Name",
                    "FacebookUrl",
                    "TwitterUrl",
                    "DribbbleUrl",
                    "InstagramUrl",
                    "LinkedinUrl",
                    "BehanceUrl",
                    "GithubUrl",
                    "Portfolio"
                };

                PropertiesRule myRules = new PropertiesRule();
                myRules.AddRule(new PropertiesRule.Rule { propertyName = "Email", propetyType = "string", ruleFunction = PropertiesRule.nullString });
                myRules.AddRule(new PropertiesRule.Rule { propertyName = "Skype", propetyType = "string", ruleFunction = PropertiesRule.nullString });
                myRules.AddRule(new PropertiesRule.Rule { propertyName = "Phone", propetyType = "string", ruleFunction = PropertiesRule.nullString });

                myRules.AddRule(new PropertiesRule.Rule { propertyName = "Name", propetyType = "string", ruleFunction = PropertiesRule.shortenName });

                myRules.AddRule(new PropertiesRule.Rule { propertyName = "FacebookUrl", propetyType = "string", ruleFunction = PropertiesRule.nullString });
                myRules.AddRule(new PropertiesRule.Rule { propertyName = "TwitterUrl", propetyType = "string", ruleFunction = PropertiesRule.nullString });
                myRules.AddRule(new PropertiesRule.Rule { propertyName = "DribbbleUrl", propetyType = "string", ruleFunction = PropertiesRule.nullString });
                myRules.AddRule(new PropertiesRule.Rule { propertyName = "InstagramUrl", propetyType = "string", ruleFunction = PropertiesRule.nullString });
                myRules.AddRule(new PropertiesRule.Rule { propertyName = "LinkedinUrl", propetyType = "string", ruleFunction = PropertiesRule.nullString });
                myRules.AddRule(new PropertiesRule.Rule { propertyName = "BehanceUrl", propetyType = "string", ruleFunction = PropertiesRule.nullString });
                myRules.AddRule(new PropertiesRule.Rule { propertyName = "GithubUrl", propetyType = "string", ruleFunction = PropertiesRule.nullString });
                myRules.AddRule(new PropertiesRule.Rule { propertyName = "Portfolio", propetyType = "string", ruleFunction = PropertiesRule.nullString });
                                
                foreach (PropertyInfo prop in response.GetType().GetProperties())
                {
                    if (prop.CanWrite)
                    {
                        if (myRules.GetPropertiesRange().Contains(prop.Name))
                        {
                            prop.SetValue(response, myRules.ApplyRule(response, prop));

                            //if (prop.PropertyType.Name.ToLower().IndexOf("decimal")> -1)
                            //    prop.SetValue(item, Decimal.Zero);
                        }
                    }
                }
            }


            return response;
        }

        [Authorize]
        [HttpPost]
        [Route("Edit")]
        public HttpResponseMessage Edit(FreelancerEditViewModel model)
        {
            var httpRequest = HttpContext.Current.Request;
            var freelancerId = int.Parse(Criptografy.Decrypt(model.Id, ConfigurationManager.AppSettings["cryptoPass"]));
            var freelancer = _serviceFreelancer.GetById(freelancerId);
            var user = _serviceUser.GetByExpression(x => x.IdFreelancer == freelancerId);

            if (freelancer != null || user != null)
            {
                var existEmail = _serviceFreelancer.GetAll(x => x.Email == model.Email && x.Id != freelancerId);

                if (existEmail.Count() != 0)
                    return Request.CreateResponse(HttpStatusCode.Forbidden, "This email is used by another freelancer");
                if (!string.IsNullOrEmpty(model.EmailSecondary))
                {
                    existEmail = _serviceFreelancer.GetAll(x => x.Id != freelancerId && (x.Email == model.EmailSecondary || x.EmailSecondary == model.EmailSecondary));
                    if (existEmail.Count() != 0)
                        return Request.CreateResponse(HttpStatusCode.Forbidden, "This email is used by another freelancer");
                }

                freelancer.Availability = model.Availability;
                freelancer.Name = model.Name;
                freelancer.Email = model.Email;
                freelancer.EmailSecondary = model.EmailSecondary;
                freelancer.Active = model.Active;
                freelancer.CityId = model.City.Id;
                freelancer.StateId = model.State.Id;
                freelancer.Country = model.Country;
                freelancer.Price = decimal.Parse(model.Price.ToString());
                freelancer.Phone = model.Phone;
                freelancer.Title = model.Title;
                freelancer.PortfolioURL = model.Portfolio;
                freelancer.Description = model.Description;
                freelancer.Skype = model.Skype;
                freelancer.CategoryId = model.CategoryId;

                if (freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Instagram) != null)
                    freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Instagram).URL = model.InstagramUrl;
                else if(!string.IsNullOrEmpty(model.InstagramUrl)) {
                    freelancer.Links.Add(new Freelancer.Link()
                    {
                        FreelancerId = freelancer.Id,
                        URL = model.InstagramUrl,
                        Website = WebsiteType.Instagram
                    });
                }

                if (freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Linkedin) != null)
                    freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Linkedin).URL = model.LinkedinUrl;
                else if (!string.IsNullOrEmpty(model.LinkedinUrl))
                {
                    freelancer.Links.Add(new Freelancer.Link()
                    {
                        FreelancerId = freelancer.Id,
                        URL = model.LinkedinUrl,
                        Website = WebsiteType.Linkedin
                    });
                }

                if (freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Twitter) != null)
                    freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Twitter).URL = model.TwitterUrl;
                else if (!string.IsNullOrEmpty(model.TwitterUrl))
                {
                    freelancer.Links.Add(new Freelancer.Link()
                    {
                        FreelancerId = freelancer.Id,
                        URL = model.TwitterUrl,
                        Website = WebsiteType.Twitter
                    });
                }


                if (freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Dribbble) != null)
                    freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Dribbble).URL = model.DribbbleUrl;
                else if (!string.IsNullOrEmpty(model.DribbbleUrl))
                {
                    freelancer.Links.Add(new Freelancer.Link()
                    {
                        FreelancerId = freelancer.Id,
                        URL = model.TwitterUrl,
                        Website = WebsiteType.Dribbble
                    });
                }


                if (freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Facebook) != null)
                    freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Facebook).URL = model.FacebookUrl;
                else if (!string.IsNullOrEmpty(model.FacebookUrl))
                {
                    freelancer.Links.Add(new Freelancer.Link()
                    {
                        FreelancerId = freelancer.Id,
                        URL = model.FacebookUrl,
                        Website = WebsiteType.Facebook
                    });
                }


                if (freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Behance) != null)
                    freelancer.Links.FirstOrDefault(x => x.Website == WebsiteType.Behance).URL = model.BehanceUrl;
                else if (!string.IsNullOrEmpty(model.BehanceUrl))
                {
                    freelancer.Links.Add(new Freelancer.Link()
                    {
                        FreelancerId = freelancer.Id,
                        URL = model.BehanceUrl,
                        Website = WebsiteType.Behance
                    });
                }

                freelancer.Status = model.Available ? StatusUserType.Active : StatusUserType.Disabled;

                freelancer.Birthday = model.Birthday;

                freelancer.Segments.Clear();
                freelancer.Skills.Clear();

                Array.ForEach(model.SegmentsIds, x => { freelancer.Segments.Add(_serviceSegment.GetById(x)); });
                Array.ForEach(model.SkillsIds, x => { freelancer.Skills.Add(_serviceSkill.GetById(x)); });

                /* Experience */
                _serviceFreelancer.Update(freelancer);

                freelancer.Languages.ToList().ForEach(lng =>
                {
                    freelancer.Languages.Remove(lng);
                    _serviceLanguage.Remove(lng);
                });

                _serviceFreelancer.Update(freelancer);

                Array.ForEach(model.Languages.Split(','), x => {
                    freelancer.Languages.Add(
                        new Freelancer.Language()
                        {
                            FreelancerId = freelancer.Id,
                            Freelancer = freelancer,
                            Value = x.Trim()
                        }
                    );
                });

                _serviceFreelancer.Update(freelancer);

                var experiencies = new List<Freelancer.Experience>();
                var auxExp = new List<FreelancerExperienceViewModel>();

                freelancer.Experiences.ToList().ForEach(item =>
                {
                    auxExp.Add(new FreelancerExperienceViewModel()
                    {
                        Company = item.Company,
                        Description = item.Description,
                        Role = item.Role,
                        EndDate = item.EndDate,
                        StartDate = item.StartDate,
                        FreelancerId = freelancer.Id,
                        Id = item.Id
                    });
                });

                var experiencesToRemove = auxExp.Except(model.Experiences, new Comparer()).ToList();
                //freelancer.Experiences.Where(x => model.Experiences.Any(i => i.Id != x.Id && i.Id != 0));

                experiencesToRemove.ToList().ForEach(exp =>
                {
                    _serviceFreelancerExperience.Remove(_serviceFreelancerExperience.GetById(exp.Id));
                });

                model.Experiences.ForEach(item =>
                {
                    if (!string.IsNullOrEmpty(item.Id.ToString()) && item.Id != 0)
                    {

                        var experienceItem = _serviceFreelancerExperience.GetById(item.Id);

                        experienceItem.Company = item.Company;
                        experienceItem.Description = item.Description;
                        experienceItem.Role = item.Role;
                        experienceItem.EndDate = item.EndDate;
                        experienceItem.StartDate = item.StartDate;
                        _serviceFreelancerExperience.Update(experienceItem);
                    }
                    else
                    {
                        freelancer.Experiences.Add(new Freelancer.Experience()
                        {
                            Company = item.Company,
                            Description = item.Description,
                            Role = item.Role,
                            EndDate = item.EndDate,
                            StartDate = item.StartDate,
                            FreelancerId = freelancer.Id
                        });
                    }
                });

                _serviceFreelancer.Update(freelancer);

                /* Awards */
                var awards = new List<Freelancer.Award>();
                var auxAwards = new List<FreelancerAwardsViewModel>();

                freelancer.Awards.ToList().ForEach(item =>
                {
                    auxAwards.Add(new FreelancerAwardsViewModel()
                    {
                        Date = item.Date,
                        Title = item.Title,
                        URL = item.URL,
                        FreelancerId = freelancer.Id,
                        Id = item.Id,
                        Type = item.Type
                    });
                });

                var awardsToRemove = auxAwards.Except(model.Awards, new ComparerAwards()).ToList();

                awardsToRemove.ToList().ForEach(awrd =>
                {
                    _serviceFreelancerAwards.Remove(_serviceFreelancerAwards.GetById(awrd.Id));
                });

                model.Awards.ForEach(item =>
                {
                    if (!string.IsNullOrEmpty(item.Id.ToString()) && item.Id != 0)
                    {
                        var awardItem = _serviceFreelancerAwards.GetById(item.Id);
                        awardItem.Title = item.Title;
                        awardItem.Date = item.Date;
                        awardItem.URL = item.URL;
                        awardItem.Type = item.Type;
                        _serviceFreelancerAwards.Update(awardItem);
                    }
                    else
                    {
                        freelancer.Awards.Add(new Freelancer.Award()
                        {
                            Title = item.Title,
                            Date = item.Date,
                            URL = item.URL,
                            FreelancerId = freelancer.Id,
                            Type = item.Type
                        });
                    }
                });

                if (model.Cover != null)
                {
                    if (model.Cover.Type == FileType.IMAGE)
                    {
                        string folder = "/Content/images/cover";
                        string path = HttpContext.Current.Server.MapPath("~/Content/images/cover");
                        byte[] bytes = Convert.FromBase64String(model.Cover.PhotoBase64);
                        string filename = Guid.NewGuid().ToString();
                        path += filename + "." + model.Cover.PhotoExtension;
                        File.WriteAllBytes(path, bytes);

                        freelancer.CoverType = FileType.IMAGE;
                        freelancer.Cover = folder + filename + "." + model.Cover.PhotoExtension;
                    }
                    else
                    {
                        freelancer.CoverType = FileType.VIDEO;
                        freelancer.Cover = model.Cover.URL;
                    }
                }

                _serviceFreelancer.Update(freelancer);


                user.IdFreelancer = freelancer.Id;
                user.Role = RoleType.FREELANCER;
                user.Name = freelancer.Name;
                user.Email = freelancer.Email;

                if (!string.IsNullOrEmpty(model.Password))
                    user.Password = Criptografy.GetMD5Hash(model.Password);

                if (!string.IsNullOrEmpty(model.PhotoBase64))
                {
                    string folder = "/Content/images/user/";
                    string path = HttpContext.Current.Server.MapPath("~/Content/images/user/");
                    byte[] bytes = Convert.FromBase64String(model.PhotoBase64);
                    string filename = Guid.NewGuid().ToString();
                    path += filename + "." + model.PhotoExtension;
                    File.WriteAllBytes(path, bytes);

                    user.Photo = folder + filename + "." + model.PhotoExtension;
                }

                _serviceUser.Update(user);

                //Add Index
                var lucene = new LuceneSearchController();
                lucene.AddUpdateLuceneIndex(freelancer);

                object loggedUser;
                var token = CreateJwtToken(user, out loggedUser);

                return Request.CreateResponse(new { loggedUser, token });

            }
            return Request.CreateResponse(HttpStatusCode.NotFound);
        }


        public static string CreateJwtToken(User user, out object loggedUser)
        {
            var claimList = new List<Claim>()
                {
                    new Claim(ClaimTypes.Name, user.Email),
                    new Claim(ClaimTypes.Role, user.Role.ToString()),
                    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
                };

            var tokenHandler = new JwtSecurityTokenHandler();
            var sSKey = new InMemorySymmetricSecurityKey(SecurityConstants.KeyForHmacSha256);

            var jwtToken = tokenHandler.CreateToken(makeSecurityTokenDescriptor(sSKey, claimList));

            var logo = user.Customer != null ? user.Customer.Logo : string.Empty;
            var customer = user.Customer != null ? user.Customer.Name : string.Empty;
            var plan = user.Customer != null ? user.Customer.PlanType.ToString() : string.Empty;
            var IdFreelancer = Criptografy.Encrypt(user.IdFreelancer.ToString(), ConfigurationManager.AppSettings["cryptoPass"]);
            var Code = user.Freelancers != null ? user.Freelancers.Code.ToString() : string.Empty;
            loggedUser = new
            {
                user.Email,
                user.Id,
                user.Role,
                user.Photo,
                user.Name,
                CustomerLogo = logo,
                Customer = customer,
                CustomerPlan = plan,
                IdCustomer = user.IdCustomer,
                IdFreelancer,
                Code,
                user.RoleCompany,
                user.AcceptTerms
            };

            return tokenHandler.WriteToken(jwtToken);
        }


        private static SecurityTokenDescriptor makeSecurityTokenDescriptor(InMemorySymmetricSecurityKey sSKey, List<Claim> claimList)
        {
            var now = DateTime.UtcNow;
            Claim[] claims = claimList.ToArray();
            return new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                TokenIssuerName = SecurityConstants.TokenIssuer,
                AppliesToAddress = SecurityConstants.TokenAudience,
                Lifetime = new Lifetime(now, now.AddMinutes(SecurityConstants.TokenLifetimeMinutes)),
                SigningCredentials = new SigningCredentials(sSKey,
                    "http://www.w3.org/2001/04/xmldsig-more#hmac-sha256",
                    "http://www.w3.org/2001/04/xmlenc#sha256"),
            };
        }


        public static string EncryptPassword(string password, string salt)
        {
            using (var sha256 = SHA256.Create())
            {
                var saltedPassword = string.Format("{0}{1}", salt, password);
                var saltedPasswordAsBytes = Encoding.UTF8.GetBytes(saltedPassword);
                return Convert.ToBase64String(sha256.ComputeHash(saltedPasswordAsBytes));
            }
        }

        [HttpPost]
        [Route("ForgotPassword")]
        public HttpResponseMessage ForgotPassword(string email)
        {
            var guid = _serviceUser.RecoverPassword(email);
            if (guid != null)
            {
                var freelancer = _serviceUser.GetByExpression(p => p.Email.Equals(email), true, "Freelancers");
                var variables = new Dictionary<string, string> { { "EMAIL", email }, { "ID", guid.ToString() } };
                Email sendEmail = new Email();
                sendEmail.Send(freelancer.Email, freelancer.Name, "Alterar a Senha", "change-password-new", variables);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }


        [HttpPost]
        [Route("ValidateHash")]
        public HttpResponseMessage ValidateHash(string email, string hash)
        {
            var user = _serviceUser.GetByExpression(x => x.Email.Equals(email) && x.RecoverPassword.HasValue
                && x.RecoverPassword.Value.ToString().Equals(hash));

            if (user != null)
                return Request.CreateResponse(HttpStatusCode.OK);

            return Request.CreateResponse(HttpStatusCode.Unauthorized);
        }


        [HttpPost]
        [Route("ChangePassword")]
        public HttpResponseMessage ChangePassword(string email, string password, string hash)
        {
            var change = _serviceUser.ChangePassword(email, password, hash);
            if (change)
                return Request.CreateResponse(HttpStatusCode.OK);

            return Request.CreateResponse(HttpStatusCode.Unauthorized);
        }

        [Authorize]
        [HttpPost]
        [Route("EditUser")]
        public HttpResponseMessage EditUser(UserEditViewModel model)
        {
            var user = _serviceUser.GetById(model.Id);

            if (user != null)
            {
                user.Name = model.Name;
                user.RoleCompany = model.RoleCompany;
                user.Email = model.Email;

                if (!string.IsNullOrEmpty(model.Password))
                    user.Password = Criptografy.GetMD5Hash(model.Password);

                if (!string.IsNullOrEmpty(model.PhotoBase64))
                {
                        string folder = "/Content/images/user/";
                        string path = HttpContext.Current.Server.MapPath("~/Content/images/user/");
                        byte[] bytes = Convert.FromBase64String(model.PhotoBase64);
                        string filename = Guid.NewGuid().ToString();
                        path += filename + "." + model.PhotoExtension;
                        File.WriteAllBytes(path, bytes);

                        user.Photo = folder + filename + "." + model.PhotoExtension;
                }

                

                _serviceUser.Update(user);

                model.Photo = user.Photo;

                return Request.CreateResponse(model);
            }

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }



        [HttpGet]
        [Route("ConfirmEmail")]
        public HttpResponseMessage ConfirmEmail(string email, string hash)
        {
            var response = Request.CreateResponse(HttpStatusCode.Redirect);

            var user = _serviceUser.GetByExpression(x => x.Email.Equals(email) && x.ConfirmEmail.HasValue
                && x.ConfirmEmail.Value.ToString().Equals(hash));

            if (user != null)
            {
                if(user.Active && user.ConfirmEmailAt != null)
                {
                    response.Headers.Location = new Uri("http://www.becrowd.co/confirmacao-de-email-ja-confirmado/");                    
                }
                else
                {
                    user.Active = true;
                    user.ConfirmEmailAt = DateTime.Now;
                    _serviceUser.Update(user);

                    Email sendEmail = new Email();
                    switch (user.Role)
                    {
                        case RoleType.FREELANCER:
                            user.Freelancers.Active = true;
                            _serviceFreelancer.Update(user.Freelancers);
                            
                            sendEmail.Send(user.Email, user.Name, "Seja muito bem vindo a Crowd!", "new-freelancer");
                            
                            break;

                        case RoleType.CLIENT:
                            var password = System.Web.Security.Membership.GeneratePassword(8, 4);
                            user.Customer.Active = true;
                            user.Password = password;
                            _serviceCustomer.Update(user.Customer);
                            
                            sendEmail.Send(user.Email, user.Name, "Seja muito bem vindo à Crowd!", "new-customer", new Dictionary<string, string> {
                                { "USER", user.Email },
                                { "PASS", password },
                                { "EMPRESA", user.Customer.Name }
                            });
                            break;
                    }

                    response.Headers.Location = new Uri("http://www.becrowd.co/confirmacao-de-email-sucesso/");
                }                
            }
            else
            {
                response.Headers.Location = new Uri("http://www.becrowd.co/confirmacao-de-email-erro/");
            }

            return response;
        }

    }
}

