using Freelancers.Crowd.Domain.Entities;
using Freelancers.Crowd.Domain.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ImageResizer;
using API.Models;
using Freelancers.Crowd.Domain.Util;
using Freelancers.Crowd.Domain.Util.Security;
using Freelancers.Crowd.Domain.Util.Network;
using Freelancers.Crowd.Domain.Interfaces.Services;
using Freelancers.Crowd.Domain.Entities.Enum;
using System.Configuration;

namespace API.Controllers
{

    [RoutePrefix("Customer")]
    public class CustomerController : BaseAPIController
    {
        private IServiceBase<Customer> _serviceCustomer;
        private IServiceBase<Customer.Tracking> _serviceCustomerTracking;
        private IServiceBase<State> _serviceState;
        private IServiceBase<City> _serviceCity;
        private IServiceUser _serviceUser;
        private IServiceFreelancer _serviceFreelancer;

        public CustomerController()
        {
        }

        public CustomerController(
            IServiceBase<Customer> serviceCustomer,
            IServiceFreelancer serviceFreelancer,
            IServiceBase<State> serviceState,
            IServiceBase<City> serviceCity,
            IServiceUser serviceUser,
            IServiceBase<Customer.Tracking> serviceCustomerTracking
            )
        {
            _serviceCustomer = serviceCustomer;
            _serviceCity = serviceCity;
            _serviceState = serviceState;
            _serviceUser = serviceUser;
            _serviceCustomerTracking = serviceCustomerTracking;
            _serviceFreelancer = serviceFreelancer;
        }


        [Route("Get")]
        public CustomerViewModel Get(int customer_id)
        {
            var customer = _serviceCustomer.GetById(customer_id);
            var response = new CustomerViewModel();
            if (customer == null)
                return response;

            response = new CustomerViewModel()
            {
                Id = customer.Id,
                Name = customer.Name,
                Trade = customer.Trade,
                NameResponsible = customer.NameResponsible,
                EmailResponsible = customer.EmailResponsible,
                Logo = customer.Logo,
                Active = customer.Active,
                Address = customer.Address,
                City = customer.CityId,
                CNPJ = customer.CNPJ,
                Complement = customer.Complement,
                InscricaoEstadual = customer.InscricaoEstadual,
                InscricaoMunicipal = customer.InscricaoMunicipal,
                Number = customer.Number,
                QtyPersons = customer.QtyPersons,
                State = customer.StateId,
                Prospect = customer.Prospect,
                Phone = customer.Phone,
                PlanType = customer.PlanType,
                CEP = customer.CEP
            };

            return response;
        }


        [Route("Add")]
        public HttpResponseMessage Add(CustomerViewModel model)
        {
            var customer = new Customer();
            string folder = "/Content/images/customer/";

            if (model.Id == 0 || string.IsNullOrEmpty(model.Id.ToString()))
            {
                var existingUser = _serviceUser.GetByExpression(x => x.Email == model.EmailResponsible);

                if(existingUser != null)
                    return Request.CreateResponse(HttpStatusCode.Conflict, "Email already exist");

                if (!string.IsNullOrEmpty(model.CNPJ))
                {
                    if(!Validations.Validations.validarCNPJ(model.CNPJ))
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "CNPJ is not valid");

                    var cnpjExist = _serviceCustomer.GetAll(x => x.CNPJ == model.CNPJ);
                    if (cnpjExist.Count() != 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.Conflict, new
                        {
                            message = "CNPJ is in use by another customer. Please contact the admin.",
                            contact = cnpjExist.OrderByDescending(x => x.CreatedAt).First().NameResponsible
                        });
                    }
                }

                customer.Name = model.Name;
                customer.Trade = model.Trade;
                customer.CNPJ = model.CNPJ;
                customer.QtyPersons = model.QtyPersons;
                customer.City = _serviceCity.GetById(model.City);
                customer.State = _serviceState.GetById(model.State);
                customer.EmailResponsible = model.EmailResponsible;
                customer.NameResponsible = model.NameResponsible;
                customer.InscricaoEstadual = model.InscricaoEstadual;
                customer.InscricaoMunicipal = model.InscricaoMunicipal;
                customer.Address = model.Address;
                customer.Complement = model.Complement;
                customer.Number = model.Number;
                customer.CEP = model.CEP;
                customer.Logo = string.IsNullOrEmpty(model.Logo) ? string.Empty : (folder + model.Logo);
                customer.Phone = model.Phone;
                customer.PlanType = model.PlanType == 0 ? PlanType.ONE : model.PlanType;
                customer.Active = false;
            
                _serviceCustomer.Add(customer);

                var password = System.Web.Security.Membership.GeneratePassword(8, 4);
                var user = new User()
                {
                    Email = model.EmailResponsible,
                    Name = model.NameResponsible,
                    Password = password,
                    IdCustomer = customer.Id,
                    Role = RoleType.CLIENT,
                    Active = false,
                    AcceptTerms = model.AcceptTerms,
                    AcceptTermsAt = DateTime.Now,
                    ConfirmEmail = Guid.NewGuid()
                };

                _serviceUser.Add(user);

                Email sendEmail = new Email();
                sendEmail.Send(user.Email, user.Name, "Seja muito bem vindo a Crowd!", "confirm-email",
                    new Dictionary<string, string> {
                        { "CONFIRMEMAIL", user.ConfirmEmail.ToString() },
                        { "EMAIL", user.Email }
                });

            }
            else
            {
                var existingCustomer = _serviceCustomer.GetById(model.Id);
                customer = existingCustomer;

                customer.Id = model.Id;
                customer.Name = model.Name;
                customer.Trade = model.Trade; 
                customer.CNPJ = model.CNPJ;
                customer.QtyPersons = model.QtyPersons;
                customer.City = _serviceCity.GetById(model.City);
                customer.State = _serviceState.GetById(model.State);
                customer.EmailResponsible = model.EmailResponsible;
                customer.NameResponsible = model.NameResponsible;
                customer.InscricaoEstadual = model.InscricaoEstadual;
                customer.InscricaoMunicipal = model.InscricaoMunicipal;
                customer.Address = model.Address;
                customer.Complement = model.Complement;
                customer.Number = model.Number;
                customer.CEP = model.CEP;
                
                customer.Phone = model.Phone;
                customer.PlanType = model.PlanType == 0 ? PlanType.ONE : model.PlanType;

                customer.Logo = string.IsNullOrEmpty(model.Logo) ? string.Empty : (folder + model.Logo);
               
                _serviceCustomer.Update(customer);

                var existingUser = _serviceUser.GetByExpression(x => x.IdCustomer == customer.Id);
                existingUser.Email = model.EmailResponsible;
                existingUser.Name = model.NameResponsible;
                existingUser.Password = existingUser.Password;
                existingUser.IdCustomer = customer.Id;
                existingUser.Role = existingUser.Role;

                try
                {
                    _serviceUser.Update(existingUser);
                }
                catch(Exception)
                {
                    return Request.CreateResponse(HttpStatusCode.Conflict, "Email already exist");
                }                
            }

            model.Id = customer.Id;
            model.Logo = customer.Logo;
            model.Active = customer.Active;

            return Request.CreateResponse(model);
        }

        [Route("AddUser")]
        public HttpResponseMessage AddUser(CustomerUserViewModel model)
        {
            
            var password = model.Password;
            User user = null;
            var existing_user = _serviceUser.GetByExpression(x => x.Email == model.Email);
            var customer = _serviceCustomer.GetById(model.IdCustomer);

            if(customer == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, "Cliente não encontrado.");

            var customerUsersCount = _serviceUser.GetAll(x => x.IdCustomer == customer.Id).Count();
            if((customer.PlanType == PlanType.START || customer.PlanType == PlanType.ONE) && string.IsNullOrEmpty(model.Id.ToString()))
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Your plan allow only 1 user.");

            if ((customer.PlanType == PlanType.PRO && customerUsersCount >= 5) && string.IsNullOrEmpty(model.Id.ToString()))
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Your plan allow only 5 users.");


            if (string.IsNullOrEmpty(model.Id.ToString()))           
            {
                
                if (existing_user != null)
                {
                    return Request.CreateResponse(HttpStatusCode.Conflict, "Email já cadastrado!");
                }

                user = new User()
                {
                    IdCustomer = model.IdCustomer,
                    Name = model.Name,
                    Email = model.Email,
                    RoleCompany = model.RoleCompany,
                    Role = RoleType.EMPLOYEE                    
                };
            }
            else
            {
                user = existing_user;
            }


            if (!string.IsNullOrEmpty(model.Id.ToString()))
            {
                user.Name = model.Name;
                user.Email = model.Email;
                user.RoleCompany = model.RoleCompany;
                user.Phone = model.Phone;

                user.AcceptTerms = model.AcceptTerms;
                user.AcceptTermsAt = DateTime.Now;

                if (!string.IsNullOrEmpty(password))
                    user.Password = Criptografy.GetMD5Hash(password);

                string folder = "/Content/images/user/";
                string path = HttpContext.Current.Server.MapPath("~/Content/images/user/");
                if (!string.IsNullOrEmpty(model.PhotoBase64))
                {
                    byte[] bytes = Convert.FromBase64String(model.PhotoBase64);
                    string filename = Guid.NewGuid().ToString();
                    path += filename + "." + model.PhotoExtension;
                    File.WriteAllBytes(path, bytes);
                    user.Photo = folder + filename + "." + model.PhotoExtension;
                }

                _serviceUser.Update(user);
            }
            else
            {
                user.Password = password;
                user.Active = false;
                user.ConfirmEmail = Guid.NewGuid();
                user.AcceptTerms = false;
                user.AcceptTermsAt = DateTime.Now;
                _serviceUser.Add(user);
            }
            

            model.Id = user.Id;
            model.Active = user.Active;
            model.Role = RoleType.EMPLOYEE;

            if (existing_user == null)
            {
                Email sendEmail = new Email();
                sendEmail.Send(user.Email, user.Name, "Seja muito bem vindo a Crowd!", "confirm-email-new-customer-user",
                    new Dictionary<string, string> {
                        { "DE", customer.NameResponsible },
                        { "EMPRESA", customer.Name },
                        { "USER", user.Email },
                        { "PASS", password },
                        { "CONFIRMEMAIL", user.ConfirmEmail.ToString() },
                        { "EMAIL", user.Email }
                });
            }

            return Request.CreateResponse(new { model });
        }

        [Route("ChangeStatusUser")]
        [HttpPost]
        public HttpResponseMessage ChangeStatusUser(int userId, bool active)
        {
            var user = _serviceUser.GetById(userId);
            user.Active = !user.Active;

            _serviceUser.Update(user);

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Route("ChangeStatusCustomer")]
        [HttpPost]
        public HttpResponseMessage ChangeStatusCustomer(int customerId, bool active)
        {
            var customer = _serviceCustomer.GetById(customerId);
            customer.Active = !customer.Active;
            customer.LastChangeActivation = DateTime.Now;

            _serviceCustomer.Update(customer);

            var users = _serviceUser.GetAll(x => x.IdCustomer == customer.Id);

            if(active){

                var password = System.Web.Security.Membership.GeneratePassword(8, 4);
                var user = users.First();
                user.Active = true;
                _serviceUser.ChangePassword(user.Id, password);
                _serviceUser.Update(user);


                Email sendEmail = new Email();
                sendEmail.Send(user.Email, user.Name, "Seja muito bem vindo a Crowd!", "new-customer-user",
                    new Dictionary<string, string> {
                        { "DE", customer.NameResponsible },
                        { "EMPRESA", customer.Name },
                        { "USER", user.Email },
                        { "PASS", password } });
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Route("GetCustomers")]
        public List<CustomerViewModel> GetCustomers()
        {
            var customers = _serviceCustomer.GetAll(true).OrderByDescending(x => x.Id);
            
            var response = new List<CustomerViewModel>();

            customers.ToList().ForEach(item => response.Add(
                new CustomerViewModel()
                {
                   Id = item.Id,
                   Name = item.Name,
                   Trade = item.Trade,
                   NameResponsible = item.NameResponsible,
                   EmailResponsible = item.EmailResponsible,
                   Logo = item.Logo,
                   Active = item.Active,
                   Address = item.Address,
                   CEP = item.CEP,
                   City = item.CityId,
                   CNPJ = item.CNPJ,
                   Complement = item.Complement,
                   InscricaoEstadual = item.InscricaoEstadual,
                   InscricaoMunicipal = item.InscricaoMunicipal,
                   Number = item.Number,
                   QtyPersons = item.QtyPersons,
                   State = item.StateId,
                   Prospect = item.Prospect,
                   Phone = item.Phone,
                   PlanType = item.PlanType
                }
            ));

            return response;
        }

        [Route("GetCustomerUser")]
        public List<CustomerUserViewModel> GetCustomerUser(int customerId)
        {
            var customersUsers = _serviceUser.GetAll(x => x.IdCustomer == customerId).OrderBy(x => x.Role);

            var response = new List<CustomerUserViewModel>();

            customersUsers.ToList().ForEach(item => response.Add(
                new CustomerUserViewModel()
                {
                    Email = item.Email,
                    RoleCompany = item.RoleCompany,
                    Id = item.Id,
                    Name = item.Name,
                    Photo = item.Photo,
                    Active = item.Active,
                    Role = item.Role,
                    Phone = item.Phone
                }
            ));

            return response;
        }


        [AllowAnonymous()]
        [Route("AttachFile")]
        public HttpResponseMessage AttachFile()
        {
            int iUploadedCnt = 0;

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
            string folder = "/Content/images/customer/";
            string sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Content/images/customer/");

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            var path = "";
            var filename = "";

            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];
                var fileName = Guid.NewGuid().ToString();
                var fileExtenstion = Path.GetExtension(hpf.FileName);
                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!File.Exists(sPath + Path.GetFileName(fileName + fileExtenstion)))
                    {
                        // SAVE THE FILES IN THE FOLDER.
                        hpf.SaveAs(sPath + fileName + fileExtenstion);
                        filename = fileName + fileExtenstion;
                        path = sPath + fileName + fileExtenstion;
                        //ImageBuilder.Current.Build(path, path,
                        //                    new ResizeSettings("width=80"));
                        iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            if (iUploadedCnt > 0)
            {
                return Request.CreateResponse(new { folder, filename });
                }
            else {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Cannot upload file");
            }
        }


        [AllowAnonymous]
        [Route("AddInterestedClient")]
        public HttpResponseMessage AddInterestedClient(CustomerInterestedViewModel model)
        {
            var userExist = _serviceUser.GetByExpression(x => x.Email == model.EmailResponsible);

            if(userExist != null)
                return Request.CreateResponse(HttpStatusCode.Forbidden, "This email is already used");

            if (!string.IsNullOrEmpty(model.CNPJ))
            {
                if (!Validations.Validations.validarCNPJ(model.CNPJ))
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "CNPJ is not valid");

                var cnpjExist = _serviceCustomer.GetAll(x => x.CNPJ == model.CNPJ);
                if (cnpjExist.Count() != 0)
                {
                    return Request.CreateResponse(HttpStatusCode.Conflict, new
                    {
                        message = "CNPJ is in use by another customer. Please contact the admin.",
                        contact = cnpjExist.OrderByDescending(x => x.CreatedAt).First().NameResponsible
                    });
                }
            }

            var customer = new Customer()
            {
                Name = model.Name,
                Trade = model.Trade, 
                State = _serviceState.GetById(model.State),
                City = _serviceCity.GetById(model.City),
                Address = model.Address,
                Number = model.Number,
                Complement = model.Complement,
                NameResponsible = model.NameResponsible,
                EmailResponsible = model.EmailResponsible,
                QtyPersons = model.QtyPersons,
                Active = false,
                Prospect = true,
                Phone = model.Phone,
                PlanType = model.PlanType == 0 ? Freelancers.Crowd.Domain.Entities.Enum.PlanType.ONE : model.PlanType,
                CNPJ = model.CNPJ
            };


            _serviceCustomer.Add(customer);

            var password = System.Web.Security.Membership.GeneratePassword(8, 4);
            var user = new User()
            {
                Email = model.EmailResponsible,
                Name = model.NameResponsible,
                Password = password,
                IdCustomer = customer.Id,
                Role = Freelancers.Crowd.Domain.Entities.Enum.RoleType.CLIENT,
                Active = false,
                ConfirmEmail = Guid.NewGuid(),
                AcceptTerms = model.AcceptTerms,                
                AcceptTermsAt = DateTime.Now
            };

            _serviceUser.Add(user);

            

            Email sendEmail = new Email();
            sendEmail.Send(user.Email, user.Name, "Obrigado por entrar para a Crowd", "prospect-customer");

            sendEmail.Send(
                ConfigurationManager.AppSettings["contactEmail"], "Comunidade Crowd",  "Cadastro de nova empresa", "new-interested-customer", 
                new Dictionary<string, string> {
                    { "RAZAO", customer.Name },
                    { "FANTASIA", customer.Trade },
                    { "CNPJ", customer.CNPJ },
                    { "RESPONSAVEL", customer.NameResponsible },
                    { "EMAIL", customer.EmailResponsible },
                    { "PHONE", customer.Phone },
                    { "QTD", customer.QtyPersons.ToString() },
                    { "ADDRESS", customer.Address },
                    { "NUMBER", customer.Number },
                    { "COMPLEMENT", string.IsNullOrEmpty(customer.Complement) ? " " : customer.Complement },
                    { "CITY", customer.City.Name },
                    { "STATE", customer.State.UF },
                    { "PLANTYPE", customer.PlanType.ToString() }
                });            

            return Request.CreateResponse(new { model });
        }

        [HttpPost]
        [Route("ChangePlan")]
        public HttpResponseMessage ChangePlan(int userId, PlanType planType)
        {
            var user = _serviceUser.GetById(userId);

            if (user == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, "This user does not exist.");

            if (user.Customer.PlanType == planType)
                return Request.CreateResponse(HttpStatusCode.BadRequest, "This customer have the same plan selected.");

            var customer = _serviceCustomer.GetById(user.Customer.Id);
            var oldPlan = customer.PlanType;
            customer.PlanType = planType;
            _serviceCustomer.Update(customer);            

            Email sendEmail = new Email();
            sendEmail.Send(ConfigurationManager.AppSettings["plataformaEmail"], "Crowd", "Mudança de plano", "customer-change-plan",
                new Dictionary<string, string> {
                    { "EMPLOYEE", user.Name },
                    { "CUSTOMER", user.Customer.Trade },
                    { "CURRENT", oldPlan.ToString() },
                    { "NEW", planType.ToString() },
                });

            if (user.Role == RoleType.EMPLOYEE)
            {                
                sendEmail.Send(user.Customer.EmailResponsible, user.Customer.NameResponsible, "Mudança de plano", "customer-change-plan",
                    new Dictionary<string, string> {
                        { "EMPLOYEE", user.Name },
                        { "CUSTOMER", user.Customer.Trade },
                        { "CURRENT", oldPlan.ToString() },
                        { "NEW", planType.ToString() },
                    });  
            }

            return Request.CreateResponse(HttpStatusCode.OK, "Plan changed successfully");
        }
        
        [HttpPost]
        [Route("TrackField")]
        public HttpResponseMessage TrackField(CustomerTrackingFieldViewModel model)
        {
            HttpResponseMessage response = null;

            if (ModelState.IsValid)
            {
                var freelancerId = int.Parse(Criptografy.Decrypt(model.FreelancerId, ConfigurationManager.AppSettings["cryptoPass"]));
                var freelancer = _serviceFreelancer.GetById(freelancerId);
                var user = _serviceUser.GetById(model.UserId);

                if (freelancer == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Freelancer does not exist");

                if (user == null)
                    return Request.CreateResponse(HttpStatusCode.NotFound, "User does not exist");

                if (user.Customer != null)
                {
                    var customerTracking = new Customer.Tracking()
                    {
                        FreelancerId = freelancerId,
                        UserId = model.UserId,
                        FieldRequested = model.FieldRequested,
                        SearchTerm = model.SearchTerm,
                        PlanType = user.Customer.PlanType
                    };

                    _serviceCustomerTracking.Add(customerTracking);

                }

                response = Request.CreateResponse(HttpStatusCode.OK);

            }
            else
            {
                response = Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            return response;
        }

    }
}



